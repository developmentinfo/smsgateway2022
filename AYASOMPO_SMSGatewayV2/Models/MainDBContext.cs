﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class MainDBContext : DbContext
    {
       public IConfiguration Configuration { get; set; }

        public MainDBContext()
        {
            var builder = new ConfigurationBuilder()
          .SetBasePath(Directory.GetCurrentDirectory())
          .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
        } 

        public virtual DbSet<Department> Department { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<SentSms> SentSms { get; set; }
        public virtual DbSet<SysUser> SysUser { get; set; }
        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<ProductClass> ProductClass { get; set; }

        public virtual DbSet<FireRenewalRegister> FireRenewalRegister { get; set; }
        public virtual DbSet<MotorRenewalRegister> MotorRenewalRegister { get; set; }
        public virtual DbSet<HealthRenewalRegister> HealthRenewalRegister { get; set; }
        public virtual DbSet<ParenewalRegister> ParenewalRegister { get; set; }
        public virtual DbSet<EngineeringRenewalRegister> EngineeringRenewalRegister { get; set; }
        public virtual DbSet<PropertyRenewalRegister> PropertyRenewalRegister { get; set; }
        public virtual DbSet<RenewalRegisterRemark> RenewalRegisterRemark { get; set; }
        public virtual DbSet<TempRenewalCaseCreate> TempRenewalCaseCreate { get; set; }

        public virtual DbSet<SurveySms> SurveySms { get; set; }
        public virtual DbSet<CoreAccountCode> CoreAccountCode { get; set; }
        public virtual DbSet<Log> Log { get; set; }

        // For Rule Base underwritting System
        public virtual DbSet<Rules> Rules { get; set; }
        public virtual DbSet<RuleAction> RuleAction { get; set; }
        public virtual DbSet<Region> Region { get; set; }
        public virtual DbSet<RiskLocation> RiskLocation { get; set; }
        
        public virtual DbSet<Column> Column { get; set; }
        public virtual DbSet<Floor> Floor { get; set; }
        public virtual DbSet<Roof> Roof { get; set; }
        public virtual DbSet<Wall> Wall { get; set; }

        // Rule Base Underwriting System 
        public virtual DbSet<FireConstruction> FireConstruction { get; set; }
        public virtual DbSet<FireCoverageinfo> FireCoverageinfo { get; set; }
        public virtual DbSet<FireFeaInfo> FireFeaInfo { get; set; }
        public virtual DbSet<FireFirewatertank> FireFirewatertank { get; set; }
        public virtual DbSet<FireInventoryinfo> FireInventoryinfo { get; set; }
        public virtual DbSet<FireRiskdetail> FireRiskdetail { get; set; }
        public virtual DbSet<FireSecurity> FireSecurity { get; set; }
        public virtual DbSet<RuleProposal> RuleProposal { get; set; }
        public virtual DbSet<AutoGenerate> AutoGenerate { get; set; }

        public virtual DbSet<FireRequireddocuments> FireRequireddocuments { get; set; }
        public virtual DbSet<FireRequireddocumentsetting> FireRequireddocumentsetting { get; set; }
        public virtual DbSet<Requestdetail> Requestdetail { get; set; }
        public virtual DbSet<UwPerson> UwPerson { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Category>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Department>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DeletedDate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(200);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.RoleId).ValueGeneratedNever();

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(100);

                entity.Property(e => e.RoleName)
                    .IsRequired()
                    .HasMaxLength(30);
            });

            modelBuilder.Entity<SentSms>(entity =>
            {
                entity.ToTable("SentSMS");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Channel)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ClaimNo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CrmrenewalCreateFlag).HasColumnName("CRMRenewalCreateFlag");

                entity.Property(e => e.CusCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ExpireDate).HasColumnType("datetime");

                entity.Property(e => e.FromDate).HasColumnType("datetime");

                entity.Property(e => e.Link)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Message).IsRequired();

                entity.Property(e => e.Month)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Pic)
                    .HasColumnName("PIC")
                    .HasMaxLength(200);

                entity.Property(e => e.PolicyNo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Product)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RenewalPremium).HasColumnType("decimal(16, 2)");

                entity.Property(e => e.ResponsibleParty).HasMaxLength(200);

                entity.Property(e => e.SentDate).HasColumnType("datetime");

                entity.Property(e => e.Smscount)
                    .HasColumnName("SMSCount")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Smsflag)
                    .HasColumnName("SMSFlag")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ToDate).HasColumnType("datetime");

                entity.Property(e => e.UserName).IsRequired();

                entity.Property(e => e.VehicleNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SysUser>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.ConfirmCode)
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy).HasMaxLength(200);

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.EmailAddress).HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.PasswordSalt)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ProductClass>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.ClassCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ClassName)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<FireRenewalRegister>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AbctypeExtinguisher)
                    .HasColumnName("ABCTypeExtinguisher")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AccountCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AgentCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AgentName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AgreedSiamount)
                    .HasColumnName("AgreedSIAmount")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ApproveSendSms)
                    .HasColumnName("ApproveSendSMS")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ApproveSendSmsdate)
                    .HasColumnName("ApproveSendSMSDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.AuthorizedCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AuthorizedName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BlackListed)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Branch)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BranchAuthority)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BranchAuthorityDate).HasColumnType("datetime");

                entity.Property(e => e.BuildingClass)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CapacityofFireTank)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ClaimsExists)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ClaimsPaid).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Class)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Co2typeExtinguishers)
                    .HasColumnName("CO2TypeExtinguishers")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ConvertedNetPremium).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ConvertedOutPremium).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Coverage)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CrmrenewalCreateFlag).HasColumnName("CRMRenewalCreateFlag");

                entity.Property(e => e.CrmrenewalCreatedDate)
                    .HasColumnName("CRMRenewalCreatedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Currency)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CusCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerName).IsUnicode(false);

                entity.Property(e => e.DaysLapsed)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DetailOccupancy).IsUnicode(false);

                entity.Property(e => e.DominantHolder).IsUnicode(false);

                entity.Property(e => e.Excess_Amt).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ExpiryDate).HasColumnType("datetime");

                entity.Property(e => e.Financial_Interest)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Facri)
                    .HasColumnName("FACRI")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FireExtinguishersWeight)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FromDate).HasColumnType("datetime");

                entity.Property(e => e.GoogleMap).IsUnicode(false);

                entity.Property(e => e.GrossPremium).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.IntermediaryType)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.JointMember).IsUnicode(false);

                entity.Property(e => e.Link)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.LocationRisk).IsUnicode(false);

                entity.Property(e => e.LossRecord)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MaxLossRatio)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MinLossRatio)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Month)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NumberofCctvcamera)
                    .HasColumnName("NumberofCCTVCamera")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NumberofClaims)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NumberofFireHoseReel)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NumberofFireTank)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NumberofHydrant)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OtherExtinguisher)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OutClaimAmt).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Outstanding_Amt).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.PersonInCharge)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PolicyNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Product)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RenewalPremium).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.RequiredExtinguishers)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ResponsibleParty)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RiskName).IsUnicode(false);

                entity.Property(e => e.SecurityGuards)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Smsflag).HasColumnName("SMSFlag");

                entity.Property(e => e.SqMeterofBuilding)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SumInsured).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ToDate).HasColumnType("datetime");

                entity.Property(e => e.TypeofFireTank)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Uwapproval)
                    .HasColumnName("UWApproval")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Uwdecision)
                    .HasColumnName("UWDecision")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.WorkinProcess)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MotorRenewalRegister>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AccountCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AgentCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AgentName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AgreedSiamount)
                    .HasColumnName("AgreedSIAmount")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ApproveSendSms)
                    .HasColumnName("ApproveSendSMS")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ApproveSendSmsdate)
                    .HasColumnName("ApproveSendSMSDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.AuthorizedCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AuthorizedName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Branch)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BranchAuthority)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BranchAuthorityDate).HasColumnType("datetime");

                entity.Property(e => e.Car_Model)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Class)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CrmrenewalCreateFlag).HasColumnName("CRMRenewalCreateFlag");

                entity.Property(e => e.CrmrenewalCreatedDate)
                    .HasColumnName("CRMRenewalCreatedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Currency)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CusCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerName)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.DominantHolder)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Excess_Amt).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ExpiryDate).HasColumnType("datetime");

                entity.Property(e => e.Financial_Interest)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.FromDate).HasColumnType("datetime");

                entity.Property(e => e.IntermediaryType)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.JointMember)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Link)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Manufacturer)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Month)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Outstanding_Amt).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.PersonInCharge)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PolicyNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PreviousWindscreenValue).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Product)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RenewalPremium).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.RenewedSivalue)
                    .HasColumnName("RenewedSIValue")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.RenewedWindscreenValue).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ResponsibleParty)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RiskName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Sivalue20)
                    .HasColumnName("SIValue20")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Smsflag).HasColumnName("SMSFlag");

                entity.Property(e => e.SumInsured).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ToDate).HasColumnType("datetime");

                entity.Property(e => e.Uwapproval)
                    .HasColumnName("UWApproval")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Uwdecision)
                    .HasColumnName("UWDecision")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<HealthRenewalRegister>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AccountCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AgentCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AgentName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AgreedSiamount)
                    .HasColumnName("AgreedSIAmount")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ApproveSendSms)
                    .HasColumnName("ApproveSendSMS")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ApproveSendSmsdate)
                    .HasColumnName("ApproveSendSMSDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.AuthorizedCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AuthorizedName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BasicCoverage)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Branch)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BranchAuthority)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BranchAuthorityDate).HasColumnType("datetime");

                entity.Property(e => e.Class)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CrmrenewalCreateFlag).HasColumnName("CRMRenewalCreateFlag");

                entity.Property(e => e.CrmrenewalCreatedDate)
                    .HasColumnName("CRMRenewalCreatedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.CusCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerName)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Excess_Amt).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ExpiryDate).HasColumnType("datetime");

                entity.Property(e => e.Financial_Interest)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.FromDate).HasColumnType("datetime");

                entity.Property(e => e.InstallmentPlan)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IntermediaryType)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Link)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Month)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NcbclaimLoading)
                    .HasColumnName("NCBClaimLoading")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.OptionalCoverage1)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OptionalCoverage2)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Outstanding_Amt).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.PersonInCharge)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PolicyNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Product)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RenewalPremium).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ResponsibleParty)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RiskName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Smsflag).HasColumnName("SMSFlag");

                entity.Property(e => e.ToDate).HasColumnType("datetime");

                entity.Property(e => e.Uwapproval)
                    .HasColumnName("UWApproval")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Uwdecision)
                    .HasColumnName("UWDecision")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ParenewalRegister>(entity =>
            {
                entity.ToTable("PARenewalRegister");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AccountCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AgentCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AgentName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AgreedSiamount)
                    .HasColumnName("AgreedSIAmount")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ApproveSendSms)
                    .HasColumnName("ApproveSendSMS")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ApproveSendSmsdate)
                    .HasColumnName("ApproveSendSMSDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.AuthorizedCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AuthorizedName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BasicCoverage)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Branch)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BranchAuthority)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BranchAuthorityDate).HasColumnType("datetime");

                entity.Property(e => e.Class)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CrmrenewalCreateFlag).HasColumnName("CRMRenewalCreateFlag");

                entity.Property(e => e.CrmrenewalCreatedDate)
                    .HasColumnName("CRMRenewalCreatedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.CusCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Excess_Amt).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ExpiryDate).HasColumnType("datetime");

                entity.Property(e => e.Financial_Interest)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.FromDate).HasColumnType("datetime");

                entity.Property(e => e.InstallmentPlan)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IntermediaryType)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Link)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Month)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NcbclaimLoading)
                    .HasColumnName("NCBClaimLoading")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.OptionalCoverage1)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OptionalCoverage2)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Outstanding_Amt).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.PersonInCharge)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PolicyNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Product)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RenewalPremium).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ResponsibleParty)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RiskName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Smsflag).HasColumnName("SMSFlag");

                entity.Property(e => e.ToDate).HasColumnType("datetime");

                entity.Property(e => e.Uwapproval)
                    .HasColumnName("UWApproval")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Uwdecision)
                    .HasColumnName("UWDecision")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EngineeringRenewalRegister>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AbctypeExtinguisher)
                    .HasColumnName("ABCTypeExtinguisher")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AccountCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AgentCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AgentName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AgreedSiamount)
                    .HasColumnName("AgreedSIAmount")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ApproveSendSms)
                    .HasColumnName("ApproveSendSMS")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ApproveSendSmsdate)
                    .HasColumnName("ApproveSendSMSDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.AuthorizedCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AuthorizedName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BlackListed)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Branch)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BranchAuthority)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BranchAuthorityDate).HasColumnType("datetime");

                entity.Property(e => e.BuildingClass)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CapacityofFireTank)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ClaimsExists)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ClaimsPaid).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Class)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Co2typeExtinguishers)
                    .HasColumnName("CO2TypeExtinguishers")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ConvertedNetPremium).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ConvertedOutPremium).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Coverage)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CrmrenewalCreateFlag).HasColumnName("CRMRenewalCreateFlag");

                entity.Property(e => e.CrmrenewalCreatedDate)
                    .HasColumnName("CRMRenewalCreatedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Currency)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CusCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerName).IsUnicode(false);

                entity.Property(e => e.DaysLapsed)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DetailOccupancy).IsUnicode(false);

                entity.Property(e => e.Dmsupload)
                    .HasColumnName("DMSUpload")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DonePending)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Excess_Amt).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ExpiryDate).HasColumnType("datetime");

                entity.Property(e => e.Financial_Interest)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Facri)
                    .HasColumnName("FACRI")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FireExtinguishersWeight)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FromDate).HasColumnType("datetime");

                entity.Property(e => e.GoogleMap).IsUnicode(false);

                entity.Property(e => e.GrossPremium).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.IntermediaryType)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Link)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.LocationRisk).IsUnicode(false);

                entity.Property(e => e.LossRecord)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MaxLossRatio)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MinLossRatio)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Month)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NumberofCctvcamera)
                    .HasColumnName("NumberofCCTVCamera")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NumberofClaims)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NumberofFireHoseReel)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NumberofFireTank)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NumberofHydrant)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OtherExtinguisher)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OutClaimAmt).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Outstanding_Amt).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.PersonInCharge)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PolicyNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Product)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Remarks).IsUnicode(false);

                entity.Property(e => e.RenewalPremium).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.RequiredExtinguishers)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ResponsibleParty)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RiskName).IsUnicode(false);

                entity.Property(e => e.SecurityGuards)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Smsflag).HasColumnName("SMSFlag");

                entity.Property(e => e.SqMeterofBuilding)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SumInsured).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ToDate).HasColumnType("datetime");

                entity.Property(e => e.TypeofFireTank)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Uwapproval)
                    .HasColumnName("UWApproval")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Uwdecision)
                    .HasColumnName("UWDecision")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.WorkinProcess)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PropertyRenewalRegister>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AbctypeExtinguisher)
                    .HasColumnName("ABCTypeExtinguisher")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AccountCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AgentCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AgentName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AgreedSiamount)
                    .HasColumnName("AgreedSIAmount")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ApproveSendSms)
                    .HasColumnName("ApproveSendSMS")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ApproveSendSmsdate)
                    .HasColumnName("ApproveSendSMSDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.AuthorizedCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AuthorizedName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BlackListed)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Branch)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BranchAuthority)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BranchAuthorityDate).HasColumnType("datetime");

                entity.Property(e => e.BuildingClass)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CapacityofFireTank)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ClaimsExists)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ClaimsPaid).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Class)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Co2typeExtinguishers)
                    .HasColumnName("CO2TypeExtinguishers")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ConvertedNetPremium).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ConvertedOutPremium).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Coverage)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CrmrenewalCreateFlag).HasColumnName("CRMRenewalCreateFlag");

                entity.Property(e => e.CrmrenewalCreatedDate)
                    .HasColumnName("CRMRenewalCreatedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Currency)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CusCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerName).IsUnicode(false);

                entity.Property(e => e.DaysLapsed)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DetailOccupancy).IsUnicode(false);

                entity.Property(e => e.Dmsupload)
                    .HasColumnName("DMSUpload")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DonePending)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Excess_Amt).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ExpiryDate).HasColumnType("datetime");

                entity.Property(e => e.Financial_Interest)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Facri)
                    .HasColumnName("FACRI")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FireExtinguishersWeight)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FromDate).HasColumnType("datetime");

                entity.Property(e => e.GoogleMap).IsUnicode(false);

                entity.Property(e => e.GrossPremium).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.IntermediaryType)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Link)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.LocationRisk).IsUnicode(false);

                entity.Property(e => e.LossRecord)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MaxLossRatio)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MinLossRatio)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Month)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NumberofCctvcamera)
                    .HasColumnName("NumberofCCTVCamera")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NumberofClaims)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NumberofFireHoseReel)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NumberofFireTank)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NumberofHydrant)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OtherExtinguisher)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OutClaimAmt).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Outstanding_Amt).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.PersonInCharge)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PolicyNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Product)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Remarks).IsUnicode(false);

                entity.Property(e => e.RenewalPremium).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.RequiredExtinguishers)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ResponsibleParty)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RiskName).IsUnicode(false);

                entity.Property(e => e.SecurityGuards)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Smsflag).HasColumnName("SMSFlag");

                entity.Property(e => e.SqMeterofBuilding)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SumInsured).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ToDate).HasColumnType("datetime");

                entity.Property(e => e.TypeofFireTank)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Uwapproval)
                    .HasColumnName("UWApproval")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Uwdecision)
                    .HasColumnName("UWDecision")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.WorkinProcess)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RenewalRegisterRemark>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.RemarkDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TempRenewalCaseCreate>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AccountCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AgentCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AgentName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AuthorizedCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AuthorizedName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Branch)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Class)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CusCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerName)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.ExpiryDate).HasColumnType("datetime");

                entity.Property(e => e.FromDate).HasColumnType("datetime");

                entity.Property(e => e.IntermediaryType)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Link)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Month)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PersonInCharge)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PolicyNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Product)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RenewalPremium)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ResponsibleParty)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RiskName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ToDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<SurveySms>(entity =>
            {
                entity.ToTable("SurveySMS");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AccountCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AgentCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AgentName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AuthorizedCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AuthorizedName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Branch)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CusCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerName)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.ExpiryDate).HasColumnType("datetime");

                entity.Property(e => e.FromDate).HasColumnType("datetime");

                entity.Property(e => e.IntermediaryType)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Link)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Month)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PersonInCharge)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PolicyNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Product)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.RenewalPremiun).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ResponsibleParty)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RiskName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Smscount)
                    .HasColumnName("SMSCount")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ToDate).HasColumnType("datetime");

                entity.Property(e => e.UploadedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<CoreAccountCode>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AccountCode).HasMaxLength(100);
            });

            modelBuilder.Entity<Log>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.LogTime).HasColumnType("datetime");

                entity.Property(e => e.LogTitle)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Logdetail).IsUnicode(false);
            });

            modelBuilder.Entity<Rules>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Operator1).HasMaxLength(200);

                entity.Property(e => e.Operator2).HasMaxLength(200);

                entity.Property(e => e.Preference1).HasMaxLength(300);

                entity.Property(e => e.Preference2).HasMaxLength(300);

                entity.Property(e => e.PriorityLevel).HasMaxLength(100);

                entity.Property(e => e.Product).HasMaxLength(100);

                entity.Property(e => e.Value2).HasMaxLength(200);
            });

            modelBuilder.Entity<RuleAction>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<Region>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.CreatedOn)
                    .HasColumnName("Created_On")
                    .HasColumnType("datetime");

                entity.Property(e => e.CrmRegionId).HasColumnName("CRM_Region_ID");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnName("Modified_On")
                    .HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RegionCode)
                    .HasColumnName("Region_Code")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RegionMyanmar)
                    .HasColumnName("Region_Myanmar")
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<RiskLocation>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.BuildingNo)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Currency)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DistrictCode)
                    .HasColumnName("District_Code")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DistrictDescription)
                    .HasColumnName("District_Description")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Occupancy)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.RegionCode)
                    .HasColumnName("Region_Code")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RegionDescription)
                    .HasColumnName("Region_Description")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.RiskName)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.RiskSumInsured).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Street).HasMaxLength(500);

                entity.Property(e => e.TownCode)
                    .HasColumnName("Town_Code")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TownDescription)
                    .HasColumnName("Town_Description")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.TownshipCode)
                    .HasColumnName("Township_Code")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TownshipDescription)
                    .HasColumnName("Township_Description")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.UploadedDate)
                    .HasColumnName("Uploaded_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.WardCode)
                    .HasColumnName("Ward_Code")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.WardDescription)
                    .HasColumnName("Ward_Description")
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<FireInventoryinfo>(entity =>
            {
                entity.ToTable("fire_inventoryinfo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.DeclaredAmount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ItemCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ItemDescription)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ProposalReferenceno)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Column>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Floor>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Roof>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Wall>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<FireConstruction>(entity =>
            {
                entity.ToTable("fire_construction");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Columns)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ConstructionClass)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Floor)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProposalReferenceno)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Roof)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Walls)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<FireCoverageinfo>(entity =>
            {
                entity.ToTable("fire_coverageinfo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.ProposalReferenceno)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<FireFeaInfo>(entity =>
            {
                entity.ToTable("fire_feaInfo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.NumberOfCo2type)
                    .HasColumnName("NumberOfCO2Type")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NumberOfCo2typeDesp)
                    .HasColumnName("NumberOfCO2TypeDesp")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NumberOfHoseReel)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NumberOfHoseReelDesp)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NumberOfHydrant)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NumberOfHydrantDesp)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NumberOfOtherExtinguisher)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NumberOfOtherExtinguisherDesp)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NumberofAbctype)
                    .HasColumnName("NumberofABCType")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NumberofAbctypeDesp)
                    .HasColumnName("NumberofABCTypeDesp")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProposalReferenceno)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RequiredNumberOfFireExtinguisher)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RequiredNumberOfFireExtinguisherDesp)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.WeightOfFireExtinguisher)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.WeightOfFireExtinguisherDesp)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<FireFirewatertank>(entity =>
            {
                entity.ToTable("fire_firewatertank");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.CapacityofFireTank)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NumberofFireTank)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProposalReferenceno)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TypeofFireTank)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<FireInventoryinfo>(entity =>
            {
                entity.ToTable("fire_inventoryinfo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.DeclaredAmount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ItemCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ItemDescription)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ProposalReferenceno)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<FireRiskdetail>(entity =>
            {
                entity.ToTable("fire_riskdetail");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.BuildingNo)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Currency)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DistrictCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DistrictDescription)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Occupancy)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ProposalReferenceno)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RegionCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RegionDescription)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RiskLocation).IsUnicode(false);

                entity.Property(e => e.RiskName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RiskSumInsured).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Street)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.TownCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TownDescription)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.TownshipCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TownshipDescription)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.WardCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.WardDescription)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<FireSecurity>(entity =>
            {
                entity.ToTable("fire_security");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Cctvbackdated)
                    .HasColumnName("CCTVBackdated")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Geocoordinate)
                    .HasColumnName("GEOCoordinate")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Losswithin5Years)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NearestFireStation)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NumberofCctvcamera)
                    .HasColumnName("NumberofCCTVCamera")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NumberofLabour)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NumberofSecurityGuards)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProposalReferenceno)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ReferralPoints)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SecurityGuardsDutyPerHour)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SecurityGuardsDutyShift)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RuleProposal>(entity =>
            {
                entity.ToTable("rule_proposal");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AccountCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AccountHandlerCode)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AccountUserCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AgentCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AgentName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Class)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CusCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DrsreferenceNo)
                    .HasColumnName("drsreference_no")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IntermediaryCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IntermediaryName).HasMaxLength(200);

                entity.Property(e => e.IssueDate).HasColumnType("datetime");

                entity.Property(e => e.PolicyEndDate).HasColumnType("datetime");

                entity.Property(e => e.PolicyStartDate).HasColumnType("datetime");

                entity.Property(e => e.Premium).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ProductCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProposalNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ReferenceNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TotalSumInsured).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TransactionType)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Uwremark)
                    .HasColumnName("UWRemark")
                    .IsUnicode(false);

                entity.Property(e => e.Uwstatus)
                    .HasColumnName("UWStatus")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UwstatusDate)
                    .HasColumnName("UWStatusDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.UwsubmittedDate)
                    .HasColumnName("UWSubmittedDate")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<AutoGenerate>(entity =>
            {
                entity.Property(e => e.Maxcount).HasColumnName("MAXCOUNT");

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Prefix)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<FireRequireddocumentsetting>(entity =>
            {
                entity.ToTable("fire_requireddocumentsetting");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Building)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentName)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.IsRequiredDms).HasColumnName("IsRequiredDMS");

                entity.Property(e => e.IsRequiredUw).HasColumnName("IsRequiredUW");

                entity.Property(e => e.Stock)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Requestdetail>(entity =>
            {
                entity.ToTable("requestdetail");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Buremark)
                    .HasColumnName("BURemark")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProposalReferenceNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RequestedDate).HasColumnType("datetime");

                entity.Property(e => e.RespondedDate).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UwpersonEmail)
                    .HasColumnName("UWPersonEmail")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UwpersonId).HasColumnName("UWPersonId");

                entity.Property(e => e.Uwremark)
                    .HasColumnName("UWRemark")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UwPerson>(entity =>
            {
                entity.ToTable("UW_Person");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Bulevel).HasColumnName("BULevel");

                entity.Property(e => e.PersonEmail)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PersonName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Uwlevel).HasColumnName("UWLevel");
            });


            modelBuilder.Entity<FireRequireddocuments>(entity =>
            {
                entity.ToTable("fire_requireddocuments");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.DocumentFile).HasMaxLength(50);

                entity.Property(e => e.DocumentName)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ProposalReferenceNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

        }


    }

   

}
