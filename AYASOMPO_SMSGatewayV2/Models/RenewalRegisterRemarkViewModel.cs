﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class RenewalRegisterRemarkViewModel
    {
        public Guid Id { get; set; }
        public Guid? RemarkBy { get; set; }
        public DateTime? RemarkDate { get; set; }
        public string Remark { get; set; }
        public Guid RenewalRegisterId { get; set; }
        public string UserName  { get; set; }
        public string ProductClass { get; set; }
    }
}
