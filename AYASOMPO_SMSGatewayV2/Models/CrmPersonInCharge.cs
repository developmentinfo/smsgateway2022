﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class CrmPersonInCharge
    {
        public Guid Id { get; set; }
        public Guid? CrmPicId { get; set; }
        public string Picname { get; set; }
        public string ResponsibleParty { get; set; }
    }
}
