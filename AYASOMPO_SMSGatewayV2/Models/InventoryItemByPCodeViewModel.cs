﻿namespace AYASOMPO_SMSGatewayV2.Models
{
    public class InventoryItemByPCodeViewModel
    {
        //PCT_PRD_CODE,ITM_CODE,ITM_DESCRIPTION
        public string PCT_PRD_CODE { get; set; }
        public string ITM_CODE { get; set; }
        public string ITM_DESCRIPTION { get; set; }

    }
}
