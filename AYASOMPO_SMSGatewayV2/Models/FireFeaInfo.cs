﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class FireFeaInfo
    {
        public Guid Id { get; set; }
        public string ProposalReferenceno { get; set; }
        public string NumberofAbctype { get; set; }
        public string NumberOfCo2type { get; set; }
        public string NumberOfOtherExtinguisher { get; set; }
        public string WeightOfFireExtinguisher { get; set; }
        public string RequiredNumberOfFireExtinguisher { get; set; }
        public string NumberOfHoseReel { get; set; }
        public string NumberOfHydrant { get; set; }
        public int? ProposedNumberOfFireExtinguisher { get; set; }
        public Guid? RiskId { get; set; }
        public string NumberofAbctypeDesp { get; set; }
        public string NumberOfCo2typeDesp { get; set; }
        public string NumberOfOtherExtinguisherDesp { get; set; }
        public string WeightOfFireExtinguisherDesp { get; set; }
        public string RequiredNumberOfFireExtinguisherDesp { get; set; }
        public string NumberOfHoseReelDesp { get; set; }
        public string NumberOfHydrantDesp { get; set; }
    }
}
