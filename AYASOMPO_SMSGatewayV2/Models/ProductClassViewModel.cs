﻿namespace AYASOMPO_SMSGatewayV2.Models
{
    public class ProductClassViewModel
    {
        public string PRD_CODE { get; set; }
        public string PRD_CLA_CODE { get; set; }
        public string PRD_DESCRIPTION { get; set; }
    }
}
