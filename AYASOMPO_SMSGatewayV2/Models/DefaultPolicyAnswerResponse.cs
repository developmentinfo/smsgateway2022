﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class DefaultPolicyAnswerResponse
    {
        public List<PolicyAnswerDto> policyAnswerDtos { get; set; }
        public List<RiskDetailDto> riskDetailDtos { get; set; }
        public string seqNo { get; set; }
        public string customerCode { get; set; }
        public string polSeqNo { get; set; }
        public string agentCode { get; set; }
        public string classCode { get; set; }
        public string productCode { get; set; }
        public string transitionDate { get; set; }
        public string status { get; set; }
        public string type { get; set; }
        public string policyNo { get; set; }
        public string accountHandlerCode { get; set; }
        public string proposalNo { get; set; }
        public string className { get; set; }
        public string productName { get; set; }
        public string customer { get; set; }
        public string customerName { get; set; }
        public string agent { get; set; }
        public string intermediaryType { get; set; }
        public double backDate { get; set; }
        public string userCode { get; set; }

        public class PolicyAnswerDto
        {
            public string question { get; set; }
            public string page { get; set; }
            public string ans { get; set; }
            public string fieldCode { get; set; }
            public string limit { get; set; }
            public string lovReq { get; set; }
            public string ansId { get; set; }
            public string displayOrder { get; set; }
            public string newRiskFlag { get; set; }
            public string plcSeqNo { get; set; }
            public string locCode { get; set; }
            public string prsSeqNo { get; set; }
            public string riskRSeq { get; set; }
            public string newLocFlag { get; set; }
            public string newPelFlag { get; set; }
            public string riskSeq { get; set; }
            public string locSeq { get; set; }
            public string dataType { get; set; }
            public string ponDefault { get; set; }
            public string qid { get; set; }
        }

        public class RiskDetailDto
        {
            public List<LocationDetail> locationDetails { get; set; }
            public List<RiskDetail> riskDetails { get; set; }
            public List<CoverDetail> coverDetails { get; set; }
            public List<Inventory> inventory { get; set; }
        }

        public class LocationDetail
        {
            public string question { get; set; }
            public string page { get; set; }
            public string ans { get; set; }
            public string fieldCode { get; set; }
            public string limit { get; set; }
            public string lovReq { get; set; }
            public string ansId { get; set; }
            public string displayOrder { get; set; }
            public string newRiskFlag { get; set; }
            public string plcSeqNo { get; set; }
            public string locCode { get; set; }
            public string prsSeqNo { get; set; }
            public string riskRSeq { get; set; }
            public string newLocFlag { get; set; }
            public string newPelFlag { get; set; }
            public string riskSeq { get; set; }
            public string locSeq { get; set; }
            public string dataType { get; set; }
            public string ponDefault { get; set; }
            public string qid { get; set; }
        }

        public class RiskDetail
        {
            public string question { get; set; }
            public string page { get; set; }
            public string ans { get; set; }
            public string fieldCode { get; set; }
            public string limit { get; set; }
            public string lovReq { get; set; }
            public string ansId { get; set; }
            public string displayOrder { get; set; }
            public string newRiskFlag { get; set; }
            public string plcSeqNo { get; set; }
            public string locCode { get; set; }
            public string prsSeqNo { get; set; }
            public string riskRSeq { get; set; }
            public string newLocFlag { get; set; }
            public string newPelFlag { get; set; }
            public string riskSeq { get; set; }
            public string locSeq { get; set; }
            public string dataType { get; set; }
            public string ponDefault { get; set; }
            public string qid { get; set; }
        }

        public class CoverDetail
        {
            public string question { get; set; }
            public string page { get; set; }
            public string ans { get; set; }
            public string fieldCode { get; set; }
            public string limit { get; set; }
            public string lovReq { get; set; }
            public string ansId { get; set; }
            public string displayOrder { get; set; }
            public string newRiskFlag { get; set; }
            public string plcSeqNo { get; set; }
            public string locCode { get; set; }
            public string prsSeqNo { get; set; }
            public string riskRSeq { get; set; }
            public string newLocFlag { get; set; }
            public string newPelFlag { get; set; }
            public string riskSeq { get; set; }
            public string locSeq { get; set; }
            public string dataType { get; set; }
            public string ponDefault { get; set; }
            public string qid { get; set; }
        }

        public class Inventory
        {
            public string codeDescriptionDto { get; set; }
            public string quantity { get; set; }
            public string referenceNo { get; set; }
            public string declaredAmount { get; set; }
            public string itemDescription { get; set; }
            public string riskSequenceNo { get; set; }
            public string newInventoryStatus { get; set; }
            public List<InventoryDetail> inventoryDetails { get; set; }
            public string policyInventory { get; set; }
        }

        public class InventoryDetail
        {
            public string inventoryDetSeq { get; set; }
            public string referenceNo1 { get; set; }
            public string referenceNo2 { get; set; }
            public string referenceNo3 { get; set; }
            public string date { get; set; }
            public string date2 { get; set; }
            public string value { get; set; }
            public string value2 { get; set; }
            public string referenceNo4 { get; set; }
            public string referenceNo5 { get; set; }
            public string referenceNo6 { get; set; }
            public string inventoryDetDescription { get; set; }
            public string invenDetflag { get; set; }
            public string policyInventoryDet { get; set; }
        }


    }
}
