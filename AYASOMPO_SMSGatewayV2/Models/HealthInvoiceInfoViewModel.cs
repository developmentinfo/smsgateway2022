﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class HealthInvoiceInfoViewModel
    {
        public string pol_policy_no { get; set; }
        public string deb_deb_note_no { get; set; }
        public string pol_cla_code { get; set; }
        public string pol_prd_code { get; set; }
        public string pol_slc_brn_code { get; set; }
        public string account_code { get; set; }
        public string account_handler_name { get; set; }
        public string month { get; set; }
        public string intermediatery_type { get; set; }
        public string agent_code { get; set; }
        public string agent_name { get; set; }
        public string si { get; set; }
        public string no_claim_bonus { get; set; }
        public decimal? claim_loading { get; set; }
        public decimal? renewal_premium { get; set; }
        public string renewalwinscreenvalue { get; set; }

        public string PhoneNumber { get; set; }
        public string CustomerName { get; set; }
        public string InvoiceNo { get; set; }
        public string RiskName { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string Product { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Link { get; set; }
        public string CusCode { get; set; }
        public string PersonInCharge { get; set; }
        public string ResponsibleParty { get; set; }
        public string PolicyNo { get; set; }
        public string Branch { get; set; }
        public string IntermediaryType { get; set; }
        public string AgentCode { get; set; }
        public string AgentName { get; set; }
        public string AccountCode { get; set; }
        public string AuthorizedCode { get; set; }
        public string AuthorizedName { get; set; }
        public string Class { get; set; }
        public string installment_plan { get; set; }
        public string basic_coverage { get; set; }
        public string optional_coverage_1 { get; set; }
        public string optional_coverage_2 { get; set; }
        public string policy_status { get; set; }
        public decimal? outstanding_amt { get; set; }
        public decimal? excess_amt { get; set; }
        public string financial_interest { get; set; }
    }
}
