﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class FireInventoryinfo
    {
        public Guid Id { get; set; }
        public string ProposalReferenceno { get; set; }
        public string ItemCode { get; set; }
        public string ItemDescription { get; set; }
        public decimal? DeclaredAmount { get; set; }
        public Guid? RiskId { get; set; }
    }
}
