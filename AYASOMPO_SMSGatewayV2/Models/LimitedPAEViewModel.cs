﻿namespace AYASOMPO_SMSGatewayV2.Models
{
    public class LimitedPAEViewModel
    {
        public int paepersqft { get; set; }
        public int limitedpae { get; set; }
        public decimal limitedvalue { get; set; }
    }
}
