﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class FireProposalViewModel
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);

        public List<FireRiskDetailView> fireRiskDetailViews { get; set; }
        public string Id { get; set; }
        public string Class { get; set; }
        public string ProductCode { get; set; }
        public string CusCode { get; set; }
        public string CusName { get; set; }
        public string MailingAddress { get; set; }
        public string TotalSumInsured { get; set; }
        public string IssueDate { get; set; }
        public string TransactionType { get; set; }
        public string AgentCode { get; set; }
        public string AgentName { get; set; }
        public string AccountCode { get; set; }
        public string AccountHandlerCode { get; set; }
        public string IntermediaryCode { get; set; }
        public string IntermediaryName { get; set; }
        public string PolicyStartDate { get; set; }
        public string PolicyEndDate { get; set; }
        public string Premium { get; set; }
        public string createdby { get; set; }

        public class FireRiskDetailView
        {
            public Constructiondetail constructiondetail { get; set; }
            public Feainformation feainformation { get; set; }
            public Firewatertank firewatertank { get; set; }
            public Firesecurity firesecurity { get; set; }
            public List<Inventory> inventory { get; set; }
            public Coverage coverage { get; set; }
            public string Id { get; set; }

            public string RiskName { get; set; }
            public string RiskSumInsured { get; set; }
            public string RiskLocation { get; set; }
            public string Occupancy { get; set; }

            public string OccupancyCode { get; set; }
            public string Currency { get; set; }
            public string BuildingNo { get; set; }
            public string Street { get; set; }
            public string WardCode { get; set; }
        }

        public class Constructiondetail
        {
            public string Id { get; set; }
            public string areaOfBuilding { get; set; }
            public string roof { get; set; }
            public string walls { get; set; }
            public string columns { get; set; }
            public string floor { get; set; }
            public string constructionClass { get; set; }
            public string lifeOfBuilding { get; set; }
            public string numberOfStorey { get; set; }
            public string proposedSI { get; set; }
            public string proposedPAE { get; set; }
            public string limitedSI { get; set; }
            public string limitedPAE { get; set; }

            public string roofDescription { get; set; }
            public string wallDescription { get; set; }
            public string columnDescription { get; set; }
            public string floorDescription { get; set; }
            public string constructionDescription { get; set; }
        }

        public class Coverage
        {
            public string Id { get; set; }
            public bool basicCover { get; set; }
            public bool riotStrikeAndMaliciousDamage { get; set; }
            public bool aircraftDamage { get; set; }
            public bool impactDamage { get; set; }
            public bool subsidenceAndLandslide { get; set; }
            public bool earthQuakeFireFireAndShockDamage { get; set; }
            public bool explosion { get; set; }
            public bool spontaneousCombustion { get; set; }
            public bool stormTyphoonHurricaneTempest { get; set; }
            public bool burglary { get; set; }
            public bool warRisk { get; set; }
            public bool floodAndInundation { get; set; }
        }

        public class Feainformation
        {
            public string Id { get; set; }
            public string numberofAbctype { get; set; }
            public string numberOfCo2type { get; set; }
            public string numberOfOtherExtinguisher { get; set; }
            public string weightOfFireExtinguisher { get; set; }
            public string requiredNumberOfFireExtinguisher { get; set; }
            public string numberOfHoseReel { get; set; }
            public string numberOfHydrant { get; set; }
            public string proposedNumberOfFireExtinguisher { get; set; }

            public string numberofAbctypeDesp { get; set; }
            public string numberOfCo2typeDesp { get; set; }
            public string numberOfOtherExtinguisherDesp { get; set; }
            public string weightOfFireExtinguisherDesp { get; set; }
            public string requiredNumberOfFireExtinguisherDesp { get; set; }
            public string numberOfHoseReelDesp { get; set; }
            public string numberOfHydrantDesp { get; set; }
        }

        public class Firesecurity
        {
            public string Id { get; set; }
            public string numberofSecurityGuards { get; set; }
            public string securityGuardsDutyShift { get; set; }
            public string securityGuardsDutyPerHour { get; set; }
            public string numberofCctvcamera { get; set; }
            public string cctvbackdated { get; set; }
            public string numberofLabour { get; set; }
            public string nearestFireStation { get; set; }
            public string geocoordinate { get; set; }
            public string losswithin5Years { get; set; }
            public string referralPoints { get; set; }

            public string numberofSecurityGuardsDesp { get; set; }
            public string securityGuardsDutyShiftDesp { get; set; }
            public string securityGuardsDutyPerHourDesp { get; set; }
            public string numberofCCTVCameraDesp { get; set; }
            

        }

        public class Firewatertank
        {
            public string Id { get; set; }
            public string numberofFireTank { get; set; }
            public string typeofFireTank { get; set; }
            public string capacityofFireTank { get; set; }

            public string numberofFireTankDesp { get; set; }
            public string typeofFireTankDesp { get; set; }
        }

        public class Inventory
        {
            public string Id { get; set; }
            public string itemCode { get; set; }
            public string itemDescription { get; set; }
            public string declaredAmount { get; set; }
        }

        

    }
}
