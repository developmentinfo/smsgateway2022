﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class RenewalRegisterRemark
    {
        public Guid Id { get; set; }
        public Guid? RemarkBy { get; set; }
        public DateTime? RemarkDate { get; set; }
        public string Remark { get; set; }
        public Guid RenewalRegisterId { get; set; }
    }
}
