﻿namespace AYASOMPO_SMSGatewayV2.Models
{
    public class LocationDetials
    {
        public string country_code { get; set; }
        public string country_description { get; set; }
        public string region_code { get; set; }
        public string region_description { get; set; }
        public string district_code { get; set; }

        public string district_description { get; set; }
        public string township_code { get; set; }
        public string township_description { get; set; }
        public string town_code { get; set; }
        public string town_description { get; set; }

        public string ward_code { get; set; }
        public string ward_description { get; set; }
    }
}
