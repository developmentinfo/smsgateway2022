﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class SchedulerLog
    {
        public Guid Id { get; set; }
        public Guid? JobId { get; set; }
        public string JobName { get; set; }
        public DateTime? Datetime { get; set; }
        public string Status { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string Remark { get; set; }
        public int? FailCount { get; set; }
        public int? SuccessCount { get; set; }
        public int? TotalCount { get; set; }
        public string JobStatus { get; set; }
    }
}
