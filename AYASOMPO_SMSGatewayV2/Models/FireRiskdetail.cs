﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class FireRiskdetail
    {
        public Guid Id { get; set; }
        public Guid? ProposalId { get; set; }
        public string ProposalReferenceno { get; set; }
        public string RiskName { get; set; }
        public decimal? RiskSumInsured { get; set; }
        public string RiskLocation { get; set; }
        public string Occupancy { get; set; }
        public string OccupancyCode { get; set; }        
        public string Currency { get; set; }
        public string BuildingNo { get; set; }
        public string Street { get; set; }
        public string WardCode { get; set; }
        public string WardDescription { get; set; }
        public string TownCode { get; set; }
        public string TownDescription { get; set; }
        public string TownshipCode { get; set; }
        public string TownshipDescription { get; set; }
        public string DistrictCode { get; set; }
        public string DistrictDescription { get; set; }
        public string RegionCode { get; set; }
        public string RegionDescription { get; set; }
        public bool? AnyLoss { get; set; }
    }
}
