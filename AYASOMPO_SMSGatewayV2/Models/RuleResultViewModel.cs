﻿namespace AYASOMPO_SMSGatewayV2.Models
{
    public class RuleResultViewModel
    {
        public string riskName { get; set; }
        public string questionId {  get; set; }
        public string questionname { get; set; }
        public string questionvalue { get; set; }
        public string action { get; set; }
    }
}
