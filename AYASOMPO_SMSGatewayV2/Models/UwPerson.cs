﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class UwPerson
    {
        public Guid Id { get; set; }
        public string PersonName { get; set; }
        public string PersonEmail { get; set; }
        public bool? Uwlevel { get; set; }
        public bool? Bulevel { get; set; }
        public bool? Active { get; set; }
    }
}
