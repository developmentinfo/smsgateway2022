﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class RenewalCaseInvoiceInfoResponse
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
        public List<Value> value { get; set; }

        public class Value
        {
            public string incidentid { get; set; }
            public string ayasompo_vehicleno { get; set; }
        }
    }
}
