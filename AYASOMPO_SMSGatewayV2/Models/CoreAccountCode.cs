﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class CoreAccountCode
    {
        public Guid Id { get; set; }
        public string AccountCode { get; set; }
    }
}
