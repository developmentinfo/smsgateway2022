﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class FireRequireddocuments
    {
        public Guid Id { get; set; }
        public Guid requestdetailId { get; set; }
        public string ProposalReferenceNo { get; set; }
        public Guid? RiskId { get; set; }
        public string DocumentName { get; set; }
        public byte[] DocumentFile { get; set; }
        public string UploadedFileName { get; set; }
        
    }
}
