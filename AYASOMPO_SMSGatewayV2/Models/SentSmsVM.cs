﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class SentSmsVM
    {
        public Guid Id { get; set; }
        public string PhoneNumber { get; set; }
        public string UserName { get; set; }
        public string StaffName { get; set; }
        public string Department { get; set; }
        public Guid SysId { get; set; }
        public Guid StaffId { get; set; }
        public Guid DeptId { get; set; }
        public string Message { get; set; }
        public DateTime SentDate { get; set; }
        public List<SentSmsVM> lst { get; set; }
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public string Smscount { get; set; }
        public string Category { get; set; }

        public string PolicyNo { get; set; }
        public DateTime? ExpireDate { get; set; }
        public decimal? RenewalPremium { get; set; }
        public string Product { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Link { get; set; }
        public string CusCode { get; set; }
        public bool? ActivitiesFlag { get; set; }

        public string ClaimNo { get; set; }
        public string VehicleNo { get; set; }
        public Guid CategoryId { get; set; }
        public string Status { get; set; }

        public List<SysUser> StaffList { get; set; }
        public List<Department> DepartmentList { get; set; }
    }
}
