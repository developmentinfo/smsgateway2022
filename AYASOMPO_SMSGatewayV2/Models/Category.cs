﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class Category
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public string MsgTemplate { get; set; }
        public bool RenewalFlag { get; set; }
    }
}
