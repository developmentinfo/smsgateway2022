﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class filterRenewalSMSDataModel
    {
        public DateTime month { get; set; }
        public string classname { get; set; }
        public string message { get; set; }
    }
}
