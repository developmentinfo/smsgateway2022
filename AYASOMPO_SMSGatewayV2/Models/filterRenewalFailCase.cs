﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class filterRenewalFailCase
    {
        public DateTime filterFromDate { get; set; }
        public DateTime filterToDate { get; set; }
        public string classname { get; set; }
        public List<SMSRenewalViewModel> lstSentSMS { get; set; }
    }
}
