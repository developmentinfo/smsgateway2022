﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class MasterDataViewModel
    {
        public string RFT_TYPE { get; set; }
        public string RFT_CODE { get; set; }
        public string RFT_DESCRIPTION { get; set; }
    }
}
