﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class RuleProposal
    {
        public Guid Id { get; set; }
        public string ReferenceNo { get; set; }
        public string Class { get; set; }
        public string ProductCode { get; set; }
        public DateTime? IssueDate { get; set; }
        public string ProposalNo { get; set; }
        public string DrsreferenceNo { get; set; }
        public string TransactionType { get; set; }
        public string CusCode { get; set; }
        public string CusName { get; set; }
        
        public string IntermediaryCode { get; set; }
        public string IntermediaryName { get; set; }
        public string AgentCode { get; set; }
        public string AgentName { get; set; }
        public string AccountCode { get; set; }
        public string AccountUserCode { get; set; }
        public string AccountHandlerCode { get; set; }
        public decimal? TotalSumInsured { get; set; }
        public decimal? Premium { get; set; }
        public DateTime? PolicyStartDate { get; set; }
        public DateTime? PolicyEndDate { get; set; }
        public DateTime? UwsubmittedDate { get; set; }
        public string Uwstatus { get; set; }
        public DateTime? UwstatusDate { get; set; }
        public string Uwremark { get; set; }
        public int? Version { get; set; }

        public string InsuredMailingAddress { get; set; }
        public string createdby { get; set; }
    }
}
