﻿namespace AYASOMPO_SMSGatewayV2.Models
{
    public class TokenResponseViewModel
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public string expire_in { get; set; }
    }
}
