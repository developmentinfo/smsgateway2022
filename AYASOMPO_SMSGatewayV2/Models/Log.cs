﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class Log
    {
        public Guid Id { get; set; }
        public string LogTitle { get; set; }
        public string Logdetail { get; set; }
        public DateTime? LogTime { get; set; }
    }
}
