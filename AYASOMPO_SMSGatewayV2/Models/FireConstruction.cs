﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class FireConstruction
    {
        public Guid Id { get; set; }
        public Guid RiskId { get; set; }
        public string ProposalReferenceno { get; set; }
        public int? AreaOfBuilding { get; set; }
        public string Roof { get; set; }
        public string Walls { get; set; }
        public string Columns { get; set; }
        public string Floor { get; set; }
        public string ConstructionClass { get; set; }
        public int? LifeOfBuilding { get; set; }
        public int? NumberOfStorey { get; set; }

        public decimal ProposedSI { get; set; }
        public decimal ProposedPAE { get; set; }
        public decimal LimitedSI { get; set; }
        public decimal LimitedPAE { get; set; }

        public string RoofDescription { get; set; }
        public string WallDescription { get; set; }
        public string ColumnDescription { get; set; }
        public string FloorDescription { get; set; }
        public string ConstructionDescription { get; set; }






    }
}
