﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class ProductClass
    {
        public Guid Id { get; set; }
        public string ClassCode { get; set; }
        public string ClassName { get; set; }
    }
}
