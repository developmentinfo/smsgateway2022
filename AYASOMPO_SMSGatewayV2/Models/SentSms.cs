﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class SentSms
    {
        public Guid Id { get; set; }
        public string PhoneNumber { get; set; }
        public string UserName { get; set; }
        public Guid SysId { get; set; }
        public string Message { get; set; }
        public DateTime SentDate { get; set; }
        public string Channel { get; set; }
        public string ClaimNo { get; set; }
        public string VehicleNo { get; set; }
        public string Smsflag { get; set; }
        public string Smscount { get; set; }
        public Guid? CategoryId { get; set; }
        public string PolicyNo { get; set; }
        public DateTime? ExpireDate { get; set; }
        public decimal? RenewalPremium { get; set; }
        public string Product { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Link { get; set; }
        public string CusCode { get; set; }
        public bool? ActivitiesFlag { get; set; }
        public string Month { get; set; }
        public string Pic { get; set; }
        public bool? CrmrenewalCreateFlag { get; set; }
        public string ResponsibleParty { get; set; }
    }
}
