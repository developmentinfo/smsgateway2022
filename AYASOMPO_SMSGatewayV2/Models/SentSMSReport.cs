﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class SentSMSReport
    {
        public List<SysUser> StaffList { get; set; }
        public List<Department> DepartmentList { get; set; }
        public Guid StaffId { get; set; }
        public Guid DeptId { get; set; }
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public Guid CategoryId { get; set; }
        public string Category { get; set; }
        public bool ActivitiesFlag { get; set; }
        public string Status { get; set; }
    }
}
