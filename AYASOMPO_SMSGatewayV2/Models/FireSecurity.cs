﻿using System;
using System.Collections.Generic;
using System.IO;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class FireSecurity
    {
        public Guid Id { get; set; }
        public Guid RiskId { get; set; }
        public string ProposalReferenceno { get; set; }
        public string NumberofSecurityGuards { get; set; }
        public string SecurityGuardsDutyShift { get; set; }
        public string SecurityGuardsDutyPerHour { get; set; }
        public string NumberofCctvcamera { get; set; }
        public string Cctvbackdated { get; set; }
        public string NumberofLabour { get; set; }
        public string NearestFireStation { get; set; }
        public string Geocoordinate { get; set; }
        public string Losswithin5Years { get; set; }
        public string ReferralPoints { get; set; }
        public string NumberofSecurityGuardsDesp { get; set; }
        public string SecurityGuardsDutyShiftDesp { get; set; }
        public string SecurityGuardsDutyPerHourDesp { get; set; }
        public string NumberofCCTVCameraDesp { get; set; }
        
    }
}
