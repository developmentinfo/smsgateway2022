﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class MotorRenewalRegister
    {
        public Guid Id { get; set; }
        public string PhoneNumber { get; set; }
        public string CustomerName { get; set; }
        public string InvoiceNo { get; set; }
        public string RiskName { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public decimal? RenewalPremium { get; set; }
        public string Product { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Link { get; set; }
        public string CusCode { get; set; }
        public string Month { get; set; }
        public string PersonInCharge { get; set; }
        public string ResponsibleParty { get; set; }
        public string PolicyNo { get; set; }
        public string Branch { get; set; }
        public string IntermediaryType { get; set; }
        public string AgentCode { get; set; }
        public string AgentName { get; set; }
        public string AccountCode { get; set; }
        public string AuthorizedCode { get; set; }
        public string AuthorizedName { get; set; }
        public string Class { get; set; }
        public string Currency { get; set; }
        public decimal? SumInsured { get; set; }
        public decimal? Sivalue20 { get; set; }
        public decimal? RenewedSivalue { get; set; }
        public decimal? PreviousWindscreenValue { get; set; }
        public decimal? RenewedWindscreenValue { get; set; }
        public string ApproveSendSms { get; set; }
        public DateTime? ApproveSendSmsdate { get; set; }
        public string Uwdecision { get; set; }
        public bool? RemarkId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool? Smsflag { get; set; }
        public bool? CrmrenewalCreateFlag { get; set; }
        public bool? ActivitiesFlag { get; set; }
        public string JointMember { get; set; }
        public string DominantHolder { get; set; }
        public string BranchAuthority { get; set; }
        public string Uwapproval { get; set; }
        public bool? RemarkFlag { get; set; }
        public DateTime? CrmrenewalCreatedDate { get; set; }
        public DateTime? BranchAuthorityDate { get; set; }
        public decimal? AgreedSiamount { get; set; }
        public decimal? Outstanding_Amt { get; set; }
        public decimal? Excess_Amt { get; set; }
        public string Financial_Interest { get; set; }
        public string Manufacturer { get; set; }
        public string Car_Model { get; set; }
    }
}
