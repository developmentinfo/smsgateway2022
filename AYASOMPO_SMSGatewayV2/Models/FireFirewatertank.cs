﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class FireFirewatertank
    {
        public Guid Id { get; set; }

        public Guid RiskId { get; set; }
        public string ProposalReferenceno { get; set; }
        public string NumberofFireTank { get; set; }
        public string TypeofFireTank { get; set; }
        public string CapacityofFireTank { get; set; }

        public string NumberofFireTankDesp { get; set; }
        public string TypeofFireTankDesp { get; set; }

    }
}
