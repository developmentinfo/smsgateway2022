﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class FireUWViewModel
    {
        public Guid Id { get; set; }
        public string ReferenceNo { get; set; }
        public string Class { get; set; }
        public string ProductCode { get; set; }
        public DateTime? IssueDate { get; set; }
        public string ProposalNo { get; set; }
        public string DrsreferenceNo { get; set; }
        public string TransactionType { get; set; }
        public string CusCode { get; set; }
        public string CusName { get; set; }
        public string IntermediaryCode { get; set; }
        public byte[] IntermediaryName { get; set; }
        public string AgentCode { get; set; }
        public string AgentName { get; set; }
        public string AccountCode { get; set; }
        public string AccountUserCode { get; set; }
        public string AccountHandlerCode { get; set; }
        public decimal? TotalSumInsured { get; set; }
        public decimal? Premium { get; set; }
        public DateTime? PolicyStartDate { get; set; }
        public DateTime? PolicyEndDate { get; set; }
        public DateTime? UwsubmittedDate { get; set; }
        public string Uwstatus { get; set; }
        public DateTime? UwstatusDate { get; set; }
        public string Uwremark { get; set; }
        public int? Version { get; set; }

        public string MailingAddress { get; set; }

        public List<RiskLocation> lstRiskLocations = new List<RiskLocation>();

        public FireConstruction fireConstruction = new FireConstruction();
        public FireRiskdetail fireRiskdetail = new FireRiskdetail();
        public FireFeaInfo fireFeaInfo = new FireFeaInfo();
        public FireFirewatertank fireFirewatertank = new FireFirewatertank();
        public FireSecurity fireSecurity = new FireSecurity();
        public FireInventoryinfo fireInventoryinfo = new FireInventoryinfo();
        public FireCoverageinfo fireCoverageinfo = new FireCoverageinfo();
        
        public int riskCount { get; set; }

    }
}
