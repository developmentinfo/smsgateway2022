﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class FireCoverageinfo
    {
        public Guid Id { get; set; }
        public Guid RiskId { get; set; }
        public string ProposalReferenceno { get; set; }
        public bool RiotStrikeAndMaliciousDamage { get; set; }
        public bool AircraftDamage { get; set; }
        public bool ImpactDamage { get; set; }
        public bool SubsidenceAndLandslide { get; set; }
        public bool EarthQuakeFireFireAndShockDamage { get; set; }
        public bool Explosion { get; set; }
        public bool SpontaneousCombustion { get; set; }
        public bool StormTyphoonHurricaneTempest { get; set; }
        public bool Burglary { get; set; }
        public bool WarRisk { get; set; }
        public bool FloodAndInundation { get; set; }
        public bool BasicCover { get; set; }
    }
}
