﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class InvoiceInfo
    {
        public string pol_policy_no { get; set; }
        public string deb_deb_note_no { get; set; }
        public string pol_cla_code { get; set; }
        public string pol_prd_code { get; set; }
        public string pol_slc_brn_code { get; set; }
        public string account_code { get; set; }
        public string account_handler_name { get; set; }
        public string month { get; set; }
        public string intermediatery_type { get; set; }
        public string agent_code { get; set; }
        public string agent_name { get; set; }
        public string si { get; set; }
        public string no_claim_bonus { get; set; }
        public string renewalpremium { get; set; }
        public string renewalwinscreenvalue { get; set; }
    }
}
