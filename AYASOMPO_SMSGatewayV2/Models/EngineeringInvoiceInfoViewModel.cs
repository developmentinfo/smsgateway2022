﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class EngineeringInvoiceInfoViewModel
    {
        public string pol_policy_no { get; set; }
        public string deb_deb_note_no { get; set; }
        public string pol_cla_code { get; set; }
        public string pol_prd_code { get; set; }
        public string pol_slc_brn_code { get; set; }
        public string account_code { get; set; }
        public string account_handler_name { get; set; }
        public string month { get; set; }
        public string intermediatery_type { get; set; }
        public string agent_code { get; set; }
        public string agent_name { get; set; }
        public string si { get; set; }
        public string no_claim_bonus { get; set; }
        public decimal? renewal_premium { get; set; }
        public string renewalwinscreenvalue { get; set; }

        public string PhoneNumber { get; set; }
        public string CustomerName { get; set; }
        public string InvoiceNo { get; set; }
        public string RiskName { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public decimal? RenewalPremium { get; set; }
        public string Product { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Link { get; set; }
        public string CusCode { get; set; }

        public string PersonInCharge { get; set; }
        public string ResponsibleParty { get; set; }
        public string PolicyNo { get; set; }
        public string Branch { get; set; }
        public string IntermediaryType { get; set; }
        public string AgentCode { get; set; }
        public string AgentName { get; set; }
        public string AccountCode { get; set; }
        public string AuthorizedCode { get; set; }
        public string AuthorizedName { get; set; }
        public string Class { get; set; }
        public string Currency { get; set; }
        public decimal? SumInsured { get; set; }
        public string DetailOccupancy { get; set; }
        public string LocationRisk { get; set; }
        public string BuildingClass { get; set; }
        public string DaysLapsed { get; set; }
        public decimal? GrossPremium { get; set; }
        public decimal? ConvertedNetPremium { get; set; }
        public decimal? ConvertedOutPremium { get; set; }
        public string NumberofClaims { get; set; }
        public decimal? ClaimsPaid { get; set; }
        public decimal? OutClaimAmt { get; set; }
        public string BlackListed { get; set; }
        public string ClaimsExists { get; set; }
        public string Facri { get; set; }
        public string MaxLossRatio { get; set; }
        public string MinLossRatio { get; set; }
        public string LossRecord { get; set; }
        public string Coverage { get; set; }
        public string GoogleMap { get; set; }
        public string AbctypeExtinguisher { get; set; }
        public string Co2typeExtinguishers { get; set; }
        public string OtherExtinguisher { get; set; }
        public string FireExtinguishersWeight { get; set; }
        public string NumberofFireHoseReel { get; set; }
        public string NumberofHydrant { get; set; }
        public string CapacityofFireTank { get; set; }
        public string SecurityGuards { get; set; }
        public string NumberofCctvcamera { get; set; }
        public string SqMeterofBuilding { get; set; }
        public string RequiredExtinguishers { get; set; }
        public string WorkinProcess { get; set; }
        public string UwApproval { get; set; }
        public DateTime? UwApprovalDate { get; set; }
        public string UwDecision { get; set; }
        public Guid? RemarkId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool? Smsflag { get; set; }
        public bool? CrmrenewalCreateFlag { get; set; }
        public bool? ActivitiesFlag { get; set; }
        public string JointMember { get; set; }
        public string DominantHolder { get; set; }
        public string number_of_fire_tank { get; set; }
        public string type_of_fire_tank { get; set; }
        public string policy_status { get; set; }
        public decimal? outstanding_amt { get; set; }
        public decimal? excess_amt { get; set; }
        public string financial_interest { get; set; }
    }
}
