﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class filterRenewalDataModel
    {
        public DateTime month { get; set; }
        public string classname { get; set; }
        public string account_code { get; set; }
        public int motortotalcount { get; set; }
        public string picname { get; set; }
        public string responsibleparty { get; set; }
        public string uwapproval { get; set; }
        public string approvesendsms { get; set; }
        public string personincharge { get; set; }
        public bool? RemarkId { get; set; }

        public string[] AccountCodes { get; set; }
        public string[] ResponsibleParties { get; set; }
        public int checkedcount { get; set; }
    }
}
