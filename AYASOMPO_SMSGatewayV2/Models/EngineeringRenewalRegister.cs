﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class EngineeringRenewalRegister
    {
        public Guid Id { get; set; }
        public string PhoneNumber { get; set; }
        public string CustomerName { get; set; }
        public string InvoiceNo { get; set; }
        public string RiskName { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public decimal? RenewalPremium { get; set; }
        public string Product { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Link { get; set; }
        public string CusCode { get; set; }
        public string Month { get; set; }
        public string PersonInCharge { get; set; }
        public string ResponsibleParty { get; set; }
        public string PolicyNo { get; set; }
        public string Branch { get; set; }
        public string IntermediaryType { get; set; }
        public string AgentCode { get; set; }
        public string AgentName { get; set; }
        public string AccountCode { get; set; }
        public string AuthorizedCode { get; set; }
        public string AuthorizedName { get; set; }
        public string Class { get; set; }
        public string Currency { get; set; }
        public decimal? SumInsured { get; set; }
        public string DetailOccupancy { get; set; }
        public string LocationRisk { get; set; }
        public string BuildingClass { get; set; }
        public string DaysLapsed { get; set; }
        public decimal? GrossPremium { get; set; }
        public decimal? ConvertedNetPremium { get; set; }
        public decimal? ConvertedOutPremium { get; set; }
        public string NumberofClaims { get; set; }
        public decimal? ClaimsPaid { get; set; }
        public decimal? OutClaimAmt { get; set; }
        public string BlackListed { get; set; }
        public string ClaimsExists { get; set; }
        public string Facri { get; set; }
        public string MaxLossRatio { get; set; }
        public string MinLossRatio { get; set; }
        public string LossRecord { get; set; }
        public string Coverage { get; set; }
        public string GoogleMap { get; set; }
        public string AbctypeExtinguisher { get; set; }
        public string Co2typeExtinguishers { get; set; }
        public string OtherExtinguisher { get; set; }
        public string FireExtinguishersWeight { get; set; }
        public string NumberofFireHoseReel { get; set; }
        public string NumberofHydrant { get; set; }
        public string NumberofFireTank { get; set; }
        public string TypeofFireTank { get; set; }
        public string CapacityofFireTank { get; set; }
        public string SecurityGuards { get; set; }
        public string NumberofCctvcamera { get; set; }
        public string SqMeterofBuilding { get; set; }
        public string RequiredExtinguishers { get; set; }
        public string WorkinProcess { get; set; }
        public string Dmsupload { get; set; }
        public string DonePending { get; set; }
        public string Remarks { get; set; }
        public string ApproveSendSms { get; set; }
        public DateTime? ApproveSendSmsdate { get; set; }
        public string Uwdecision { get; set; }
        public bool? RemarkId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string BranchAuthority { get; set; }
        public string Uwapproval { get; set; }
        public bool? Smsflag { get; set; }
        public bool? CrmrenewalCreateFlag { get; set; }
        public bool? ActivitiesFlag { get; set; }
        public bool? RemarkFlag { get; set; }
        public DateTime? CrmrenewalCreatedDate { get; set; }
        public DateTime? BranchAuthorityDate { get; set; }
        public decimal? AgreedSiamount { get; set; }
        public decimal? Outstanding_Amt { get; set; }
        public decimal? Excess_Amt { get; set; }
        public string Financial_Interest { get; set; }
    }
}
