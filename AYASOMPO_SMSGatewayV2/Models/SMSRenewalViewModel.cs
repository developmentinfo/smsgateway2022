﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class SMSRenewalViewModel
    {
        public Guid Id { get; set; }
        public Guid smsId { get; set; }
        public Guid temprenewalcaseId { get; set; }
        public Guid? renewalregisterId { get; set; }
        public string PhoneNumber { get; set; }
        public string CustomerName { get; set; }
        public string InvoiceNo { get; set; }
        public string RiskName { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public decimal? RenewalPremium { get; set; }
        public string Product { get; set; }
        public string Class { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Link { get; set; }
        public string CusCode { get; set; }
        public string Month { get; set; }
        public string PersonInCharge { get; set; }
        public string ResponsibleParty { get; set; }
        public string PolicyNo { get; set; }
        public string Branch { get; set; }
        public string IntermediaryType { get; set; }
        public string AgentCode { get; set; }
        public string AgentName { get; set; }
        public string AccountCode { get; set; }
        public string AuthorizedCode { get; set; }
        public string AuthorizedName { get; set; }
        public bool Smsflag { get; set; }
        public bool ActivitiesFlag { get; set; }
        public string Message { get; set; }
        public string SMSCount { get; set; }
        public Guid CategoryID { get; set; }
        public bool CrmrenewalCreateFlag { get; set; }

    }
}
