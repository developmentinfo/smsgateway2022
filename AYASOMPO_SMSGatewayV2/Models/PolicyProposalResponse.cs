﻿using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class PolicyProposalResponse
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);

        public PremiumBreakDownDto premiumBreakDownDto { get; set; }
        public PolicyAgreementDto policyAgreementDto { get; set; }
        public string tempPolSeqNo { get; set; }
        public object policyProposalNo { get; set; }
        public object polSeqNo { get; set; }
        public object quotSeqNo { get; set; }
        public object commissionAmount { get; set; }
        public object payTerms { get; set; }

        public class Clauses
        {
        }

        public class Conditions
        {
        }

        public class Funds
        {
            public double PREMIUM { get; set; }
        }

        public class OtherCharges
        {
        }

        public class PolicyAgreementDto
        {
            public Clauses clauses { get; set; }
            public Warranties warranties { get; set; }
            public Conditions conditions { get; set; }
            public bool agreement { get; set; }
        }

        public class PolicyOnlineRisk
        {
            public string podSeqNo { get; set; }
            public string podPolSeqNo { get; set; }
            public string podRiskName { get; set; }
            public double podRiskPremium { get; set; }
        }

        public class PremiumBreakDownDto
        {
            public Funds funds { get; set; }
            public Taxes taxes { get; set; }
            public List<PolicyOnlineRisk> policyOnlineRisks { get; set; }
            public OtherCharges otherCharges { get; set; }
            public double netPremium { get; set; }
        }

        public class Taxes
        {
        }

        public class Warranties
        {
        }


    }
}
