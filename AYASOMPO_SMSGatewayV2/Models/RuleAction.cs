﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class RuleAction
    {
        public Guid Id { get; set; }
        public string ActionName { get; set; }
    }
}
