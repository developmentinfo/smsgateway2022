﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class DefaultPolicyAnswerRequest
    {
        public string customerCode { get; set; }
        public string policyNumber { get; set; }
        public string productCode { get; set; }
        public string questionCode { get; set; }
        public string type { get; set; }
        public string userCode { get; set; }
    }
}
