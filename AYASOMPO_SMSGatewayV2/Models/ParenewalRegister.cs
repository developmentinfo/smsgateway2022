﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class ParenewalRegister
    {
        public Guid Id { get; set; }
        public string PhoneNumber { get; set; }
        public string CustomerName { get; set; }
        public string InvoiceNo { get; set; }
        public string RiskName { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public decimal? RenewalPremium { get; set; }
        public string Product { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Link { get; set; }
        public string CusCode { get; set; }
        public string Month { get; set; }
        public string PersonInCharge { get; set; }
        public string ResponsibleParty { get; set; }
        public string PolicyNo { get; set; }
        public string Branch { get; set; }
        public string IntermediaryType { get; set; }
        public string AgentCode { get; set; }
        public string AgentName { get; set; }
        public string AccountCode { get; set; }
        public string AuthorizedCode { get; set; }
        public string AuthorizedName { get; set; }
        public string Class { get; set; }
        public string InstallmentPlan { get; set; }
        public string BasicCoverage { get; set; }
        public string OptionalCoverage1 { get; set; }
        public string OptionalCoverage2 { get; set; }
        public decimal? NcbclaimLoading { get; set; }
        public string ApproveSendSms { get; set; }
        public DateTime? ApproveSendSmsdate { get; set; }
        public string Uwdecision { get; set; }
        public bool? RemarkId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string BranchAuthority { get; set; }
        public string Uwapproval { get; set; }
        public bool? Smsflag { get; set; }
        public bool? CrmrenewalCreateFlag { get; set; }
        public bool? ActivitiesFlag { get; set; }
        public bool? RemarkFlag { get; set; }
        public DateTime? CrmrenewalCreatedDate { get; set; }
        public DateTime? BranchAuthorityDate { get; set; }
        public decimal? AgreedSiamount { get; set; }
        public decimal? Outstanding_Amt { get; set; }
        public decimal? Excess_Amt { get; set; }
        public string Financial_Interest { get; set; }
    }
}
