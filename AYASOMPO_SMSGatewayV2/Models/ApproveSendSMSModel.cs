﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class ApproveSendSMSModel
    {
        public string[] ids { get; set; }
        public string approvesendsms { get; set; }
        public string productclass { get; set; }
        public string[] phonenos { get; set; }
    }
}
