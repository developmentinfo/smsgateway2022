﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class Department
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsDelete { get; set; }
        public long Version { get; set; }
        public Guid? CreatedUserId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? UpdatedUserId { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public Guid? DeletedUserId { get; set; }
        public DateTime? DeletedDate { get; set; }
    }
}
