﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class Rules
    {
        public Guid Id { get; set; }
        public string Product { get; set; }
        public string Preference1 { get; set; }
        public string Preference1_Name { get; set; }
        public string Operator1 { get; set; }
        public string Value1 { get; set; }
        public string Preference2 { get; set; }
        public string Preference2_Name { get; set; }
        public string Operator2 { get; set; }
        public string Value2 { get; set; }
        public string PriorityLevel { get; set; }
        public string Action { get; set; }
    }
}
