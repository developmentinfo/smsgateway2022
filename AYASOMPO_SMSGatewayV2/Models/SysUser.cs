﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class SysUser
    {
        public Guid Id { get; set; }
        public Guid Role { get; set; }
        public Guid? Department { get; set; }
        public string Name { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsActive { get; set; }
        public string ConfirmCode { get; set; }
        public bool? FirstTimeLogin { get; set; }
        public string Email { get; set; }
        public bool DeleteFlag { get; set; }
        public bool EditFlag { get; set; }
        public bool ViewFlag { get; set; }


    }
}
