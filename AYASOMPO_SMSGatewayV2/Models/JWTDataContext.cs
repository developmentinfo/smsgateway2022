﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class JWTDataContext : DbContext
    {
        public IConfiguration Configuration { get; set; }

        public JWTDataContext()
        {
            var builder = new ConfigurationBuilder()
          .SetBasePath(Directory.GetCurrentDirectory())
          .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
        }

        public JWTDataContext(DbContextOptions<JWTDataContext> options)
           : base(options)
        {
        }

        public virtual DbSet<CrmPersonInCharge> CrmPersonInCharge { get; set; }
        public virtual DbSet<CrmenquiryProductType> CrmenquiryProductType { get; set; }
        public virtual DbSet<AccountHandlerPic> AccountHandlerPic { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(Configuration.GetConnectionString("JWTDBConnection"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<CrmPersonInCharge>(entity =>
            {
                entity.ToTable("CRM_PersonInCharge");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.CrmPicId).HasColumnName("CRM_PIC_Id");

                entity.Property(e => e.Picname).HasColumnName("PICName");

                entity.Property(e => e.ResponsibleParty)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AccountHandlerPic>(entity =>
            {
                entity.ToTable("AccountHandlerPIC");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AccountCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CrmPicId).HasColumnName("CRM_PIC_Id");
            });
        }
    }
}
