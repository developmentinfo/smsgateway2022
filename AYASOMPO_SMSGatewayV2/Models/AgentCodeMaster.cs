﻿namespace AYASOMPO_SMSGatewayV2.Models
{
    public class AgentCodeMaster
    {
        public string sfc_code { get; set; }
        public string sfc_surname { get; set; }
    }
}
