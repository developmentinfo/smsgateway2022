﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class RuleViewModel
    {
        public Guid Id { get; set; }
        public string Product { get; set; }
        public string QuestionId1 { get; set; }
        public string Value1 { get; set; }
        public string QuestionId2 { get; set; }
        public string Value2 { get; set; }
        public string Operator1 { get; set; }
        public string Operator2 { get; set; }
        //public string CustomValue { get; set; }
        public string Priority { get; set; }
        public string Action { get; set; }
    }
}
