﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class ReCreateRenewalFailCase
    {
        public Guid Id { get; set; }
        public Guid smsId { get; set; }
        public bool isCRMRenewalCreateFlag { get; set; }
        public string Class { get; set; }
        public string PolicyNo { get; set; }

    }
}
