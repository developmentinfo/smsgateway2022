﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class Region
    {
        public Guid Id { get; set; }
        public Guid? CrmRegionId { get; set; }
        public string Name { get; set; }
        public string RegionCode { get; set; }
        public string RegionMyanmar { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}
