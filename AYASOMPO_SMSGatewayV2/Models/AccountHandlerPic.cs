﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class AccountHandlerPic
    {
        public Guid Id { get; set; }
        public Guid? CrmPicId { get; set; }
        public string AccountCode { get; set; }
    }
}
