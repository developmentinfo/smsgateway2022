﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class NewCustomerInfo
    {
        public string cus_code { get; set; }
        public string cus_type { get; set; }
        public string cus_phone_1 { get; set; }
        public string cus_phone_2 { get; set; }
        public string created_date { get; set; }
        public string modified_date { get; set; }
    }
}
