﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class ValidateCustomerResponse
    {
        [DataMember(Name = "@odata.context")]
        public string context { get; set; }
        public IList<Value> value { get; set; }

        public class Value
        {
            [DataMember(Name = "@odata.etag")]
            public string etag { get; set; }
            public string fullname { get; set; }
            public string contactid { get; set; }
        }
    }
}
