﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class Requestdetail
    {
        public Guid Id { get; set; }
        public string ProposalReferenceNo { get; set; }
        public Guid? UwpersonId { get; set; }
        public string UwpersonEmail { get; set; }
        public DateTime? RequestedDate { get; set; }
        public DateTime? RespondedDate { get; set; }
        public string Status { get; set; }
        public string Buremark { get; set; }
        public string Uwremark { get; set; }
    }
}
