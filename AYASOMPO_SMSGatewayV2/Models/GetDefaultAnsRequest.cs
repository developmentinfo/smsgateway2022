﻿namespace AYASOMPO_SMSGatewayV2.Models
{
    public class GetDefaultAnsRequest
    {
        public string customerCode { get; set; }
        public string productCode { get; set; }
        public string type { get; set; }
    }
}
