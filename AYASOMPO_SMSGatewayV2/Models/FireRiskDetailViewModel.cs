﻿using System;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class FireRiskDetailViewModel
    {
        public Guid Id { get; set; }
        public Guid? ProposalId { get; set; }
        public string ProposalReferenceno { get; set; }
        public string RiskName { get; set; }
        public decimal? RiskSumInsured { get; set; }
        public string RiskLocation { get; set; }
        public string Occupancy { get; set; }
        public string Currency { get; set; }
        public string BuildingNo { get; set; }
        public string Street { get; set; }
        public string WardCode { get; set; }
        public string WardDescription { get; set; }
        public string TownCode { get; set; }
        public string TownDescription { get; set; }
        public string TownshipCode { get; set; }
        public string TownshipDescription { get; set; }
        public string DistrictCode { get; set; }
        public string DistrictDescription { get; set; }
        public string RegionCode { get; set; }
        public string RegionDescription { get; set; }
        public bool? AnyLoss { get; set; }

        public FireRiskdetail riskdetail = new FireRiskdetail();
        public FireConstruction constructiondetail = new FireConstruction();
        public FireFeaInfo feainformation = new FireFeaInfo();
        public FireFirewatertank firewatertank = new FireFirewatertank();
        public FireSecurity firesecurity = new FireSecurity();
        public FireInventoryinfo inventory = new FireInventoryinfo();
        public FireCoverageinfo coverage = new FireCoverageinfo();
    }
}
