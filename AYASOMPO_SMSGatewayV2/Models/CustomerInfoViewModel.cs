﻿using System.Collections.Generic;
using System;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class CustomerInfoViewModel
    {
        public string CUS_CODE { get; set; }
        public string CUS_TYPE { get; set; }
        public string SURNAME { get; set; }
        public object CUS_INDV_OTHER_NAMES { get; set; }
        public object CUS_INDV_INITIALS { get; set; }
        public string CUS_INDV_NIC_NO { get; set; }
        public string CUS_OCCUPATION { get; set; }
        public string CUS_DISTRICT { get; set; }
        public string CUS_PROVINCE { get; set; }
        public string CUS_POSTAL_CODE { get; set; }
        public string CUS_PHONE_1 { get; set; }
        public object CUS_PHONE_2 { get; set; }
        public object CUS_FAX { get; set; }
        public string CUS_EMAIL { get; set; }
        public object CUS_WEB_PAGE { get; set; }
        public string CUS_ADDRESS_1 { get; set; }
        public object CUS_ADDRESS_2 { get; set; }
        public string CUS_ADDRESS_3 { get; set; }
        public object CUS_RLC_RLOC_CODE { get; set; }
        public string CUS_INDV_GENDER { get; set; }
        public double CUS_OUTSTAND_AMT { get; set; }
        public object CUS_GRP_CODE { get; set; }
        public object CUS_CORP_REG_NO { get; set; }
        public string CUS_INDV_TITLE { get; set; }
        public object CUS_RC_OUTSTAND_CHQ_AMT { get; set; }
        public string CUS_CTY_CODE { get; set; }
        public string CUS_CITY { get; set; }
        public string CUS_STREET { get; set; }
        public object CUS_BUILDING { get; set; }
        public string CUS_NUMBER { get; set; }
        public string CUS_LOC_DESCRIPTION { get; set; }
        public object CUS_JOINT { get; set; }
        public object CUS_FATHERS_NAME { get; set; }
        public object CUS_DEBTOR_INSERT { get; set; }
        public object CUS_CHANGE_STATUS { get; set; }
        public object CUS_CNL_CODE { get; set; }
        public object CUS_INDV_FIRST_NAME { get; set; }
        public DateTime CUS_INDV_DOB { get; set; }
        public object CUS_INDV_PP_NO { get; set; }
        public object CUS_INDV_DL_NO { get; set; }
        public string CUS_STATUS { get; set; }
        public object CUS_ONLINE_LOGIN { get; set; }
        public object CUS_ONLINE_PASSWORD { get; set; }
        public object CUS_BRN_CODE { get; set; }
        public object CUS_BPARTY_CODE { get; set; }
        public object CUS_SEND_EMAIL { get; set; }
        public object CUS_SEND_SMS { get; set; }
        public object CUS_PORTAL_ACCESS_REQ { get; set; }
        public List<CUSTOMERADDRESS> CUSTOMERADDRESSES { get; set; }
        public class CUSTOMERADDRESS
        {
            public string ADDRESSSEQNO { get; set; }
            public string ADDRESSCUSCODE { get; set; }
            public string ADCITY { get; set; }
            public string ADSTREET { get; set; }
            public object ADBUILDING { get; set; }
            public string ADNUMBER { get; set; }
            public string ADLOCATIONDESCRIPTION { get; set; }
            public string ADDEFAULTADDRESS { get; set; }
            public string ADPROVINCE { get; set; }
            public string ADDISTRICT { get; set; }
            public string ADPOSTALCODE { get; set; }
            public string ADEARTHQUAKEZONE { get; set; }
            public string ADCYCLONEZONE { get; set; }
            public int COUNT { get; set; }
        }

       
    }
}
