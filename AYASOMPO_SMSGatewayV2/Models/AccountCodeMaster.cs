﻿namespace AYASOMPO_SMSGatewayV2.Models
{
    public class AccountCodeMaster
    {
        public string sfc_code { get; set; }
        public string sfc_username { get; set; }
        public string sfc_acc_code { get; set; }
        public string sfc_surname { get; set; }
    }
}
