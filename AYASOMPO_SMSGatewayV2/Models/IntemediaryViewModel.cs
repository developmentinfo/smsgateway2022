﻿namespace AYASOMPO_SMSGatewayV2.Models
{
    public class IntemediaryViewModel
    {

        public string BSS_BSS_CODE { get; set; }
        public string BSS_BSS_DESC { get; set; }
    }
}
