﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class SummaryRenewalRegister
    {
        public int year { get; set; }
        public string month { get; set; }
        public int totalcount { get; set; }
        public string productclass { get ;set;}
    }
}
