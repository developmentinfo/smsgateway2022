﻿using System;
using System.Collections.Generic;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public partial class FireRequireddocumentsetting
    {
        public Guid Id { get; set; }
        public string DocumentName { get; set; }
        public string Building { get; set; }
        public string Stock { get; set; }
        public bool? IsRequiredUw { get; set; }
        public bool? IsRequiredDms { get; set; }
        public string DocumentShortName { get; set; }
        
    }
}
