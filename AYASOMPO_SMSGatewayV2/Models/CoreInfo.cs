﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class CoreInfo
    {
        public string Class { get; set; }
        public string Product { get; set; }
        public string Product_Code { get; set; }
        public string Cus_Name { get; set; }
        public string Phone_No { get; set; }
        public string Policy_No { get; set; }
        public DateTime Period_From { get; set; }
        public DateTime Period_To { get; set; }
        public string Premium { get; set; }
        public string Account_Code { get; set; }
        public string Vehicle_No { get; set; }
        public string Link { get; set; }
        public string Cus_Code { get; set; }
        public string Credit_term { get; set; }
        public string IsExcluded { get; set; }
        public string filter { get; set; }
        public string Transaction_type { get; set; }
        public string Authorized_code { get; set; }
        public string Authorized_name { get; set; }
        public string Invoice_No { get; set; }
        public DateTime Expiry_Date { get; set; }
        public string Expiry { get; set; }

        public string Branch { get; set; }
        public string Intermediary_Type { get; set; }
        public string Agent_Code { get; set; }
        public string Agent_Name { get; set; }

        public string SFC_ACC_CODE { get; set; }
        public Guid CategoryId { get; set; }
        public string Category { get; set; }
        public string from { get; set; }
        public string to { get; set; }
        public string[] ProductLIST { get; set; }
        public string[] ClassLIST { get; set; }
        public string[] ACC_CODE_LIST { get; set; }

        public List<CoreInfo> lst { get; set; }
    }
}
