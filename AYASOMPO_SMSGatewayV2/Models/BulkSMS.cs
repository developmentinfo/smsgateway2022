﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class BulkSMS
    {
        public List<Guid> Id { get; set; }
        //public List<string> Id { get; set; }
        public List<string> PhoneNoList { get; set; }
        public List<string> UserList { get; set; }
        public string Message { get; set; }

        public List<string> InvoiceNoList { get; set; }
        public List<string> VehicleNoList { get; set; }
        public List<DateTime> ExpiryDateList { get; set; }
        public List<decimal> RenewalPremiumList { get; set; }
        public List<string> ProductList { get; set; }
        public List<DateTime> FromDateList { get; set; }
        public List<DateTime> ToDateList { get; set; }
        public List<string> LinkList { get; set; }
        public List<string> CusCodeList { get; set; }
        public List<string> MonthList { get; set; }
        public List<string> PICList { get; set; }
        public List<string> ResponsiblePartyList { get; set; }
        public Guid? CategoryId { get; set; }
        public List<string> PolicyNoList { get; set; }
        public List<string> AccountCodeList { get; set; }

        public List<BulkSMSDetail> BulkSMSDetail { get; set; }
    }

    public class BulkSMSDetail
    {
        public string PhoneNo { get; set; }
        public string User { get; set; }
        public string InvoiceNo { get; set; }
        public string VehicleNo { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public decimal? RenewalPremium { get; set; }
        public string Product { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Link { get; set; }
        public string CusCode { get; set; }
        public string Month { get; set; }
        public string PIC { get; set; }
        public string ResponsibleParty { get; set; }
        public Guid Id { get; set; }
        public string PolicyNo { get; set; }
        public string AccountCode { get; set; }
    }
}
