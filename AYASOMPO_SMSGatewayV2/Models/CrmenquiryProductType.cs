﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Models
{
    public class CrmenquiryProductType
    {
        public Guid Id { get; set; }
        public Guid? CrmproductId { get; set; }
        public string CrmenquiryProductName { get; set; }
        public string CoreProductName { get; set; }
        public string Category { get; set; }
        public bool? RenewalStatus { get; set; }
    }
}
