#pragma checksum "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\FireRuleBase\FireRuleSetup.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0faf11ae7d6da9d76d6e4e2a685bf64d0d8faaeb"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_FireRuleBase_FireRuleSetup), @"mvc.1.0.view", @"/Views/FireRuleBase/FireRuleSetup.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/FireRuleBase/FireRuleSetup.cshtml", typeof(AspNetCore.Views_FireRuleBase_FireRuleSetup))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\_ViewImports.cshtml"
using AYASOMPO_SMSGatewayV2;

#line default
#line hidden
#line 2 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\_ViewImports.cshtml"
using AYASOMPO_SMSGatewayV2.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0faf11ae7d6da9d76d6e4e2a685bf64d0d8faaeb", @"/Views/FireRuleBase/FireRuleSetup.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"62aa250c0e39e8ffc033415f13860b4d515bb115", @"/Views/_ViewImports.cshtml")]
    public class Views_FireRuleBase_FireRuleSetup : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<RuleViewModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/rule.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("href", new global::Microsoft.AspNetCore.Html.HtmlString("~/css/select2/css/select2.min.css"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("rel", new global::Microsoft.AspNetCore.Html.HtmlString("stylesheet"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/css/select2/js/select2.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", "text", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("form-control"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("placeholder", new global::Microsoft.AspNetCore.Html.HtmlString("Your Custom Value"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_7 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_8 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("ddlValue1"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_9 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("ddlValue2"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.SelectTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\FireRuleBase\FireRuleSetup.cshtml"
  
    ViewData["Title"] = "FireRuleSetup";
    Layout = "~/Views/Shared/_ManageLayout.cshtml";

#line default
#line hidden
            BeginContext(124, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(126, 36, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0faf11ae7d6da9d76d6e4e2a685bf64d0d8faaeb7528", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(162, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(164, 66, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("link", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "0faf11ae7d6da9d76d6e4e2a685bf64d0d8faaeb8705", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(230, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(232, 55, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0faf11ae7d6da9d76d6e4e2a685bf64d0d8faaeb9957", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(287, 1502, true);
            WriteLiteral(@"

<script type=""text/javascript"">
    $('document').ready(function () {

        //   onload: call the above function
        $("".select2"").each(function () {
            initializeSelect2($(this));
        });

        $("".select2"").select2({
            allowClear: true,
            tags: true,
            width: 'resolve' // need to override the changed default
        });


    });

    //function to initialize select2
    function initializeSelect2(selectElementObj) {
        selectElementObj.select2({
            //tags: false,
            allowClear: true,
        });
    }

</script>

<div class=""row"">
    <div class=""col-md-12"">
        <div class=""x_content"">
            <div class=""row"">
                <div class=""col-md-12 col-sm-12"">
                    <div class=""x_panel"">
                        <div class=""x_title"">
                            <h2>Fire Rule Base Setup<small></small></h2>
                            <ul class=""nav navbar-right panel_toolbo");
            WriteLiteral(@"x"">
                                <li>
                                    <a class=""collapse-link""><i class=""fa fa-chevron-up""></i></a>
                                </li>

                                <li>
                                    <a class=""close-link""><i class=""fa fa-close""></i></a>
                                </li>
                            </ul>
                            <div class=""clearfix""></div>
                        </div>

");
            EndContext();
#line 58 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\FireRuleBase\FireRuleSetup.cshtml"
                         using (Html.BeginForm("SaveFireRule", "FireRuleBase", FormMethod.Post, new { enctype = "multipart/form-data" }))
                        {

#line default
#line hidden
            BeginContext(1955, 1336, true);
            WriteLiteral(@"                            <div class=""x_content"">
                                <div class=""form-horizontal form-label-left"">
                                    <div class=""form-group row"">
                                        <div class=""col-md-9 col-sm-9  offset-md-3"">
                                            <button type=""button"" class=""btn btn-primary"">Cancel</button>
                                            <button class=""btn btn-primary"" type=""reset"">Reset</button>
                                            <button type=""submit"" class=""btn btn-success"">Save</button>
                                        </div>
                                    </div>

                                    <div class=""ln_solid""></div>

                                    <div class=""form-group row"">
                                        <label class=""control-label col-md-3 col-sm-3"" style=""font-weight:700;"">If the following conditions are met</label>
                                    </");
            WriteLiteral(@"div>


                                    <div class=""form-group row"">
                                        <label class=""control-label col-md-3 col-sm-3 "">Your Mean Preference</label>
                                        <div class=""col-md-9 col-sm-9 "">
                                            ");
            EndContext();
            BeginContext(3292, 191, false);
#line 80 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\FireRuleBase\FireRuleSetup.cshtml"
                                       Write(Html.DropDownListFor(model => model.QuestionId1, ViewBag.RuleQuestion1 as IEnumerable<SelectListItem>, "Your Mean Preference", new { @class = "form-control select2", @id = "ddlQuestionId1" }));

#line default
#line hidden
            EndContext();
            BeginContext(3483, 404, true);
            WriteLiteral(@"
                                        </div>
                                    </div>

                                    <div class=""form-group row"" id=""divOperation"">
                                        <label class=""control-label col-md-3 col-sm-3 "">Operator</label>
                                        <div class=""col-md-9 col-sm-9 "">
                                            ");
            EndContext();
            BeginContext(3888, 167, false);
#line 87 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\FireRuleBase\FireRuleSetup.cshtml"
                                       Write(Html.DropDownListFor(model => model.Operator1, ViewBag.OperatorList as IEnumerable<SelectListItem>, "Select Compare Operator", new { @class = "form-control select2" }));

#line default
#line hidden
            EndContext();
            BeginContext(4055, 488, true);
            WriteLiteral(@"
                                        </div>
                                    </div>


                                    <!-- Add Additional rule (AND) -->

                                    <div class=""form-group row"" id=""divTextBox2"">
                                        <label class=""control-label col-md-3 col-sm-3 "">Your Custom Value</label>
                                        <div class=""col-md-9 col-sm-9 "">
                                            ");
            EndContext();
            BeginContext(4543, 91, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "0faf11ae7d6da9d76d6e4e2a685bf64d0d8faaeb16543", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.InputTypeName = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
#line 97 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\FireRuleBase\FireRuleSetup.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.Value1);

#line default
#line hidden
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(4634, 413, true);
            WriteLiteral(@"
                                        </div>
                                    </div>

                                    <div class=""form-group row"" id=""divQuestion2"">
                                        <label class=""control-label col-md-3 col-sm-3 "">Your Mean Value 2</label>
                                        <div class=""col-md-9 col-sm-9 "">
                                            ");
            EndContext();
            BeginContext(5047, 204, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("select", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0faf11ae7d6da9d76d6e4e2a685bf64d0d8faaeb18899", async() => {
                BeginContext(5108, 50, true);
                WriteLiteral("\r\n                                                ");
                EndContext();
                BeginContext(5158, 38, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0faf11ae7d6da9d76d6e4e2a685bf64d0d8faaeb19334", async() => {
                    BeginContext(5175, 12, true);
                    WriteLiteral("Choose Value");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_7.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_7);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(5196, 46, true);
                WriteLiteral("\r\n                                            ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.SelectTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_8);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
#line 104 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\FireRuleBase\FireRuleSetup.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.Value1);

#line default
#line hidden
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(5251, 1059, true);
            WriteLiteral(@"
                                        </div>
                                    </div>
                                    <div class=""clearfix""></div>

                                    <div class=""form-group row"" style=""margin-top:10px;"">
                                        <label class=""control-label col-md-3 col-sm-3 ""></label>
                                        <div class=""col-md-9 col-sm-9"">
                                            <input type=""button"" value=""Add Condition"" class=""btn btn-primary"" id=""btnRuleAdd"" />

                                            <div class=""row"" style=""border: 1px solid #aeabab;padding:20px;margin-top:20px;"" id=""divAdditionalRule"">
                                                <div class=""form-group row"">
                                                    <label class=""control-label col-md-3 col-sm-3 "">Your Mean Preference 3</label>
                                                    <div class=""col-md-9 col-sm-9 "">
                     ");
            WriteLiteral("                                   ");
            EndContext();
            BeginContext(6311, 183, false);
#line 120 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\FireRuleBase\FireRuleSetup.cshtml"
                                                   Write(Html.DropDownListFor(model => model.QuestionId2, ViewBag.RuleQuestion1 as IEnumerable<SelectListItem>, "Your Mean Preference", new { @class = "form-control", @id = "ddlQuestionId3" }));

#line default
#line hidden
            EndContext();
            BeginContext(6494, 476, true);
            WriteLiteral(@"
                                                    </div>
                                                </div>

                                                <div class=""form-group row"" id=""divOperation"">
                                                    <label class=""control-label col-md-3 col-sm-3 "">Operator</label>
                                                    <div class=""col-md-9 col-sm-9 "">
                                                        ");
            EndContext();
            BeginContext(6971, 159, false);
#line 127 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\FireRuleBase\FireRuleSetup.cshtml"
                                                   Write(Html.DropDownListFor(model => model.Operator2, ViewBag.OperatorList as IEnumerable<SelectListItem>, "Select Compare Operator", new { @class = "form-control" }));

#line default
#line hidden
            EndContext();
            BeginContext(7130, 485, true);
            WriteLiteral(@"
                                                    </div>
                                                </div>

                                                <div class=""form-group row"" id=""divQuestion3"">
                                                    <label class=""control-label col-md-3 col-sm-3 "">Your Mean Value 4</label>
                                                    <div class=""col-md-9 col-sm-9 "">
                                                        ");
            EndContext();
            BeginContext(7615, 228, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("select", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0faf11ae7d6da9d76d6e4e2a685bf64d0d8faaeb25539", async() => {
                BeginContext(7676, 62, true);
                WriteLiteral("\r\n                                                            ");
                EndContext();
                BeginContext(7738, 38, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0faf11ae7d6da9d76d6e4e2a685bf64d0d8faaeb25986", async() => {
                    BeginContext(7755, 12, true);
                    WriteLiteral("Choose Value");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_7.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_7);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(7776, 58, true);
                WriteLiteral("\r\n                                                        ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.SelectTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_9);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
#line 134 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\FireRuleBase\FireRuleSetup.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.Value2);

#line default
#line hidden
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(7843, 828, true);
            WriteLiteral(@"
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>


                                    <div class=""form-group row"">
                                        <label class=""control-label col-md-3 col-sm-3"" style=""font-weight:700;"">Perform the following action</label>
                                    </div>

                                    <div class=""form-group row"">
                                        <label class=""control-label col-md-3 col-sm-3 "">Priority Level</label>
                                        <div class=""col-md-9 col-sm-9 "">
                                            ");
            EndContext();
            BeginContext(8672, 161, false);
#line 153 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\FireRuleBase\FireRuleSetup.cshtml"
                                       Write(Html.DropDownListFor(model => model.Priority, ViewBag.PriorityList as IEnumerable<SelectListItem>, "Priority", new { @class = "form-control ddlMultipleSelect" }));

#line default
#line hidden
            EndContext();
            BeginContext(8833, 390, true);
            WriteLiteral(@"
                                        </div>
                                    </div>

                                    <div class=""form-group row"">
                                        <label class=""control-label col-md-3 col-sm-3 "">Your Actions</label>
                                        <div class=""col-md-9 col-sm-9 "">
                                            ");
            EndContext();
            BeginContext(9224, 154, false);
#line 160 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\FireRuleBase\FireRuleSetup.cshtml"
                                       Write(Html.DropDownListFor(model => model.Action, ViewBag.lstRuleActions as IEnumerable<SelectListItem>, "Your Action", new { @class = "form-control select2" }));

#line default
#line hidden
            EndContext();
            BeginContext(9378, 176, true);
            WriteLiteral("\r\n                                        </div>\r\n                                    </div>\r\n\r\n                                </div>\r\n\r\n\r\n                            </div>\r\n");
            EndContext();
#line 168 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\FireRuleBase\FireRuleSetup.cshtml"
                        }

#line default
#line hidden
            BeginContext(9581, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 170 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\FireRuleBase\FireRuleSetup.cshtml"
                         if (ViewBag.lstRules != null)
                        {

#line default
#line hidden
            BeginContext(9666, 929, true);
            WriteLiteral(@"                            <div class=""card-box table-responsive"">
                                <table id=""datatable-responsive"" class=""table table-striped table-bordered dt-responsive nowrap"" cellspacing=""0"" width=""100%"">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Preference1 Name</th>
                                            <th>Preference1 Value</th>
                                            <th>Preference2 Name</th>
                                            <th>Preference2 Value</th>
                                            <th>Priority Level</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
");
            EndContext();
#line 186 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\FireRuleBase\FireRuleSetup.cshtml"
                                         foreach (var rule in (List<Rules>)ViewBag.lstRules)
                                        {

#line default
#line hidden
            BeginContext(10732, 227, true);
            WriteLiteral("                                            <tr>\r\n                                               \r\n                                                <td style=\"width:50px;\">\r\n                                                    <a");
            EndContext();
            BeginWriteAttribute("href", " href=\'", 10959, "\'", 11031, 1);
#line 191 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\FireRuleBase\FireRuleSetup.cshtml"
WriteAttributeValue("", 10966, Url.Action("DeleteRule", "FireRuleBase", new { @Id = @rule.Id }), 10966, 65, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(11032, 292, true);
            WriteLiteral(@">
                                                        <span class=""fa fa-trash ayascolor""></span>
                                                    </a>
                                                </td>

                                                <td style=""width:100px;"">");
            EndContext();
            BeginContext(11325, 21, false);
#line 196 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\FireRuleBase\FireRuleSetup.cshtml"
                                                                    Write(rule.Preference1_Name);

#line default
#line hidden
            EndContext();
            BeginContext(11346, 80, true);
            WriteLiteral("</td>\r\n                                                <td style=\"width:300px;\">");
            EndContext();
            BeginContext(11427, 11, false);
#line 197 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\FireRuleBase\FireRuleSetup.cshtml"
                                                                    Write(rule.Value1);

#line default
#line hidden
            EndContext();
            BeginContext(11438, 80, true);
            WriteLiteral("</td>\r\n                                                <td style=\"width:100px;\">");
            EndContext();
            BeginContext(11519, 21, false);
#line 198 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\FireRuleBase\FireRuleSetup.cshtml"
                                                                    Write(rule.Preference2_Name);

#line default
#line hidden
            EndContext();
            BeginContext(11540, 80, true);
            WriteLiteral("</td>\r\n                                                <td style=\"width:300px;\">");
            EndContext();
            BeginContext(11621, 11, false);
#line 199 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\FireRuleBase\FireRuleSetup.cshtml"
                                                                    Write(rule.Value2);

#line default
#line hidden
            EndContext();
            BeginContext(11632, 80, true);
            WriteLiteral("</td>\r\n                                                <td style=\"width:100px;\">");
            EndContext();
            BeginContext(11713, 18, false);
#line 200 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\FireRuleBase\FireRuleSetup.cshtml"
                                                                    Write(rule.PriorityLevel);

#line default
#line hidden
            EndContext();
            BeginContext(11731, 80, true);
            WriteLiteral("</td>\r\n                                                <td style=\"width:300px;\">");
            EndContext();
            BeginContext(11812, 11, false);
#line 201 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\FireRuleBase\FireRuleSetup.cshtml"
                                                                    Write(rule.Action);

#line default
#line hidden
            EndContext();
            BeginContext(11823, 58, true);
            WriteLiteral("</td>\r\n                                            </tr>\r\n");
            EndContext();
#line 203 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\FireRuleBase\FireRuleSetup.cshtml"
                                        }

#line default
#line hidden
            BeginContext(11924, 128, true);
            WriteLiteral("\r\n                                    </tbody>\r\n                                </table>\r\n\r\n                            </div>\r\n");
            EndContext();
#line 209 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\FireRuleBase\FireRuleSetup.cshtml"

                        }

#line default
#line hidden
            BeginContext(12081, 114, true);
            WriteLiteral("\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<RuleViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
