#pragma checksum "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseDashboard.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "b80b3892afc056abc88f551fc61e2971296b8a38"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_SMS_RenewalCaseDashboard), @"mvc.1.0.view", @"/Views/SMS/RenewalCaseDashboard.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/SMS/RenewalCaseDashboard.cshtml", typeof(AspNetCore.Views_SMS_RenewalCaseDashboard))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\_ViewImports.cshtml"
using AYASOMPO_SMSGatewayV2;

#line default
#line hidden
#line 2 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\_ViewImports.cshtml"
using AYASOMPO_SMSGatewayV2.Models;

#line default
#line hidden
#line 1 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseDashboard.cshtml"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b80b3892afc056abc88f551fc61e2971296b8a38", @"/Views/SMS/RenewalCaseDashboard.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"62aa250c0e39e8ffc033415f13860b4d515bb115", @"/Views/_ViewImports.cshtml")]
    public class Views_SMS_RenewalCaseDashboard : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(84, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 4 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseDashboard.cshtml"
  
    ViewData["Title"] = "RenewalCaseDashboard";
    Layout = "~/Views/Shared/_ManageLayout.cshtml";

#line default
#line hidden
            BeginContext(195, 62, true);
            WriteLiteral("\r\n<div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n        <div");
            EndContext();
            BeginWriteAttribute("class", " class=\"", 257, "\"", 265, 0);
            EndWriteAttribute();
            BeginContext(266, 40, true);
            WriteLiteral(">\r\n            <div class=\"x_content\">\r\n");
            EndContext();
#line 13 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseDashboard.cshtml"
                 if (HttpContextAccessor.HttpContext.Session.GetString("roleName") == "OP" || HttpContextAccessor.HttpContext.Session.GetString("roleName") == "Admin" || HttpContextAccessor.HttpContext.Session.GetString("roleName") == "CS") //SystemAdministrator
                {

#line default
#line hidden
            BeginContext(589, 1437, true);
            WriteLiteral(@"                    <div class=""row"">

                        <div class=""animated flipInY col-lg-3 col-md-3 col-sm-6"">
                            <div class=""tile-stats"">
                                <div class=""icon"">

                                    <i class=""fa fa-caret-square-o-right""></i>
                                </div>
                                <div class=""count"">179</div>

                                <h3>Data Sync</h3>
                                <p>Renewal Notice data sync via OP team</p>
                                <a href=""#"" class=""form-control btn btn-info"" style=""margin-top:5px;""> Detail </a>
                            </div>
                        </div>
                        <div class=""animated flipInY col-lg-3 col-md-3 col-sm-6  "">
                            <div class=""tile-stats"">
                                <div class=""icon"">
                                    <i class=""fa fa-caret-square-o-right""></i>
                        ");
            WriteLiteral(@"        </div>
                                <div class=""count"">100</div>

                                <h3>Renewal Notice </h3>
                                <p>Monthly Renewal Notice</p>
                                <a href=""#"" class=""form-control btn btn-info"" style=""margin-top:5px;""> Detail </a>
                            </div>
                        </div>
                    </div>
");
            EndContext();
#line 43 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseDashboard.cshtml"
                }
                else
                {

#line default
#line hidden
            BeginContext(2086, 636, true);
            WriteLiteral(@"                    <div class=""animated flipInY col-lg-3 col-md-3 col-sm-6  "">
                        <div class=""tile-stats"">
                            <div class=""icon"">
                                <i class=""fa fa-caret-square-o-right""></i>
                            </div>
                            <div class=""count"">100</div>

                            <h3>Renewal Notice </h3>
                            <p>Monthly Renewal Notice</p>
                            <a href=""#"" class=""form-control btn btn-info"" style=""margin-top:5px;""> Detail </a>
                        </div>
                    </div>
");
            EndContext();
#line 58 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseDashboard.cshtml"
                }

#line default
#line hidden
            BeginContext(2741, 58, true);
            WriteLiteral("            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public IHttpContextAccessor HttpContextAccessor { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
