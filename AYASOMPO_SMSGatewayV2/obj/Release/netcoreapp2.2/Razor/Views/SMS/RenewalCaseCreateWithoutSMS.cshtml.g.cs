#pragma checksum "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "b27386014a0bb2da8ee5544b156a12fc713f8127"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_SMS_RenewalCaseCreateWithoutSMS), @"mvc.1.0.view", @"/Views/SMS/RenewalCaseCreateWithoutSMS.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/SMS/RenewalCaseCreateWithoutSMS.cshtml", typeof(AspNetCore.Views_SMS_RenewalCaseCreateWithoutSMS))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\_ViewImports.cshtml"
using AYASOMPO_SMSGatewayV2;

#line default
#line hidden
#line 2 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\_ViewImports.cshtml"
using AYASOMPO_SMSGatewayV2.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b27386014a0bb2da8ee5544b156a12fc713f8127", @"/Views/SMS/RenewalCaseCreateWithoutSMS.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"62aa250c0e39e8ffc033415f13860b4d515bb115", @"/Views/_ViewImports.cshtml")]
    public class Views_SMS_RenewalCaseCreateWithoutSMS : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 2 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
  
    ViewData["Title"] = "RenewalCaseCreateWithoutSMS";
    Layout = "~/Views/Shared/_ManageLayout.cshtml";

#line default
#line hidden
            BeginContext(118, 690, true);
            WriteLiteral(@"
<div class=""row"">
    <div class=""col-md-12"">
        <div class=""x_panel"">
            <div class=""x_title"">
                <h2>Create Renewal Case By OP</h2>
                <ul class=""nav navbar-right panel_toolbox"">
                    <li>
                        <a class=""collapse-link""><i class=""fa fa-chevron-up""></i></a>
                    </li>
                    <li>
                        <a class=""close-link""><i class=""fa fa-close""></i></a>
                    </li>
                </ul>
                <div class=""clearfix""></div>
            </div>

            <div class=""x_content"">
                <br />
                <div class=""row"">

");
            EndContext();
#line 27 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                     if (TempData["Message"] != null)
                    {

#line default
#line hidden
            BeginContext(886, 237, true);
            WriteLiteral("                        <script type=\"text/javascript\">\r\n                            window.onload = function () {\r\n                                $(\'#divMsg\').show();\r\n                            };\r\n                        </script>\r\n");
            EndContext();
#line 34 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                    }

#line default
#line hidden
            BeginContext(1146, 374, true);
            WriteLiteral(@"
                    <div id=""divMsg"" class=""alert alert-info col-md-12"" style=""display:none;"">
                        <a href=""#"" class=""close"" data-dismiss=""alert"" aria-label=""close"">
                            &times;
                        </a>

                        <i class=""fa fa-info-circle"" style=""font-size:16px;""></i>
                        <strong>");
            EndContext();
            BeginContext(1521, 19, false);
#line 42 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                           Write(TempData["Message"]);

#line default
#line hidden
            EndContext();
            BeginContext(1540, 69, true);
            WriteLiteral("</strong>\r\n                    </div>\r\n\r\n                </div>\r\n\r\n\r\n");
            EndContext();
#line 48 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                 using (Html.BeginForm("CreateCustomRenewalCase", "SMS", FormMethod.Post, new { enctype = "multipart/form-data" }))
                {

#line default
#line hidden
            BeginContext(1761, 1057, true);
            WriteLiteral(@"                <div class=""form-horizontal form-label-left"">
                    <div class=""col-md-6"">
                        <div class=""col-md-9 col-sm-9 "">
                            <div class=""checkbox"">
                                <label>
                                    <input type=""checkbox"" id=""chkSentSMS"" value=""true"" name=""chkSentSMS""> Sent SMS
                                </label>
                            </div>
                        </div>

                    </div>

                    <div class=""clearfix""></div>
                    <div class=""ln_solid""></div>

                    <div class=""form-group"">
                        <div class=""col-md-9 col-sm-9  offset-md-3"">
                            <button type=""submit"" class=""btn btn-default btnayas-style"">
                                <i class=""fa fa-upload""></i>
                                Create Case
                            </button>

                        </div>
                   ");
            WriteLiteral(" </div>\r\n                </div>\r\n");
            EndContext();
            BeginContext(2820, 61, true);
            WriteLiteral("                    <div class=\"card-box table-responsive\">\r\n");
            EndContext();
#line 77 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                         if (ViewBag.lstTempRenewalCaseCreate != null)
                        {

#line default
#line hidden
            BeginContext(2980, 1749, true);
            WriteLiteral(@"                            <table id=""tblOP"" class=""table table-striped table-bordered dt-responsive datatable-responsive nowrap"" cellspacing=""0"" width=""100%"">
                                <thead>
                                    <tr>
                                        <th>Phone Number</th>
                                        <th>Customer Name</th>
                                        <th>Invoice No</th>
                                        <th>Risk Name</th>
                                        <th>Expired Date</th>

                                        <th>Renewal Premium</th>
                                        <th>Product</th>
                                        <th>From Date</th>
                                        <th>To Date</th>
                                        <th>Link</th>

                                        <th>Customer Code</th>
                                        <th>Month</th>
                                        <th>Per");
            WriteLiteral(@"sonInCharge</th>
                                        <th>Responsiable Party</th>
                                        <th>Policy Number</th>

                                        <th>Branch</th>
                                        <th>Intermediary Type</th>
                                        <th>Agent Code</th>
                                        <th>Agent Name</th>
                                        <th>Account Code</th>

                                        <th>Authorized Code</th>
                                        <th>Authorized Name</th>

                                    </tr>
                                </thead>
                                <tbody>
");
            EndContext();
#line 112 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                                     foreach (var motor in (List<TempRenewalCaseCreate>)ViewBag.lstTempRenewalCaseCreate)
                                    {

#line default
#line hidden
            BeginContext(4891, 107, true);
            WriteLiteral("                                    <tr>\r\n                                        <td style=\"width:100px;\">");
            EndContext();
            BeginContext(4999, 17, false);
#line 115 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                                                            Write(motor.PhoneNumber);

#line default
#line hidden
            EndContext();
            BeginContext(5016, 72, true);
            WriteLiteral("</td>\r\n                                        <td style=\"width:100px;\">");
            EndContext();
            BeginContext(5089, 18, false);
#line 116 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                                                            Write(motor.CustomerName);

#line default
#line hidden
            EndContext();
            BeginContext(5107, 72, true);
            WriteLiteral("</td>\r\n                                        <td style=\"width:100px;\">");
            EndContext();
            BeginContext(5180, 15, false);
#line 117 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                                                            Write(motor.InvoiceNo);

#line default
#line hidden
            EndContext();
            BeginContext(5195, 72, true);
            WriteLiteral("</td>\r\n                                        <td style=\"width:100px;\">");
            EndContext();
            BeginContext(5268, 14, false);
#line 118 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                                                            Write(motor.RiskName);

#line default
#line hidden
            EndContext();
            BeginContext(5282, 72, true);
            WriteLiteral("</td>\r\n                                        <td style=\"width:300px;\">");
            EndContext();
            BeginContext(5355, 49, false);
#line 119 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                                                            Write(String.Format("{0:MMM/dd/yy}", @motor.ExpiryDate));

#line default
#line hidden
            EndContext();
            BeginContext(5404, 74, true);
            WriteLiteral("</td>\r\n\r\n                                        <td style=\"width:100px;\">");
            EndContext();
            BeginContext(5479, 20, false);
#line 121 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                                                            Write(motor.RenewalPremium);

#line default
#line hidden
            EndContext();
            BeginContext(5499, 72, true);
            WriteLiteral("</td>\r\n                                        <td style=\"width:300px;\">");
            EndContext();
            BeginContext(5572, 13, false);
#line 122 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                                                            Write(motor.Product);

#line default
#line hidden
            EndContext();
            BeginContext(5585, 72, true);
            WriteLiteral("</td>\r\n                                        <td style=\"width:300px;\">");
            EndContext();
            BeginContext(5658, 47, false);
#line 123 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                                                            Write(String.Format("{0:MMM/dd/yy}", @motor.FromDate));

#line default
#line hidden
            EndContext();
            BeginContext(5705, 72, true);
            WriteLiteral("</td>\r\n                                        <td style=\"width:300px;\">");
            EndContext();
            BeginContext(5778, 45, false);
#line 124 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                                                            Write(String.Format("{0:MMM/dd/yy}", @motor.ToDate));

#line default
#line hidden
            EndContext();
            BeginContext(5823, 72, true);
            WriteLiteral("</td>\r\n                                        <td style=\"width:100px;\">");
            EndContext();
            BeginContext(5896, 10, false);
#line 125 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                                                            Write(motor.Link);

#line default
#line hidden
            EndContext();
            BeginContext(5906, 74, true);
            WriteLiteral("</td>\r\n\r\n                                        <td style=\"width:300px;\">");
            EndContext();
            BeginContext(5981, 13, false);
#line 127 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                                                            Write(motor.CusCode);

#line default
#line hidden
            EndContext();
            BeginContext(5994, 72, true);
            WriteLiteral("</td>\r\n                                        <td style=\"width:300px;\">");
            EndContext();
            BeginContext(6067, 11, false);
#line 128 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                                                            Write(motor.Month);

#line default
#line hidden
            EndContext();
            BeginContext(6078, 72, true);
            WriteLiteral("</td>\r\n                                        <td style=\"width:300px;\">");
            EndContext();
            BeginContext(6151, 20, false);
#line 129 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                                                            Write(motor.PersonInCharge);

#line default
#line hidden
            EndContext();
            BeginContext(6171, 72, true);
            WriteLiteral("</td>\r\n                                        <td style=\"width:300px;\">");
            EndContext();
            BeginContext(6244, 22, false);
#line 130 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                                                            Write(motor.ResponsibleParty);

#line default
#line hidden
            EndContext();
            BeginContext(6266, 72, true);
            WriteLiteral("</td>\r\n                                        <td style=\"width:300px;\">");
            EndContext();
            BeginContext(6339, 14, false);
#line 131 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                                                            Write(motor.PolicyNo);

#line default
#line hidden
            EndContext();
            BeginContext(6353, 74, true);
            WriteLiteral("</td>\r\n\r\n                                        <td style=\"width:100px;\">");
            EndContext();
            BeginContext(6428, 12, false);
#line 133 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                                                            Write(motor.Branch);

#line default
#line hidden
            EndContext();
            BeginContext(6440, 72, true);
            WriteLiteral("</td>\r\n                                        <td style=\"width:300px;\">");
            EndContext();
            BeginContext(6513, 22, false);
#line 134 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                                                            Write(motor.IntermediaryType);

#line default
#line hidden
            EndContext();
            BeginContext(6535, 72, true);
            WriteLiteral("</td>\r\n                                        <td style=\"width:300px;\">");
            EndContext();
            BeginContext(6608, 15, false);
#line 135 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                                                            Write(motor.AgentCode);

#line default
#line hidden
            EndContext();
            BeginContext(6623, 72, true);
            WriteLiteral("</td>\r\n                                        <td style=\"width:300px;\">");
            EndContext();
            BeginContext(6696, 15, false);
#line 136 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                                                            Write(motor.AgentName);

#line default
#line hidden
            EndContext();
            BeginContext(6711, 72, true);
            WriteLiteral("</td>\r\n                                        <td style=\"width:300px;\">");
            EndContext();
            BeginContext(6784, 17, false);
#line 137 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                                                            Write(motor.AccountCode);

#line default
#line hidden
            EndContext();
            BeginContext(6801, 74, true);
            WriteLiteral("</td>\r\n\r\n                                        <td style=\"width:300px;\">");
            EndContext();
            BeginContext(6876, 20, false);
#line 139 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                                                            Write(motor.AuthorizedCode);

#line default
#line hidden
            EndContext();
            BeginContext(6896, 72, true);
            WriteLiteral("</td>\r\n                                        <td style=\"width:100px;\">");
            EndContext();
            BeginContext(6969, 20, false);
#line 140 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                                                            Write(motor.AuthorizedName);

#line default
#line hidden
            EndContext();
            BeginContext(6989, 50, true);
            WriteLiteral("</td>\r\n                                    </tr>\r\n");
            EndContext();
#line 142 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                                    }

#line default
#line hidden
            BeginContext(7078, 82, true);
            WriteLiteral("\r\n                                </tbody>\r\n                            </table>\r\n");
            EndContext();
#line 146 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                        }

#line default
#line hidden
            BeginContext(7187, 28, true);
            WriteLiteral("                    </div>\r\n");
            EndContext();
#line 148 "D:\Development\SMSGateWay V2\1. smsgateway2022\AYASOMPO_SMSGatewayV2\Views\SMS\RenewalCaseCreateWithoutSMS.cshtml"
                 }

#line default
#line hidden
            BeginContext(7235, 82, true);
            WriteLiteral("\r\n                    </div>\r\n                </div>\r\n    </div>\r\n\r\n\r\n</div>\r\n\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
