﻿$('document').ready(function () {

    var reqNumberofFireExtinguisher = 0;
    //$('#btnCustomerSearch').click(function () {
    //    var cusCode = $('#txtCustomerCode').val();
    //    if (cusCode !== null || cusCode !== '') {
            
    //        const xhr = new XMLHttpRequest();
    //        xhr.open("POST", "https://auth.ayasompo.com/users/authenticate");
    //        xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
    //        const body = JSON.stringify({
    //            username: "hninly",
    //            password: "12345678"
    //        });

    //        xhr.send(body);

    //        xhr.onload = () => {
    //            if (xhr.readyState == 4 && xhr.status == 200) {
    //                console.log(JSON.parse(xhr.responseText));
    //                var jsonData = JSON.parse(xhr.responseText);
    //                if (jsonData.access_token != null) {

    //                    // Call GetCustomer by Customer Code

    //                    var token = jsonData.access_token;
    //                    const xhr = new XMLHttpRequest();
    //                    xhr.open("POST", "https://uatecom.ayasompo.com/customer/GetCustomer");
    //                    xhr.setRequestHeader("Authorization", "Bearer " + token);
    //                    xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
    //                    const body = JSON.stringify({
    //                        customerCode: cusCode
    //                    });

    //                    xhr.send(body);

    //                    xhr.onload = () => {
    //                        if (xhr.readyState == 4 && xhr.status == 200) {
    //                            console.log(JSON.parse(xhr.responseText));
    //                            var cusData = JSON.parse(xhr.responseText);
    //                            $('#txtInsuredName').val(cusData.SURNAME);
    //                            var mailingaddress = "";
    //                            var cusAddressList = cusData.CUSTOMERADDRESSES.filter(function (el) {
    //                                return el.ADDEFAULTADDRESS === 'Y'
    //                            });
                                
    //                            var wardCode = cusAddressList[0]["ADPOSTALCODE"];
    //                            mailingaddress = cusAddressList[0]["ADLOCATIONDESCRIPTION"];
                                
    //                            // Call Customer Address Detail
    //                            if (wardCode !== null) {
    //                                var apiUrl = "https://uatecom.ayasompo.com:5005/api/UnderWritting/GetLocationDetailByWardCode/" + wardCode;
    //                                //request.open('GET', 'https://uatecom.ayasompo.com:5005/api/UnderWritting/GetLocationDetailByWardCode/MMR02700001');
    //                                var req = new XMLHttpRequest();
    //                                req.open("GET", apiUrl, true);
    //                                req.setRequestHeader('Content-Type', 'application/json');
    //                                req.setRequestHeader("Accept", "*/*");
    //                                req.setRequestHeader("Authorization", "Bearer " + token);
                                    
    //                                req.send();

    //                                req.onreadystatechange = function () {
    //                                    if (req.readyState == 4 && req.status == 200) {
    //                                        debugger;
    //                                        console.log(JSON.parse(req.responseText));
    //                                        var addressData = JSON.parse(req.responseText);
    //                                        mailingaddress += "," + addressData.town_description + "," + addressData.township_description + "," + addressData.district_description + "," + addressData.region_description;
                                           
    //                                        $('#txtInsuredMailAddress').val(mailingaddress);
    //                                        $('#txtLocationAddress').val(mailingaddress);
                                            
    //                                    }
    //                                    else {
    //                                        console.log(`Error: ${req.status}`);
    //                                    }
    //                                };
    //                            }
    //                        } else {
    //                            console.log(`Error: ${xhr.status}`);
    //                        }
    //                    };
    //                }
    //            } else {
    //                console.log(`Error: ${xhr.status}`);
    //                return null;
    //            }
    //        };
    //    }
    //    else {
    //        alert("Please enter Customer Code!");
    //    }
    //});

    $('#btnCustomerSearch').click(function () {
        var cusCode = $('#txtCustomerCode').val();
        if (cusCode !== null || cusCode !== '') {
            
            $.ajax(
                {
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "/FireRuleBase/GetCustomerInfo",
                    data: JSON.stringify(cusCode),
                    dataType: "Json",
                    success: function (data) {
                        if (data != null) {
                            alert(data['name']);
                            $('#txtInsuredName').val(data['name']);
                            $('#txtInsuredMailAddress').val(data['mailingaddress']);                            
                        }
                    }
                }
            );

        }
    });

    $('#btnFireRunRule').click(function () {
        var riskcount = $("#hdfRiskCount").val();
        var fireUWViewModel = {};
        fireUWViewModel.riskDetailInfos = [];
        fireUWViewModel.class = "FI";
        fireUWViewModel.productCode = $("#ProductCode").val();
        //alert("Class " + fireUWViewModel.class);
        //alert("Product Code " + fireUWViewModel.productCode);

        var riskInfos = [];
        if (riskcount > 0) {
            for (var num = 1; num <= riskcount; num++) {
                var riskdetail = {};
                riskdetail.riskName = $("#txtRiskName_" + num).val();

                riskInfos.push(riskdetail);
                //alert(riskdetail.riskName); 
            }
            fireUWViewModel.riskDetailInfos.push(riskInfos);
        }
    });


    $('#txtTotalSI').change(function () {
        var tsi = $('#txtTotalSI').val();
        let tsival = parseFloat(tsi.replace(/,/g, ''));

        if (parseInt(tsival) > 500000000) {
            $('#mdlMsg').modal('show');
            $('#pMsg').html("This should be register as a IAR product.");

        }
    });

    $('.btnRiskDetailAdd').on('click', function () {
        var $BOX_PANEL = $(this).closest('.x_panel'),
            $ICON = $(this).find('i'),
            $BOX_CONTENT = $BOX_PANEL.find('.x_content');

        // fix for some div with hardcoded fix class
        if ($BOX_PANEL.attr('style')) {
            $BOX_CONTENT.slideToggle(200, function () {
                $BOX_PANEL.removeAttr('style');
            });
        } else {
            $BOX_CONTENT.slideToggle(200);
            $BOX_PANEL.css('height', 'auto');
        }

        $ICON.toggleClass('fa-chevron-up fa-chevron-down');
    });

});

function calculateProposePAE(id) {
    var proposeSI = document.getElementById(id).value;
    var index = id.substr(id.length - 1);
    var totalAreaOfBuilding = document.getElementById("txtTotalAreaofBuilding_" + index).value;
    let tproposeSI = parseFloat(proposeSI.replace(/,/g, ''));

    if (totalAreaOfBuilding == null || totalAreaOfBuilding == "NaN" || totalAreaOfBuilding == "underfined" || totalAreaOfBuilding == "") {
        alert("Please insert total area of building!");
        document.getElementById(id).value = "";
        return false;
    }

    if (proposeSI == null || proposeSI == "NaN" || proposeSI == "underfined" || proposeSI == "") {
        alert("Please insert propose SI!");
        
        return false;
    }

    if (totalAreaOfBuilding != null) {
        var proposePAE = tproposeSI / totalAreaOfBuilding;
       
        $("#txtProposePAE_" + index).val(Math.round(proposePAE));
       
    }
  
}
function getFileName(elm) {
    var id = $(elm).closest('.checkboxrow').find('input:checkbox').attr('id');
    var el = document.getElementById(id);
    el.setAttribute("checked", "checked").click();
}

function addInventory_click(clicked_id) {
    debugger;
    var index = clicked_id.substr(clicked_id.length - 1);
    

    //Reference the Name and Country TextBoxes.
    var ddlInventory = $("#ddlInventory_" + index).val();
    var txtDeclaredAmt = $("#txtDeclaredAmt_" + index).val();
  
    if (ddlInventory == null || ddlInventory == 'undefined' || ddlInventory == "") {
        alert('Data should not be empty!');
    }
    if (txtDeclaredAmt == null || txtDeclaredAmt == 'undefined' || txtDeclaredAmt == "") {
        alert('Data should not be empty!');
    }

    //Get the reference of the Table's TBODY element.
    var tBody = $("#tblInventoryInfo_" + index +" > TBODY")[0];
    //var rowId = uuidv4();
    //Add Row.
    var row = tBody.insertRow(-1);

    //Add Weight of Fire Extinguisher cell.
    var cell = $(row.insertCell(-1));
    cell.html(ddlInventory);

    //Add Type of Fire Extinguisher cell.
    cell = $(row.insertCell(-1));
    cell.html(txtDeclaredAmt);

   
    //Add Button cell.
    cell = $(row.insertCell(-1));
    var btnRemove = $("<button />");
    btnRemove.attr("type", "button");
    btnRemove.attr("id", "btnInventoryRemove_" + index);
    btnRemove.attr("class", "btn");    
    btnRemove.attr("onclick", "Remove(this);");
    //btnRemove.addClass("fas fa-remove"); 

    var icon = $("<i />");
    icon.attr("class", "fa fa-remove");
    btnRemove.append(icon);
    cell.append(btnRemove);

    //Clear the TextBoxes.
    $("#ddlInventory_" + index).val("");
    $("#txtDeclaredAmt_" + index).val("");
   
}

function Remove(button) {
    var id = button.id;
    var index = id.substr(id.length - 1);
    //Determine the reference of the Row using the Button.
    var row = $(button).closest("TR");
    var name = $("TD", row).eq(0).html();

    var table = $("#tblInventoryInfo_" + index)[0];
    //Delete the Table row using it's Index.
    table.deleteRow(row[0].rowIndex);

}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function getWardData() {
    debugger;
    //const xhr = new XMLHttpRequest();
    //xhr.open("GET", "https://uatecom.ayasompo.com:5005/api/UnderWritting/GetLocationDetailByWardCode/MMR02700001");
    //xhr.setRequestHeader("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjgzMGQwMmZmLWI3NzUtNDJiNy05MmJjLWVhMDI4YjU4MTZiYSIsIm5iZiI6MTY4MzE4NjQ5OSwiZXhwIjoxNjgzMTk3Mjk5LCJpYXQiOjE2ODMxODY0OTl9.nhGsHlO8s6HXzCd2v6Fz1TvIerOo8fnl5cADeHw3FPs");
    //xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
   
    //xhr.send();

    //xhr.onload = () => {
    //    if (xhr.readyState == 4 && xhr.status == 200) {
    //        console.log(JSON.parse(xhr.responseText));
    //        var addressData = JSON.parse(xhr.responseText);
           
    //        alert(addressData.town_description);
    //    } else {
    //        console.log(`Error: ${xhr.status}`);
    //    }
    //};

    //var request = new XMLHttpRequest();
    var apiUrl = "https://uatecom.ayasompo.com:5005/api/UnderWritting/GetLocationDetailByWardCode/MMR02700001";
    //request.open('GET', 'https://uatecom.ayasompo.com:5005/api/UnderWritting/GetLocationDetailByWardCode/MMR02700001');
    var req = new XMLHttpRequest();
    req.open("GET", apiUrl, true);
    req.setRequestHeader('Content-Type', 'application/json');
    req.setRequestHeader("Accept", "*/*");
    req.setRequestHeader("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjgzMGQwMmZmLWI3NzUtNDJiNy05MmJjLWVhMDI4YjU4MTZiYSIsIm5iZiI6MTY4MzE5NDM0MywiZXhwIjoxNjgzMjA1MTQzLCJpYXQiOjE2ODMxOTQzNDN9.NyE7wZgis4wkkO1fwEMi1JFnVv3jGZzd1PyNqTDejYc");

    req.send();

    req.onreadystatechange = function () {
        if (req.readyState == 4 && req.status == 200) {
            debugger;
            console.log(JSON.parse(req.responseText));
            var addressData = JSON.parse(req.responseText);
            alert(addressData.town_description);
        }
        else {
            alert(req.readyState);
        }
    };

   
    
}

function calculateSqlMeter(id) {
    
    var areaOftheBuilding = document.getElementById(id).value;
   
    var index = id.substr(id.length - 1);

    var sqlMeter = 0.092903;
    var sqlMeterresult = 0;
    var requiredFireExtenguisher = 0;
    if (areaOftheBuilding != null || areaOftheBuilding != '') {
        sqlMeterresult = sqlMeter * areaOftheBuilding;
        requiredFireExtenguisher = (sqlMeterresult - 150) / 200 + 2;

        reqNumberofFireExtinguisher = Math.round(requiredFireExtenguisher);

        let dd = document.getElementById("ddlRequiredNumberofFireExtinguishers_" + index);

        for (var i = 0; i < dd.options.length; i++) {
            if (dd.options[i].text == reqNumberofFireExtinguisher) {
                dd.options[i].setAttribute('selected', true);
                return;
            }
        }


    }
    else {
        alert("Please enter total area of the building!");
        return false;
    }
}

function OnGradeChanged(id) {
    //alert(id);
    //var index = id.substr(id.length - 1);
    //alert(index);

    //let dd = document.getElementById("ddlNumberofABCTypeExtinguisher_" + index);
    //alert(dd);
    //alert(dd.options[index].text);
}

function GetBuildingClass(id) {
    var index = id.substr(id.length - 1);
   
    var roofVal = document.getElementById("ddlRoof_" + index).value;
    var wallVal = document.getElementById("ddlWalls_" + index).value;
    var columnVal = document.getElementById("ddlColumn_" + index).value;
    var floorVal = document.getElementById("ddlFloor_" + index).value;
   
    $.ajax(
        {
            url: "/FireRuleBase/GetBuildingClass",
            data: { roof: roofVal, wall: wallVal, column: columnVal, floor: floorVal },
            contentType: "application/json; charset=utf-8",
            type: "GET",
            success: function (data) {
                if (data != null) {
                    let dd = document.getElementById("ddlTypeOfConstruction_" + index);         
                    for (var i = 0; i < dd.options.length; i++) {
                        if (dd.options[i].value == data) {
                            dd.options[i].setAttribute('selected', true);
                            return;
                        }
                    }
                }               
            }
        }    
    );

}

function CalculatePAE(id) {
    var index = id.substr(id.length - 1);
    var totalAreaOfBuilding = document.getElementById("txtTotalAreaofBuilding_" + index).value;
    var typeOfConstruction = document.getElementById("ddlTypeOfConstruction_" + index).value;
    var lifeofBuilding = document.getElementById("txtLifeOfBuilding_" + index).value;
    if (totalAreaOfBuilding == null || totalAreaOfBuilding == 'underfined') {
        alert("Please select the roof, column, wall and floor value!");
        return false;
    }

    if (typeOfConstruction == null || typeOfConstruction == 'underfined' || typeOfConstruction == "") {
        alert("Please select the roof, column, wall and floor value!");

        return false;
    }
    if (lifeofBuilding == null || lifeofBuilding == 'underfined' || lifeofBuilding == "") {
        alert("Please select the life of building value!");
        return false;
    }

    var tboby = $('#tblLimitedPAE_' + index + ' tbody');
    tboby.empty();

    
    $.ajax(
        {
            url: "/FireRuleBase/CalculatePAE",
            data: { classofBuilding: typeOfConstruction, lifetimeofBuilding: lifeofBuilding, totalAreaOfBuilding: totalAreaOfBuilding },
            contentType: "application/json; charset=utf-8",
            type: "GET",
            success: function (data) {
                if (data != null) {
                    var divLimitedPAE = document.getElementById('divLimitedPAE_' + index);
                    divLimitedPAE.style.display = 'block';
                    var tr = $('<tr></tr>')
                    tr.append('<td>' + data['limitedpae'] + '</td>')
                    tr.append('<td>' + data['limitedvalue'] + '</td>')
                    tboby.append(tr);

                }
            }
        }
    );

}

function GOECoordinate(id) {
    var index = id.substr(id.length - 1);
    
    $("#GeoModal").modal("show");
    var mapOptions = {
        center: new google.maps.LatLng(16.774160319891767, 96.16846909458707),
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var infoWindow = new google.maps.InfoWindow();
    var latlngbounds = new google.maps.LatLngBounds();
    var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
    google.maps.event.addListener(map, 'click', function (e) {
        // alert("Latitude: " + e.latLng.lat() + "\r\nLongitude: " + e.latLng.lng());

        document.getElementById("txtGoogleMapGEOcoordinate_" + index).value = e.latLng.lat() + "," + e.latLng.lng();
        $("#GeoModal").modal("hide");
    });
 
}

//function uploadFiles(inputId) {
//    var input = document.getElementById(inputId);
//    var files = input.files;
//    var formData = new FormData();

//    for (var i = 0; i !== files.length; i++) {
//        formData.append("files", files[i]);
//    }

//    $.ajax(
//        {
//            url: "/FireRuleBase/Upload",
//            data: formData,
//            processData: false,
//            contentType: false,
//            type: "POST",
//            success: function (data) {
//                //var count = data.length;
//                //$('#riskCount').val(count);
//                alert($('#riskCount').val());
//                alert(data.length);
//            }
//        }
//    );
//}
