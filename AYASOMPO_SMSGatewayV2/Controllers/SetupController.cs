﻿using System.Linq;
using System.Threading.Tasks;
using AYASOMPO_SMSGatewayV2.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using System.IO;
using System;

namespace AYASOMPO_SMSGatewayV2.Controllers
{
    public class SetupController : Controller
    {
        MainDBContext _db = new MainDBContext();
        private IHostingEnvironment _env;
        Common common = new Common();
        public ISession session;
        public IConfigurationRoot Configuration { get; }

        public SetupController(IHostingEnvironment env)
        {
            _env = env;
            var builder = new ConfigurationBuilder()
             .SetBasePath(Directory.GetCurrentDirectory())
             .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
        }

        #region SMS Format
        public IActionResult SMSFormat()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            ViewBag.SMSFormatList = _db.Category.OrderByDescending(d => d.Description).ToList();
            return View();
        }

        public IActionResult SaveSMSTempFormat(Category category)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            if (category.Id == Guid.Empty)
            {
                category.Id = Guid.NewGuid();
                category.Status = category.Status;
                _db.Entry(category).State = EntityState.Added;
                _db.SaveChanges();
                TempData["Message"] = "Save Successful.";
                return RedirectToAction("SMSFormat");
            }
            else
            {
                if (ModelState.IsValid)
                {
                    Category updatecategory = new Category();
                    updatecategory = _db.Category.Where(c => c.Id == category.Id).FirstOrDefault();
                    updatecategory.Description = category.Description;
                    updatecategory.MsgTemplate = category.MsgTemplate;
                    updatecategory.Status = category.Status;
                    updatecategory.RenewalFlag = category.RenewalFlag;

                    _db.Update(updatecategory);
                    _db.SaveChanges();
                    TempData["Message"] = "Update Successful.";
                    return RedirectToAction("SMSFormat");
                }
                return RedirectToAction("SMSFormat");
            }
        }
        public IActionResult EditSMSFormat(Guid Id)
        {
            try
            {
                if (!CheckLoginUser())
                    return RedirectToAction("Login", "Account");

                Category category = _db.Category.Where(r => r.Id == Id).FirstOrDefault();
                ViewBag.SMSFormatList = _db.Category.OrderByDescending(d => d.Description).ToList();
                return View("SMSFormat", category);
            }
            catch (Exception ex)
            {
                return View();
            }


        }

        #endregion

        #region Common Functions
        public bool CheckLoginUser()
        {
            if (HttpContext.Session.GetString("userId") == null)
                return false;
            return true;
        }

        #endregion

        #region UW Rule Set up 
        public IActionResult RuleSetup()
        {
            return View();
        }
        #endregion
    }
}