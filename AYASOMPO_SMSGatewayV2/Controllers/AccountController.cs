﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AYASOMPO_SMSGatewayV2.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;

namespace AYASOMPO_SMSGatewayV2.Controllers
{
    public class AccountController : Controller
    {
        MainDBContext _db = new MainDBContext();
        private IHostingEnvironment _env;
        Common common = new Common();
        public ISession session;
        public IConfigurationRoot Configuration { get; }

        public AccountController(IHostingEnvironment env)
        {
            _env = env;
            var builder = new ConfigurationBuilder()
             .SetBasePath(Directory.GetCurrentDirectory())
             .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Login()
        {
            return View();
        }

        public IActionResult Loginrulebasesystem()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(SysUser sysUser)
        {
            SysUser user = _db.SysUser.Where(u => u.Email == sysUser.Email).FirstOrDefault();
            if (user != null && user.Id != null && user.IsActive == true)
            {
                string Encodepassword = common.DecryptPassword(user.Password, user.PasswordSalt);

                HttpContext.Session.SetString("email", user.Email);
                HttpContext.Session.SetString("name", user.Name);
                HttpContext.Session.SetString("userId", Convert.ToString(user.Id));
                HttpContext.Session.SetString("roleId", Convert.ToString(user.Role));

                Role role = new Role();
                role = _db.Role.Where(r => r.RoleId == user.Role).FirstOrDefault();
                HttpContext.Session.SetString("roleName", Convert.ToString(role.RoleName));

                if (sysUser.Password == "SmsG@123" && user.FirstTimeLogin == true)
                    return RedirectToAction("ChangePasswordForFirstTimeLogin");

                else if (Encodepassword == sysUser.Password)
                    return RedirectToAction("Dashboard", "SMS");

                else
                {
                    TempData["LoginMessage"] = "Wrong password.Try again or click Forgot password to reset it.";
                    return View("Login", sysUser);
                }
            }
            else
            {
                TempData["LoginMessage"] = "You are not registerd. Please contact to administrator.";
                return View("Login", sysUser);
            }
        }

        #region User Register
        public IActionResult UserRegister(SysUser sysUser)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");
            
            PrepareData();
            return View(sysUser);
        }
        
        public IActionResult UserList()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            PrepareData();
            return View();
        }

        public IActionResult SaveUser(SysUser model)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");
            if (!ModelState.IsValid)
            {
                PrepareData();
                return RedirectToAction("UserRegister");
            }

            try
            {
                if (model.Id == Guid.Empty)
                {
                        model.Id = Guid.NewGuid();
                        model.CreatedDate = DateTime.Now;
                        model.FirstTimeLogin = true;
                        model.PasswordSalt = common.GenerateSalt(8);
                        model.Password = common.EncryptPassword("SmsG@123", model.PasswordSalt);
                        model.IsActive = model.IsActive;
                        model.DeleteFlag = model.DeleteFlag;
                        model.EditFlag = model.EditFlag;
                        model.ViewFlag = model.ViewFlag;

                        _db.Entry(model).State = EntityState.Added;
                        _db.SaveChanges();
                        TempData["Message"] = "Save Successful.";
                        return RedirectToAction("UserRegister");
                    
                }
                else
                {
                        SysUser sysUser = new SysUser();
                        sysUser = _db.SysUser.Where(u => u.Id == model.Id).FirstOrDefault();
                        sysUser.Name = model.Name;
                        sysUser.Email = model.Email;
                        sysUser.Role = model.Role;
                        sysUser.Department = model.Department;
                        sysUser.IsActive = model.IsActive;
                        sysUser.DeleteFlag = model.DeleteFlag;
                        sysUser.EditFlag = model.EditFlag;
                        sysUser.ViewFlag = model.ViewFlag;

                        _db.SysUser.Update(sysUser);
                        _db.SaveChanges();

                        TempData["Message"] = "Update Successful.";
                        return RedirectToAction("UserRegister");
                }
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Error in Saving Process.";
                return RedirectToAction("UserList");
            }
            
        }
        
        public IActionResult EditUser(Guid UserId)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            var sysUser = _db.SysUser.Where(u => u.Id == UserId).FirstOrDefault();
            PrepareData();
            return View("UserRegister", sysUser);
        }

        public IActionResult DeleteUser(Guid Id)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");
            SysUser sysUser = new SysUser();
            try
            {
                sysUser = _db.SysUser.Where(u => u.Id == Id).FirstOrDefault();
                _db.Entry(sysUser).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                _db.SaveChanges();
                PrepareData();

                TempData["Message"] = "Delete Success";
                return RedirectToAction("UserList");
            }
            catch(Exception ex)
            {
                PrepareData();
                TempData["Message"] = "Delete Fail";
                return RedirectToAction("UserList");
            }
           
        }

        public IActionResult CheckUserInfo(Guid userId)
        {
            UserInfo data = (from u in _db.SysUser
                             join r in _db.Role on u.Role equals r.RoleId
                             join d in _db.Department on u.Department equals d.Id
                             where u.Id == userId
                             orderby u.Name
                             select new UserInfo
                             {
                                 Id = u.Id,
                                 Name = u.Name,
                                 Email = u.Email,
                                 Department = d.Name,
                                 Role = r.RoleName,
                                 IsActive = u.IsActive
                             }).FirstOrDefault();
            return View(data);
        }

        public IActionResult ChangePasswordForFirstTimeLogin()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login");
            return View();
        }

        [HttpPost] //Change Password
        public IActionResult ChangePasswordForFirstTimeLogin(string npassword, string opassword, string ConfirmPassword)
        {
            string oldPassword = "";
            Guid userId = Guid.Parse(HttpContext.Session.GetString("userId"));
            if (npassword == ConfirmPassword)
            {
                var user = _db.SysUser.Where<SysUser>(u => u.Id == userId).FirstOrDefault();
                oldPassword = common.DecryptPassword(user.Password, user.PasswordSalt);
                if (opassword == npassword)
                {
                    ViewBag.Message = "System doesn't allow your password because oldPwd and  newPwd are same.";
                    return View();
                }
                else if (oldPassword == opassword)
                {
                    user.Password = common.EncryptPassword(npassword, user.PasswordSalt);
                    user.FirstTimeLogin = false;
                    _db.SaveChanges();
                    return RedirectToAction("Login");
                }
                else if (oldPassword != opassword)
                {
                    ViewBag.Message = "Your old password is wrong.";
                    return View();
                }
                else
                {
                    ViewBag.Message = "Password does not match.";
                    return View();
                }
            }
            else
            {
                ViewBag.Message = "New Password & Confirm Password does not match";
                return View();
            }
        }

        public IActionResult ResetUser(Guid Id)
        {
            try
            {
                SysUser model = _db.SysUser.Where(u => u.Id == Id).FirstOrDefault();
                model.FirstTimeLogin = true;
                model.PasswordSalt = common.GenerateSalt(8);
                model.Password = common.EncryptPassword("SmsG@123", model.PasswordSalt);
                model.IsActive = true;
                _db.Update(model);
                _db.SaveChanges();
                TempData["Message"] = "Reset Password for ' " + model.Name + " ' is successful.";
                return RedirectToAction("UserList");
            }
            catch(Exception ex)
            {
                TempData["Message"] = "Reset Password fail.";
                return RedirectToAction("UserList");
            }
           
        }
        #endregion
        
        #region Common Function
        private void PrepareData()
        {
            try
            {
                ViewBag.RoleList = new SelectList(_db.Role.OrderBy(r => r.RoleName).ToList(), "RoleId", "RoleName");
                ViewBag.DeptList = new SelectList(_db.Department.OrderBy(d => d.Name).ToList(), "Id", "Name");
                ViewBag.UserList = (from u in _db.SysUser
                                    join r in _db.Role on u.Role equals r.RoleId
                                    join d in _db.Department on u.Department equals d.Id
                                    orderby u.Name descending
                                    select new UserInfo
                                    {
                                        Id = u.Id,
                                        Name = u.Name,
                                        Email = u.Email,
                                        Role = r.RoleName,
                                        Department = d.Name,
                                        IsActive = u.IsActive,
                                        DeleteFlag = u.DeleteFlag,
                                        EditFlag = u.EditFlag,
                                        ViewFlag = u.ViewFlag
                                    }).ToList();
            }
            catch(Exception ex)
            {

            }
        }

        public bool CheckLoginUser()
        {
            if (HttpContext.Session.GetString("userId") == null)
                return false;
            return true;
        }
        #endregion

        #region Role

        public IActionResult Role()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            ViewBag.RoleList = _db.Role.ToList();
            return View(new Role());
        }

        public IActionResult AddRole()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            return View(new Role());
        }

        [HttpPost]
        public IActionResult AddRole(Role model)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");
            if (!ModelState.IsValid)
            {
                TempData["retMsg"] = "Error in Saving Process.";
                return RedirectToAction("AddRole");
            }

            try
            {
                if (model.RoleId == Guid.Empty)
                {
                    model.RoleId = Guid.NewGuid();
                    model.CreatedDate = DateTime.Now;
                    _db.Entry(model).State = EntityState.Added;
                    _db.SaveChanges();
                    TempData["Message"] = "Save Successful.";
                    return RedirectToAction("AddRole");

                }
                else
                {
                    Role rolemodel = new Role();
                    rolemodel = _db.Role.Where(r => r.RoleId == model.RoleId).FirstOrDefault();
                    rolemodel.RoleName = model.RoleName;
                    rolemodel.Description = model.Description;

                    _db.Role.Update(rolemodel);
                    _db.SaveChanges();

                    TempData["Message"] = "Update Successful.";
                    return RedirectToAction("AddRole");
                }
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Error in Saving Process.";
                return RedirectToAction("Role");
            }
        }

        public IActionResult EditRole(Guid roleId)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");
            Role role = _db.Role.Where(r => r.RoleId == roleId).FirstOrDefault();
            ViewBag.RoleList = _db.Role.ToList();
            return View("AddRole", role);
        }

        public IActionResult DeleteRole(Guid roleId)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");
            try {
                if (roleId != Guid.Empty)
                {
                    Role role = _db.Role.Where(r => r.RoleId == roleId).FirstOrDefault();

                    var userList = _db.SysUser.Where(u => u.Role == roleId).ToList();

                    if (userList.Count == 0)
                    {
                        _db.Role.Remove(role);
                        _db.SaveChanges();
                        TempData["Message"] = "Delete Success";
                        return RedirectToAction("Role");
                    }
                    else
                    {
                        TempData["Message"] = "This role has already used in process.";
                        return RedirectToAction("Role");
                    }
                }
            }
            catch (Exception)
            {
                TempData["Message"] = "Delete Fail";
                return RedirectToAction("Role");
            }
            return RedirectToAction("Role");
        }
        #endregion

        #region Department
        public IActionResult Department()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            ViewBag.DepartmentList = _db.Department.ToList();
            return View(new Department());
        }

        public IActionResult AddDepartment()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            return View(new Department());
        }

        [HttpPost]
        public IActionResult AddDepartment(Department model)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");
            if (!ModelState.IsValid)
            {
                TempData["retMsg"] = "Error in Saving Process.";
                return RedirectToAction("AddDepartment");
            }

            try
            {
                if (model.Id == Guid.Empty)
                {
                    model.Id = Guid.NewGuid();
                    model.CreatedDate = DateTime.Now;
                    _db.Entry(model).State = EntityState.Added;
                    _db.SaveChanges();
                    TempData["Message"] = "Save Successful.";
                    return RedirectToAction("AddDepartment");

                }
                else
                {
                    Department depmodel = new Department();
                    depmodel = _db.Department.Where(d => d.Id == model.Id).FirstOrDefault();
                    depmodel.Name = model.Name;
                    depmodel.Description = model.Description;

                    _db.Department.Update(depmodel);
                    _db.SaveChanges();

                    TempData["Message"] = "Update Successful.";
                    return RedirectToAction("AddDepartment");
                }
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Error in Saving Process.";
                return RedirectToAction("Department");
            }
        }

        public IActionResult EditDepartment(Guid depId)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");
            Department dep = _db.Department.Where(d => d.Id == depId).FirstOrDefault();
            ViewBag.Department = _db.Department.ToList();
            return View("AddDepartment", dep);
        }

        public IActionResult DeleteDepartment(Guid depId)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");
            try
            {
                if (depId != Guid.Empty)
                {
                    Department dep = _db.Department.Where(d => d.Id == depId).FirstOrDefault();

                    var userList = _db.SysUser.Where(u => u.Department == depId).ToList();

                    if (userList.Count == 0)
                    {
                        _db.Department.Remove(dep);
                        _db.SaveChanges();
                        TempData["Message"] = "Delete Success";
                        return RedirectToAction("Department");
                    }
                    else
                    {
                        TempData["Message"] = "This department has already used in process.";
                        return RedirectToAction("Department");
                    }
                }
            }
            catch (Exception)
            {
                TempData["Message"] = "Delete Fail";
                return RedirectToAction("Department");
            }
            return RedirectToAction("Department");
        }
        #endregion
    }
}