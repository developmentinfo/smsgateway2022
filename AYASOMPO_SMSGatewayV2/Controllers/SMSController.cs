﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using OfficeOpenXml.Style;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using AYASOMPO_SMSGatewayV2.Models;
using AYASOMPO_SMSGatewayV2.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;

namespace AYASOMPO_SMSGatewayV2.Controllers
{
    public class SMSController : Controller
    {
        MainDBContext _db = new MainDBContext();
        JWTDataContext _JWTDataContext = new JWTDataContext();
        private IHostingEnvironment _env;
        Common common = new Common();
        public ISession session;
        public IConfigurationRoot Configuration { get; }
        private HttpClient _client;
        DataServices dataService = new DataServices();
        CRMAPIService cRMAPIService = new CRMAPIService();
        SmsService SmsService = new SmsService();

        public enum ApproveSendSMS
        {
            Pending,
            Send_SMS,
            No_SMS
        }

        public enum UwApproval
        {
            Approved_with_conditions,
            Approved,
            Decline,
            Policy_Cancel,
            Lapsed_Policy_Cancel
        }

        public enum Y_N_Condition
        {
            Yes,
            No
        }

        public enum DonePending
        {
            Done,
            Pending
        }

        public SMSController(IHostingEnvironment env)
        {
            _env = env;
            var builder = new ConfigurationBuilder()
             .SetBasePath(Directory.GetCurrentDirectory())
             .AddJsonFile("appsettings.json");
            Configuration = builder.Build();

            _client = new HttpClient();
            string url = Configuration["CoreMiddleware:IPAddress"].ToString();
            _client.BaseAddress = new Uri(Configuration["CoreMiddleware:IPAddress"].ToString());
            _client.DefaultRequestHeaders.Clear();
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public IActionResult Dashboard()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            ViewBag.TotalMotorCount = _db.MotorRenewalRegister.Count();
            ViewBag.TotalFireCount = _db.FireRenewalRegister.Count();
            List<SummaryRenewalRegister> lstSummaryRenewalRegister = new List<SummaryRenewalRegister>();
            lstSummaryRenewalRegister = dataService.GetSummaryRenewalRegister();

            ViewBag.lstMotorSummaryRenewalRegister = lstSummaryRenewalRegister.Where(m=> m.productclass== "MOTOR INSURANCE").ToList();
            ViewBag.lstFireSummaryRenewalRegister = lstSummaryRenewalRegister.Where(m => m.productclass == "FIRE INSURANCE").ToList();
            ViewBag.lstHealthSummaryRenewalRegister = lstSummaryRenewalRegister.Where(m => m.productclass == "HEALTH INSURANCE").ToList();
            ViewBag.lstPASummaryRenewalRegister = lstSummaryRenewalRegister.Where(m => m.productclass == "PERSONAL ACCIDENT INSURANCE").ToList();
            ViewBag.lstENGGSummaryRenewalRegister = lstSummaryRenewalRegister.Where(m => m.productclass == "ENGINEERING").ToList();
            ViewBag.lstPropertySummaryRenewalRegister = lstSummaryRenewalRegister.Where(m => m.productclass == "PROPERTY").ToList();
            return View();
        }

        public IActionResult RenewalCaseDashboard()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            return View();
        }

        #region Data Sync
        public IActionResult RenewalDataSync()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            filterRenewalDataModel filterRenewalData = new filterRenewalDataModel();
            PrepareData();
            return View(filterRenewalData);
        }
        
        [HttpPost]
        public async Task<IActionResult> RenewalDataSync(filterRenewalDataModel filterRenewalData)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            PrepareData();
            try
            {
                if (filterRenewalData.month == null || filterRenewalData.classname == null)
                {
                    TempData["Message"] = "Invalid request data";
                    return View("RenewalDataSync");
                }

                var firstDayOfMonth = new DateTime(filterRenewalData.month.Year, filterRenewalData.month.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                
                if (filterRenewalData.classname == "FI")
                {
                    List<FireRenewalRegister> lstFireRenewalRegisters = new List<FireRenewalRegister>();
                    lstFireRenewalRegisters = await GetFireRenewalRegisterAsync(filterRenewalData);
                    ViewBag.lstFireRenewalRegisters = lstFireRenewalRegisters;
                    var fireRenewalRegisterlist = (from fire in _db.FireRenewalRegister
                                                   where Convert.ToDateTime(fire.ExpiryDate).Date >= Convert.ToDateTime(firstDayOfMonth).Date
                                                   where Convert.ToDateTime(fire.ExpiryDate).Date <= Convert.ToDateTime(lastDayOfMonth).Date
                                                   select fire).ToList();
                  
                    if (fireRenewalRegisterlist.Count() == 0)
                    {
                        SaveFireRenewalRegister(lstFireRenewalRegisters);
                        var motorRenewalRegister = (from fire in _db.FireRenewalRegister
                                                    where Convert.ToDateTime(fire.ExpiryDate).Date >= Convert.ToDateTime(firstDayOfMonth).Date
                                                    where Convert.ToDateTime(fire.ExpiryDate).Date <= Convert.ToDateTime(lastDayOfMonth).Date
                                                    select fire).ToList();

                        TempData["SyncMessage"] = "Fire Data Sync is success for total " + motorRenewalRegister.Count() + " transactions.";
                    }
                    else
                    {
                        TempData["SyncMessage"] = "Already fire data sync for " + Convert.ToDateTime(filterRenewalData.month).ToString("MMMM");
                    }
                }
                else if(filterRenewalData.classname == "MT")
                {
                    List<MotorRenewalRegister> lstMotorRenewalRegisters = new List<MotorRenewalRegister>();
                    lstMotorRenewalRegisters = await GetMotorRenewalRegisterAsync(filterRenewalData);
                    ViewBag.lstMotorRenewalRegisters = lstMotorRenewalRegisters;
                    var motorRenewalRegisterlist = (from motor in _db.MotorRenewalRegister
                                                    where Convert.ToDateTime(motor.ExpiryDate).Date >= Convert.ToDateTime(firstDayOfMonth).Date
                                                    where Convert.ToDateTime(motor.ExpiryDate).Date <= Convert.ToDateTime(lastDayOfMonth).Date
                                                    select motor).ToList();


                    if (motorRenewalRegisterlist.Count() == 0)
                    {
                        SaveMotorRenewalRegister(lstMotorRenewalRegisters);
                        var motorRenewalRegister = (from motor in _db.MotorRenewalRegister
                                                    where Convert.ToDateTime(motor.ExpiryDate).Date >= Convert.ToDateTime(firstDayOfMonth).Date
                                                    where Convert.ToDateTime(motor.ExpiryDate).Date <= Convert.ToDateTime(lastDayOfMonth).Date
                                                    select motor).ToList();

                        TempData["SyncMessage"] = "Motor Data Sync is success for total " + motorRenewalRegister.Count() + " transactions.";
                    }
                    else
                    {
                        TempData["SyncMessage"] = "Already motor data sync for " + Convert.ToDateTime(filterRenewalData.month).ToString("MMMM");
                    }
                }
                else if(filterRenewalData.classname == "LF")
                {
                    List<HealthRenewalRegister> lstHealthRenewalRegisters = new List<HealthRenewalRegister>();
                    lstHealthRenewalRegisters = await GetHealthRenewalRegisterAsync(filterRenewalData);
                    ViewBag.lstHealthRenewalRegisters = lstHealthRenewalRegisters;
                    var healthRenewalRegisterlist = (from health in _db.HealthRenewalRegister
                                                    where Convert.ToDateTime(health.ExpiryDate).Date >= Convert.ToDateTime(firstDayOfMonth).Date
                                                    where Convert.ToDateTime(health.ExpiryDate).Date <= Convert.ToDateTime(lastDayOfMonth).Date
                                                    select health).ToList();


                    if (healthRenewalRegisterlist.Count() == 0)
                    {
                        SaveHealthRenewalRegister(lstHealthRenewalRegisters);
                        var healthRenewalRegister = (from health in _db.HealthRenewalRegister
                                                    where Convert.ToDateTime(health.ExpiryDate).Date >= Convert.ToDateTime(firstDayOfMonth).Date
                                                    where Convert.ToDateTime(health.ExpiryDate).Date <= Convert.ToDateTime(lastDayOfMonth).Date
                                                    select health).ToList();

                        TempData["SyncMessage"] = "Health Data Sync is success for total " + healthRenewalRegister.Count() + " transactions.";
                    }
                    else
                    {
                        TempData["SyncMessage"] = "Already health data sync for " + Convert.ToDateTime(filterRenewalData.month).ToString("MMMM");
                    }
                }
                else if(filterRenewalData.classname == "MS")
                {
                    List<ParenewalRegister> lstPARenewalRegisters = new List<ParenewalRegister>();
                    lstPARenewalRegisters = await GetPARenewalRegisterAsync(filterRenewalData);
                    ViewBag.lstPARenewalRegisters = lstPARenewalRegisters;
                    var paRenewalRegisterlist = (from pa in _db.ParenewalRegister
                                                 where Convert.ToDateTime(pa.ExpiryDate).Date >= Convert.ToDateTime(firstDayOfMonth).Date
                                                 where Convert.ToDateTime(pa.ExpiryDate).Date <= Convert.ToDateTime(lastDayOfMonth).Date
                                                 select pa).ToList();

                    if(paRenewalRegisterlist.Count() == 0)
                    {
                        SavePARenewalRegister(lstPARenewalRegisters);
                        var paRenewalRegister = (from pa in _db.ParenewalRegister
                                                 where Convert.ToDateTime(pa.ExpiryDate).Date >= Convert.ToDateTime(firstDayOfMonth).Date
                                                 where Convert.ToDateTime(pa.ExpiryDate).Date <= Convert.ToDateTime(lastDayOfMonth).Date
                                                 select pa).ToList();
                        TempData["SyncMessage"] = "PA Data Sync is success for total " + paRenewalRegister.Count() + " transactions.";
                    }
                    else
                    {
                        TempData["SyncMessage"] = "Already PA data sync for " + Convert.ToDateTime(filterRenewalData.month).ToString("MMMM");
                    }

                }
                else if (filterRenewalData.classname == "ENGG")
                {
                    List<EngineeringRenewalRegister> lstEnggRenewalRegisters = new List<EngineeringRenewalRegister>();
                    lstEnggRenewalRegisters = await GetEnggRenewalRegisterAsync(filterRenewalData);
                    ViewBag.lstEnggRenewalRegisters = lstEnggRenewalRegisters;
                    var enggRenewalRegisterlist = (from pa in _db.EngineeringRenewalRegister
                                                 where Convert.ToDateTime(pa.ExpiryDate).Date >= Convert.ToDateTime(firstDayOfMonth).Date
                                                 where Convert.ToDateTime(pa.ExpiryDate).Date <= Convert.ToDateTime(lastDayOfMonth).Date
                                                 select pa).ToList();

                    if (enggRenewalRegisterlist.Count() == 0)
                    {
                        SaveEnggRenewalRegister(lstEnggRenewalRegisters);
                        var enggRenewalRegister = (from pa in _db.EngineeringRenewalRegister
                                                 where Convert.ToDateTime(pa.ExpiryDate).Date >= Convert.ToDateTime(firstDayOfMonth).Date
                                                 where Convert.ToDateTime(pa.ExpiryDate).Date <= Convert.ToDateTime(lastDayOfMonth).Date
                                                 select pa).ToList();
                        TempData["SyncMessage"] = "Engineering Data Sync is success for total " + enggRenewalRegister.Count() + " transactions.";
                    }
                    else
                    {
                        TempData["SyncMessage"] = "Already Engineering data sync for " + Convert.ToDateTime(filterRenewalData.month).ToString("MMMM");
                    }

                }
                else if(filterRenewalData.classname == "PTY")
                {
                    List<PropertyRenewalRegister> lstPropertyRenewalRegisters = new List<PropertyRenewalRegister>();
                    lstPropertyRenewalRegisters = await GetPropertyRenewalRegisterAsync(filterRenewalData);
                    ViewBag.lstPropertyRenewalRegisters = lstPropertyRenewalRegisters;
                    var propertyRenewalRegisterlist = (from property in _db.PropertyRenewalRegister
                                                       where Convert.ToDateTime(property.ExpiryDate).Date >= Convert.ToDateTime(firstDayOfMonth).Date
                                                       where Convert.ToDateTime(property.ExpiryDate).Date <= Convert.ToDateTime(lastDayOfMonth).Date
                                                       select property).ToList();

                    if (propertyRenewalRegisterlist.Count() == 0)
                    {
                        SavePropertyRenewalRegister(lstPropertyRenewalRegisters);
                        var propertyRenewalRegister = (from property in _db.PropertyRenewalRegister
                                                       where Convert.ToDateTime(property.ExpiryDate).Date >= Convert.ToDateTime(firstDayOfMonth).Date
                                                       where Convert.ToDateTime(property.ExpiryDate).Date <= Convert.ToDateTime(lastDayOfMonth).Date
                                                       select property).ToList();
                        TempData["SyncMessage"] = "Property Data Sync is success for total " + propertyRenewalRegister.Count() + " transactions.";
                    }
                    else
                    {
                        TempData["SyncMessage"] = "Already Property data sync for " + Convert.ToDateTime(filterRenewalData.month).ToString("MMMM");
                    }
                }

                return View("RenewalDataSync", filterRenewalData);
            }
            catch (Exception ex)
            {
                return View();
            }

        }

        private void SaveMotorRenewalRegister(List<MotorRenewalRegister> lstMotorRenewalRegisters)
        {
            try
            {
                List<MotorRenewalRegister> lstMotorRenewalRegister = new List<MotorRenewalRegister>(); 
                if (lstMotorRenewalRegisters.Count() > 0)
                {
                    foreach(var motor in lstMotorRenewalRegisters)
                    {
                        //if(motor.PolicyNo == "AYA/YGN/MCP/22001728")
                        //{
                        //    string val = "1";
                        //}
                        motor.Id = Guid.NewGuid();
                        motor.CreatedDate = DateTime.Now.Date;
                        motor.Smsflag = false;
                        motor.CrmrenewalCreateFlag = false;
                        motor.ActivitiesFlag = false;
                        motor.InvoiceNo = null;

                        #region AccountHandler to PIC

                        CrmPersonInCharge personInCharge = new CrmPersonInCharge();
                        if(motor.AccountCode != null)
                        {
                            personInCharge = GetPICByAccountCode(motor.AccountCode);
                        }

                        if(personInCharge != null)
                        {
                            motor.PersonInCharge = personInCharge.Picname;
                            motor.ResponsibleParty = personInCharge.ResponsibleParty;
                        }

                        #endregion

                        lstMotorRenewalRegister.Add(motor);
                    }
                    _db.MotorRenewalRegister.AddRange(lstMotorRenewalRegister);
                    _db.SaveChanges();
                }
            }
            catch(Exception ex)
            {

            }
        }

        private void SaveFireRenewalRegister(List<FireRenewalRegister> lstFireRenewalRegisters)
        {
            try
            {
                List<FireRenewalRegister> lstFireRenewalRegister = new List<FireRenewalRegister>();
                if (lstFireRenewalRegisters.Count() > 0)
                {
                    foreach (var fire in lstFireRenewalRegisters)
                    {
                        fire.Id = Guid.NewGuid();
                        fire.CreatedDate = DateTime.Now.Date;
                        fire.Smsflag = false;
                        fire.CrmrenewalCreateFlag = false;
                        fire.ActivitiesFlag = false;
                        fire.InvoiceNo = null;

                        #region AccountHandler to PIC

                        CrmPersonInCharge personInCharge = new CrmPersonInCharge();
                        if (fire.AccountCode != null)
                        {
                            personInCharge = GetPICByAccountCode(fire.AccountCode);
                        }

                        if (personInCharge != null)
                        {
                            fire.PersonInCharge = personInCharge.Picname;
                            fire.ResponsibleParty = personInCharge.ResponsibleParty;
                        }

                        #endregion

                        lstFireRenewalRegister.Add(fire);
                    }
                    _db.FireRenewalRegister.AddRange(lstFireRenewalRegister);
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void SaveHealthRenewalRegister(List<HealthRenewalRegister> lstHealthRenewalRegisters)
        {
            try
            {
                List<HealthRenewalRegister> lstHealthRenewalRegister = new List<HealthRenewalRegister>();
                if (lstHealthRenewalRegisters.Count() > 0)
                {
                    foreach (var health in lstHealthRenewalRegisters)
                    {
                        health.Id = Guid.NewGuid();
                        health.CreatedDate = DateTime.Now.Date;
                        health.Smsflag = false;
                        health.CrmrenewalCreateFlag = false;
                        health.ActivitiesFlag = false;
                        health.InvoiceNo = null;

                        #region AccountHandler to PIC

                        CrmPersonInCharge personInCharge = new CrmPersonInCharge();
                        if (health.AccountCode != null)
                        {
                            personInCharge = GetPICByAccountCode(health.AccountCode);
                        }

                        if (personInCharge != null)
                        {
                            health.PersonInCharge = personInCharge.Picname;
                            health.ResponsibleParty = personInCharge.ResponsibleParty;
                        }

                        #endregion

                        lstHealthRenewalRegister.Add(health);
                    }
                    _db.HealthRenewalRegister.AddRange(lstHealthRenewalRegister);
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void SavePARenewalRegister(List<ParenewalRegister> lstPARenewalRegisters)
        {
            try
            {
                List<ParenewalRegister> lstPARenewalRegister = new List<ParenewalRegister>();
                if(lstPARenewalRegisters.Count() > 0)
                {
                    foreach (var pa in lstPARenewalRegisters)
                    {
                        pa.Id = Guid.NewGuid();
                        pa.CreatedDate = DateTime.Now.Date;
                        pa.Smsflag = false;
                        pa.CrmrenewalCreateFlag = false;
                        pa.ActivitiesFlag = false;
                        pa.InvoiceNo = null;

                        #region AccountHandler to PIC
                        CrmPersonInCharge personInCharge = new CrmPersonInCharge();
                        if (pa.AccountCode != null)
                        {
                            personInCharge = GetPICByAccountCode(pa.AccountCode);
                        }

                        if (personInCharge != null)
                        {
                            pa.PersonInCharge = personInCharge.Picname;
                            pa.ResponsibleParty = personInCharge.ResponsibleParty;
                        }
                        #endregion

                        lstPARenewalRegister.Add(pa);
                    }
                    _db.ParenewalRegister.AddRange(lstPARenewalRegister);
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void SaveEnggRenewalRegister(List<EngineeringRenewalRegister> lstEnggRenewalRegisters)
        {
            try
            {
                List<EngineeringRenewalRegister> lstEnggRenewalRegister = new List<EngineeringRenewalRegister>();
                if(lstEnggRenewalRegisters.Count() > 0)
                {
                    foreach (var engg in lstEnggRenewalRegisters)
                    {
                        engg.Id = Guid.NewGuid();
                        engg.CreatedDate = DateTime.Now.Date;
                        engg.Smsflag = false;
                        engg.CrmrenewalCreateFlag = false;
                        engg.ActivitiesFlag = false;
                        engg.InvoiceNo = null;

                        #region AccountHandler to PIC
                        CrmPersonInCharge personInCharge = new CrmPersonInCharge();
                        if (engg.AccountCode != null)
                        {
                            personInCharge = GetPICByAccountCode(engg.AccountCode);
                        }

                        if (personInCharge != null)
                        {
                            engg.PersonInCharge = personInCharge.Picname;
                            engg.ResponsibleParty = personInCharge.ResponsibleParty;
                        }
                        #endregion

                        lstEnggRenewalRegister.Add(engg);
                    }
                    _db.EngineeringRenewalRegister.AddRange(lstEnggRenewalRegister);
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void SavePropertyRenewalRegister(List<PropertyRenewalRegister> lstPropertyRenewalRegisters)
        {
            try
            {
                List<PropertyRenewalRegister> lstPropertyRenewalRegister = new List<PropertyRenewalRegister>();
                if (lstPropertyRenewalRegisters.Count() > 0)
                {
                    foreach (var property in lstPropertyRenewalRegisters)
                    {
                        property.Id = Guid.NewGuid();
                        property.CreatedDate = DateTime.Now.Date;
                        property.Smsflag = false;
                        property.CrmrenewalCreateFlag = false;
                        property.ActivitiesFlag = false;
                        property.InvoiceNo = null;

                        #region AccountHandler to PIC
                        CrmPersonInCharge personInCharge = new CrmPersonInCharge();
                        if (property.AccountCode != null)
                        {
                            personInCharge = GetPICByAccountCode(property.AccountCode);
                        }

                        if (personInCharge != null)
                        {
                            property.PersonInCharge = personInCharge.Picname;
                            property.ResponsibleParty = personInCharge.ResponsibleParty;
                        }
                        #endregion

                        lstPropertyRenewalRegister.Add(property);
                    }
                    _db.PropertyRenewalRegister.AddRange(lstPropertyRenewalRegister);
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private CrmPersonInCharge GetPICByAccountCode(string accountcode)
        {
            try
            {
                AccountHandlerPic accountHandlerPic = new AccountHandlerPic();
                accountHandlerPic = _JWTDataContext.AccountHandlerPic.Where(a => a.AccountCode == accountcode).FirstOrDefault();

                CrmPersonInCharge personInCharge = new CrmPersonInCharge();
                if (accountHandlerPic != null)
                {
                    personInCharge = _JWTDataContext.CrmPersonInCharge.Where(p => p.CrmPicId == accountHandlerPic.CrmPicId).FirstOrDefault();

                }
                return personInCharge;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion

        #region BU View
        public IActionResult RenewalRegisterList(filterRenewalDataModel filterRenewalData)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            //filterRenewalDataModel filterRenewalData = new filterRenewalDataModel();
            PrepareDataForRenewalReg();
            return View(filterRenewalData);
        }

        //[HttpPost]
        public IActionResult GetRenewalRegisterList(filterRenewalDataModel filterRenewalData)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            session = HttpContext.Session;
            PrepareDataForRenewalReg();
            try
            {
                if (filterRenewalData.month == null || filterRenewalData.classname == null)
                {
                    TempData["Message"] = "Invalid request data";
                    return View("RenewalRegisterList", filterRenewalData);
                }

                if (filterRenewalData.classname == "FI")
                {
                    List<FireRenewalRegister> lstFireRenewalRegisters = new List<FireRenewalRegister>();
                    lstFireRenewalRegisters = GetFireRenewalRegisterSql(filterRenewalData);
                    ViewBag.lstFireRenewalRegisters = lstFireRenewalRegisters;
                    filterRenewalData.motortotalcount = lstFireRenewalRegisters.Count();
                    HttpContext.Session.SetObjectAsJson("lstFireRenewalRegistersfilter", lstFireRenewalRegisters);
                }
                else if(filterRenewalData.classname == "MT")
                {
                    List<MotorRenewalRegister> lstMotorRenewalRegisters = new List<MotorRenewalRegister>();
                    lstMotorRenewalRegisters = GetMotorRenewalRegisterSql(filterRenewalData);
                    ViewBag.lstMotorRenewalRegisters = lstMotorRenewalRegisters;
                    HttpContext.Session.SetObjectAsJson("lstMotorRenewalRegistersfilter", lstMotorRenewalRegisters);
                }
                else if(filterRenewalData.classname == "LF")
                {
                    List<HealthRenewalRegister> lstHealthRenewalRegisters = new List<HealthRenewalRegister>();
                    lstHealthRenewalRegisters = GetHealthRenewalRegisterSql(filterRenewalData);
                    ViewBag.lstHealthRenewalRegisters = lstHealthRenewalRegisters;
                    HttpContext.Session.SetObjectAsJson("lstHealthRenewalRegistersfilter", lstHealthRenewalRegisters);
                }
                else if(filterRenewalData.classname == "MS")
                {
                    List<ParenewalRegister> lstPARenewalRegisters = new List<ParenewalRegister>();
                    lstPARenewalRegisters = GetPARenewalRegisterSql(filterRenewalData);
                    ViewBag.lstPARenewalRegisters = lstPARenewalRegisters;
                    HttpContext.Session.SetObjectAsJson("lstPARenewalRegistersfilter", lstPARenewalRegisters);
                }
                else if(filterRenewalData.classname == "ENGG")
                {
                    List<EngineeringRenewalRegister> lstENGGRenewalRegisters = new List<EngineeringRenewalRegister>();
                    lstENGGRenewalRegisters = GetENGGRenewalRegisterSql(filterRenewalData);
                    ViewBag.lstENGGRenewalRegisters = lstENGGRenewalRegisters;
                    HttpContext.Session.SetObjectAsJson("lstENGGRenewalRegistersfilter", lstENGGRenewalRegisters);
                }
                else if(filterRenewalData.classname == "PTY")
                {
                    List<PropertyRenewalRegister> lstPropertyRenewalRegisters = new List<PropertyRenewalRegister>();
                    lstPropertyRenewalRegisters = GetPropertyRenewalRegisterSql(filterRenewalData);
                    ViewBag.lstPropertyRenewalRegisters = lstPropertyRenewalRegisters;
                    HttpContext.Session.SetObjectAsJson("lstPropertyRenewalRegistersfilter", lstPropertyRenewalRegisters);
                }

                return View("RenewalRegisterList", filterRenewalData);
            }
            catch (Exception ex)
            {
                return View("RenewalRegisterList", filterRenewalData);
            }
        }

        private List<MotorRenewalRegister> GetMotorRenewalRegisterSql(filterRenewalDataModel filterRenewalData)
        {
            List<MotorRenewalRegister> lstMotorRenewalRegisters = new List<MotorRenewalRegister>();
            try
            {
                lstMotorRenewalRegisters = dataService.GetMotorRenewalListByFilter(filterRenewalData);
                return lstMotorRenewalRegisters;
            }
            catch (Exception ex)
            {
                return lstMotorRenewalRegisters;
            }
        }

        private List<FireRenewalRegister> GetFireRenewalRegisterSql(filterRenewalDataModel filterRenewalData)
        {
            List<FireRenewalRegister> lstFireRenewalRegisters = new List<FireRenewalRegister>();
            try
            {
                lstFireRenewalRegisters = dataService.GetFireRenewalListByFilter(filterRenewalData);
                return lstFireRenewalRegisters;
            }
            catch (Exception ex)
            {
                return lstFireRenewalRegisters;
            }
        }

        private List<HealthRenewalRegister> GetHealthRenewalRegisterSql(filterRenewalDataModel filterRenewalData)
        {
            List<HealthRenewalRegister> lstHealthRenewalRegisters = new List<HealthRenewalRegister>();
            try
            {
                lstHealthRenewalRegisters = dataService.GetHealthRenewalListByFilter(filterRenewalData);
                return lstHealthRenewalRegisters;
            }
            catch(Exception ex)
            {
                return lstHealthRenewalRegisters;
            }
        }

        private List<ParenewalRegister> GetPARenewalRegisterSql(filterRenewalDataModel filterRenewalData)
        {
            List<ParenewalRegister> lstPARenewalRegisters = new List<ParenewalRegister>();
            try
            {
                lstPARenewalRegisters = dataService.GetPARenewalListByFilter(filterRenewalData);
                return lstPARenewalRegisters;
            }
            catch(Exception ex)
            {
                return lstPARenewalRegisters;
            }
        }

        private List<EngineeringRenewalRegister> GetENGGRenewalRegisterSql(filterRenewalDataModel filterRenewalData)
        {
            List<EngineeringRenewalRegister> lstENGGRenewalRegisters = new List<EngineeringRenewalRegister>();
            try
            {
                lstENGGRenewalRegisters = dataService.GetENGGRenewalListByFilter(filterRenewalData);
                return lstENGGRenewalRegisters;
            }
            catch(Exception ex)
            {
                return lstENGGRenewalRegisters;
            }
        }

        private List<PropertyRenewalRegister> GetPropertyRenewalRegisterSql(filterRenewalDataModel filterRenewalData)
        {
            List<PropertyRenewalRegister> lstPropertyRenewalRegisters = new List<PropertyRenewalRegister>();
            try
            {
                lstPropertyRenewalRegisters = dataService.GetPropertyRenewalListByFilter(filterRenewalData);
                return lstPropertyRenewalRegisters;
            }
            catch(Exception ex)
            {
                return lstPropertyRenewalRegisters;
            }
        }

        #endregion

        #region OP View
        public IActionResult OPRenewalRegisterList(filterRenewalDataModel filterRenewalData)
        {
            //filterRenewalDataModel filterRenewalData = new filterRenewalDataModel();
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            PrepareDataForRenewalReg();
            return View(filterRenewalData);
        }
        
        //[HttpPost]
        public IActionResult GetOPRenewalRegisterList(filterRenewalDataModel filterRenewalData)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            PrepareDataForRenewalReg();
            try
            {
                if (filterRenewalData.month == null || filterRenewalData.classname == null)
                {
                    TempData["Message"] = "Invalid request data";
                    return View("RenewalRegisterList", filterRenewalData);
                }

                filterRenewalData.checkedcount = 0;

                if (filterRenewalData.classname == "FI")
                {
                    List<FireRenewalRegister> lstFireRenewalRegisters = new List<FireRenewalRegister>();
                    lstFireRenewalRegisters = GetFireRenewalRegisterSql(filterRenewalData);
                    ViewBag.lstFireRenewalRegisters = lstFireRenewalRegisters;

                    for (int i = 0; i < lstFireRenewalRegisters.Count; i++)
                    {
                        if (lstFireRenewalRegisters[i].PhoneNumber != null && lstFireRenewalRegisters[i].InvoiceNo != null && lstFireRenewalRegisters[i].PersonInCharge != null)
                        {
                            filterRenewalData.checkedcount++;
                        }
                    }
                    HttpContext.Session.SetObjectAsJson("lstFireRenewalRegistersfilter", lstFireRenewalRegisters);
                    //filterRenewalData.motortotalcount = lstFireRenewalRegisters.Count();
                }
                else if(filterRenewalData.classname == "MT")
                {
                    List<MotorRenewalRegister> lstMotorRenewalRegisters = new List<MotorRenewalRegister>();
                    lstMotorRenewalRegisters = GetMotorRenewalRegisterSql(filterRenewalData);
                    ViewBag.lstMotorRenewalRegisters = lstMotorRenewalRegisters;

                    for (int i = 0; i < lstMotorRenewalRegisters.Count; i++)
                    {
                        if (lstMotorRenewalRegisters[i].PhoneNumber != null && lstMotorRenewalRegisters[i].InvoiceNo != null && lstMotorRenewalRegisters[i].PersonInCharge != null)
                        {
                            filterRenewalData.checkedcount++;
                        }
                    }
                    HttpContext.Session.SetObjectAsJson("lstMotorRenewalRegistersfilter", lstMotorRenewalRegisters);
                }
                else if(filterRenewalData.classname == "LF")
                {
                    List<HealthRenewalRegister> lstHealthRenewalRegisters = new List<HealthRenewalRegister>();
                    lstHealthRenewalRegisters = GetHealthRenewalRegisterSql(filterRenewalData);
                    ViewBag.lstHealthRenewalRegisters = lstHealthRenewalRegisters;

                    for(int i = 0; i < lstHealthRenewalRegisters.Count; i++)
                    {
                        if(lstHealthRenewalRegisters[i].PhoneNumber != null && lstHealthRenewalRegisters[i].InvoiceNo != null && lstHealthRenewalRegisters[i].PersonInCharge != null)
                        {
                            filterRenewalData.checkedcount++;
                        }
                    }
                    HttpContext.Session.SetObjectAsJson("lstHealthRenewalRegistersfilter", lstHealthRenewalRegisters);
                }
                else if(filterRenewalData.classname == "MS")
                {
                    List<ParenewalRegister> lstPARenewalRegisters = new List<ParenewalRegister>();
                    lstPARenewalRegisters = GetPARenewalRegisterSql(filterRenewalData);
                    ViewBag.lstPARenewalRegisters = lstPARenewalRegisters;

                    for(int i = 0; i < lstPARenewalRegisters.Count; i++)
                    {
                        if(lstPARenewalRegisters[i].PhoneNumber != null && lstPARenewalRegisters[i].InvoiceNo != null && lstPARenewalRegisters[i].PersonInCharge != null)
                        {
                            filterRenewalData.checkedcount++;
                        }
                    }
                    HttpContext.Session.SetObjectAsJson("lstPARenewalRegistersfilter", lstPARenewalRegisters);
                }
                else if(filterRenewalData.classname == "ENGG")
                {
                    List<EngineeringRenewalRegister> lstENGGRenewalRegisters = new List<EngineeringRenewalRegister>();
                    lstENGGRenewalRegisters = GetENGGRenewalRegisterSql(filterRenewalData);
                    ViewBag.lstENGGRenewalRegisters = lstENGGRenewalRegisters;

                    for (int i = 0; i < lstENGGRenewalRegisters.Count; i++)
                    {
                        if (lstENGGRenewalRegisters[i].PhoneNumber != null && lstENGGRenewalRegisters[i].InvoiceNo != null && lstENGGRenewalRegisters[i].PersonInCharge != null)
                        {
                            filterRenewalData.checkedcount++;
                        }
                    }
                    HttpContext.Session.SetObjectAsJson("lstENGGRenewalRegistersfilter", lstENGGRenewalRegisters);
                }
                else if(filterRenewalData.classname == "PTY")
                {
                    List<PropertyRenewalRegister> lstPropertyRenewalRegisters = new List<PropertyRenewalRegister>();
                    lstPropertyRenewalRegisters = GetPropertyRenewalRegisterSql(filterRenewalData);
                    ViewBag.lstPropertyRenewalRegisters = lstPropertyRenewalRegisters;

                    for(int i = 0; i < lstPropertyRenewalRegisters.Count; i++)
                    {
                        if (lstPropertyRenewalRegisters[i].PhoneNumber != null && lstPropertyRenewalRegisters[i].InvoiceNo != null && lstPropertyRenewalRegisters[i].PersonInCharge != null)
                        {
                            filterRenewalData.checkedcount++;
                        }
                    }
                    HttpContext.Session.SetObjectAsJson("lstPropertyRenewalRegistersfilter", lstPropertyRenewalRegisters);
                }
                return View("OPRenewalRegisterList", filterRenewalData);
            }
            catch (Exception ex)
            {
                return View("OPRenewalRegisterList", filterRenewalData);
            }
        }

        #endregion

        #region Export RenewalRegister

        #region Export MotorRenewalRegister
        public IActionResult ExportMotorRenewalRegisters()
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                List<MotorRenewalRegister> lstMotorRenewalRegisters = new List<MotorRenewalRegister>();
                lstMotorRenewalRegisters = HttpContext.Session.GetObjectFromJson<List<MotorRenewalRegister>>("lstMotorRenewalRegistersfilter");

                var columnHeader = new List<string>
                {
                    "Customer Name",
                    "Phone Number",
                    "Approve Send SMS",
                    "UW Approval",
                    "Branch Authority",
                    "Agreed SI Amount",
                    "Invoice No",
                    "Policy Number",
                    "PersonInCharge",
                    "Responsible Party",
                    "Customer Code",
                    "Risk Name",
                    "Expired Date",
                    "From Date",
                    "To Date",
                    "Renewal Premium",
                    "Make",
                    "Model",
                    "Product",
                    "Branch",
                    "Account Code",
                    "Intermediary Type",
                    "Agent Code",
                    "Agent Name",
                    "Authorized Code",
                    "Authorized Name",
                    "Product Class",
                    "Currency",
                    "SumInsured",
                    "SI Increased Value",
                    "Renewal SI value",
                    "Previous Winscreen Value",
                    "Renewed Winscreen Value",
                    "Outstanding Amount",
                    "Excess Amount",
                    "Send SMS Date",
                    "Joint Member",
                    "Dominant Share Holder",
                    "Financial Interest",
                    "Branch Authority Date",
                    "Remark"
                };
                var fileContent = ExportExcelForMotorRenewalRegistersReport(lstMotorRenewalRegisters, columnHeader, "Motor Renewal Registers Report");
                return File(fileContent, "application/ms-excel", "MotorRenewalRegisters.xlsx");
            }
        }

        public byte[] ExportExcelForMotorRenewalRegistersReport(List<MotorRenewalRegister> lst, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 30;
                using (var cells = worksheet.Cells[1, 1, 1, 60])
                    cells.Style.Font.Bold = true;

                for (int i = 0; i < columnHeader.Count(); i++)
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];

                //For Remark Column
                worksheet.Column(41).Width = 100;

                var j = 2;
                var count = 1;
                foreach (var item in lst)
                {
                    #region Get RemarkInfo
                    List<RenewalRegisterRemarkViewModel> lstremarkViewModels = new List<RenewalRegisterRemarkViewModel>();
                    lstremarkViewModels = GetRenewalRegisterRemarkInfo(item.Id);
                    #endregion

                    worksheet.Cells["A" + j].Value = item.CustomerName;
                    worksheet.Cells["B" + j].Value = item.PhoneNumber;
                    worksheet.Cells["C" + j].Value = item.ApproveSendSms;
                    worksheet.Cells["D" + j].Value = item.Uwapproval;
                    worksheet.Cells["E" + j].Value = item.BranchAuthority;
                    worksheet.Cells["F" + j].Value = string.Format("{0:n0}", item.AgreedSiamount);
                    worksheet.Cells["G" + j].Value = item.InvoiceNo;
                    worksheet.Cells["H" + j].Value = item.PolicyNo;
                    worksheet.Cells["I" + j].Value = item.PersonInCharge;
                    worksheet.Cells["J" + j].Value = item.ResponsibleParty;
                    worksheet.Cells["K" + j].Value = item.CusCode;
                    worksheet.Cells["L" + j].Value = item.RiskName;
                    worksheet.Cells["M" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ExpiryDate);
                    worksheet.Cells["N" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.FromDate);
                    worksheet.Cells["O" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ToDate);
                    worksheet.Cells["P" + j].Value = string.Format("{0:n0}", item.RenewalPremium);
                    worksheet.Cells["Q" + j].Value = item.Manufacturer;
                    worksheet.Cells["R" + j].Value = item.Car_Model;
                    worksheet.Cells["S" + j].Value = item.Product;
                    worksheet.Cells["T" + j].Value = item.Branch;
                    worksheet.Cells["U" + j].Value = item.AccountCode;
                    worksheet.Cells["V" + j].Value = item.IntermediaryType;
                    worksheet.Cells["W" + j].Value = item.AgentCode;
                    worksheet.Cells["X" + j].Value = item.AgentName;
                    worksheet.Cells["Y" + j].Value = item.AuthorizedCode;
                    worksheet.Cells["Z" + j].Value = item.AuthorizedName;
                    worksheet.Cells["AA" + j].Value = item.Class;
                    worksheet.Cells["AB" + j].Value = item.Currency;
                    worksheet.Cells["AC" + j].Value = string.Format("{0:n0}", item.SumInsured);
                    worksheet.Cells["AD" + j].Value = string.Format("{0:n0}", item.Sivalue20);
                    worksheet.Cells["AE" + j].Value = string.Format("{0:n0}", item.RenewedSivalue);
                    worksheet.Cells["AF" + j].Value = string.Format("{0:n0}", item.PreviousWindscreenValue);
                    worksheet.Cells["AG" + j].Value = string.Format("{0:n0}", item.RenewedWindscreenValue);
                    worksheet.Cells["AH" + j].Value = string.Format("{0:n0}", item.Outstanding_Amt);
                    worksheet.Cells["AI" + j].Value = string.Format("{0:n0}", item.Excess_Amt);
                    worksheet.Cells["AJ" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ApproveSendSmsdate);
                    worksheet.Cells["AK" + j].Value = item.JointMember;
                    worksheet.Cells["AL" + j].Value = item.DominantHolder;
                    worksheet.Cells["AM" + j].Value = item.Financial_Interest;
                    worksheet.Cells["AN" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.BranchAuthorityDate);

                    foreach (var remarkinfo in lstremarkViewModels)
                    {
                        worksheet.Cells["AO" + j].Value += "[" + remarkinfo.Remark + "] ";
                    }

                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }
        #endregion

        #region Export FireRenewalRegister

        public IActionResult ExportFireRenewalRegisters()
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                List<FireRenewalRegister> lstFireRenewalRegisters = new List<FireRenewalRegister>();
                lstFireRenewalRegisters = HttpContext.Session.GetObjectFromJson<List<FireRenewalRegister>>("lstFireRenewalRegistersfilter");

                var columnHeader = new List<string>
                {
                    "Customer Name",
                    "Phone Number",
                    "Approve Send SMS",
                    "UW Approval",
                    "Branch Authority",
                    "Agreed SI Amount",
                    "Invoice No",
                    "Policy Number",
                    "PersonInCharge",
                    "Responsible Party",
                    "Customer Code",
                    "Risk Name",
                    "Expired Date",
                    "From Date",
                    "To Date",
                    "Renewal Premium",
                    "Product",
                    "Branch",
                    "Account Code",
                    "Intermediary Type",
                    "Agent Code",
                    "Agent Name",
                    "Authorized Code",
                    "Authorized Name",
                    "Product Class",
                    "Currency",
                    "SumInsured",
                    "Detail Occupancy",
                    "Location Risk",
                    "Building Class",
                    "Loss Record",
                    "Coverage",
                    "Google Map/ Geo Coordinate",
                    "Number of ABC Type Extinguisher",
                    "Number of CO2 Type Extinguishers",
                    "Number of Other Extinguisher",
                    "Weight of Fire Extinguisher",
                    "No. of Fire Hose Reel",
                    "No. of Hydrant",
                    "Capacity of Fire Tank",
                    "Security Guards",
                    "Number of CCTV Camera",
                    "Sq M of the Building",
                    "Required No. of Extinguishers",
                    "Work in process if there is any",
                    "Send SMS Date",
                    "Joint Member",
                    "Dominant Share Holder",
                    "Number of Fire Tank",
                    "Type of Fire Tank",
                    "Outstanding Amount",
                    "Excess Amount",
                    "Financial Interest",
                    "Branch Authority Date",
                    "Remark"

                };
                var fileContent = ExportExcelForFireRenewalRegistersReport(lstFireRenewalRegisters, columnHeader, "Fire Renewal Registers Report");
                return File(fileContent, "application/ms-excel", "FireRenewalRegisters.xlsx");
            }
        }

        public byte[] ExportExcelForFireRenewalRegistersReport(List<FireRenewalRegister> lst, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 30;
                using (var cells = worksheet.Cells[1, 1, 1, 60])
                    cells.Style.Font.Bold = true;

                for (int i = 0; i < columnHeader.Count(); i++)
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];

                //For Remark
                worksheet.Column(55).Width = 100;

                var j = 2;
                var count = 1;
                foreach (var item in lst)
                {
                    #region Get RemarkInfo
                    List<RenewalRegisterRemarkViewModel> lstremarkViewModels = new List<RenewalRegisterRemarkViewModel>();
                    lstremarkViewModels = GetRenewalRegisterRemarkInfo(item.Id);
                    #endregion

                    worksheet.Cells["A" + j].Value = item.CustomerName;
                    worksheet.Cells["B" + j].Value = item.PhoneNumber;
                    worksheet.Cells["C" + j].Value = item.ApproveSendSms;
                    worksheet.Cells["D" + j].Value = item.Uwapproval;
                    worksheet.Cells["E" + j].Value = item.BranchAuthority;
                    worksheet.Cells["F" + j].Value = string.Format("{0:n0}", item.AgreedSiamount);
                    worksheet.Cells["G" + j].Value = item.InvoiceNo;
                    worksheet.Cells["H" + j].Value = item.PolicyNo;
                    worksheet.Cells["I" + j].Value = item.PersonInCharge;
                    worksheet.Cells["J" + j].Value = item.ResponsibleParty;
                    worksheet.Cells["K" + j].Value = item.CusCode;
                    worksheet.Cells["L" + j].Value = item.RiskName;
                    worksheet.Cells["M" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ExpiryDate);
                    worksheet.Cells["N" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.FromDate);
                    worksheet.Cells["O" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ToDate);
                    worksheet.Cells["P" + j].Value = string.Format("{0:n0}", item.RenewalPremium);
                    worksheet.Cells["Q" + j].Value = item.Product;
                    worksheet.Cells["R" + j].Value = item.Branch;
                    worksheet.Cells["S" + j].Value = item.AccountCode;
                    worksheet.Cells["T" + j].Value = item.IntermediaryType;
                    worksheet.Cells["U" + j].Value = item.AgentCode;
                    worksheet.Cells["V" + j].Value = item.AgentName;
                    worksheet.Cells["W" + j].Value = item.AuthorizedCode;
                    worksheet.Cells["X" + j].Value = item.AuthorizedName;
                    worksheet.Cells["Y" + j].Value = item.Class;
                    worksheet.Cells["Z" + j].Value = item.Currency;
                    worksheet.Cells["AA" + j].Value = string.Format("{0:n0}", item.SumInsured);
                    worksheet.Cells["AB" + j].Value = string.Format("{0:n0}", item.DetailOccupancy);
                    worksheet.Cells["AC" + j].Value = string.Format("{0:n0}", item.LocationRisk);
                    worksheet.Cells["AD" + j].Value = string.Format("{0:n0}", item.BuildingClass);
                    worksheet.Cells["AE" + j].Value = item.LossRecord;
                    worksheet.Cells["AF" + j].Value = item.Coverage;
                    worksheet.Cells["AG" + j].Value = item.GoogleMap;
                    worksheet.Cells["AH" + j].Value = item.AbctypeExtinguisher;
                    worksheet.Cells["AI" + j].Value = item.Co2typeExtinguishers;
                    worksheet.Cells["AJ" + j].Value = item.OtherExtinguisher;
                    worksheet.Cells["AK" + j].Value = item.FireExtinguishersWeight;
                    worksheet.Cells["AL" + j].Value = item.NumberofFireHoseReel;
                    worksheet.Cells["AM" + j].Value = item.NumberofHydrant;
                    worksheet.Cells["AN" + j].Value = item.CapacityofFireTank;
                    worksheet.Cells["AO" + j].Value = item.SecurityGuards;
                    worksheet.Cells["AP" + j].Value = item.NumberofCctvcamera;
                    worksheet.Cells["AQ" + j].Value = item.SqMeterofBuilding;
                    worksheet.Cells["AR" + j].Value = item.RequiredExtinguishers;
                    worksheet.Cells["AS" + j].Value = item.WorkinProcess;
                    worksheet.Cells["AT" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ApproveSendSmsdate);
                    worksheet.Cells["AU" + j].Value = item.JointMember;
                    worksheet.Cells["AV" + j].Value = item.DominantHolder;
                    worksheet.Cells["AW" + j].Value = item.NumberofFireTank;
                    worksheet.Cells["AX" + j].Value = item.TypeofFireTank;
                    worksheet.Cells["AY" + j].Value = string.Format("{0:n0}", item.Outstanding_Amt);
                    worksheet.Cells["AZ" + j].Value = string.Format("{0:n0}", item.Excess_Amt);
                    worksheet.Cells["BA" + j].Value = item.Financial_Interest;
                    worksheet.Cells["BB" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.BranchAuthorityDate);
                    foreach (var remarkinfo in lstremarkViewModels)
                    {
                        worksheet.Cells["BC" + j].Value += "[" + remarkinfo.Remark + "] ";
                    }

                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        #endregion

        #region Export HealthRenewalRegister

        public IActionResult ExportHealthRenewalRegisters()
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                List<HealthRenewalRegister> lstHealthRenewalRegisters = new List<HealthRenewalRegister>();
                lstHealthRenewalRegisters = HttpContext.Session.GetObjectFromJson<List<HealthRenewalRegister>>("lstHealthRenewalRegistersfilter");

                var columnHeader = new List<string>
                {
                    "Customer Name",
                    "Phone Number",
                    "Approve Send SMS",
                    "UW Approval",
                    "Branch Authority",
                    "Agreed SI Amount",
                    "Invoice No",
                    "Policy Number",
                    "PersonInCharge",
                    "Responsible Party",
                    "Customer Code",
                    "Risk Name",
                    "Expired Date",
                    "From Date",
                    "To Date",
                    "Renewal Premium",
                    "Product",
                    "Branch",
                    "Account Code",
                    "Intermediary Type",
                    "Agent Code",
                    "Agent Name",
                    "Authorized Code",
                    "Authorized Name",
                    "Product Class",
                    "Installment Plan",
                    "Basic Coverage",
                    "Optional Coverage (1)",
                    "Optional Coverage (2)",
                    "NCB/Claim Loading",
                    "Outstanding Amount",
                    "Excess Amount",
                    "Financial Interest",
                    "Send SMS Date",
                    "Branch Authority Date",
                    "Remark"
                };
                var fileContent = ExportExcelForHealthRenewalRegistersReport(lstHealthRenewalRegisters, columnHeader, "Health Renewal Registers Report");
                return File(fileContent, "application/ms-excel", "HealthRenewalRegisters.xlsx");
            }
        }

        public byte[] ExportExcelForHealthRenewalRegistersReport(List<HealthRenewalRegister> lst, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 30;
                using (var cells = worksheet.Cells[1, 1, 1, 60])
                    cells.Style.Font.Bold = true;

                for (int i = 0; i < columnHeader.Count(); i++)
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];

                worksheet.Column(36).Width = 100;

                var j = 2;
                var count = 1;
                foreach (var item in lst)
                {
                    #region Get RemarkInfo
                    List<RenewalRegisterRemarkViewModel> lstremarkViewModels = new List<RenewalRegisterRemarkViewModel>();
                    lstremarkViewModels = GetRenewalRegisterRemarkInfo(item.Id);
                    #endregion

                    worksheet.Cells["A" + j].Value = item.CustomerName;
                    worksheet.Cells["B" + j].Value = item.PhoneNumber;
                    worksheet.Cells["C" + j].Value = item.ApproveSendSms;
                    worksheet.Cells["D" + j].Value = item.Uwapproval;
                    worksheet.Cells["E" + j].Value = item.BranchAuthority;
                    worksheet.Cells["F" + j].Value = string.Format("{0:n0}", item.AgreedSiamount);
                    worksheet.Cells["G" + j].Value = item.InvoiceNo;
                    worksheet.Cells["H" + j].Value = item.PolicyNo;
                    worksheet.Cells["I" + j].Value = item.PersonInCharge;
                    worksheet.Cells["J" + j].Value = item.ResponsibleParty;
                    worksheet.Cells["K" + j].Value = item.CusCode;
                    worksheet.Cells["L" + j].Value = item.RiskName;
                    worksheet.Cells["M" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ExpiryDate);
                    worksheet.Cells["N" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.FromDate);
                    worksheet.Cells["O" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ToDate);
                    worksheet.Cells["P" + j].Value = string.Format("{0:n0}", item.RenewalPremium);
                    worksheet.Cells["Q" + j].Value = item.Product;
                    worksheet.Cells["R" + j].Value = item.Branch;
                    worksheet.Cells["S" + j].Value = item.AccountCode;
                    worksheet.Cells["T" + j].Value = item.IntermediaryType;
                    worksheet.Cells["U" + j].Value = item.AgentCode;
                    worksheet.Cells["V" + j].Value = item.AgentName;
                    worksheet.Cells["W" + j].Value = item.AuthorizedCode;
                    worksheet.Cells["X" + j].Value = item.AuthorizedName;
                    worksheet.Cells["Y" + j].Value = item.Class;
                    worksheet.Cells["Z" + j].Value = item.InstallmentPlan;
                    worksheet.Cells["AA" + j].Value = item.BasicCoverage;
                    worksheet.Cells["AB" + j].Value = item.OptionalCoverage1;
                    worksheet.Cells["AC" + j].Value = item.OptionalCoverage2;
                    worksheet.Cells["AD" + j].Value = string.Format("{0:n0}", item.NcbclaimLoading);
                    worksheet.Cells["AE" + j].Value = string.Format("{0:n0}", item.Outstanding_Amt);
                    worksheet.Cells["AF" + j].Value = string.Format("{0:n0}", item.Excess_Amt);
                    worksheet.Cells["AG" + j].Value = item.Financial_Interest;
                    worksheet.Cells["AH" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ApproveSendSmsdate);
                    worksheet.Cells["AI" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.BranchAuthorityDate);
                    foreach (var remarkinfo in lstremarkViewModels)
                    {
                        worksheet.Cells["AJ" + j].Value += "[" + remarkinfo.Remark + "] ";
                    }

                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        #endregion

        #region Export PARenewalRegister

        public IActionResult ExportPARenewalRegisters()
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                List<ParenewalRegister> lstPARenewalRegisters = new List<ParenewalRegister>();
                lstPARenewalRegisters = HttpContext.Session.GetObjectFromJson<List<ParenewalRegister>>("lstPARenewalRegistersfilter");

                var columnHeader = new List<string>
                {
                    "Customer Name",
                    "Phone Number",
                    "Approve Send SMS",
                    "UW Approval",
                    "Branch Authority",
                    "Agreed SI Amount",
                    "Invoice No",
                    "Policy Number",
                    "PersonInCharge",
                    "Responsible Party",
                    "Customer Code",
                    "Risk Name",
                    "Expired Date",
                    "From Date",
                    "To Date",
                    "Renewal Premium",
                    "Product",
                    "Branch",
                    "Account Code",
                    "Intermediary Type",
                    "Agent Code",
                    "Agent Name",
                    "Authorized Code",
                    "Authorized Name",
                    "Product Class",
                    "Installment Plan",
                    "Basic Coverage",
                    "Optional Coverage (1)",
                    "Optional Coverage (2)",
                    "NCB/Claim Loading",
                    "Outstanding Amount",
                    "Excess Amount",
                    "Financial Interest",
                    "Send SMS Date",
                    "Branch Authority Date",
                    "Remark"
                };
                var fileContent = ExportExcelForPARenewalRegistersReport(lstPARenewalRegisters, columnHeader, "PA Renewal Registers Report");
                return File(fileContent, "application/ms-excel", "PARenewalRegisters.xlsx");
            }
        }

        public byte[] ExportExcelForPARenewalRegistersReport(List<ParenewalRegister> lst, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 30;
                using (var cells = worksheet.Cells[1, 1, 1, 60])
                    cells.Style.Font.Bold = true;

                for (int i = 0; i < columnHeader.Count(); i++)
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];

                worksheet.Column(36).Width = 100;

                var j = 2;
                var count = 1;
                foreach (var item in lst)
                {
                    #region Get RemarkInfo
                    List<RenewalRegisterRemarkViewModel> lstremarkViewModels = new List<RenewalRegisterRemarkViewModel>();
                    lstremarkViewModels = GetRenewalRegisterRemarkInfo(item.Id);
                    #endregion

                    worksheet.Cells["A" + j].Value = item.CustomerName;
                    worksheet.Cells["B" + j].Value = item.PhoneNumber;
                    worksheet.Cells["C" + j].Value = item.ApproveSendSms;
                    worksheet.Cells["D" + j].Value = item.Uwapproval;
                    worksheet.Cells["E" + j].Value = item.BranchAuthority;
                    worksheet.Cells["F" + j].Value = string.Format("{0:n0}", item.AgreedSiamount);
                    worksheet.Cells["G" + j].Value = item.InvoiceNo;
                    worksheet.Cells["H" + j].Value = item.PolicyNo;
                    worksheet.Cells["I" + j].Value = item.PersonInCharge;
                    worksheet.Cells["J" + j].Value = item.ResponsibleParty;
                    worksheet.Cells["K" + j].Value = item.CusCode;
                    worksheet.Cells["L" + j].Value = item.RiskName;
                    worksheet.Cells["M" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ExpiryDate);
                    worksheet.Cells["N" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.FromDate);
                    worksheet.Cells["O" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ToDate);
                    worksheet.Cells["P" + j].Value = string.Format("{0:n0}", item.RenewalPremium);
                    worksheet.Cells["Q" + j].Value = item.Product;
                    worksheet.Cells["R" + j].Value = item.Branch;
                    worksheet.Cells["S" + j].Value = item.AccountCode;
                    worksheet.Cells["T" + j].Value = item.IntermediaryType;
                    worksheet.Cells["U" + j].Value = item.AgentCode;
                    worksheet.Cells["V" + j].Value = item.AgentName;
                    worksheet.Cells["W" + j].Value = item.AuthorizedCode;
                    worksheet.Cells["X" + j].Value = item.AuthorizedName;
                    worksheet.Cells["Y" + j].Value = item.Class;
                    worksheet.Cells["Z" + j].Value = item.InstallmentPlan;
                    worksheet.Cells["AA" + j].Value = item.BasicCoverage;
                    worksheet.Cells["AB" + j].Value = item.OptionalCoverage1;
                    worksheet.Cells["AC" + j].Value = item.OptionalCoverage2;
                    worksheet.Cells["AD" + j].Value = string.Format("{0:n0}", item.NcbclaimLoading);
                    worksheet.Cells["AE" + j].Value = string.Format("{0:n0}", item.Outstanding_Amt);
                    worksheet.Cells["AF" + j].Value = string.Format("{0:n0}", item.Excess_Amt);
                    worksheet.Cells["AG" + j].Value = item.Financial_Interest;
                    worksheet.Cells["AH" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ApproveSendSmsdate);
                    worksheet.Cells["AI" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.BranchAuthorityDate);
                    foreach (var remarkinfo in lstremarkViewModels)
                    {
                        worksheet.Cells["AJ" + j].Value += "[" + remarkinfo.Remark + "] ";
                    }

                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        #endregion

        #region Export EngineeringRenewalRegister

        public IActionResult ExportENGGRenewalRegisters()
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                List<EngineeringRenewalRegister> lstENGGRenewalRegisters = new List<EngineeringRenewalRegister>();
                lstENGGRenewalRegisters = HttpContext.Session.GetObjectFromJson<List<EngineeringRenewalRegister>>("lstENGGRenewalRegistersfilter");

                var columnHeader = new List<string>
                {
                    "Customer Name",
                    "Phone Number",
                    "Approve Send SMS",
                    "UW Approval",
                    "Branch Authority",
                    "DMS Upload",
                    "Done/Pending",
                    "Agreed SI Amount",
                    "Invoice No",
                    "Policy Number",
                    "PersonInCharge",
                    "Responsible Party",
                    "Customer Code",
                    "Risk Name",
                    "Expired Date",
                    "From Date",
                    "To Date",
                    "Renewal Premium",
                    "Product",
                    "Branch",
                    "Account Code",
                    "Intermediary Type",
                    "Agent Code",
                    "Agent Name",
                    "Authorized Code",
                    "Authorized Name",
                    "Product Class",
                    "Currency",
                    "SumInsured",
                    "Detail Occupancy",
                    "Location Risk",
                    "Building Class",
                    "Loss Record",
                    "Coverage",
                    "Google Map/ Geo Coordinate",
                    "Number of ABC Type Extinguisher",
                    "Number of CO2 Type Extinguishers",
                    "Number of Other Extinguisher",
                    "Weight of Fire Extinguisher",
                    "No. of Fire Hose Reel",
                    "No. of Hydrant",
                    "Capacity of Fire Tank",
                    "Security Guards",
                    "Number of CCTV Camera",
                    "Sq M of the Building",
                    "Required No. of Extinguishers",
                    "Work in process if there is any",
                    "Outstanding Amount",
                    "Excess Amount",
                    "Financial Interest",
                    "Send SMS Date",
                    "Brach Authority Date",
                    "Remark"
                };
                var fileContent = ExportExcelForENGGRenewalRegistersReport(lstENGGRenewalRegisters, columnHeader, "Engineering Renewal Registers Report");
                return File(fileContent, "application/ms-excel", "EngineeringRenewalRegisters.xlsx");
            }
        }

        public byte[] ExportExcelForENGGRenewalRegistersReport(List<EngineeringRenewalRegister> lst, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 30;
                using (var cells = worksheet.Cells[1, 1, 1, 60])
                    cells.Style.Font.Bold = true;

                for (int i = 0; i < columnHeader.Count(); i++)
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];

                worksheet.Column(53).Width = 100;

                var j = 2;
                var count = 1;
                foreach (var item in lst)
                {
                    #region Get RemarkInfo
                    List<RenewalRegisterRemarkViewModel> lstremarkViewModels = new List<RenewalRegisterRemarkViewModel>();
                    lstremarkViewModels = GetRenewalRegisterRemarkInfo(item.Id);
                    #endregion

                    worksheet.Cells["A" + j].Value = item.CustomerName;
                    worksheet.Cells["B" + j].Value = item.PhoneNumber;
                    worksheet.Cells["C" + j].Value = item.ApproveSendSms;
                    worksheet.Cells["D" + j].Value = item.Uwapproval;
                    worksheet.Cells["E" + j].Value = item.BranchAuthority;
                    worksheet.Cells["F" + j].Value = item.Dmsupload;
                    worksheet.Cells["G" + j].Value = item.DonePending;
                    worksheet.Cells["H" + j].Value = string.Format("{0:n0}", item.AgreedSiamount);
                    worksheet.Cells["I" + j].Value = item.InvoiceNo;
                    worksheet.Cells["J" + j].Value = item.PolicyNo;
                    worksheet.Cells["K" + j].Value = item.PersonInCharge;
                    worksheet.Cells["L" + j].Value = item.ResponsibleParty;
                    worksheet.Cells["M" + j].Value = item.CusCode;
                    worksheet.Cells["N" + j].Value = item.RiskName;
                    worksheet.Cells["O" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ExpiryDate);
                    worksheet.Cells["P" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.FromDate);
                    worksheet.Cells["Q" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ToDate);
                    worksheet.Cells["R" + j].Value = string.Format("{0:n0}", item.RenewalPremium);
                    worksheet.Cells["S" + j].Value = item.Product;
                    worksheet.Cells["T" + j].Value = item.Branch;
                    worksheet.Cells["U" + j].Value = item.AccountCode;
                    worksheet.Cells["V" + j].Value = item.IntermediaryType;
                    worksheet.Cells["W" + j].Value = item.AgentCode;
                    worksheet.Cells["X" + j].Value = item.AgentName;
                    worksheet.Cells["Y" + j].Value = item.AuthorizedCode;
                    worksheet.Cells["Z" + j].Value = item.AuthorizedName;
                    worksheet.Cells["AA" + j].Value = item.Class;
                    worksheet.Cells["AB" + j].Value = item.Currency;
                    worksheet.Cells["AC" + j].Value = string.Format("{0:n0}", item.SumInsured);
                    worksheet.Cells["AD" + j].Value = string.Format("{0:n0}", item.DetailOccupancy);
                    worksheet.Cells["AE" + j].Value = string.Format("{0:n0}", item.LocationRisk);
                    worksheet.Cells["AF" + j].Value = string.Format("{0:n0}", item.BuildingClass);
                    worksheet.Cells["AG" + j].Value = item.LossRecord;
                    worksheet.Cells["AH" + j].Value = item.Coverage;
                    worksheet.Cells["AI" + j].Value = item.GoogleMap;
                    worksheet.Cells["AJ" + j].Value = item.AbctypeExtinguisher;
                    worksheet.Cells["AK" + j].Value = item.Co2typeExtinguishers;
                    worksheet.Cells["AL" + j].Value = item.OtherExtinguisher;
                    worksheet.Cells["AM" + j].Value = item.FireExtinguishersWeight;
                    worksheet.Cells["AN" + j].Value = item.NumberofFireHoseReel;
                    worksheet.Cells["AO" + j].Value = item.NumberofHydrant;
                    worksheet.Cells["AP" + j].Value = item.CapacityofFireTank;
                    worksheet.Cells["AQ" + j].Value = item.SecurityGuards;
                    worksheet.Cells["AR" + j].Value = item.NumberofCctvcamera;
                    worksheet.Cells["AS" + j].Value = item.SqMeterofBuilding;
                    worksheet.Cells["AT" + j].Value = item.RequiredExtinguishers;
                    worksheet.Cells["AU" + j].Value = item.WorkinProcess;
                    worksheet.Cells["AV" + j].Value = string.Format("{0:n0}", item.Outstanding_Amt);
                    worksheet.Cells["AW" + j].Value = string.Format("{0:n0}", item.Excess_Amt);
                    worksheet.Cells["AX" + j].Value = item.Financial_Interest;
                    worksheet.Cells["AY" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ApproveSendSmsdate);
                    worksheet.Cells["AZ" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.BranchAuthorityDate);
                    foreach (var remarkinfo in lstremarkViewModels)
                    {
                        worksheet.Cells["BA" + j].Value += "[" + remarkinfo.Remark + "] ";
                    }
                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        #endregion

        #region Export PropertyRenewalRegister

        public IActionResult ExportPropertyRenewalRegisters()
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                List<PropertyRenewalRegister> lstPropertyRenewalRegisters = new List<PropertyRenewalRegister>();
                lstPropertyRenewalRegisters = HttpContext.Session.GetObjectFromJson<List<PropertyRenewalRegister>>("lstPropertyRenewalRegistersfilter");

                var columnHeader = new List<string>
                {
                    "Customer Name",
                    "Phone Number",
                    "Approve Send SMS",
                    "UW Approval",
                    "Branch Authority",
                    "DMS Upload",
                    "Done/Pending",
                    "Agreed SI Amount",
                    "Invoice No",
                    "Policy Number",
                    "PersonInCharge",
                    "Responsible Party",
                    "Customer Code",
                    "Risk Name",
                    "Expired Date",
                    "From Date",
                    "To Date",
                    "Renewal Premium",
                    "Product",
                    "Branch",
                    "Account Code",
                    "Intermediary Type",
                    "Agent Code",
                    "Agent Name",
                    "Authorized Code",
                    "Authorized Name",
                    "Product Class",
                    "Currency",
                    "SumInsured",
                    "Detail Occupancy",
                    "Location Risk",
                    "Building Class",
                    "Loss Record",
                    "Coverage",
                    "Google Map/ Geo Coordinate",
                    "Number of ABC Type Extinguisher",
                    "Number of CO2 Type Extinguishers",
                    "Number of Other Extinguisher",
                    "Weight of Fire Extinguisher",
                    "No. of Fire Hose Reel",
                    "No. of Hydrant",
                    "Capacity of Fire Tank",
                    "Security Guards",
                    "Number of CCTV Camera",
                    "Sq M of the Building",
                    "Required No. of Extinguishers",
                    "Work in process if there is any",
                    "Outstanding Amount",
                    "Excess Amount",
                    "Financial Interest",
                    "Send SMS Date",
                    "Branch Authority Date",
                    "Remark"
                };
                var fileContent = ExportExcelForPropertyRenewalRegistersReport(lstPropertyRenewalRegisters, columnHeader, "Property Renewal Registers Report");
                return File(fileContent, "application/ms-excel", "PropertyRenewalRegisters.xlsx");
            }
        }

        public byte[] ExportExcelForPropertyRenewalRegistersReport(List<PropertyRenewalRegister> lst, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 30;
                using (var cells = worksheet.Cells[1, 1, 1, 60])
                    cells.Style.Font.Bold = true;

                for (int i = 0; i < columnHeader.Count(); i++)
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];

                worksheet.Column(53).Width = 100;

                var j = 2;
                var count = 1;
                foreach (var item in lst)
                {
                    #region Get RemarkInfo
                    List<RenewalRegisterRemarkViewModel> lstremarkViewModels = new List<RenewalRegisterRemarkViewModel>();
                    lstremarkViewModels = GetRenewalRegisterRemarkInfo(item.Id);
                    #endregion

                    worksheet.Cells["A" + j].Value = item.CustomerName;
                    worksheet.Cells["B" + j].Value = item.PhoneNumber;
                    worksheet.Cells["C" + j].Value = item.ApproveSendSms;
                    worksheet.Cells["D" + j].Value = item.Uwapproval;
                    worksheet.Cells["E" + j].Value = item.BranchAuthority;
                    worksheet.Cells["F" + j].Value = item.Dmsupload;
                    worksheet.Cells["G" + j].Value = item.DonePending;
                    worksheet.Cells["H" + j].Value = string.Format("{0:n0}", item.AgreedSiamount);
                    worksheet.Cells["I" + j].Value = item.InvoiceNo;
                    worksheet.Cells["J" + j].Value = item.PolicyNo;
                    worksheet.Cells["K" + j].Value = item.PersonInCharge;
                    worksheet.Cells["L" + j].Value = item.ResponsibleParty;
                    worksheet.Cells["M" + j].Value = item.CusCode;
                    worksheet.Cells["N" + j].Value = item.RiskName;
                    worksheet.Cells["O" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ExpiryDate);
                    worksheet.Cells["P" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.FromDate);
                    worksheet.Cells["Q" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ToDate);
                    worksheet.Cells["R" + j].Value = string.Format("{0:n0}", item.RenewalPremium);
                    worksheet.Cells["S" + j].Value = item.Product;
                    worksheet.Cells["T" + j].Value = item.Branch;
                    worksheet.Cells["U" + j].Value = item.AccountCode;
                    worksheet.Cells["V" + j].Value = item.IntermediaryType;
                    worksheet.Cells["W" + j].Value = item.AgentCode;
                    worksheet.Cells["X" + j].Value = item.AgentName;
                    worksheet.Cells["Y" + j].Value = item.AuthorizedCode;
                    worksheet.Cells["Z" + j].Value = item.AuthorizedName;
                    worksheet.Cells["AA" + j].Value = item.Class;
                    worksheet.Cells["AB" + j].Value = item.Currency;
                    worksheet.Cells["Ac" + j].Value = string.Format("{0:n0}", item.SumInsured);
                    worksheet.Cells["AD" + j].Value = string.Format("{0:n0}", item.DetailOccupancy);
                    worksheet.Cells["AE" + j].Value = string.Format("{0:n0}", item.LocationRisk);
                    worksheet.Cells["AF" + j].Value = string.Format("{0:n0}", item.BuildingClass);
                    worksheet.Cells["AG" + j].Value = item.LossRecord;
                    worksheet.Cells["AH" + j].Value = item.Coverage;
                    worksheet.Cells["AI" + j].Value = item.GoogleMap;
                    worksheet.Cells["AJ" + j].Value = item.AbctypeExtinguisher;
                    worksheet.Cells["AK" + j].Value = item.Co2typeExtinguishers;
                    worksheet.Cells["AL" + j].Value = item.OtherExtinguisher;
                    worksheet.Cells["AM" + j].Value = item.FireExtinguishersWeight;
                    worksheet.Cells["AN" + j].Value = item.NumberofFireHoseReel;
                    worksheet.Cells["AO" + j].Value = item.NumberofHydrant;
                    worksheet.Cells["AP" + j].Value = item.CapacityofFireTank;
                    worksheet.Cells["AQ" + j].Value = item.SecurityGuards;
                    worksheet.Cells["AR" + j].Value = item.NumberofCctvcamera;
                    worksheet.Cells["AS" + j].Value = item.SqMeterofBuilding;
                    worksheet.Cells["AT" + j].Value = item.RequiredExtinguishers;
                    worksheet.Cells["AU" + j].Value = item.WorkinProcess;
                    worksheet.Cells["AV" + j].Value = string.Format("{0:n0}", item.Outstanding_Amt);
                    worksheet.Cells["AW" + j].Value = string.Format("{0:n0}", item.Excess_Amt);
                    worksheet.Cells["AX" + j].Value = item.Financial_Interest;
                    worksheet.Cells["AY" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ApproveSendSmsdate);
                    worksheet.Cells["AZ" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.BranchAuthorityDate);
                    foreach (var remarkinfo in lstremarkViewModels)
                    {
                        worksheet.Cells["BA" + j].Value += "[" + remarkinfo.Remark + "] ";
                    }
                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        #endregion

        #endregion

        #region Send SMS and Create Renewal Case
        public IActionResult SendRenewalSMS()
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }
            
            filterRenewalSMSDataModel filterRenewalSMSDataModel = new filterRenewalSMSDataModel();
            PrepareDataForRenewalReg();
            var message = (from c in _db.Category
                           where c.RenewalFlag == true
                           && c.Description == "Renewal Notice(EN)"
                           select c.MsgTemplate).FirstOrDefault();

            filterRenewalSMSDataModel.message = message.ToString();
            return View(filterRenewalSMSDataModel);
        }
        
        //[HttpPost]
        public IActionResult GetSMSRenewalRegisterList(filterRenewalSMSDataModel filterRenewalData)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            PrepareDataForRenewalReg();
            try
            {
                if (filterRenewalData.month == null)
                {
                    TempData["Message"] = "Invalid request data";
                    return View("SendRenewalSMS");
                }

                List<SMSRenewalViewModel> lstSMSRenewalViewModels = new List<SMSRenewalViewModel>();
                lstSMSRenewalViewModels = GetSMSRenewalRegisterSql(filterRenewalData);
                ViewBag.lstSMSRenewalViewModels = lstSMSRenewalViewModels;

                HttpContext.Session.SetObjectAsJson("lstSMSRenewalViewModels", null);
                HttpContext.Session.SetObjectAsJson("lstSMSRenewalViewModels", lstSMSRenewalViewModels);
                return View("SendRenewalSMS", filterRenewalData);
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        #region Excel Export For SendRenewalSMS

        public IActionResult ExportSendRenewalSMS()
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                List<SMSRenewalViewModel> lstSMSRenewalViewModels = new List<SMSRenewalViewModel>();
                lstSMSRenewalViewModels = HttpContext.Session.GetObjectFromJson<List<SMSRenewalViewModel>>("lstSMSRenewalViewModels");

                if (lstSMSRenewalViewModels == null)
                {
                    TempData["Message"] = "Invalid request data";
                    return RedirectToAction("SendRenewalSMS");
                }
                var columnHeader = new List<string>
                {
                    "Phone Number",
                    "Customer Name",
                    "Invoice No",
                    "Risk Name",
                    "Expired Date",
                    "Renewal Premium",
                    "Product",
                    "From Date",
                    "To Date",
                    "Link",
                    "Customer Code",
                    "Month",
                    "PersonInCharge",
                    "Responsiable Party",
                    "Policy Number",
                    "Branch",
                    "Intermediary Type",
                    "Agent Code",
                    "Agent Name",
                    "Account Code",
                    "Authorized Code",
                    "Authorized Name"
                };
                var fileContent = ExportExcelForSendRenewalSMSReport(lstSMSRenewalViewModels, columnHeader, "Send Renewal SMS Report");
                return File(fileContent, "application/ms-excel", "SendRenewalSMS.xlsx");
            }
        }

        public byte[] ExportExcelForSendRenewalSMSReport(List<SMSRenewalViewModel> lst, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 30;
                using (var cells = worksheet.Cells[1, 1, 1, 60])
                    cells.Style.Font.Bold = true;

                for (int i = 0; i < columnHeader.Count(); i++)
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];


                var j = 2;
                var count = 1;
                foreach (var item in lst)
                {
                    worksheet.Cells["A" + j].Value = item.PhoneNumber;
                    worksheet.Cells["B" + j].Value = item.CustomerName;
                    worksheet.Cells["C" + j].Value = item.InvoiceNo;
                    worksheet.Cells["D" + j].Value = item.RiskName;
                    worksheet.Cells["E" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ExpiryDate);
                    worksheet.Cells["F" + j].Value = item.RenewalPremium;
                    worksheet.Cells["G" + j].Value = item.Class;
                    worksheet.Cells["H" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.FromDate);
                    worksheet.Cells["I" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ToDate);
                    worksheet.Cells["J" + j].Value = item.Link;
                    worksheet.Cells["K" + j].Value = item.CusCode;
                    worksheet.Cells["L" + j].Value = item.Month;
                    worksheet.Cells["M" + j].Value = item.PersonInCharge;
                    worksheet.Cells["N" + j].Value = item.ResponsibleParty;
                    worksheet.Cells["O" + j].Value = item.PolicyNo;
                    worksheet.Cells["P" + j].Value = item.Branch;
                    worksheet.Cells["Q" + j].Value = item.IntermediaryType;
                    worksheet.Cells["R" + j].Value = item.AgentCode;
                    worksheet.Cells["S" + j].Value = item.AgentName;
                    worksheet.Cells["T" + j].Value = item.AccountCode;
                    worksheet.Cells["U" + j].Value = item.AuthorizedCode;
                    worksheet.Cells["V" + j].Value = item.AuthorizedName;
                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        #endregion

        public async Task<IActionResult> SendCreateRenewalSMS(string msg)
        {
            try
            {
                PrepareDataForRenewalReg();
                if (!CheckLoginUser())
                    return RedirectToAction("Login", "Account");

                if (HttpContext.Session.GetObjectFromJson<List<SMSRenewalViewModel>>("lstSMSRenewalViewModels") != null)
                {
                    List<SMSRenewalViewModel> lstSMSRenewalViewModels = new List<SMSRenewalViewModel>();
                    lstSMSRenewalViewModels = HttpContext.Session.GetObjectFromJson<List<SMSRenewalViewModel>>("lstSMSRenewalViewModels");

                    await SendSMSRenewalNoticeAsync(lstSMSRenewalViewModels, msg , true, false);

                    string SMSSuccess =  HttpContext.Session.GetString("successCountSMS");
                    string SMSFail =  HttpContext.Session.GetString("failCountSMS");
                    //string RenewalSuccess = HttpContext.Session.GetString("successCountRenewal");
                   //string RenewalFail = HttpContext.Session.GetString("failCountRenewal");
                    TempData["Message"] = "Total SMS sent = "+ SMSSuccess+ " success and "+ SMSFail +" fail.";
                }
                else
                {
                    TempData["Message"] = "Error in sending SMS and create case.";
                }
                return RedirectToAction("SendRenewalSMS");
            }
            catch (Exception ex)
            {
                return RedirectToAction("SendRenewalSMS");
            }
        }

        private async Task SendSMSRenewalNoticeAsync(List<SMSRenewalViewModel> lstSMSRenewalViewModels, string msg, bool chkSentSMS, bool isCustom)
        {
            int successCountSMS = 0;
            int failCountSMS = 0;

            int successCountRenewal = 0;
            int failCountRenewal = 0;
            try
            {
                List<SentSms> lstSentSMS = new List<SentSms>();
                decimal lenmessge = msg.Length;
                string Smscount = Convert.ToString(Math.Ceiling(lenmessge / 70));
                Smscount = Smscount == "0" ? "1" : Smscount;
                

                Category smsCategory = new Category();
                smsCategory = _db.Category.Where(c => c.Description == "Renewal Notice(EN)").FirstOrDefault();

                foreach (var renewal in lstSMSRenewalViewModels)
                {
                    SentSms sentSms = new SentSms();
                    // Create Renewal Case Activity
                    if(smsCategory.Status == true)
                    {
                        if (renewal.CusCode != null || renewal.CusCode != "")
                        {
                            string CRMActivityRetMsg = await CreateCRMActivityAsync(renewal.CusCode, smsCategory.Description);
                            if (CRMActivityRetMsg != "1")
                            {
                                sentSms.ActivitiesFlag = false;
                                renewal.ActivitiesFlag = false;
                            }
                            else
                            {
                                sentSms.ActivitiesFlag = true;
                                renewal.ActivitiesFlag = true;
                            }
                        }
                        else
                        {
                            sentSms.ActivitiesFlag = false;
                            renewal.ActivitiesFlag = false;
                        }
                    }

                    #region Prepare for Renewal SMS template
                    string smsmessage = string.Empty;
                    smsmessage = msg;
                    
                    if (renewal.InvoiceNo != null)
                    {
                        smsmessage = smsmessage.Replace("@InvoiceNo", "Invoice No :" + renewal.InvoiceNo);
                    }
                    else
                    {
                        smsmessage = smsmessage.Replace("@InvoiceNo", "Invoice No :" + "-");
                    }

                    if (renewal.Product != null)
                    {
                        smsmessage = smsmessage.Replace("@Product", renewal.Product);
                    }
                    else
                    {
                        smsmessage = smsmessage.Replace("@Product", "-");
                    }

                    if (renewal.ExpiryDate != null)
                    {
                        smsmessage = smsmessage.Replace("@ExpiryDate", "Expiry Date : " + string.Format("{0:dd/MMM/yyyy}", renewal.ExpiryDate));
                    }
                    else
                    {
                        smsmessage = smsmessage.Replace("@ExpiryDate", "Expiry Date : " + string.Format("{0:dd/MMM/yyyy}", "-1"));
                    }

                    renewal.Message = smsmessage;
                    renewal.SMSCount = Smscount;
                    renewal.CategoryID = smsCategory.Id;

                    bool smsFlag = false;
                    if(chkSentSMS == true)
                    {
                        if (renewal.PhoneNumber != null)
                        {
                            string PhoneNo = renewal.PhoneNumber.ToString();
                            PhoneNo = PhoneNo.Substring(1, PhoneNo.Length - 1); //Remove 'zero' from phoneNumber
                            PhoneNo = PhoneNo.Insert(0, "+95");// Add '+95' to phoneNumber
                            smsFlag = SmsService.InvokeServiceForSendSmsBulkRequest(PhoneNo, smsmessage);
                        }
                        if (smsFlag == true)
                        {
                            sentSms.Smsflag = "1";
                            renewal.Smsflag = true;
                            successCountSMS += 1;
                        }
                        else
                        {
                            sentSms.Smsflag = "0";
                            renewal.Smsflag = false;
                            failCountSMS += 1;
                        }
                    }

                    #endregion

                    #region Create Renewal Case 
                    if (smsCategory.RenewalFlag == true && chkSentSMS == false)
                    {
                        RenewalCaseInvoiceInfoResponse renewalCaseInvoiceInfoResponse = new RenewalCaseInvoiceInfoResponse();
                        renewalCaseInvoiceInfoResponse = GetRenewalInvoiceNoByInvoiceNo(renewal.InvoiceNo);

                        if (renewalCaseInvoiceInfoResponse.value.Count() > 0)
                        {
                            renewal.CrmrenewalCreateFlag = true;
                            successCountRenewal += 1;
                        }
                        else
                        {
                            string CreateRenewalCaseRetMsg = await CreateRenewalCaseAsync(renewal);
                            if (CreateRenewalCaseRetMsg != "1")
                            {
                                sentSms.CrmrenewalCreateFlag = false;
                                renewal.CrmrenewalCreateFlag = false;
                                failCountRenewal += 1;
                            }
                            else
                            {
                                sentSms.CrmrenewalCreateFlag = true;
                                renewal.CrmrenewalCreateFlag = true;
                                successCountRenewal += 1;
                            }
                        }

                    }
                    #endregion

                    #region Save SentSMS and Update Case Table 
                    if (chkSentSMS == false)
                    {
                        if(renewal.CrmrenewalCreateFlag == true)
                        {
                            SaveSentSMS(renewal);
                        }
                    }
                    else
                    {
                        SaveSentSMS(renewal);
                    }
                    
                    if (isCustom == false)
                    {
                        UpdateRenewalRegister(renewal);
                    }
                    else
                    {
                        if(renewal.CrmrenewalCreateFlag == true)
                        {
                            if(renewal.renewalregisterId != null)
                            {
                                UpdateNoSMSRenewalRegister(renewal);
                            }

                            DeleteTempData(renewal);
                        }
                    }

                    HttpContext.Session.SetString("successCountSMS", successCountSMS.ToString());
                    HttpContext.Session.SetString("failCountSMS", failCountSMS.ToString());
                    //HttpContext.Session.SetString("successCountRenewal", successCountRenewal.ToString());
                    //HttpContext.Session.SetString("failCountRenewal", failCountRenewal.ToString());
                    #endregion
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void DeleteTempData(SMSRenewalViewModel renewal)
        {
            try
            {
                TempRenewalCaseCreate tempRenewalCaseCreate = new TempRenewalCaseCreate();
                tempRenewalCaseCreate = _db.TempRenewalCaseCreate.Where(x => x.Id == renewal.temprenewalcaseId).FirstOrDefault();
                if(tempRenewalCaseCreate != null)
                {
                    _db.Entry(tempRenewalCaseCreate).State = EntityState.Deleted;
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateRenewalRegister(SMSRenewalViewModel renewal)
        {
            try
            {
                if(renewal.Class == "MOTOR INSURANCE")
                {
                    MotorRenewalRegister motorRenewalRegister = new MotorRenewalRegister();
                    motorRenewalRegister = _db.MotorRenewalRegister.Where(m => m.Id == renewal.Id).FirstOrDefault();
                    motorRenewalRegister.Smsflag = renewal.Smsflag;
                    motorRenewalRegister.CrmrenewalCreateFlag = renewal.CrmrenewalCreateFlag;
                    motorRenewalRegister.ActivitiesFlag = renewal.ActivitiesFlag;
                    _db.Entry(motorRenewalRegister).State = EntityState.Modified;
                    _db.SaveChanges();
                }
                else if(renewal.Class == "FIRE INSURANCE")
                {
                    FireRenewalRegister fireRenewalRegister = new FireRenewalRegister();
                    fireRenewalRegister = _db.FireRenewalRegister.Where(m => m.Id == renewal.Id).FirstOrDefault();
                    fireRenewalRegister.Smsflag = renewal.Smsflag;
                    fireRenewalRegister.CrmrenewalCreateFlag = renewal.CrmrenewalCreateFlag;
                    fireRenewalRegister.ActivitiesFlag = renewal.ActivitiesFlag;
                    _db.Entry(fireRenewalRegister).State = EntityState.Modified;
                    _db.SaveChanges();
                }
                else if(renewal.Class == "HEALTH INSURANCE")
                {
                    HealthRenewalRegister healthRenewalRegister = new HealthRenewalRegister();
                    healthRenewalRegister = _db.HealthRenewalRegister.Where(m => m.Id == renewal.Id).FirstOrDefault();
                    healthRenewalRegister.Smsflag = renewal.Smsflag;
                    healthRenewalRegister.CrmrenewalCreateFlag = renewal.CrmrenewalCreateFlag;
                    healthRenewalRegister.ActivitiesFlag = renewal.ActivitiesFlag;
                    _db.Entry(healthRenewalRegister).State = EntityState.Modified;
                    _db.SaveChanges();
                }
                else if(renewal.Class == "PERSONAL ACCIDENT INSURANCE")
                {
                    ParenewalRegister parenewalRegister = new ParenewalRegister();
                    parenewalRegister = _db.ParenewalRegister.Where(m => m.Id == renewal.Id).FirstOrDefault();
                    parenewalRegister.Smsflag = renewal.Smsflag;
                    parenewalRegister.CrmrenewalCreateFlag = renewal.ActivitiesFlag;
                    parenewalRegister.ActivitiesFlag = renewal.ActivitiesFlag;
                    _db.Entry(parenewalRegister).State = EntityState.Modified;
                    _db.SaveChanges();
                }
                else if(renewal.Class == "ENGINEERING")
                {
                    EngineeringRenewalRegister engineeringRenewalRegister = new EngineeringRenewalRegister();
                    engineeringRenewalRegister = _db.EngineeringRenewalRegister.Where(m => m.Id == renewal.Id).FirstOrDefault();
                    engineeringRenewalRegister.Smsflag = renewal.Smsflag;
                    engineeringRenewalRegister.CrmrenewalCreateFlag = renewal.ActivitiesFlag;
                    engineeringRenewalRegister.ActivitiesFlag = renewal.ActivitiesFlag;
                    _db.Entry(engineeringRenewalRegister).State = EntityState.Modified;
                    _db.SaveChanges();
                }
                else if(renewal.Class == "PROPERTY")
                {
                    PropertyRenewalRegister propertyRenewalRegister = new PropertyRenewalRegister();
                    propertyRenewalRegister = _db.PropertyRenewalRegister.Where(m => m.Id == renewal.Id).FirstOrDefault();
                    propertyRenewalRegister.Smsflag = renewal.Smsflag;
                    propertyRenewalRegister.CrmrenewalCreateFlag = renewal.ActivitiesFlag;
                    propertyRenewalRegister.ActivitiesFlag = renewal.ActivitiesFlag;
                    _db.Entry(propertyRenewalRegister).State = EntityState.Modified;
                    _db.SaveChanges();
                }
            }
            catch(Exception ex)
            {

            }
        }

        //ForNoSMS
        private void UpdateNoSMSRenewalRegister(SMSRenewalViewModel renewal)
        {
            try
            {
                if (renewal.Class == "MOTOR INSURANCE")
                {
                    MotorRenewalRegister motorRenewalRegister = new MotorRenewalRegister();
                    motorRenewalRegister = _db.MotorRenewalRegister.Where(m => m.Id == renewal.renewalregisterId).FirstOrDefault();
                    motorRenewalRegister.Smsflag = renewal.Smsflag;
                    motorRenewalRegister.CrmrenewalCreateFlag = renewal.CrmrenewalCreateFlag;
                    motorRenewalRegister.CrmrenewalCreatedDate = DateTime.Now.Date;
                    motorRenewalRegister.ActivitiesFlag = renewal.ActivitiesFlag;
                    _db.Entry(motorRenewalRegister).State = EntityState.Modified;
                    _db.SaveChanges();
                }
                else if (renewal.Class == "FIRE INSURANCE")
                {
                    FireRenewalRegister fireRenewalRegister = new FireRenewalRegister();
                    fireRenewalRegister = _db.FireRenewalRegister.Where(m => m.Id == renewal.renewalregisterId).FirstOrDefault();
                    fireRenewalRegister.Smsflag = renewal.Smsflag;
                    fireRenewalRegister.CrmrenewalCreateFlag = renewal.CrmrenewalCreateFlag;
                    fireRenewalRegister.CrmrenewalCreatedDate = DateTime.Now.Date;
                    fireRenewalRegister.ActivitiesFlag = renewal.ActivitiesFlag;
                    _db.Entry(fireRenewalRegister).State = EntityState.Modified;
                    _db.SaveChanges();
                }
                else if (renewal.Class == "HEALTH INSURANCE")
                {
                    HealthRenewalRegister healthRenewalRegister = new HealthRenewalRegister();
                    healthRenewalRegister = _db.HealthRenewalRegister.Where(m => m.Id == renewal.renewalregisterId).FirstOrDefault();
                    healthRenewalRegister.Smsflag = renewal.Smsflag;
                    healthRenewalRegister.CrmrenewalCreateFlag = renewal.CrmrenewalCreateFlag;
                    healthRenewalRegister.CrmrenewalCreatedDate = DateTime.Now.Date;
                    healthRenewalRegister.ActivitiesFlag = renewal.ActivitiesFlag;
                    _db.Entry(healthRenewalRegister).State = EntityState.Modified;
                    _db.SaveChanges();
                }
                else if (renewal.Class == "PERSONAL ACCIDENT INSURANCE")
                {
                    ParenewalRegister parenewalRegister = new ParenewalRegister();
                    parenewalRegister = _db.ParenewalRegister.Where(m => m.Id == renewal.renewalregisterId).FirstOrDefault();
                    parenewalRegister.Smsflag = renewal.Smsflag;
                    parenewalRegister.CrmrenewalCreateFlag = renewal.CrmrenewalCreateFlag;
                    parenewalRegister.CrmrenewalCreatedDate = DateTime.Now.Date;
                    parenewalRegister.ActivitiesFlag = renewal.ActivitiesFlag;
                    _db.Entry(parenewalRegister).State = EntityState.Modified;
                    _db.SaveChanges();
                }
                else if (renewal.Class == "ENGINEERING")
                {
                    EngineeringRenewalRegister engineeringRenewalRegister = new EngineeringRenewalRegister();
                    engineeringRenewalRegister = _db.EngineeringRenewalRegister.Where(m => m.Id == renewal.renewalregisterId).FirstOrDefault();
                    engineeringRenewalRegister.Smsflag = renewal.Smsflag;
                    engineeringRenewalRegister.CrmrenewalCreateFlag = renewal.CrmrenewalCreateFlag;
                    engineeringRenewalRegister.CrmrenewalCreatedDate = DateTime.Now.Date;
                    engineeringRenewalRegister.ActivitiesFlag = renewal.ActivitiesFlag;
                    _db.Entry(engineeringRenewalRegister).State = EntityState.Modified;
                    _db.SaveChanges();
                }
                else if (renewal.Class == "PROPERTY")
                {
                    PropertyRenewalRegister propertyRenewalRegister = new PropertyRenewalRegister();
                    propertyRenewalRegister = _db.PropertyRenewalRegister.Where(m => m.Id == renewal.renewalregisterId).FirstOrDefault();
                    propertyRenewalRegister.Smsflag = renewal.Smsflag;
                    propertyRenewalRegister.CrmrenewalCreateFlag = renewal.CrmrenewalCreateFlag;
                    propertyRenewalRegister.CrmrenewalCreatedDate = DateTime.Now.Date;
                    propertyRenewalRegister.ActivitiesFlag = renewal.ActivitiesFlag;
                    _db.Entry(propertyRenewalRegister).State = EntityState.Modified;
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void SaveSentSMS(SMSRenewalViewModel renewal)
        {
            try
            {
                
                SentSms sentSms = new SentSms();
                sentSms.Id = Guid.NewGuid();
                sentSms.PhoneNumber = renewal.PhoneNumber;
                sentSms.UserName = renewal.CustomerName;
                sentSms.SysId = Guid.Parse(HttpContext.Session.GetString("userId"));
                sentSms.Message = renewal.Message;
                sentSms.SentDate = DateTime.Now;
                sentSms.Channel = "RenewalNotice";
                sentSms.VehicleNo = renewal.RiskName;
                if(renewal.Smsflag == true)
                {
                    sentSms.Smsflag = "1";
                }
                else
                {
                    sentSms.Smsflag = "0";
                }
                sentSms.Smscount = renewal.SMSCount;
                sentSms.CategoryId = renewal.CategoryID;
                sentSms.PolicyNo = renewal.InvoiceNo;
                sentSms.ExpireDate = renewal.ExpiryDate;
                sentSms.RenewalPremium = renewal.RenewalPremium;
                sentSms.Product = renewal.Product;
                sentSms.FromDate = renewal.FromDate;
                sentSms.ToDate = renewal.ToDate;
                sentSms.Link = renewal.Link;
                sentSms.CusCode = renewal.CusCode;
                sentSms.ActivitiesFlag = renewal.ActivitiesFlag;
                sentSms.Month = renewal.Month;
                sentSms.Pic = renewal.PersonInCharge;
                sentSms.CrmrenewalCreateFlag = renewal.CrmrenewalCreateFlag;
                sentSms.ResponsibleParty = renewal.ResponsibleParty;

                _db.Entry(sentSms).State = EntityState.Added;
                _db.SaveChanges();
            }
            catch(Exception ex)
            {

            }
        }

        private List<SMSRenewalViewModel> GetSMSRenewalRegisterSql(filterRenewalSMSDataModel filterRenewalData)
        {
            List<SMSRenewalViewModel> lstSMSRenewalViewModels = new List<SMSRenewalViewModel>();
            try
            {
                lstSMSRenewalViewModels = dataService.GetSMSRenewalListByFilter(filterRenewalData);
                return lstSMSRenewalViewModels;
            }
            catch (Exception ex)
            {
                return lstSMSRenewalViewModels;
            }
        }
        
        #endregion

        #region Sync Invoice Info

        public async Task<JsonResult> SyncRenewalInvoiceUpdate(string id, string productclass)
        {
            try
            {
                if (productclass == "MT")
                {
                    MotorRenewalRegister motorRenewalRegister = new MotorRenewalRegister();
                    await UpdateMotorInvoiceInfoAsync(Guid.Parse(id));
                    motorRenewalRegister = _db.MotorRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                    return Json(motorRenewalRegister);
                }
                else if (productclass == "FI")
                {
                    FireRenewalRegister fireRenewalRegister = new FireRenewalRegister();
                    await UpdateFireInvoiceInfoAsync(Guid.Parse(id));
                    fireRenewalRegister = _db.FireRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                    return Json(fireRenewalRegister);
                }
                else if (productclass == "LF")
                {
                    HealthRenewalRegister healthRenewalRegister = new HealthRenewalRegister();
                    await UpdateHealthInvoiceInfoAsync(Guid.Parse(id));
                    healthRenewalRegister = _db.HealthRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                    return Json(healthRenewalRegister);
                }
                else if (productclass == "MS")
                {
                    ParenewalRegister parenewalRegister = new ParenewalRegister();
                    await UpdatePAInvoiceInfoAsync(Guid.Parse(id));
                    parenewalRegister = _db.ParenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                    return Json(parenewalRegister);
                }
                else if (productclass == "ENGG")
                {
                    EngineeringRenewalRegister engineeringRenewalRegister = new EngineeringRenewalRegister();
                    await UpdateEngineeringInvoiceInfoAsync(Guid.Parse(id));
                    engineeringRenewalRegister = _db.EngineeringRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                    return Json(engineeringRenewalRegister);
                }
                else if (productclass == "PTY")
                {
                    PropertyRenewalRegister propertyRenewalRegister = new PropertyRenewalRegister();
                    await UpdatePropertyInvoiceInfoAsync(Guid.Parse(id));
                    propertyRenewalRegister = _db.PropertyRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                    return Json(propertyRenewalRegister);
                }
                return Json("-2");
            }
            catch (Exception ex)
            {
                return Json("-1");
            }

        }

        private async Task UpdateMotorInvoiceInfoAsync(Guid id)
        {
            try
            {
                MotorInvoiceInfoViewModel invoiceInfo = new MotorInvoiceInfoViewModel();
                MotorRenewalRegister motorRenewalRegister = new MotorRenewalRegister();
                motorRenewalRegister = _db.MotorRenewalRegister.Where(m => m.Id == id).FirstOrDefault();

                invoiceInfo = await GetMotorInvoiceInfoAsync(motorRenewalRegister.PolicyNo);
                if (invoiceInfo.CustomerName != null && invoiceInfo.PolicyNo != null)
                {
                    if(invoiceInfo.ToDate > motorRenewalRegister.ExpiryDate)
                    {
                        motorRenewalRegister.InvoiceNo = invoiceInfo.deb_deb_note_no;
                    }

                    motorRenewalRegister.RenewalPremium = Convert.ToDecimal(invoiceInfo.renewal_premium);
                    motorRenewalRegister.Manufacturer = invoiceInfo.manufacturer;
                    motorRenewalRegister.Car_Model = invoiceInfo.car_model;
                    motorRenewalRegister.RenewedWindscreenValue = Convert.ToDecimal(invoiceInfo.renewalwinscreenvalue);
                    motorRenewalRegister.RenewedSivalue = Convert.ToDecimal(invoiceInfo.si);

                    if(motorRenewalRegister.PhoneNumber == null)
                    {
                        motorRenewalRegister.PhoneNumber = invoiceInfo.PhoneNumber;
                    }
                    
                    motorRenewalRegister.CustomerName = invoiceInfo.CustomerName;
                    motorRenewalRegister.RiskName = invoiceInfo.RiskName;
                    //motorRenewalRegister.ExpiryDate = Convert.ToDateTime(invoiceInfo.ExpiryDate);
                    motorRenewalRegister.FromDate = invoiceInfo.FromDate;
                    motorRenewalRegister.ToDate = invoiceInfo.ToDate;
                    motorRenewalRegister.Branch = invoiceInfo.Branch;

                    motorRenewalRegister.IntermediaryType = invoiceInfo.IntermediaryType;
                    motorRenewalRegister.AgentCode = invoiceInfo.AgentCode;
                    motorRenewalRegister.AgentName = invoiceInfo.AgentName;
                    motorRenewalRegister.AuthorizedCode = invoiceInfo.AuthorizedCode;
                    motorRenewalRegister.AuthorizedName = invoiceInfo.AuthorizedName;

                    motorRenewalRegister.SumInsured = Convert.ToDecimal(invoiceInfo.SumInsured);
                    //motorRenewalRegister.Sivalue20 = Convert.ToDecimal(invoiceInfo.Sivalue20);
                    motorRenewalRegister.AccountCode = invoiceInfo.account_code;
                    motorRenewalRegister.Outstanding_Amt = invoiceInfo.outstanding_amt;
                    motorRenewalRegister.Excess_Amt = invoiceInfo.excess_amt;
                    motorRenewalRegister.Financial_Interest = invoiceInfo.financial_interest;

                    _db.Update(motorRenewalRegister);
                    _db.SaveChanges();

                }
            }
            catch (Exception ex)
            {

            }
        }

        private async Task UpdateFireInvoiceInfoAsync(Guid id)
        {
            try
            {
                FireInvoiceInfoViewModel invoiceInfo = new FireInvoiceInfoViewModel();
                FireRenewalRegister fireRenewalRegister = new FireRenewalRegister();
                fireRenewalRegister = _db.FireRenewalRegister.Where(m => m.Id == id).FirstOrDefault();
                invoiceInfo = await GetFireInvoiceInfoAsync(fireRenewalRegister.PolicyNo);
                if (invoiceInfo.CustomerName != null && invoiceInfo.PolicyNo != null)
                {
                    if(invoiceInfo.ToDate > fireRenewalRegister.ExpiryDate)
                    {
                        fireRenewalRegister.InvoiceNo = invoiceInfo.deb_deb_note_no;
                    }

                    fireRenewalRegister.RenewalPremium = Convert.ToDecimal(invoiceInfo.renewal_premium);

                    if(fireRenewalRegister.PhoneNumber == null)
                    {
                        fireRenewalRegister.PhoneNumber = invoiceInfo.PhoneNumber;
                    }

                    fireRenewalRegister.CustomerName = invoiceInfo.CustomerName;
                    fireRenewalRegister.RiskName = invoiceInfo.RiskName;

                    //fireRenewalRegister.ExpiryDate = invoiceInfo.ExpiryDate;
                    fireRenewalRegister.FromDate = invoiceInfo.FromDate;
                    fireRenewalRegister.ToDate = invoiceInfo.ToDate;
                    fireRenewalRegister.Branch = invoiceInfo.Branch;
                    fireRenewalRegister.AccountCode = invoiceInfo.account_code;
                    fireRenewalRegister.IntermediaryType = invoiceInfo.IntermediaryType;
                    fireRenewalRegister.AgentCode = invoiceInfo.AgentCode;

                    fireRenewalRegister.AgentName = invoiceInfo.AgentName;
                    fireRenewalRegister.AuthorizedCode = invoiceInfo.AuthorizedCode;
                    fireRenewalRegister.AuthorizedName = invoiceInfo.AuthorizedName;
                    fireRenewalRegister.SumInsured = invoiceInfo.SumInsured;
                    fireRenewalRegister.DetailOccupancy = invoiceInfo.DetailOccupancy;

                    fireRenewalRegister.LocationRisk = invoiceInfo.LocationRisk;
                    fireRenewalRegister.BuildingClass = invoiceInfo.BuildingClass;
                    fireRenewalRegister.LossRecord = invoiceInfo.LossRecord;
                    fireRenewalRegister.Coverage = invoiceInfo.Coverage;
                    fireRenewalRegister.GoogleMap = invoiceInfo.GoogleMap;

                    fireRenewalRegister.AbctypeExtinguisher = invoiceInfo.AbctypeExtinguisher;
                    fireRenewalRegister.Co2typeExtinguishers = invoiceInfo.Co2typeExtinguishers;
                    fireRenewalRegister.OtherExtinguisher = invoiceInfo.OtherExtinguisher;
                    fireRenewalRegister.FireExtinguishersWeight = invoiceInfo.FireExtinguishersWeight;
                    fireRenewalRegister.NumberofFireHoseReel = invoiceInfo.NumberofFireHoseReel;

                    fireRenewalRegister.NumberofHydrant = invoiceInfo.NumberofHydrant;
                    fireRenewalRegister.CapacityofFireTank = invoiceInfo.CapacityofFireTank;
                    fireRenewalRegister.SecurityGuards = invoiceInfo.SecurityGuards;
                    fireRenewalRegister.NumberofCctvcamera = invoiceInfo.NumberofCctvcamera;
                    fireRenewalRegister.SqMeterofBuilding = invoiceInfo.SqMeterofBuilding;

                    fireRenewalRegister.RequiredExtinguishers = invoiceInfo.RequiredExtinguishers;
                    fireRenewalRegister.WorkinProcess = invoiceInfo.WorkinProcess;
                    fireRenewalRegister.NumberofFireTank = invoiceInfo.number_of_fire_tank;
                    fireRenewalRegister.TypeofFireTank = invoiceInfo.type_of_fire_tank;
                    fireRenewalRegister.Outstanding_Amt = invoiceInfo.outstanding_amt;

                    fireRenewalRegister.Excess_Amt = invoiceInfo.excess_amt;
                    fireRenewalRegister.Financial_Interest = invoiceInfo.financial_interest;

                    _db.Update(fireRenewalRegister);
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private async Task UpdateHealthInvoiceInfoAsync(Guid id)
        {
            try
            {
                HealthInvoiceInfoViewModel invoiceInfo = new HealthInvoiceInfoViewModel();
                HealthRenewalRegister healthRenewalRegister = new HealthRenewalRegister();
                healthRenewalRegister = _db.HealthRenewalRegister.Where(m => m.Id == id).FirstOrDefault();
                invoiceInfo = await GetHealthInvoiceInfoAsync(healthRenewalRegister.PolicyNo);
                if(invoiceInfo.CustomerName != null && invoiceInfo.PolicyNo != null)
                {
                    if(invoiceInfo.ToDate > healthRenewalRegister.ExpiryDate)
                    {
                        healthRenewalRegister.InvoiceNo = invoiceInfo.deb_deb_note_no;
                    }

                    healthRenewalRegister.RenewalPremium = Convert.ToDecimal(invoiceInfo.renewal_premium);

                    if(healthRenewalRegister.PhoneNumber == null)
                    {
                        healthRenewalRegister.PhoneNumber = invoiceInfo.PhoneNumber;
                    }
                    healthRenewalRegister.CustomerName = invoiceInfo.CustomerName;
                    healthRenewalRegister.RiskName = invoiceInfo.RiskName;
                    healthRenewalRegister.FromDate = invoiceInfo.FromDate;
                    healthRenewalRegister.ToDate = invoiceInfo.ToDate;
                    healthRenewalRegister.Branch = invoiceInfo.Branch;
                    healthRenewalRegister.IntermediaryType = invoiceInfo.IntermediaryType;
                    healthRenewalRegister.AgentCode = invoiceInfo.AgentCode;
                    healthRenewalRegister.AgentName = invoiceInfo.AgentName;
                    healthRenewalRegister.AccountCode = invoiceInfo.account_code;
                    healthRenewalRegister.AuthorizedCode = invoiceInfo.AuthorizedCode;
                    healthRenewalRegister.AuthorizedName = invoiceInfo.AuthorizedName;
                    healthRenewalRegister.InstallmentPlan = invoiceInfo.installment_plan;
                    healthRenewalRegister.BasicCoverage = invoiceInfo.basic_coverage;
                    healthRenewalRegister.OptionalCoverage1 = invoiceInfo.optional_coverage_1;
                    healthRenewalRegister.OptionalCoverage2 = invoiceInfo.optional_coverage_2;
                    healthRenewalRegister.NcbclaimLoading = invoiceInfo.claim_loading;
                    healthRenewalRegister.Outstanding_Amt = invoiceInfo.outstanding_amt;
                    healthRenewalRegister.Excess_Amt = invoiceInfo.excess_amt;
                    healthRenewalRegister.Financial_Interest = invoiceInfo.financial_interest;

                    _db.Update(healthRenewalRegister);
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private async Task UpdatePAInvoiceInfoAsync(Guid id)
        {
            try
            {
                PAInvoiceInfoViewModel invoiceInfo = new PAInvoiceInfoViewModel();
                ParenewalRegister parenewalRegister = new ParenewalRegister();
                parenewalRegister = _db.ParenewalRegister.Where(m => m.Id == id).FirstOrDefault();
                invoiceInfo = await GetPAInvoiceInfoAsync(parenewalRegister.PolicyNo);
                if(invoiceInfo.CustomerName != null && invoiceInfo.PolicyNo != null)
                {
                    if (invoiceInfo.ToDate > parenewalRegister.ExpiryDate)
                    {
                        parenewalRegister.InvoiceNo = invoiceInfo.deb_deb_note_no;
                    }

                    parenewalRegister.RenewalPremium = Convert.ToDecimal(invoiceInfo.renewal_premium);

                    if (parenewalRegister.PhoneNumber == null)
                    {
                        parenewalRegister.PhoneNumber = invoiceInfo.PhoneNumber;
                    }
                    parenewalRegister.CustomerName = invoiceInfo.CustomerName;
                    parenewalRegister.RiskName = invoiceInfo.RiskName;
                    parenewalRegister.FromDate = invoiceInfo.FromDate;
                    parenewalRegister.ToDate = invoiceInfo.ToDate;
                    parenewalRegister.Branch = invoiceInfo.Branch;
                    parenewalRegister.IntermediaryType = invoiceInfo.IntermediaryType;
                    parenewalRegister.AgentCode = invoiceInfo.AgentCode;
                    parenewalRegister.AgentName = invoiceInfo.AgentName;
                    parenewalRegister.AccountCode = invoiceInfo.account_code;
                    parenewalRegister.AuthorizedCode = invoiceInfo.AuthorizedCode;
                    parenewalRegister.AuthorizedName = invoiceInfo.AuthorizedName;
                    parenewalRegister.InstallmentPlan = invoiceInfo.installment_plan;
                    parenewalRegister.BasicCoverage = invoiceInfo.basic_coverage;
                    parenewalRegister.OptionalCoverage1 = invoiceInfo.optional_coverage_1;
                    parenewalRegister.OptionalCoverage2 = invoiceInfo.optional_coverage_2;
                    parenewalRegister.NcbclaimLoading = invoiceInfo.claim_loading;
                    parenewalRegister.Outstanding_Amt = invoiceInfo.outstanding_amt;
                    parenewalRegister.Excess_Amt = invoiceInfo.excess_amt;
                    parenewalRegister.Financial_Interest = invoiceInfo.financial_interest;

                    _db.Update(parenewalRegister);
                    _db.SaveChanges();
                }
            }
            catch(Exception ex)
            {

            }
        }

        private async Task UpdateEngineeringInvoiceInfoAsync(Guid id)
        {
            try
            {
                EngineeringInvoiceInfoViewModel invoiceInfo = new EngineeringInvoiceInfoViewModel();
                EngineeringRenewalRegister engineeringRenewalRegister = new EngineeringRenewalRegister();
                engineeringRenewalRegister = _db.EngineeringRenewalRegister.Where(m => m.Id == id).FirstOrDefault();
                invoiceInfo = await GetEngineeringInvoiceInfoAsync(engineeringRenewalRegister.PolicyNo);
                if (invoiceInfo.CustomerName != null && invoiceInfo.PolicyNo != null)
                {
                    if (invoiceInfo.ToDate > engineeringRenewalRegister.ExpiryDate)
                    {
                        engineeringRenewalRegister.InvoiceNo = invoiceInfo.deb_deb_note_no;
                    }

                    engineeringRenewalRegister.RenewalPremium = Convert.ToDecimal(invoiceInfo.renewal_premium);

                    if (engineeringRenewalRegister.PhoneNumber == null)
                    {
                        engineeringRenewalRegister.PhoneNumber = invoiceInfo.PhoneNumber;
                    }

                    engineeringRenewalRegister.CustomerName = invoiceInfo.CustomerName;
                    engineeringRenewalRegister.RiskName = invoiceInfo.RiskName;

                    //fireRenewalRegister.ExpiryDate = invoiceInfo.ExpiryDate;
                    engineeringRenewalRegister.FromDate = invoiceInfo.FromDate;
                    engineeringRenewalRegister.ToDate = invoiceInfo.ToDate;
                    engineeringRenewalRegister.Branch = invoiceInfo.Branch;
                    engineeringRenewalRegister.AccountCode = invoiceInfo.account_code;
                    engineeringRenewalRegister.IntermediaryType = invoiceInfo.IntermediaryType;
                    engineeringRenewalRegister.AgentCode = invoiceInfo.AgentCode;

                    engineeringRenewalRegister.AgentName = invoiceInfo.AgentName;
                    engineeringRenewalRegister.AuthorizedCode = invoiceInfo.AuthorizedCode;
                    engineeringRenewalRegister.AuthorizedName = invoiceInfo.AuthorizedName;
                    engineeringRenewalRegister.SumInsured = invoiceInfo.SumInsured;
                    engineeringRenewalRegister.DetailOccupancy = invoiceInfo.DetailOccupancy;

                    engineeringRenewalRegister.LocationRisk = invoiceInfo.LocationRisk;
                    engineeringRenewalRegister.BuildingClass = invoiceInfo.BuildingClass;
                    engineeringRenewalRegister.LossRecord = invoiceInfo.LossRecord;
                    engineeringRenewalRegister.Coverage = invoiceInfo.Coverage;
                    engineeringRenewalRegister.GoogleMap = invoiceInfo.GoogleMap;

                    engineeringRenewalRegister.AbctypeExtinguisher = invoiceInfo.AbctypeExtinguisher;
                    engineeringRenewalRegister.Co2typeExtinguishers = invoiceInfo.Co2typeExtinguishers;
                    engineeringRenewalRegister.OtherExtinguisher = invoiceInfo.OtherExtinguisher;
                    engineeringRenewalRegister.FireExtinguishersWeight = invoiceInfo.FireExtinguishersWeight;
                    engineeringRenewalRegister.NumberofFireHoseReel = invoiceInfo.NumberofFireHoseReel;

                    engineeringRenewalRegister.NumberofHydrant = invoiceInfo.NumberofHydrant;
                    engineeringRenewalRegister.CapacityofFireTank = invoiceInfo.CapacityofFireTank;
                    engineeringRenewalRegister.SecurityGuards = invoiceInfo.SecurityGuards;
                    engineeringRenewalRegister.NumberofCctvcamera = invoiceInfo.NumberofCctvcamera;
                    engineeringRenewalRegister.SqMeterofBuilding = invoiceInfo.SqMeterofBuilding;

                    engineeringRenewalRegister.RequiredExtinguishers = invoiceInfo.RequiredExtinguishers;
                    engineeringRenewalRegister.WorkinProcess = invoiceInfo.WorkinProcess;
                    engineeringRenewalRegister.NumberofFireTank = invoiceInfo.number_of_fire_tank;
                    engineeringRenewalRegister.TypeofFireTank = invoiceInfo.type_of_fire_tank;
                    engineeringRenewalRegister.Outstanding_Amt = invoiceInfo.outstanding_amt;

                    engineeringRenewalRegister.Excess_Amt = invoiceInfo.excess_amt;
                    engineeringRenewalRegister.Financial_Interest = invoiceInfo.financial_interest;

                    _db.Update(engineeringRenewalRegister);
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private async Task UpdatePropertyInvoiceInfoAsync(Guid id)
        {
            try
            {
                PropertyInvoiceInfoViewModel invoiceInfo = new PropertyInvoiceInfoViewModel();
                PropertyRenewalRegister propertyRenewalRegister = new PropertyRenewalRegister();
                propertyRenewalRegister = _db.PropertyRenewalRegister.Where(m => m.Id == id).FirstOrDefault();
                invoiceInfo = await GetPropertyInvoiceInfoAsync(propertyRenewalRegister.PolicyNo);
                if (invoiceInfo.CustomerName != null && invoiceInfo.PolicyNo != null)
                {
                    if (invoiceInfo.ToDate > propertyRenewalRegister.ExpiryDate)
                    {
                        propertyRenewalRegister.InvoiceNo = invoiceInfo.deb_deb_note_no;
                    }

                    propertyRenewalRegister.RenewalPremium = Convert.ToDecimal(invoiceInfo.renewal_premium);

                    if (propertyRenewalRegister.PhoneNumber == null)
                    {
                        propertyRenewalRegister.PhoneNumber = invoiceInfo.PhoneNumber;
                    }

                    propertyRenewalRegister.CustomerName = invoiceInfo.CustomerName;
                    propertyRenewalRegister.RiskName = invoiceInfo.RiskName;

                    //fireRenewalRegister.ExpiryDate = invoiceInfo.ExpiryDate;
                    propertyRenewalRegister.FromDate = invoiceInfo.FromDate;
                    propertyRenewalRegister.ToDate = invoiceInfo.ToDate;
                    propertyRenewalRegister.Branch = invoiceInfo.Branch;
                    propertyRenewalRegister.AccountCode = invoiceInfo.account_code;
                    propertyRenewalRegister.IntermediaryType = invoiceInfo.IntermediaryType;
                    propertyRenewalRegister.AgentCode = invoiceInfo.AgentCode;

                    propertyRenewalRegister.AgentName = invoiceInfo.AgentName;
                    propertyRenewalRegister.AuthorizedCode = invoiceInfo.AuthorizedCode;
                    propertyRenewalRegister.AuthorizedName = invoiceInfo.AuthorizedName;
                    propertyRenewalRegister.SumInsured = invoiceInfo.SumInsured;
                    propertyRenewalRegister.DetailOccupancy = invoiceInfo.DetailOccupancy;

                    propertyRenewalRegister.LocationRisk = invoiceInfo.LocationRisk;
                    propertyRenewalRegister.BuildingClass = invoiceInfo.BuildingClass;
                    propertyRenewalRegister.LossRecord = invoiceInfo.LossRecord;
                    propertyRenewalRegister.Coverage = invoiceInfo.Coverage;
                    propertyRenewalRegister.GoogleMap = invoiceInfo.GoogleMap;

                    propertyRenewalRegister.AbctypeExtinguisher = invoiceInfo.AbctypeExtinguisher;
                    propertyRenewalRegister.Co2typeExtinguishers = invoiceInfo.Co2typeExtinguishers;
                    propertyRenewalRegister.OtherExtinguisher = invoiceInfo.OtherExtinguisher;
                    propertyRenewalRegister.FireExtinguishersWeight = invoiceInfo.FireExtinguishersWeight;
                    propertyRenewalRegister.NumberofFireHoseReel = invoiceInfo.NumberofFireHoseReel;

                    propertyRenewalRegister.NumberofHydrant = invoiceInfo.NumberofHydrant;
                    propertyRenewalRegister.CapacityofFireTank = invoiceInfo.CapacityofFireTank;
                    propertyRenewalRegister.SecurityGuards = invoiceInfo.SecurityGuards;
                    propertyRenewalRegister.NumberofCctvcamera = invoiceInfo.NumberofCctvcamera;
                    propertyRenewalRegister.SqMeterofBuilding = invoiceInfo.SqMeterofBuilding;

                    propertyRenewalRegister.RequiredExtinguishers = invoiceInfo.RequiredExtinguishers;
                    propertyRenewalRegister.WorkinProcess = invoiceInfo.WorkinProcess;
                    propertyRenewalRegister.NumberofFireTank = invoiceInfo.number_of_fire_tank;
                    propertyRenewalRegister.TypeofFireTank = invoiceInfo.type_of_fire_tank;
                    propertyRenewalRegister.Outstanding_Amt = invoiceInfo.outstanding_amt;

                    propertyRenewalRegister.Excess_Amt = invoiceInfo.excess_amt;
                    propertyRenewalRegister.Financial_Interest = invoiceInfo.financial_interest;

                    _db.Update(propertyRenewalRegister);
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private async Task<MotorInvoiceInfoViewModel> GetMotorInvoiceInfoAsync(string policyno)
        {
            MotorInvoiceInfoViewModel lstinvoiceInfos = new MotorInvoiceInfoViewModel();
            List<MotorInvoiceInfoViewModel> lstinvoice = new List<MotorInvoiceInfoViewModel>();
            try
            {
                string policyNumber = Convert.ToBase64String(Encoding.UTF8.GetBytes(policyno));
                var responseTask = await _client.GetAsync($"/renewalinfo/GetMotorInvoiceInfoByPolicyNo/{policyNumber}");
                if (responseTask.IsSuccessStatusCode)
                {
                    var jsonData = await responseTask.Content.ReadAsStringAsync();
                     lstinvoiceInfos = JsonConvert.DeserializeObject<MotorInvoiceInfoViewModel>(jsonData);
                    //var invoiceInfos = JsonConvert.DeserializeObject<List<MotorInvoiceInfoViewModel>>(jsonData);
                    return lstinvoiceInfos;
                }
                else
                {
                    return lstinvoiceInfos;
                }

            }
            catch (Exception ex)
            {
                return lstinvoiceInfos;
            }
        }

        private async Task<FireInvoiceInfoViewModel> GetFireInvoiceInfoAsync(string policyno)
        {
            FireInvoiceInfoViewModel lstinvoiceInfos = new FireInvoiceInfoViewModel();
            try
            {
                string policyNumber = Convert.ToBase64String(Encoding.UTF8.GetBytes(policyno));
                var responseTask = await _client.GetAsync($"/renewalinfo/GetFireInvoiceInfoByPolicyNo/{policyNumber}");
                if (responseTask.IsSuccessStatusCode)
                {
                    string jsonData = await responseTask.Content.ReadAsStringAsync();
                    lstinvoiceInfos = JsonConvert.DeserializeObject<FireInvoiceInfoViewModel>(jsonData);
                    return lstinvoiceInfos;
                }
                else
                {
                    return lstinvoiceInfos;
                }

            }
            catch (Exception ex)
            {
                return lstinvoiceInfos;
            }
        }

        private async Task<HealthInvoiceInfoViewModel> GetHealthInvoiceInfoAsync(string policyno)
        {
            HealthInvoiceInfoViewModel lstinvoiceInfos = new HealthInvoiceInfoViewModel();
            try
            {
                string policyNumber = Convert.ToBase64String(Encoding.UTF8.GetBytes(policyno));
                var responseTask = await _client.GetAsync($"/renewalinfo/GetHealthInvoiceInfoByPolicyNo/{policyNumber}");
                if (responseTask.IsSuccessStatusCode)
                {
                    string jsonData = await responseTask.Content.ReadAsStringAsync();
                    lstinvoiceInfos = JsonConvert.DeserializeObject<HealthInvoiceInfoViewModel>(jsonData);
                    return lstinvoiceInfos;
                }
                else
                {
                    return lstinvoiceInfos;
                }
            }
            catch (Exception ex)
            {
                return lstinvoiceInfos;
            }
        }

        private async Task<PAInvoiceInfoViewModel> GetPAInvoiceInfoAsync(string policyno)
        {
            PAInvoiceInfoViewModel lstinvoiceInfos = new PAInvoiceInfoViewModel();
            try
            {
                string policyNumber = Convert.ToBase64String(Encoding.UTF8.GetBytes(policyno));
                var responseTask = await _client.GetAsync($"/renewalinfo/GetPAInvoiceInfoByPolicyNo/{policyNumber}");
                if (responseTask.IsSuccessStatusCode)
                {
                    string jsonData = await responseTask.Content.ReadAsStringAsync();
                    lstinvoiceInfos = JsonConvert.DeserializeObject<PAInvoiceInfoViewModel>(jsonData);
                    return lstinvoiceInfos;
                }
                else
                {
                    return lstinvoiceInfos;
                }
            }
            catch (Exception ex)
            {
                return lstinvoiceInfos;
            }
        }

        private async Task<EngineeringInvoiceInfoViewModel> GetEngineeringInvoiceInfoAsync(string policyno)
        {
            EngineeringInvoiceInfoViewModel lstinvoiceInfos = new EngineeringInvoiceInfoViewModel();
            try
            {
                string policyNumber = Convert.ToBase64String(Encoding.UTF8.GetBytes(policyno));
                var responseTask = await _client.GetAsync($"/renewalinfo/GetEngineeringInvoiceInfoByPolicyNo/{policyNumber}");
                if (responseTask.IsSuccessStatusCode)
                {
                    string jsonData = await responseTask.Content.ReadAsStringAsync();
                    lstinvoiceInfos = JsonConvert.DeserializeObject<EngineeringInvoiceInfoViewModel>(jsonData);
                    return lstinvoiceInfos;
                }
                else
                {
                    return lstinvoiceInfos;
                }
            }
            catch (Exception ex)
            {
                return lstinvoiceInfos;
            }
        }

        private async Task<PropertyInvoiceInfoViewModel> GetPropertyInvoiceInfoAsync(string policyno)
        {
            PropertyInvoiceInfoViewModel lstinvoiceInfos = new PropertyInvoiceInfoViewModel();
            try
            {
                string policyNumber = Convert.ToBase64String(Encoding.UTF8.GetBytes(policyno));
                var responseTask = await _client.GetAsync($"/renewalinfo/GetPropertyInvoiceInfoByPolicyNo/{policyNumber}");
                if (responseTask.IsSuccessStatusCode)
                {
                    string jsonData = await responseTask.Content.ReadAsStringAsync();
                    lstinvoiceInfos = JsonConvert.DeserializeObject<PropertyInvoiceInfoViewModel>(jsonData);
                    return lstinvoiceInfos;
                }
                else
                {
                    return lstinvoiceInfos;
                }
            }
            catch (Exception ex)
            {
                return lstinvoiceInfos;
            }
        }

        #endregion

        #region Common Function
        private void BindData()
        {
            try
            {
                ViewBag.CategoryList = new SelectList(_db.Category.OrderBy(c => c.Description).ToList(), "Id", "Description");
            }
            catch (Exception ex)
            {

            }
        }

        private void PrepareData()
        {
            try
            {
                ViewBag.ProductClass = new SelectList(_db.ProductClass.OrderBy(r => r.ClassCode).ToList(), "ClassCode", "ClassName");

            }
            catch (Exception ex)
            {

            }
        }

        private void PrepareDataForRenewalReg()
        {
            try
            {
                List<AccountCodeInfoViewModel> lstAccountCode = new List<AccountCodeInfoViewModel>();
                ViewBag.ProductClass = new SelectList(_db.ProductClass.OrderBy(r => r.ClassCode).ToList(), "ClassCode", "ClassName");
                ViewBag.PICList = new SelectList(_JWTDataContext.CrmPersonInCharge.OrderBy(r => r.Picname).ToList(), "Picname", "Picname");
                ViewBag.ResponsiblePartyList = new SelectList(_JWTDataContext.CrmPersonInCharge.Where(m => m.ResponsibleParty != null).Select(r => r.ResponsibleParty).Distinct().ToList());

                lstAccountCode = dataService.GetAccountCodeList();
                ViewBag.AccountCode = new SelectList(lstAccountCode, "account_code", "account_code");
                var ccode = from UwApproval e in Enum.GetValues(typeof(UwApproval))
                            select new { Id = e, UwApproval = e.ToString() };

                var smscode = from ApproveSendSMS e in Enum.GetValues(typeof(ApproveSendSMS))
                            select new { Id = e, ApproveSendSMS = e.ToString() };

                var y_n = from Y_N_Condition e in Enum.GetValues(typeof(Y_N_Condition))
                          select new { Id = e, DMSUpload = e.ToString() };

                var done_pending = from DonePending e in Enum.GetValues(typeof(DonePending))
                                   select new { Id = e, DonePending = e.ToString() };

                List<SelectListItem> BranchAuthority = new List<SelectListItem>();
                {
                    BranchAuthority.Add(new SelectListItem { Text = "Branch Authority", Value = "Branch Authority" });
                    BranchAuthority.Add(new SelectListItem { Text = "L1.0", Value = "L1.0" });
                    BranchAuthority.Add(new SelectListItem { Text = "L1.1", Value = "L1.1" });
                    BranchAuthority.Add(new SelectListItem { Text = "L1.2", Value = "L1.2" });
                };

                List<SelectListItem> YNCondition = new List<SelectListItem>();
                {
                    YNCondition.Add(new SelectListItem { Text = "Yes", Value = "true" });
                    YNCondition.Add(new SelectListItem { Text = "No", Value = "false" });
                };

                ViewBag.UwPpprovalList = new SelectList(ccode, "UwApproval", "UwApproval");
                ViewBag.ApproveSendSMSList = new SelectList(smscode, "ApproveSendSMS", "ApproveSendSMS");
                ViewBag.YNCCondition = YNCondition;
                ViewBag.BranchAuthorityList = BranchAuthority;
                ViewBag.DMSUpload = new SelectList(y_n, "DMSUpload", "DMSUpload");
                ViewBag.DonePending = new SelectList(done_pending, "DonePending", "DonePending");
            }
            catch (Exception ex)
            {

            }
        }

        private async Task<List<MotorRenewalRegister>> GetMotorRenewalRegisterAsync(filterRenewalDataModel filterRenewalData)
        {
            List<MotorRenewalRegister> lstMotorRenewalRegisters = new List<MotorRenewalRegister>();
            try
            {
                string invoiceNumber = Convert.ToBase64String(Encoding.UTF8.GetBytes(""));
                var responseTask = _client.GetAsync($"/renewalinfo/GetMotorRenewalByMonth?date=" + filterRenewalData.month.Date.ToString("MM/dd/yyyy") + "");
                var message = responseTask.Result;
                if (message.IsSuccessStatusCode)
                {
                    string jsonData = await message.Content.ReadAsStringAsync();
                    lstMotorRenewalRegisters = JsonConvert.DeserializeObject<List<MotorRenewalRegister>>(jsonData);
                    return lstMotorRenewalRegisters;
                }
                else
                {
                    return lstMotorRenewalRegisters;
                }

            }
            catch (Exception ex)
            {
                return lstMotorRenewalRegisters;
            }
        }

        private async Task<List<FireRenewalRegister>> GetFireRenewalRegisterAsync(filterRenewalDataModel filterRenewalData)
        {
            List<FireRenewalRegister> lstFireRenewalRegisters = new List<FireRenewalRegister>();
            try
            {
                string invoiceNumber = Convert.ToBase64String(Encoding.UTF8.GetBytes(""));
                var responseTask = _client.GetAsync($"/renewalinfo/GetFireRenewalByMonth?date=" + filterRenewalData.month.Date.ToString("MM/dd/yyyy") + "");
                var message = responseTask.Result;
                if (message.IsSuccessStatusCode)
                {
                    string jsonData = await message.Content.ReadAsStringAsync();
                    lstFireRenewalRegisters = JsonConvert.DeserializeObject<List<FireRenewalRegister>>(jsonData);
                    return lstFireRenewalRegisters;
                }
                else
                {
                    return lstFireRenewalRegisters;
                }
            }
            catch (Exception ex)
            {
                return lstFireRenewalRegisters;
            }
        }

        private async Task<List<HealthRenewalRegister>> GetHealthRenewalRegisterAsync(filterRenewalDataModel filterRenewalData)
        {
            List<HealthRenewalRegister> lstHealthRenewalRegisters = new List<HealthRenewalRegister>();
            try
            {
                string invoiceNumber = Convert.ToBase64String(Encoding.UTF8.GetBytes(""));
                var responseTask = _client.GetAsync($"/renewalinfo/GetHealthRenewalByMonth?date=" + filterRenewalData.month.Date.ToString("MM/dd/yyyy") + "");
                var message = responseTask.Result;
                if (message.IsSuccessStatusCode)
                {
                    string jsonData = await message.Content.ReadAsStringAsync();
                    lstHealthRenewalRegisters = JsonConvert.DeserializeObject<List<HealthRenewalRegister>>(jsonData);
                    return lstHealthRenewalRegisters;
                }
                else
                {
                    return lstHealthRenewalRegisters;
                }
            }
            catch (Exception ex)
            {
                return lstHealthRenewalRegisters;
            }
        }

        private async Task<List<ParenewalRegister>> GetPARenewalRegisterAsync(filterRenewalDataModel filterRenewalData)
        {
            List<ParenewalRegister> lstPARenewalRegisters = new List<ParenewalRegister>();
            try
            {
                string invoiceNumber = Convert.ToBase64String(Encoding.UTF8.GetBytes(""));
                var responseTask = _client.GetAsync($"/renewalinfo/GetPARenewalByMonth?date=" + filterRenewalData.month.Date.ToString("MM/dd/yyyy") + "");
                var message = responseTask.Result;
                if (message.IsSuccessStatusCode)
                {
                    string jsonData = await message.Content.ReadAsStringAsync();
                    lstPARenewalRegisters = JsonConvert.DeserializeObject<List<ParenewalRegister>>(jsonData);
                    return lstPARenewalRegisters;
                }
                else
                {
                    return lstPARenewalRegisters;
                }
            }
            catch (Exception ex)
            {
                return lstPARenewalRegisters;
            }
        }

        private async Task<List<EngineeringRenewalRegister>> GetEnggRenewalRegisterAsync(filterRenewalDataModel filterRenewalData)
        {
            List<EngineeringRenewalRegister> lstEnggRenewalRegisters = new List<EngineeringRenewalRegister>();
            try
            {
                string invoiceNumber = Convert.ToBase64String(Encoding.UTF8.GetBytes(""));
                var responseTask = _client.GetAsync($"/renewalinfo/GetEngineeringRenewalByMonth?date=" + filterRenewalData.month.Date.ToString("MM/dd/yyyy") + "");
                var message = responseTask.Result;
                if (message.IsSuccessStatusCode)
                {
                    string jsonData = await message.Content.ReadAsStringAsync();
                    lstEnggRenewalRegisters = JsonConvert.DeserializeObject<List<EngineeringRenewalRegister>>(jsonData);
                    return lstEnggRenewalRegisters;
                }
                else
                {
                    return lstEnggRenewalRegisters;
                }
            }
            catch (Exception ex)
            {
                return lstEnggRenewalRegisters;
            }
        }

        private async Task<List<PropertyRenewalRegister>> GetPropertyRenewalRegisterAsync(filterRenewalDataModel filterRenewalData)
        {
            List<PropertyRenewalRegister> lstPropertyRenewalRegisters = new List<PropertyRenewalRegister>();
            try
            {
                string invoiceNumber = Convert.ToBase64String(Encoding.UTF8.GetBytes(""));
                var responseTask = _client.GetAsync($"/renewalinfo/GetPropertyRenewalByMonth?date=" + filterRenewalData.month.Date.ToString("MM/dd/yyyy") + "");
                var message = responseTask.Result;
                if (message.IsSuccessStatusCode)
                {
                    string jsonData = await message.Content.ReadAsStringAsync();
                    lstPropertyRenewalRegisters = JsonConvert.DeserializeObject<List<PropertyRenewalRegister>>(jsonData);
                    return lstPropertyRenewalRegisters;
                }
                else
                {
                    return lstPropertyRenewalRegisters;
                }
            }
            catch (Exception ex)
            {
                return lstPropertyRenewalRegisters;
            }
        }

        private List<RenewalRegisterRemarkViewModel> GetRenewalRegisterRemarkInfo(Guid id)
        {
            List<RenewalRegisterRemarkViewModel> lstremarkViewModel = new List<RenewalRegisterRemarkViewModel>();
            try
            {
                lstremarkViewModel = (from r in _db.RenewalRegisterRemark
                                      join u in _db.SysUser on r.RemarkBy equals u.Id
                                      where r.RenewalRegisterId == id
                                      orderby r.RemarkDate descending
                                      select new RenewalRegisterRemarkViewModel
                                      {
                                          RenewalRegisterId = r.RenewalRegisterId,
                                          RemarkDate = r.RemarkDate,
                                          UserName = u.Name,
                                          Remark = r.Remark
                                      }).ToList();

                return lstremarkViewModel;

            }
            catch (Exception ex)
            {
                return lstremarkViewModel;
            }
        }

        #region Update PIC

        public JsonResult GetResponsiblePartyByPIC(string picname, string id, string productclass)
        {
            try
            {
                CrmPersonInCharge crmPersonInCharge = new CrmPersonInCharge();
                crmPersonInCharge = _JWTDataContext.CrmPersonInCharge.Where(p => p.Picname == picname).FirstOrDefault();
                if (crmPersonInCharge != null)
                {
                    if (productclass == "MT")
                    {
                        UpdateMotorRenewalRegister(picname, crmPersonInCharge.ResponsibleParty, id);
                    }
                    else if (productclass == "FI")
                    {
                        UpdateFireRenewalRegister(picname, crmPersonInCharge.ResponsibleParty, id);
                    }
                    else if (productclass == "LF")
                    {
                        UpdateHealthRenewalRegister(picname, crmPersonInCharge.ResponsibleParty, id);
                    }
                    else if (productclass == "MS")
                    {
                        UpdatePARenewalRegister(picname, crmPersonInCharge.ResponsibleParty, id);
                    }
                    else if (productclass == "ENGG")
                    {
                        UpdateENGGRenewalRegister(picname, crmPersonInCharge.ResponsibleParty, id);
                    }
                    else if (productclass == "PTY")
                    {
                        UpdatePropertyRenewalRegister(picname, crmPersonInCharge.ResponsibleParty, id);
                    }
                    return Json(crmPersonInCharge.ResponsibleParty);
                }
                else
                {
                    return Json("-1");
                }

            }
            catch (Exception ex)
            {
                return Json("-1");
            }

        }

        private void UpdateFireRenewalRegister(string picname, string responsibleParty, string id)
        {
            try
            {
                FireRenewalRegister fireRenewalRegister = new FireRenewalRegister();
                fireRenewalRegister = _db.FireRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                fireRenewalRegister.PersonInCharge = picname;
                fireRenewalRegister.ResponsibleParty = responsibleParty;
                _db.Update(fireRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateMotorRenewalRegister(string picname, string responsibleParty, string id)
        {
            try
            {
                MotorRenewalRegister motorRenewalRegister = new MotorRenewalRegister();
                motorRenewalRegister = _db.MotorRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                motorRenewalRegister.PersonInCharge = picname;
                motorRenewalRegister.ResponsibleParty = responsibleParty;
                _db.Update(motorRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateHealthRenewalRegister(string picname, string responsibleParty, string id)
        {
            try
            {
                HealthRenewalRegister healthRenewalRegister = new HealthRenewalRegister();
                healthRenewalRegister = _db.HealthRenewalRegister.Where(h => h.Id == Guid.Parse(id)).FirstOrDefault();
                healthRenewalRegister.PersonInCharge = picname;
                healthRenewalRegister.ResponsibleParty = responsibleParty;
                _db.Update(healthRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdatePARenewalRegister(string picname, string responsibleParty, string id)
        {
            try
            {
                ParenewalRegister parenewalRegister = new ParenewalRegister();
                parenewalRegister = _db.ParenewalRegister.Where(p => p.Id == Guid.Parse(id)).FirstOrDefault();
                parenewalRegister.PersonInCharge = picname;
                parenewalRegister.ResponsibleParty = responsibleParty;
                _db.Update(parenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateENGGRenewalRegister(string picname, string responsibleParty, string id)
        {
            try
            {
                EngineeringRenewalRegister engineeringRenewalRegister = new EngineeringRenewalRegister();
                engineeringRenewalRegister = _db.EngineeringRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                engineeringRenewalRegister.PersonInCharge = picname;
                engineeringRenewalRegister.ResponsibleParty = responsibleParty;
                _db.Update(engineeringRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdatePropertyRenewalRegister(string picname, string responsibleParty, string id)
        {
            try
            {
                PropertyRenewalRegister propertyRenewalRegister = new PropertyRenewalRegister();
                propertyRenewalRegister = _db.PropertyRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                propertyRenewalRegister.PersonInCharge = picname;
                propertyRenewalRegister.ResponsibleParty = responsibleParty;
                _db.Update(propertyRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        #endregion

        #region Update ApproveSendSMS

        public async Task<JsonResult> UpdateApproveSendSMS(string approvesendsms, string id, string productclass, string phonenumber)
        {
            try
            {
                bool crmRenewalCreateFlag = false;

                if (productclass == "MT")
                {
                    if (approvesendsms == "Send_SMS")
                    {
                        crmRenewalCreateFlag = await CreateRenewalCase(Guid.Parse(id));

                        UpdateMotorRenewalCreateFlag(crmRenewalCreateFlag, Guid.Parse(id));
                    }

                    UpdateSendSMSMotorRenewalRegister(approvesendsms, id, phonenumber);
                }
                else if (productclass == "FI")
                {
                    if (approvesendsms == "Send_SMS")
                    {
                        crmRenewalCreateFlag = await CreateRenewalCase(Guid.Parse(id));

                        UpdateFireRenewalCreateFlag(crmRenewalCreateFlag, Guid.Parse(id));
                    }

                    UpdateSendSMSFireRenewalRegister(approvesendsms, id, phonenumber);
                }
                else if (productclass == "LF")
                {
                    if (approvesendsms == "Send_SMS")
                    {
                        crmRenewalCreateFlag = await CreateRenewalCase(Guid.Parse(id));

                        UpdateHealthRenewalCreateFlag(crmRenewalCreateFlag, Guid.Parse(id));
                    }
                    UpdateSendSMSHealthRenewalRegister(approvesendsms, id, phonenumber);
                }
                else if (productclass == "MS")
                {
                    if (approvesendsms == "Send_SMS")
                    {
                        crmRenewalCreateFlag = await CreateRenewalCase(Guid.Parse(id));
                        UpdatePARenewalCreateFlag(crmRenewalCreateFlag, Guid.Parse(id));
                    }

                    UpdateSendSMSPARenewalRegister(approvesendsms, id, phonenumber);
                }
                else if (productclass == "ENGG")
                {
                    if (approvesendsms == "Send_SMS")
                    {
                        crmRenewalCreateFlag = await CreateRenewalCase(Guid.Parse(id));
                        UpdateENGGRenewalCreateFlag(crmRenewalCreateFlag, Guid.Parse(id));
                    }

                    UpdateSendSMSENGGRenewalRegister(approvesendsms, id, phonenumber);
                }
                else if (productclass == "PTY")
                {
                    if(approvesendsms == "Send_SMS")
                    {
                        crmRenewalCreateFlag = await CreateRenewalCase(Guid.Parse(id));
                        UpdatePropertyRenewalCreateFlag(crmRenewalCreateFlag, Guid.Parse(id));
                    }

                    UpdateSendSMSPropertyRenewalRegister(approvesendsms, id, phonenumber);
                }

                if (crmRenewalCreateFlag == true)
                {
                    return Json("1");
                }
                else
                {
                    return Json("-1");
                }
            }
            catch (Exception ex)
            {
                return Json("-1");
            }

        }

        private void UpdateMotorRenewalCreateFlag(bool renewalCreateFlag, Guid id)
        {
            try
            {
                MotorRenewalRegister motorRenewalRegister = new MotorRenewalRegister();
                motorRenewalRegister = _db.MotorRenewalRegister.Where(m => m.Id == id).FirstOrDefault();
                if (motorRenewalRegister != null)
                {
                    motorRenewalRegister.CrmrenewalCreateFlag = renewalCreateFlag;
                    motorRenewalRegister.CrmrenewalCreatedDate = DateTime.Now.Date;
                    _db.Update(motorRenewalRegister);
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateFireRenewalCreateFlag(bool renewalCreateFlag, Guid id)
        {
            try
            {
                FireRenewalRegister fireRenewalRegister = new FireRenewalRegister();
                fireRenewalRegister = _db.FireRenewalRegister.Where(m => m.Id == id).FirstOrDefault();
                if (fireRenewalRegister != null)
                {
                    fireRenewalRegister.CrmrenewalCreateFlag = renewalCreateFlag;
                    fireRenewalRegister.CrmrenewalCreatedDate = DateTime.Now.Date;
                    _db.Update(fireRenewalRegister);
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateHealthRenewalCreateFlag(bool renewalCreateFlag, Guid id)
        {
            try
            {
                HealthRenewalRegister healthRenewalRegister = new HealthRenewalRegister();
                healthRenewalRegister = _db.HealthRenewalRegister.Where(h => h.Id == id).FirstOrDefault();
                if (healthRenewalRegister != null)
                {
                    healthRenewalRegister.CrmrenewalCreateFlag = renewalCreateFlag;
                    healthRenewalRegister.CrmrenewalCreatedDate = DateTime.Now.Date;
                    _db.Update(healthRenewalRegister);
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdatePARenewalCreateFlag(bool renewalCreateFlag, Guid id)
        {
            try
            {
                ParenewalRegister parenewalRegister = new ParenewalRegister();
                parenewalRegister = _db.ParenewalRegister.Where(p => p.Id == id).FirstOrDefault();
                if (parenewalRegister != null)
                {
                    parenewalRegister.CrmrenewalCreateFlag = renewalCreateFlag;
                    parenewalRegister.CrmrenewalCreatedDate = DateTime.Now.Date;
                    _db.Update(parenewalRegister);
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateENGGRenewalCreateFlag(bool renewalCreateFlag, Guid id)
        {
            try
            {
                EngineeringRenewalRegister engineeringRenewalRegister = new EngineeringRenewalRegister();
                engineeringRenewalRegister = _db.EngineeringRenewalRegister.Where(m => m.Id == id).FirstOrDefault();
                if (engineeringRenewalRegister != null)
                {
                    engineeringRenewalRegister.CrmrenewalCreateFlag = renewalCreateFlag;
                    engineeringRenewalRegister.CrmrenewalCreatedDate = DateTime.Now.Date;
                    _db.Update(engineeringRenewalRegister);
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdatePropertyRenewalCreateFlag(bool renewalCreateFlag, Guid id)
        {
            try
            {
                PropertyRenewalRegister propertyRenewalRegister = new PropertyRenewalRegister();
                propertyRenewalRegister = _db.PropertyRenewalRegister.Where(p => p.Id == id).FirstOrDefault();
                if(propertyRenewalRegister != null)
                {
                    propertyRenewalRegister.CrmrenewalCreateFlag = renewalCreateFlag;
                    propertyRenewalRegister.CrmrenewalCreatedDate = DateTime.Now.Date;
                    _db.Update(propertyRenewalRegister);
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateSendSMSMotorRenewalRegister(string approvesendsms, string id, string phonenumber)
        {
            try
            {
                MotorRenewalRegister motorRenewalRegister = new MotorRenewalRegister();
                motorRenewalRegister = _db.MotorRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                motorRenewalRegister.ApproveSendSms = approvesendsms;
                motorRenewalRegister.ApproveSendSmsdate = DateTime.Now.Date;
                motorRenewalRegister.PhoneNumber = phonenumber;
                _db.Update(motorRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateSendSMSFireRenewalRegister(string approvesendsms, string id, string phonenumber)
        {
            try
            {
                FireRenewalRegister fireRenewalRegister = new FireRenewalRegister();
                fireRenewalRegister = _db.FireRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                fireRenewalRegister.ApproveSendSms = approvesendsms;
                fireRenewalRegister.ApproveSendSmsdate = DateTime.Now.Date;
                fireRenewalRegister.PhoneNumber = phonenumber;
                _db.Update(fireRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateSendSMSHealthRenewalRegister(string approvesendsms, string id, string phonenumber)
        {
            try
            {
                HealthRenewalRegister healthRenewalRegister = new HealthRenewalRegister();
                healthRenewalRegister = _db.HealthRenewalRegister.Where(h => h.Id == Guid.Parse(id)).FirstOrDefault();
                healthRenewalRegister.ApproveSendSms = approvesendsms;
                healthRenewalRegister.ApproveSendSmsdate = DateTime.Now.Date;
                healthRenewalRegister.PhoneNumber = phonenumber;
                _db.Update(healthRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateSendSMSPARenewalRegister(string approvesendsms, string id, string phonenumber)
        {
            try
            {
                ParenewalRegister parenewalRegister = new ParenewalRegister();
                parenewalRegister = _db.ParenewalRegister.Where(p => p.Id == Guid.Parse(id)).FirstOrDefault();
                parenewalRegister.ApproveSendSms = approvesendsms;
                parenewalRegister.ApproveSendSmsdate = DateTime.Now.Date;
                parenewalRegister.PhoneNumber = phonenumber;
                _db.Update(parenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateSendSMSENGGRenewalRegister(string approvesendsms, string id, string phonenumber)
        {
            try
            {
                EngineeringRenewalRegister engineeringRenewalRegister = new EngineeringRenewalRegister();
                engineeringRenewalRegister = _db.EngineeringRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                engineeringRenewalRegister.ApproveSendSms = approvesendsms;
                engineeringRenewalRegister.ApproveSendSmsdate = DateTime.Now.Date;
                engineeringRenewalRegister.PhoneNumber = phonenumber;
                _db.Update(engineeringRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateSendSMSPropertyRenewalRegister(string approvesendsms, string id, string phonenumber)
        {
            try
            {
                PropertyRenewalRegister propertyRenewalRegister = new PropertyRenewalRegister();
                propertyRenewalRegister = _db.PropertyRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                propertyRenewalRegister.ApproveSendSms = approvesendsms;
                propertyRenewalRegister.ApproveSendSmsdate = DateTime.Now.Date;
                propertyRenewalRegister.PhoneNumber = phonenumber;
                _db.Update(propertyRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        #endregion

        #region Multiple Chk ApproveSendSMS

        public async Task<JsonResult> UpdateChkApproveSendSMS([FromBody] ApproveSendSMSModel approveSendSMSModel)
        {
            try
            {
                int successCountRenewal = 0;

                if (approveSendSMSModel.ids.Length > 0 && approveSendSMSModel.approvesendsms != null)
                {
                    if (approveSendSMSModel.productclass == "MT")
                    {
                        successCountRenewal = await UpdateChkSendSMSMotorRenewalRegister(approveSendSMSModel);
                    }
                    else if (approveSendSMSModel.productclass == "FI")
                    {
                        successCountRenewal = await UpdateChkSendSMSFireRenewalRegister(approveSendSMSModel);
                    }
                    else if (approveSendSMSModel.productclass == "LF")
                    {
                        successCountRenewal = await UpdateChkSendSMSHealthRenewalRegister(approveSendSMSModel);
                    }
                    else if (approveSendSMSModel.productclass == "MS")
                    {
                        successCountRenewal = await UpdateChkSendSMSPARenewalRegister(approveSendSMSModel);
                    }
                    else if (approveSendSMSModel.productclass == "ENGG")
                    {
                        successCountRenewal = await UpdateChkSendSMSENGGRenewalRegister(approveSendSMSModel);
                    }
                    else if (approveSendSMSModel.productclass == "PTY")
                    {
                        successCountRenewal = await UpdateChkSendSMSPropertyRenewalRegister(approveSendSMSModel);
                    }
                }
                else
                {
                    return Json(successCountRenewal);
                }

                return Json(successCountRenewal);
            }
            catch (Exception ex)
            {
                return Json("-1");
            }
        }

        public async Task<int> UpdateChkSendSMSMotorRenewalRegister(ApproveSendSMSModel approveSendSMSModel)
        {
            int successCountRenewal = 0;

            try
            {
                MotorRenewalRegister motorRenewalRegister = new MotorRenewalRegister();
                for (int i = 0; i < approveSendSMSModel.ids.Length; i++)
                {
                    if (approveSendSMSModel.approvesendsms == "Send_SMS")
                    {
                        #region create renewalcase

                        bool crmRenewalCreateFlag = false;
                        crmRenewalCreateFlag = await CreateRenewalCase(Guid.Parse(approveSendSMSModel.ids[i]));

                        #endregion

                        if (crmRenewalCreateFlag == true)
                        {
                            successCountRenewal += 1;
                        }

                        UpdateMotorRenewalCreateFlag(crmRenewalCreateFlag, Guid.Parse(approveSendSMSModel.ids[i]));
                    }

                    UpdateSendSMSMotorRenewalRegister(approveSendSMSModel.approvesendsms, approveSendSMSModel.ids[i], approveSendSMSModel.phonenos[i]);

                    //motorRenewalRegister = _db.MotorRenewalRegister.Where(m => m.Id == Guid.Parse(approveSendSMSModel.ids[i])).FirstOrDefault();
                    //motorRenewalRegister.ApproveSendSms = approveSendSMSModel.approvesendsms;
                    //motorRenewalRegister.UwapprovalDate = DateTime.Now.Date;
                    //motorRenewalRegister.PhoneNumber = approveSendSMSModel.phonenos[i];
                    //_db.Update(motorRenewalRegister);
                    //_db.SaveChanges();
                }

                return successCountRenewal;
            }
            catch (Exception ex)
            {
                return successCountRenewal;
            }
        }

        public async Task<int> UpdateChkSendSMSFireRenewalRegister(ApproveSendSMSModel approveSendSMSModel)
        {
            int successCountRenewal = 0;
            try
            {
                FireRenewalRegister fireRenewalRegister = new FireRenewalRegister();
                for (int i = 0; i < approveSendSMSModel.ids.Length; i++)
                {
                    if (approveSendSMSModel.approvesendsms == "Send_SMS")
                    {
                        #region create renewalcase

                        bool crmRenewalCreateFlag = false;
                        crmRenewalCreateFlag = await CreateRenewalCase(Guid.Parse(approveSendSMSModel.ids[i]));

                        #endregion

                        if (crmRenewalCreateFlag == true)
                        {
                            successCountRenewal += 1;
                        }

                        UpdateFireRenewalCreateFlag(crmRenewalCreateFlag, Guid.Parse(approveSendSMSModel.ids[i]));
                    }

                    UpdateSendSMSFireRenewalRegister(approveSendSMSModel.approvesendsms, approveSendSMSModel.ids[i], approveSendSMSModel.phonenos[i]);

                    //fireRenewalRegister = _db.FireRenewalRegister.Where(m => m.Id == Guid.Parse(approveSendSMSModel.ids[i])).FirstOrDefault();
                    //fireRenewalRegister.ApproveSendSms = approveSendSMSModel.approvesendsms;
                    //fireRenewalRegister.UwapprovalDate = DateTime.Now.Date;
                    //fireRenewalRegister.PhoneNumber = approveSendSMSModel.phonenos[i];
                    //_db.Update(fireRenewalRegister);
                    //_db.SaveChanges();
                }

                return successCountRenewal;
            }
            catch (Exception ex)
            {
                return successCountRenewal;
            }
        }

        public async Task<int> UpdateChkSendSMSHealthRenewalRegister(ApproveSendSMSModel approveSendSMSModel)
        {
            int successCountRenewal = 0;
            try
            {
                HealthRenewalRegister healthRenewalRegister = new HealthRenewalRegister();
                for (int i = 0; i < approveSendSMSModel.ids.Length; i++)
                {
                    if (approveSendSMSModel.approvesendsms == "Send_SMS")
                    {
                        #region create renewalcase

                        bool crmRenewalCreateFlag = false;
                        crmRenewalCreateFlag = await CreateRenewalCase(Guid.Parse(approveSendSMSModel.ids[i]));

                        #endregion

                        if (crmRenewalCreateFlag == true)
                        {
                            successCountRenewal += 1;
                        }

                        UpdateHealthRenewalCreateFlag(crmRenewalCreateFlag, Guid.Parse(approveSendSMSModel.ids[i]));
                    }

                    UpdateSendSMSHealthRenewalRegister(approveSendSMSModel.approvesendsms, approveSendSMSModel.ids[i], approveSendSMSModel.phonenos[i]);

                    //fireRenewalRegister = _db.FireRenewalRegister.Where(m => m.Id == Guid.Parse(approveSendSMSModel.ids[i])).FirstOrDefault();
                    //fireRenewalRegister.ApproveSendSms = approveSendSMSModel.approvesendsms;
                    //fireRenewalRegister.UwapprovalDate = DateTime.Now.Date;
                    //fireRenewalRegister.PhoneNumber = approveSendSMSModel.phonenos[i];
                    //_db.Update(fireRenewalRegister);
                    //_db.SaveChanges();
                }

                return successCountRenewal;
            }
            catch (Exception ex)
            {
                return successCountRenewal;
            }
        }

        public async Task<int> UpdateChkSendSMSPARenewalRegister(ApproveSendSMSModel approveSendSMSModel)
        {
            int successCountRenewal = 0;
            try
            {
                ParenewalRegister parenewalRegister = new ParenewalRegister();
                for (int i = 0; i < approveSendSMSModel.ids.Length; i++)
                {
                    if (approveSendSMSModel.approvesendsms == "Send_SMS")
                    {
                        #region create renewalcase

                        bool crmRenewalCreateFlag = false;
                        crmRenewalCreateFlag = await CreateRenewalCase(Guid.Parse(approveSendSMSModel.ids[i]));

                        #endregion

                        if (crmRenewalCreateFlag == true)
                        {
                            successCountRenewal += 1;
                        }

                        UpdatePARenewalCreateFlag(crmRenewalCreateFlag, Guid.Parse(approveSendSMSModel.ids[i]));
                    }

                    UpdateSendSMSPARenewalRegister(approveSendSMSModel.approvesendsms, approveSendSMSModel.ids[i], approveSendSMSModel.phonenos[i]);

                }

                return successCountRenewal;
            }
            catch (Exception ex)
            {
                return successCountRenewal;
            }
        }

        public async Task<int> UpdateChkSendSMSENGGRenewalRegister(ApproveSendSMSModel approveSendSMSModel)
        {
            int successCountRenewal = 0;
            try
            {
                EngineeringRenewalRegister engineeringRenewalRegister = new EngineeringRenewalRegister();
                for (int i = 0; i < approveSendSMSModel.ids.Length; i++)
                {
                    if (approveSendSMSModel.approvesendsms == "Send_SMS")
                    {
                        #region create renewalcase

                        bool crmRenewalCreateFlag = false;
                        crmRenewalCreateFlag = await CreateRenewalCase(Guid.Parse(approveSendSMSModel.ids[i]));

                        #endregion

                        if (crmRenewalCreateFlag == true)
                        {
                            successCountRenewal += 1;
                        }

                        UpdateENGGRenewalCreateFlag(crmRenewalCreateFlag, Guid.Parse(approveSendSMSModel.ids[i]));
                    }

                    UpdateSendSMSENGGRenewalRegister(approveSendSMSModel.approvesendsms, approveSendSMSModel.ids[i], approveSendSMSModel.phonenos[i]);

                }

                return successCountRenewal;
            }
            catch (Exception ex)
            {
                return successCountRenewal;
            }
        }

        public async Task<int> UpdateChkSendSMSPropertyRenewalRegister(ApproveSendSMSModel approveSendSMSModel)
        {
            int successCountRenewal = 0;
            try
            {
                PropertyRenewalRegister propertyRenewalRegister = new PropertyRenewalRegister();
                for (int i = 0; i < approveSendSMSModel.ids.Length; i++)
                {
                    if (approveSendSMSModel.approvesendsms == "Send_SMS")
                    {
                        #region create renewalcase

                        bool crmRenewalCreateFlag = false;
                        crmRenewalCreateFlag = await CreateRenewalCase(Guid.Parse(approveSendSMSModel.ids[i]));

                        #endregion

                        if (crmRenewalCreateFlag == true)
                        {
                            successCountRenewal += 1;
                        }

                        UpdatePropertyRenewalCreateFlag(crmRenewalCreateFlag, Guid.Parse(approveSendSMSModel.ids[i]));
                    }

                    UpdateSendSMSPropertyRenewalRegister(approveSendSMSModel.approvesendsms, approveSendSMSModel.ids[i], approveSendSMSModel.phonenos[i]);

                }

                return successCountRenewal;
            }
            catch (Exception ex)
            {
                return successCountRenewal;
            }
        }

        #endregion

        #region Update UWApproval

        public JsonResult UpdateUWApproval(string uwapproval, string id, string productclass)
        {
            try
            {
                if (productclass == "MT")
                {
                    UpdateUWApprovalMotorRenewalRegister(uwapproval, id);
                }
                else if (productclass == "FI")
                {
                    UpdateUWApprovalFireRenewalRegister(uwapproval, id);
                }
                else if (productclass == "LF")
                {
                    UpdateUWApprovalHealthRenewalRegister(uwapproval, id);
                }
                else if (productclass == "MS")
                {
                    UpdateUWApprovalPARenewalRegister(uwapproval, id);
                }
                else if (productclass == "ENGG")
                {
                    UpdateUWApprovalENGGRenewalRegister(uwapproval, id);
                }
                else if (productclass == "PTY")
                {
                    UpdateUWApprovalPropertyRenewalRegister(uwapproval, id);
                }
                return Json("1");
            }
            catch (Exception ex)
            {
                return Json("-1");
            }
        }

        private void UpdateUWApprovalMotorRenewalRegister(string uwapproval, string id)
        {
            try
            {
                MotorRenewalRegister motorRenewalRegister = new MotorRenewalRegister();
                motorRenewalRegister = _db.MotorRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                motorRenewalRegister.Uwapproval = uwapproval;
                _db.Update(motorRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateUWApprovalFireRenewalRegister(string uwapproval, string id)
        {
            try
            {
                FireRenewalRegister fireRenewalRegister = new FireRenewalRegister();
                fireRenewalRegister = _db.FireRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                fireRenewalRegister.Uwapproval = uwapproval;
                _db.Update(fireRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateUWApprovalHealthRenewalRegister(string uwapproval, string id)
        {
            try
            {
                HealthRenewalRegister healthRenewalRegister = new HealthRenewalRegister();
                healthRenewalRegister = _db.HealthRenewalRegister.Where(h => h.Id == Guid.Parse(id)).FirstOrDefault();
                healthRenewalRegister.Uwapproval = uwapproval;
                _db.Update(healthRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateUWApprovalPARenewalRegister(string uwapproval, string id)
        {
            try
            {
                ParenewalRegister parenewalRegister = new ParenewalRegister();
                parenewalRegister = _db.ParenewalRegister.Where(p => p.Id == Guid.Parse(id)).FirstOrDefault();
                parenewalRegister.Uwapproval = uwapproval;
                _db.Update(parenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateUWApprovalENGGRenewalRegister(string uwapproval, string id)
        {
            try
            {
                EngineeringRenewalRegister engineeringRenewalRegister = new EngineeringRenewalRegister();
                engineeringRenewalRegister = _db.EngineeringRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                engineeringRenewalRegister.Uwapproval = uwapproval;
                _db.Update(engineeringRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateUWApprovalPropertyRenewalRegister(string uwapproval, string id)
        {
            try
            {
                PropertyRenewalRegister propertyRenewalRegister = new PropertyRenewalRegister();
                propertyRenewalRegister = _db.PropertyRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                propertyRenewalRegister.Uwapproval = uwapproval;
                _db.Update(propertyRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        #endregion

        #region Update BranchAuthority

        public JsonResult UpdateBranchAuthority(string branchauthority, string id, string productclass)
        {
            try
            {
                if (productclass == "MT")
                {
                    UpdateBranchAuthorityMotorRenewalRegister(branchauthority, id);
                }
                else if (productclass == "FI")
                {
                    UpdateBranchAuthorityFireRenewalRegister(branchauthority, id);
                }
                else if (productclass == "LF")
                {
                    UpdateBranchAuthorityHealthRenewalRegister(branchauthority, id);
                }
                else if (productclass == "MS")
                {
                    UpdateBranchAuthorityPARenewalRegister(branchauthority, id);
                }
                else if (productclass == "ENGG")
                {
                    UpdateBranchAuthorityENGGRenewalRegister(branchauthority, id);
                }
                else if (productclass == "PTY")
                {
                    UpdateBranchAuthorityPropertyRenewalRegister(branchauthority, id);
                }
                return Json("1");
            }
            catch (Exception ex)
            {
                return Json("-1");
            }
        }

        private void UpdateBranchAuthorityMotorRenewalRegister(string branchauthority, string id)
        {
            try
            {
                MotorRenewalRegister motorRenewalRegister = new MotorRenewalRegister();
                motorRenewalRegister = _db.MotorRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                motorRenewalRegister.BranchAuthority = branchauthority;
                motorRenewalRegister.BranchAuthorityDate = DateTime.Now.Date;
                _db.Update(motorRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateBranchAuthorityFireRenewalRegister(string branchauthority, string id)
        {
            try
            {
                FireRenewalRegister fireRenewalRegister = new FireRenewalRegister();
                fireRenewalRegister = _db.FireRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                fireRenewalRegister.BranchAuthority = branchauthority;
                fireRenewalRegister.BranchAuthorityDate = DateTime.Now.Date;
                _db.Update(fireRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateBranchAuthorityHealthRenewalRegister(string branchauthority, string id)
        {
            try
            {
                HealthRenewalRegister healthRenewalRegister = new HealthRenewalRegister();
                healthRenewalRegister = _db.HealthRenewalRegister.Where(h => h.Id == Guid.Parse(id)).FirstOrDefault();
                healthRenewalRegister.BranchAuthority = branchauthority;
                healthRenewalRegister.BranchAuthorityDate = DateTime.Now.Date;
                _db.Update(healthRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateBranchAuthorityPARenewalRegister(string branchauthority, string id)
        {
            try
            {
                ParenewalRegister parenewalRegister = new ParenewalRegister();
                parenewalRegister = _db.ParenewalRegister.Where(p => p.Id == Guid.Parse(id)).FirstOrDefault();
                parenewalRegister.BranchAuthority = branchauthority;
                parenewalRegister.BranchAuthorityDate = DateTime.Now.Date;
                _db.Update(parenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateBranchAuthorityENGGRenewalRegister(string branchauthority, string id)
        {
            try
            {
                EngineeringRenewalRegister engineeringRenewalRegister = new EngineeringRenewalRegister();
                engineeringRenewalRegister = _db.EngineeringRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                engineeringRenewalRegister.BranchAuthority = branchauthority;
                engineeringRenewalRegister.BranchAuthorityDate = DateTime.Now.Date;
                _db.Update(engineeringRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateBranchAuthorityPropertyRenewalRegister(string branchauthority, string id)
        {
            try
            {
                PropertyRenewalRegister propertyRenewalRegister = new PropertyRenewalRegister();
                propertyRenewalRegister = _db.PropertyRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                propertyRenewalRegister.BranchAuthority = branchauthority;
                propertyRenewalRegister.BranchAuthorityDate = DateTime.Now.Date;
                _db.Update(propertyRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        #endregion

        #region Update Phonenumber

        public JsonResult UpdatePhoneNo(string phonenumber, string id, string productclass)
        {
            try
            {
                if (productclass == "MT")
                {
                    UpdatePhoneNumberMotorRenewalRegister(phonenumber, id);
                }
                else if (productclass == "FI")
                {
                    UpdatePhoneNumberFireRenewalRegister(phonenumber, id);
                }
                else if (productclass == "LF")
                {
                    UpdatePhoneNumberHealthRenewalRegister(phonenumber, id);
                }
                else if (productclass == "MS")
                {
                    UpdatePhoneNumberPARenewalRegister(phonenumber, id);
                }
                else if (productclass == "ENGG")
                {
                    UpdatePhoneNumberENGGRenewalRegister(phonenumber, id);
                }
                else if (productclass == "PTY")
                {
                    UpdatePhoneNumberPropertyRenewalRegister(phonenumber, id);
                }
                return Json("1");
            }
            catch (Exception ex)
            {
                return Json("-1");
            }
        }

        private void UpdatePhoneNumberMotorRenewalRegister(string phonenumber, string id)
        {
            try
            {
                MotorRenewalRegister motorRenewalRegister = new MotorRenewalRegister();
                motorRenewalRegister = _db.MotorRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                motorRenewalRegister.PhoneNumber = phonenumber;
                _db.Update(motorRenewalRegister);
                _db.SaveChanges();

                //SurveySms surveySms = new SurveySms();
                //surveySms = _db.SurveySms.Where(s => s.PolicyNo == motorRenewalRegister.PolicyNo).FirstOrDefault();
                //surveySms.PhoneNumber = phonenumber;
                //surveySms.PhNoFlag = true;
                //_db.Update(surveySms);
                //_db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdatePhoneNumberFireRenewalRegister(string phonenumber, string id)
        {
            try
            {
                FireRenewalRegister fireRenewalRegister = new FireRenewalRegister();
                fireRenewalRegister = _db.FireRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                fireRenewalRegister.PhoneNumber = phonenumber;
                _db.Update(fireRenewalRegister);
                _db.SaveChanges();

                //SurveySms surveySms = new SurveySms();
                //surveySms = _db.SurveySms.Where(s => s.PolicyNo == fireRenewalRegister.PolicyNo).FirstOrDefault();
                //surveySms.PhoneNumber = phonenumber;
                //surveySms.PhNoFlag = true;
                //_db.Update(surveySms);
                //_db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdatePhoneNumberHealthRenewalRegister(string phonenumber, string id)
        {
            try
            {
                HealthRenewalRegister healthRenewalRegister = new HealthRenewalRegister();
                healthRenewalRegister = _db.HealthRenewalRegister.Where(h => h.Id == Guid.Parse(id)).FirstOrDefault();
                healthRenewalRegister.PhoneNumber = phonenumber;
                _db.Update(healthRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdatePhoneNumberPARenewalRegister(string phonenumber, string id)
        {
            try
            {
                ParenewalRegister parenewalRegister = new ParenewalRegister();
                parenewalRegister = _db.ParenewalRegister.Where(p => p.Id == Guid.Parse(id)).FirstOrDefault();
                parenewalRegister.PhoneNumber = phonenumber;
                _db.Update(parenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdatePhoneNumberENGGRenewalRegister(string phonenumber, string id)
        {
            try
            {
                EngineeringRenewalRegister engineeringRenewalRegister = new EngineeringRenewalRegister();
                engineeringRenewalRegister = _db.EngineeringRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                engineeringRenewalRegister.PhoneNumber = phonenumber;
                _db.Update(engineeringRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdatePhoneNumberPropertyRenewalRegister(string phonenumber, string id)
        {
            try
            {
                PropertyRenewalRegister propertyRenewalRegister = new PropertyRenewalRegister();
                propertyRenewalRegister = _db.PropertyRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                propertyRenewalRegister.PhoneNumber = phonenumber;
                _db.Update(propertyRenewalRegister);
                _db.SaveChanges();
            }
            catch(Exception ex)
            {

            }
        }

        #endregion

        #region Update DMSUpload

        public JsonResult UpdateDMSUpload(string dmsupload, string id, string productclass)
        {
            try
            {
                if (productclass == "ENGG")
                {
                    UpdateDMSUploadENGGRenewalRegister(dmsupload, id);
                }
                else if (productclass == "PTY")
                {
                    UpdateDMSUploadPropertyRenewalRegister(dmsupload, id);
                }
                return Json("1");
            }
            catch (Exception ex)
            {
                return Json("-1");
            }
        }

        private void UpdateDMSUploadENGGRenewalRegister(string dmsupload, string id)
        {
            try
            {
                EngineeringRenewalRegister engineeringRenewalRegister = new EngineeringRenewalRegister();
                engineeringRenewalRegister = _db.EngineeringRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                engineeringRenewalRegister.Dmsupload = dmsupload;
                _db.Update(engineeringRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateDMSUploadPropertyRenewalRegister(string dmsupload, string id)
        {
            try
            {
                PropertyRenewalRegister propertyRenewalRegister = new PropertyRenewalRegister();
                propertyRenewalRegister = _db.PropertyRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                propertyRenewalRegister.Dmsupload = dmsupload;
                _db.Update(propertyRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        #endregion

        #region Update Done/Pending Status

        public JsonResult UpdateDonePendingStatus(string status, string id, string productclass)
        {
            try
            {
                if(productclass == "ENGG")
                {
                    UpdateStatusENGGRenewalRegister(status, id);
                }
                else if (productclass == "PTY")
                {
                    UpdateStatusPropertyRenewalRegister(status, id);
                }
                return Json("1");
            }
            catch (Exception ex)
            {
                return Json("-1");
            }
        }

        private void UpdateStatusENGGRenewalRegister(string status, string id)
        {
            try
            {
                EngineeringRenewalRegister engineeringRenewalRegister = new EngineeringRenewalRegister();
                engineeringRenewalRegister = _db.EngineeringRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                engineeringRenewalRegister.DonePending = status;
                _db.Update(engineeringRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateStatusPropertyRenewalRegister(string status, string id)
        {
            try
            {
                PropertyRenewalRegister propertyRenewalRegister = new PropertyRenewalRegister();
                propertyRenewalRegister = _db.PropertyRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                propertyRenewalRegister.DonePending = status;
                _db.Update(propertyRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        #endregion

        #region Update AgreedSIAmount
        public JsonResult UpdateAgreedSIAmount(string agreedsiamount, string id, string productclass)
        {
            try
            {
                if (productclass == "MT")
                {
                    UpdateAgreedSIAmountMotorRenewalRegister(agreedsiamount, id);
                }
                else if(productclass == "FI")
                {
                    UpdateAgreedSIAmountFireRenewalRegister(agreedsiamount, id);
                }
                else if(productclass == "LF")
                {
                    UpdateAgreedSIAmountHealthRenewalRegister(agreedsiamount, id);
                }
                else if(productclass == "MS")
                {
                    UpdateAgreedSIAmountPARenewalRegister(agreedsiamount, id);
                }
                else if(productclass == "ENGG")
                {
                    UpdateAgreedSIAmountENGGRenewalRegister(agreedsiamount, id);
                }
                else if(productclass == "PTY")
                {
                    UpdateAgreedSIAmountPropertyRenewalRegister(agreedsiamount, id);
                }
                return Json("1");
            }
            catch(Exception ex)
            {
                return Json("-1");
            }
        }

        private void UpdateAgreedSIAmountMotorRenewalRegister(string agreedsiamount, string id)
        {
            try
            {
                MotorRenewalRegister motorRenewalRegister = new MotorRenewalRegister();
                motorRenewalRegister = _db.MotorRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                if(agreedsiamount == null)
                {
                    motorRenewalRegister.AgreedSiamount = null;
                }
                else
                {
                    motorRenewalRegister.AgreedSiamount = Decimal.Parse(agreedsiamount);
                }
                _db.Update(motorRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateAgreedSIAmountFireRenewalRegister(string agreedsiamount, string id)
        {
            try
            {
                FireRenewalRegister fireRenewalRegister = new FireRenewalRegister();
                fireRenewalRegister = _db.FireRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                if (agreedsiamount == null)
                {
                    fireRenewalRegister.AgreedSiamount = null;
                }
                else
                {
                    fireRenewalRegister.AgreedSiamount = Decimal.Parse(agreedsiamount);
                }
                _db.Update(fireRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateAgreedSIAmountHealthRenewalRegister(string agreedsiamount, string id)
        {
            try
            {
                HealthRenewalRegister healthRenewalRegister = new HealthRenewalRegister();
                healthRenewalRegister = _db.HealthRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                if (agreedsiamount == null)
                {
                    healthRenewalRegister.AgreedSiamount = null;
                }
                else
                {
                    healthRenewalRegister.AgreedSiamount = Decimal.Parse(agreedsiamount);
                }
                _db.Update(healthRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateAgreedSIAmountPARenewalRegister(string agreedsiamount, string id)
        {
            try
            {
                ParenewalRegister parenewalRegister = new ParenewalRegister();
                parenewalRegister = _db.ParenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                if (agreedsiamount == null)
                {
                    parenewalRegister.AgreedSiamount = null;
                }
                else
                {
                    parenewalRegister.AgreedSiamount = Decimal.Parse(agreedsiamount);
                }
                _db.Update(parenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateAgreedSIAmountENGGRenewalRegister(string agreedsiamount, string id)
        {
            try
            {
                EngineeringRenewalRegister engineeringRenewalRegister = new EngineeringRenewalRegister();
                engineeringRenewalRegister = _db.EngineeringRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                if (agreedsiamount == null)
                {
                    engineeringRenewalRegister.AgreedSiamount = null;
                }
                else
                {
                    engineeringRenewalRegister.AgreedSiamount = Decimal.Parse(agreedsiamount);
                }
                _db.Update(engineeringRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateAgreedSIAmountPropertyRenewalRegister(string agreedsiamount, string id)
        {
            try
            {
                PropertyRenewalRegister propertyRenewalRegister = new PropertyRenewalRegister();
                propertyRenewalRegister = _db.PropertyRenewalRegister.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                if (agreedsiamount == null)
                {
                    propertyRenewalRegister.AgreedSiamount = null;
                }
                else
                {
                    propertyRenewalRegister.AgreedSiamount = Decimal.Parse(agreedsiamount);
                }
                _db.Update(propertyRenewalRegister);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        #endregion

        public JsonResult GetRenewalRemarkById(string id)
        {
            try
            {
                List<RenewalRegisterRemarkViewModel> lstremarkViewModel = new List<RenewalRegisterRemarkViewModel>();
                lstremarkViewModel = (from r in _db.RenewalRegisterRemark
                                      join u in _db.SysUser on r.RemarkBy equals u.Id
                                      where r.RenewalRegisterId == Guid.Parse(id)
                                      orderby r.RemarkDate descending
                                      select new RenewalRegisterRemarkViewModel
                                      {
                                          RenewalRegisterId = r.RenewalRegisterId,
                                          RemarkDate = r.RemarkDate,
                                          UserName = u.Name,
                                          Remark = r.Remark
                                      }).ToList();

                return Json(lstremarkViewModel);
                
            }
            catch (Exception ex)
            {
                return Json("-1");
            }

        }

        private async Task<bool> CreateRenewalCase(Guid id)
        {
            bool renewalCreateFlag = false;
            try
            {
                int successCountRenewal = 0;
                int failCountRenewal = 0;

                Category smsCategory = new Category();
                smsCategory = _db.Category.Where(c => c.Description == "Renewal Notice(EN)").FirstOrDefault();

                List<SMSRenewalViewModel> renewalViewModel = new List<SMSRenewalViewModel>();
                renewalViewModel = dataService.GetSMSRenewalDataById(id);

                foreach(var renewal in renewalViewModel)
                {
                    #region Create Renewal Case 
                    if (smsCategory.RenewalFlag == true)
                    {
                        RenewalCaseInvoiceInfoResponse renewalCaseInvoiceInfoResponse = new RenewalCaseInvoiceInfoResponse();
                        renewalCaseInvoiceInfoResponse = GetRenewalInvoiceNoByInvoiceNo(renewal.InvoiceNo);

                        if (renewalCaseInvoiceInfoResponse.value.Count() > 0)
                        {
                            renewal.CrmrenewalCreateFlag = true;
                            successCountRenewal += 1;
                        }
                        else
                        {
                            string CreateRenewalCaseRetMsg = await CreateRenewalCaseAsync(renewal);
                            if (CreateRenewalCaseRetMsg != "1")
                            {
                                renewal.CrmrenewalCreateFlag = false;
                                failCountRenewal += 1;
                            }
                            else
                            {
                                renewal.CrmrenewalCreateFlag = true;
                                successCountRenewal += 1;
                            }
                        }

                    }
                    #endregion

                    renewalCreateFlag = renewal.CrmrenewalCreateFlag;
                }

                return renewalCreateFlag;
            }
            catch (Exception ex)
            {
                return renewalCreateFlag;
            }
        }

        [HttpPost]
        public JsonResult SaveRenewalRemarkById([FromBody]RenewalRegisterRemarkViewModel requestmodel)
        {
            try
            {
                string userid = "";
                if (CheckLoginUser() == true)
                {
                    userid = HttpContext.Session.GetString("userId");
                }
                else
                {
                    return Json("-1");
                }

                RenewalRegisterRemark renewalRegisterRemark = new RenewalRegisterRemark();
                renewalRegisterRemark.Id = Guid.NewGuid();
                renewalRegisterRemark.RemarkBy = Guid.Parse(userid);
                renewalRegisterRemark.RemarkDate = DateTime.Now;
                renewalRegisterRemark.Remark = requestmodel.Remark;
                renewalRegisterRemark.RenewalRegisterId = requestmodel.Id;
                _db.Entry(renewalRegisterRemark).State = EntityState.Added;
                _db.SaveChanges();

                if(requestmodel.ProductClass == "MOTOR INSURANCE")
                {
                    MotorRenewalRegister motorRenewalRegister = new MotorRenewalRegister();
                    motorRenewalRegister = _db.MotorRenewalRegister.Where(m => m.Id == requestmodel.Id).FirstOrDefault();
                    motorRenewalRegister.RemarkId = true;
                    motorRenewalRegister.RemarkFlag = false;
                    _db.MotorRenewalRegister.Update(motorRenewalRegister);
                    _db.SaveChanges();
                }
                else if(requestmodel.ProductClass == "FIRE INSURANCE")
                {
                    FireRenewalRegister fireRenewalRegister = new FireRenewalRegister();
                    fireRenewalRegister = _db.FireRenewalRegister.Where(f => f.Id == requestmodel.Id).FirstOrDefault();
                    fireRenewalRegister.RemarkId = true;
                    fireRenewalRegister.RemarkFlag = false;
                    _db.FireRenewalRegister.Update(fireRenewalRegister);
                    _db.SaveChanges();
                }
                else if(requestmodel.ProductClass == "HEALTH INSURANCE")
                {
                    HealthRenewalRegister healthRenewalRegister = new HealthRenewalRegister();
                    healthRenewalRegister = _db.HealthRenewalRegister.Where(h => h.Id == requestmodel.Id).FirstOrDefault();
                    healthRenewalRegister.RemarkId = true;
                    healthRenewalRegister.RemarkFlag = false;
                    _db.HealthRenewalRegister.Update(healthRenewalRegister);
                    _db.SaveChanges();
                }
                else if(requestmodel.ProductClass == "PERSONAL ACCIDENT INSURANCE")
                {
                    ParenewalRegister parenewalRegister = new ParenewalRegister();
                    parenewalRegister = _db.ParenewalRegister.Where(p => p.Id == requestmodel.Id).FirstOrDefault();
                    parenewalRegister.RemarkId = true;
                    parenewalRegister.RemarkFlag = false;
                    _db.ParenewalRegister.Update(parenewalRegister);
                    _db.SaveChanges();
                }
                else if(requestmodel.ProductClass == "ENGINEERING")
                {
                    EngineeringRenewalRegister engineeringRenewalRegister = new EngineeringRenewalRegister();
                    engineeringRenewalRegister = _db.EngineeringRenewalRegister.Where(m => m.Id == requestmodel.Id).FirstOrDefault();
                    engineeringRenewalRegister.RemarkId = true;
                    engineeringRenewalRegister.RemarkFlag = false;
                    _db.EngineeringRenewalRegister.Update(engineeringRenewalRegister);
                    _db.SaveChanges();
                }
                else if(requestmodel.ProductClass == "PROPERTY")
                {
                    PropertyRenewalRegister propertyRenewalRegister = new PropertyRenewalRegister();
                    propertyRenewalRegister = _db.PropertyRenewalRegister.Where(m => m.Id == requestmodel.Id).FirstOrDefault();
                    propertyRenewalRegister.RemarkId = true;
                    propertyRenewalRegister.RemarkFlag = false;
                    _db.PropertyRenewalRegister.Update(propertyRenewalRegister);
                    _db.SaveChanges();
                }


                List<RenewalRegisterRemarkViewModel> lstremarkViewModel = new List<RenewalRegisterRemarkViewModel>();
                lstremarkViewModel = (from r in _db.RenewalRegisterRemark
                                      join u in _db.SysUser on r.RemarkBy equals u.Id
                                      where r.RenewalRegisterId == requestmodel.Id
                                      orderby r.RemarkDate descending
                                      select new RenewalRegisterRemarkViewModel
                                      {
                                          RemarkDate = r.RemarkDate,
                                          UserName = u.Name,
                                          Remark = r.Remark
                                      }).ToList();

                if (lstremarkViewModel.Count() != 0)
                {
                    return Json(lstremarkViewModel);
                }
                else
                {
                    return Json("-1");
                }
            }
            catch (Exception ex)
            {
                return Json("-1");
            }

        }

        [HttpPost]
        public JsonResult UpdateRenewalRemarkDone([FromBody]RenewalRegisterRemarkViewModel requestmodel)
        {
            try
            {
                string userid = "";
                if (CheckLoginUser() == true)
                {
                    userid = HttpContext.Session.GetString("userId");
                }
                else
                {
                    return Json("-1");
                }

                RenewalRegisterRemark renewalRegisterRemark = new RenewalRegisterRemark();
                renewalRegisterRemark.Id = Guid.NewGuid();
                renewalRegisterRemark.RemarkBy = Guid.Parse(userid);
                renewalRegisterRemark.RemarkDate = DateTime.Now;
                renewalRegisterRemark.Remark = "This transaction was done successfully";
                renewalRegisterRemark.RenewalRegisterId = requestmodel.Id;
                _db.Entry(renewalRegisterRemark).State = EntityState.Added;
                _db.SaveChanges();

                if (requestmodel.ProductClass == "MOTOR INSURANCE")
                {
                    MotorRenewalRegister motorRenewalRegister = new MotorRenewalRegister();
                    motorRenewalRegister = _db.MotorRenewalRegister.Where(m => m.Id == requestmodel.Id).FirstOrDefault();
                    motorRenewalRegister.RemarkFlag = true;
                    motorRenewalRegister.RemarkId = true;
                    _db.MotorRenewalRegister.Update(motorRenewalRegister);
                    _db.SaveChanges();
                }
                else if(requestmodel.ProductClass == "FIRE INSURANCE")
                {
                    FireRenewalRegister fireRenewalRegister = new FireRenewalRegister();
                    fireRenewalRegister = _db.FireRenewalRegister.Where(f => f.Id == requestmodel.Id).FirstOrDefault();
                    fireRenewalRegister.RemarkFlag = true;
                    fireRenewalRegister.RemarkId = true;
                    _db.FireRenewalRegister.Update(fireRenewalRegister);
                    _db.SaveChanges();
                }
                else if(requestmodel.ProductClass == "HEALTH INSURANCE")
                {
                    HealthRenewalRegister healthRenewalRegister = new HealthRenewalRegister();
                    healthRenewalRegister = _db.HealthRenewalRegister.Where(h => h.Id == requestmodel.Id).FirstOrDefault();
                    healthRenewalRegister.RemarkFlag = true;
                    healthRenewalRegister.RemarkId = true;
                    _db.HealthRenewalRegister.Update(healthRenewalRegister);
                    _db.SaveChanges();
                }
                else if(requestmodel.ProductClass == "PERSONAL ACCIDENT INSURANCE")
                {
                    ParenewalRegister parenewalRegister = new ParenewalRegister();
                    parenewalRegister = _db.ParenewalRegister.Where(p => p.Id == requestmodel.Id).FirstOrDefault();
                    parenewalRegister.RemarkFlag = true;
                    parenewalRegister.RemarkId = true;
                    _db.ParenewalRegister.Update(parenewalRegister);
                    _db.SaveChanges();
                }
                else if(requestmodel.ProductClass == "ENGINEERING")
                {
                    EngineeringRenewalRegister engineeringRenewalRegister = new EngineeringRenewalRegister();
                    engineeringRenewalRegister = _db.EngineeringRenewalRegister.Where(m => m.Id == requestmodel.Id).FirstOrDefault();
                    engineeringRenewalRegister.RemarkFlag = true;
                    engineeringRenewalRegister.RemarkId = true;
                    _db.EngineeringRenewalRegister.Update(engineeringRenewalRegister);
                    _db.SaveChanges();
                }
                else if(requestmodel.ProductClass == "PROPERTY")
                {
                    PropertyRenewalRegister propertyRenewalRegister = new PropertyRenewalRegister();
                    propertyRenewalRegister = _db.PropertyRenewalRegister.Where(m => m.Id == requestmodel.Id).FirstOrDefault();
                    propertyRenewalRegister.RemarkFlag = true;
                    propertyRenewalRegister.RemarkId = true;
                    _db.PropertyRenewalRegister.Update(propertyRenewalRegister);
                    _db.SaveChanges();
                }

                List<RenewalRegisterRemarkViewModel> lstremarkViewModel = new List<RenewalRegisterRemarkViewModel>();
                lstremarkViewModel = (from r in _db.RenewalRegisterRemark
                                      join u in _db.SysUser on r.RemarkBy equals u.Id
                                      where r.RenewalRegisterId == requestmodel.Id
                                      orderby r.RemarkDate descending
                                      select new RenewalRegisterRemarkViewModel
                                      {
                                          RemarkDate = r.RemarkDate,
                                          UserName = u.Name,
                                          Remark = r.Remark
                                      }).ToList();

                if (lstremarkViewModel.Count() != 0)
                {
                    return Json(lstremarkViewModel);
                }
                else
                {
                    return Json("-1");
                }
            }
            catch(Exception ex)
            {
                return Json("-1");
            }
        }

        public bool CheckLoginUser()
        {
            if (HttpContext.Session.GetString("userId") == null)
                return false;
            return true;
        }

        #region Customer timeline activity
        private async Task<string> CreateCRMActivityAsync(string cusCode, string description)
        {
            var body = new JObject();
            string responebody = "-1";
            ValidateCustomerResponse validateCustomerResponse = new ValidateCustomerResponse();

            List<NewCustomerInfo> lstcustInfo = new List<NewCustomerInfo>();
            lstcustInfo = await GetNewCustomerInfoByCusCode(cusCode);

            if (lstcustInfo.Count() > 0)
            {
                foreach (var lst in lstcustInfo)
                {
                    if (lst.cus_type == "I")
                    {
                        validateCustomerResponse = GetCRMContactsCustomerByCusCode(cusCode);
                        if (validateCustomerResponse.value != null)
                        {
                            if (validateCustomerResponse.value.Count() > 0)
                            {
                                string contactid = "";
                                foreach (var custccode in validateCustomerResponse.value)
                                {
                                    contactid = custccode.contactid;
                                }
                                Guid contactId = Guid.Parse(contactid);
                                if (contactId != null)
                                {
                                    string activityrequesturl = "ayasompo_contactcustomactivities";
                                    body = new JObject
                                    {
                                         { "subject", description },
                                         { "regardingobjectid_contact@odata.bind", "/contacts("+ contactid +")" },
                                    };

                                    responebody = cRMAPIService.CreateActivities(HttpMethod.Post, body, activityrequesturl);
                                }
                            }
                        }
                    }
                    else
                    {
                        ValidateCorpCustomerResponse validateCorpCustomerResponse = new ValidateCorpCustomerResponse();
                        validateCorpCustomerResponse = GetCRMAccountCustomerByCusCode(cusCode);

                        if (validateCorpCustomerResponse.value != null)
                        {
                            if (validateCorpCustomerResponse.value.Count() > 0)
                            {
                                string contactid = "";
                                foreach (var custccode in validateCorpCustomerResponse.value)
                                {
                                    contactid = custccode.accountid;
                                }
                                Guid contactId = Guid.Parse(contactid);
                                if (contactId != null)
                                {
                                    string activityrequesturl = "ayasompo_contactcustomactivities";
                                    body = new JObject
                                    {
                                         { "subject", description },
                                         { "regardingobjectid_account@odata.bind", "/accounts("+ contactid +")" },
                                    };

                                    responebody = cRMAPIService.CreateActivities(HttpMethod.Post, body, activityrequesturl);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                return responebody;
            }

            return responebody;
        }

        private ValidateCustomerResponse GetCRMContactsCustomerByCusCode(string cuscode)
        {
            ValidateCustomerResponse validateCustomerResponse = new ValidateCustomerResponse();
            string responseBody = "";
            var body = new JObject();
            string url = "";

            if (cuscode != null)
            {
                url = "contacts?$select=fullname&$filter=ayasompo_customercode eq '" + cuscode + "'";
                responseBody = cRMAPIService.CRMGetCustomerByCustomerCode(HttpMethod.Get, body = null, url);
                validateCustomerResponse = JsonConvert.DeserializeObject<ValidateCustomerResponse>(responseBody);
            }
            return validateCustomerResponse;
        }

        private ValidateCorpCustomerResponse GetCRMAccountCustomerByCusCode(string cuscode)
        {
            ValidateCorpCustomerResponse validateCorpCustomerResponse = new ValidateCorpCustomerResponse();
            string responseBody = "";
            var body = new JObject();
            string url = "";

            if (cuscode != null)
            {
                url = "accounts?$select=name&$filter=ayasompo_code eq '" + cuscode + "'";
                responseBody = cRMAPIService.CRMGetCustomerByCustomerCode(HttpMethod.Get, body = null, url);
                validateCorpCustomerResponse = JsonConvert.DeserializeObject<ValidateCorpCustomerResponse>(responseBody);

            }
            return validateCorpCustomerResponse;
        }

        private async Task<List<NewCustomerInfo>> GetNewCustomerInfoByCusCode(string cuscode)
        {
            List<NewCustomerInfo> newCustomerInfos = new List<NewCustomerInfo>();
            try
            {
                var responseTask = await _client.GetAsync($"/customer/GetNewCustomerByCusCode/{cuscode}");
                if (responseTask.IsSuccessStatusCode)
                {
                    //newCustomerInfos = await responseTask.Content.ReadAsAsync<List<NewCustomerInfo>>();
                    //newCustomerInfos = await responseTask.Content.ReadAsStreamAsync<List<NewCustomerInfo>>();

                    string jsonData = await responseTask.Content.ReadAsStringAsync();
                    newCustomerInfos = JsonConvert.DeserializeObject<List<NewCustomerInfo>>(jsonData);
                    return newCustomerInfos;
                }
                else
                {
                    return newCustomerInfos = new List<NewCustomerInfo>();
                }
            }
            catch (Exception ex)
            {
                return newCustomerInfos = new List<NewCustomerInfo>();
            }
        }
        #endregion

        #endregion

        #region Renewal Case Create
        private RenewalCaseInvoiceInfoResponse GetRenewalInvoiceNoByInvoiceNo(string invoiceNo)
        {
            RenewalCaseInvoiceInfoResponse renewalCaseInvoiceInfoResponse = new RenewalCaseInvoiceInfoResponse();
            string responseBody = "";
            var body = new JObject();
            string url = "";

            if (invoiceNo != null)
            {
                url = "incidents?$select=incidentid&$filter=ayasompo_tempdebitnote eq '" + invoiceNo + "'";
                responseBody = cRMAPIService.CRMGetRenewalInvoiceByTempDN(HttpMethod.Get, body = null, url);
                renewalCaseInvoiceInfoResponse = JsonConvert.DeserializeObject<RenewalCaseInvoiceInfoResponse>(responseBody);
            }
            return renewalCaseInvoiceInfoResponse;
        }

        private async Task<string> CreateRenewalCaseAsync(SMSRenewalViewModel renewal)
        {
            string responebody = "-1";
            try
            {
                var body = new JObject();

                ValidateCustomerResponse validateCustomerResponse = new ValidateCustomerResponse();

                List<NewCustomerInfo> lstcustInfo = new List<NewCustomerInfo>();
                lstcustInfo = await GetNewCustomerInfoByCusCode(renewal.CusCode);

                if (lstcustInfo.Count() > 0)
                {
                    foreach (var lst in lstcustInfo)
                    {
                        if (lst.cus_type == "I")
                        {
                            validateCustomerResponse = GetCRMContactsCustomerByCusCode(renewal.CusCode);
                            if (validateCustomerResponse.value != null)
                            {
                                if (validateCustomerResponse.value.Count() > 0)
                                {
                                    string contactid = "";
                                    foreach (var custccode in validateCustomerResponse.value)
                                    {
                                        contactid = custccode.contactid;
                                    }
                                    Guid contactId = Guid.Parse(contactid);
                                    if (contactId != null)
                                    {

                                        string renewalrequesturl = "incidents";
                                        string personInchargeId = "7EA9B346-1F78-4043-8231-71D008192BA4";
                                        string producttypeId = "ed0f9883-1ec4-eb11-bacc-002248171f24"; // Need to ch
                                        string accountCode = "";
                                        string accountHandlerName = "";
                                        string productCode = "";
                                        string classCode = "";
                                        string month = "";
                                        string fromDate = Convert.ToDateTime(renewal.FromDate).ToShortDateString();
                                        string toDate = Convert.ToDateTime(renewal.ToDate).ToShortDateString();
                                        string branch = "";
                                        string intermediarytype = "";
                                        string renewalagentcode = "";
                                        string renewalagentname = "";
                                        decimal siamount = 0;
                                        decimal no_claim_bonus = 0;
                                        decimal claim_loading = 0;

                                        var PIC = _JWTDataContext.CrmPersonInCharge.Where(p => p.Picname == renewal.PersonInCharge).FirstOrDefault();

                                        if (PIC != null)
                                        {
                                            personInchargeId = PIC.CrmPicId.ToString();
                                        }

                                        CrmenquiryProductType enquiryProductType = new CrmenquiryProductType();
                                        if (renewal.Class == "ENGINEERING")
                                        {
                                            if(renewal.Product != null)
                                            {
                                                enquiryProductType = _JWTDataContext.CrmenquiryProductType.Where(p => p.CoreProductName == renewal.Product).FirstOrDefault();
                                            }
                                            else
                                            {
                                                enquiryProductType = _JWTDataContext.CrmenquiryProductType.Where(p => p.CoreProductName == renewal.Class).FirstOrDefault();
                                            }
                                            
                                        }
                                        else
                                        {
                                            enquiryProductType = _JWTDataContext.CrmenquiryProductType.Where(p => p.CoreProductName == renewal.Class).FirstOrDefault();
                                        }

                                        if (enquiryProductType != null)
                                        {
                                            producttypeId = enquiryProductType.CrmproductId.ToString();
                                        }

                                        AccountCodeInfo accountCodeInfo = new AccountCodeInfo();
                                        if (renewal.PolicyNo != null)
                                        {
                                            accountCodeInfo = await GetAccountCodeByPolicyNo(renewal.InvoiceNo);
                                            if (accountCodeInfo != null)
                                            {
                                                //policyNo = accountCodeInfo.pol_policy_no;
                                                accountCode = accountCodeInfo.account_code;
                                                accountHandlerName = accountCodeInfo.account_handler_name;
                                                productCode = accountCodeInfo.pol_prd_code;
                                                classCode = accountCodeInfo.pol_cla_code;
                                                month = Convert.ToDateTime(renewal.FromDate).ToString("dd/MMM/yyyy") + " to " + Convert.ToDateTime(renewal.ToDate).ToString("dd/MMM/yyyy");
                                                branch = accountCodeInfo.pol_slc_brn_code;
                                                intermediarytype = accountCodeInfo.intermediatery_type;
                                                renewalagentcode = accountCodeInfo.agent_code;
                                                renewalagentname = accountCodeInfo.agent_name;
                                                siamount = Convert.ToDecimal(accountCodeInfo.si);
                                                no_claim_bonus = Convert.ToDecimal(accountCodeInfo.no_claim_bonus);
                                                claim_loading = Convert.ToDecimal(accountCodeInfo.claim_loading);

                                            }
                                            else
                                            {
                                                return responebody;
                                            }
                                        }
                                        else
                                        {
                                            return responebody;
                                        }

                                        #region Json Package for Renewal Creation
                                        body = new JObject
                                        {
                                            { "casetypecode", 4 },
                                            { "title", "Individual Renewal Management List" },
                                            { "customerid_contact@odata.bind", "/contacts("+ contactid +")" },
                                            { "ayasompo_enquiryproducttype@odata.bind", "/ayasompo_enquiryproducttypes("+ producttypeId +")" },
                                            { "ayasompo_personincharge@odata.bind", "/ayasompo_personincharges("+ personInchargeId +")" },
                                            { "ayasompo_policyno", renewal.PolicyNo },
                                            { "ayasompo_productcode", productCode },
                                            { "ayasompo_classcode", classCode },
                                            { "ayasompo_vehicleno", renewal.RiskName },
                                            { "ayasompo_accountcode", renewal.AccountCode },
                                            { "ayasompo_accounthandlername", accountHandlerName },
                                            { "ayasompo_phnorenewal", renewal.PhoneNumber },
                                            { "ayasompo_renewalpaymentamount", Convert.ToDecimal(renewal.RenewalPremium) },
                                            { "ayasompo_policyperiod", month },
                                            { "ayasompo_debitnote", renewal.InvoiceNo },
                                            { "ayasompo_renewalstatus", 1 },
                                            { "ayasompo_renewalnotice", renewal.Smsflag },
                                            {"ayasompo_tempdebitnote", renewal.InvoiceNo },
                                            {"ayasompo_renewalbranch", renewal.Branch },
                                            {"ayasompo_intermediarytype", renewal.IntermediaryType },
                                            {"ayasompo_renewalagentcode", renewalagentcode },
                                            {"ayasompo_renewalagentname", renewalagentname },
                                            {"ayasompo_responsibleparty", renewal.ResponsibleParty },
                                            {"ayasompo_renewalexpirydate", Convert.ToDateTime(renewal.ExpiryDate).Date },
                                            {"ayasompo_siamount", siamount },
                                            {"ayasompo_renewalfromdate", Convert.ToDateTime(renewal.FromDate).Date  },
                                            {"ayasompo_renewaltodate", Convert.ToDateTime(renewal.ToDate).Date },
                                            {"ayasompo_claimloading", claim_loading },
                                            {"ayasompo_noclaimbonus", no_claim_bonus }
                                        };

                                        #endregion

                                        responebody = cRMAPIService.CreateRenewal(HttpMethod.Post, body, renewalrequesturl);

                                        if (responebody != "-1")
                                        {
                                            CreateRenewalTimeLineActivity(siamount, Convert.ToDecimal(renewal.RenewalPremium), renewal.InvoiceNo, no_claim_bonus, claim_loading);
                                        }

                                        return responebody;
                                    }
                                }
                            }
                        }
                        else
                        {
                            ValidateCorpCustomerResponse validateCorpCustomerResponse = new ValidateCorpCustomerResponse();
                            validateCorpCustomerResponse = GetCRMAccountCustomerByCusCode(renewal.CusCode);

                            if (validateCorpCustomerResponse.value != null)
                            {
                                if (validateCorpCustomerResponse.value.Count() > 0)
                                {
                                    string contactid = "";
                                    foreach (var custccode in validateCorpCustomerResponse.value)
                                    {
                                        contactid = custccode.accountid;
                                    }
                                    Guid contactId = Guid.Parse(contactid);
                                    if (contactId != null)
                                    {
                                        string renewalrequesturl = "incidents";
                                        string personInchargeId = "7EA9B346-1F78-4043-8231-71D008192BA4";
                                        string producttypeId = "ed0f9883-1ec4-eb11-bacc-002248171f24"; // Need to ch
                                        string accountCode = "";
                                        string accountHandlerName = "";
                                        string productCode = "";
                                        string classCode = "";
                                        string month = "";
                                        string fromDate = Convert.ToDateTime(renewal.FromDate).ToShortDateString();
                                        string toDate = Convert.ToDateTime(renewal.ToDate).ToShortDateString();
                                        string branch = "";
                                        string intermediarytype = "";
                                        string renewalagentcode = "";
                                        string renewalagentname = "";
                                        decimal siamount = 0;
                                        decimal no_claim_bonus = 0;
                                        decimal claim_loading = 0;

                                        var PIC = _JWTDataContext.CrmPersonInCharge.Where(p => p.Picname == renewal.PersonInCharge).FirstOrDefault();

                                        if (PIC != null)
                                        {
                                            personInchargeId = PIC.CrmPicId.ToString();
                                        }

                                        //var enquiryProductType = _JWTDataContext.CrmenquiryProductType.Where(p => p.CoreProductName == renewal.Class).FirstOrDefault();
                                        //if (enquiryProductType != null)
                                        //{
                                        //    producttypeId = enquiryProductType.CrmproductId.ToString();
                                        //}

                                        CrmenquiryProductType enquiryProductType = new CrmenquiryProductType();
                                        if (renewal.Class == "ENGINEERING")
                                        {
                                            if (renewal.Product != null)
                                            {
                                                enquiryProductType = _JWTDataContext.CrmenquiryProductType.Where(p => p.CoreProductName == renewal.Product).FirstOrDefault();
                                            }
                                            else
                                            {
                                                enquiryProductType = _JWTDataContext.CrmenquiryProductType.Where(p => p.CoreProductName == renewal.Class).FirstOrDefault();
                                            }

                                        }
                                        else
                                        {
                                            enquiryProductType = _JWTDataContext.CrmenquiryProductType.Where(p => p.CoreProductName == renewal.Class).FirstOrDefault();
                                        }

                                        if (enquiryProductType != null)
                                        {
                                            producttypeId = enquiryProductType.CrmproductId.ToString();
                                        }

                                        AccountCodeInfo accountCodeInfo = new AccountCodeInfo();
                                        if (renewal.PolicyNo != null)
                                        {
                                            accountCodeInfo = await GetAccountCodeByPolicyNo(renewal.InvoiceNo);
                                            if (accountCodeInfo != null)
                                            {
                                                //policyNo = accountCodeInfo.pol_policy_no;
                                                accountCode = accountCodeInfo.account_code;
                                                accountHandlerName = accountCodeInfo.account_handler_name;
                                                productCode = accountCodeInfo.pol_prd_code;
                                                classCode = accountCodeInfo.pol_cla_code;
                                                month = Convert.ToDateTime(renewal.FromDate).ToString("dd/MMM/yyyy") + " to " + Convert.ToDateTime(renewal.ToDate).ToString("dd/MMM/yyyy");
                                                branch = accountCodeInfo.pol_slc_brn_code;
                                                intermediarytype = accountCodeInfo.intermediatery_type;
                                                renewalagentcode = accountCodeInfo.agent_code;
                                                renewalagentname = accountCodeInfo.agent_name;
                                                siamount = Convert.ToDecimal(accountCodeInfo.si);
                                                no_claim_bonus = Convert.ToDecimal(accountCodeInfo.no_claim_bonus);
                                                claim_loading = Convert.ToDecimal(accountCodeInfo.claim_loading);
                                            }
                                            else
                                            {
                                                return responebody;
                                            }
                                        }
                                        else
                                        {
                                            return responebody;
                                        }

                                        #region Json Package for Renewal Creation
                                        body = new JObject
                                        {
                                            { "casetypecode", 4 },
                                            { "title", "Individual Renewal Management List" },
                                            { "customerid_account@odata.bind", "/accounts("+ contactid +")" },
                                            { "ayasompo_enquiryproducttype@odata.bind", "/ayasompo_enquiryproducttypes("+ producttypeId +")" },
                                            { "ayasompo_personincharge@odata.bind", "/ayasompo_personincharges("+ personInchargeId +")" },
                                             { "ayasompo_policyno", renewal.PolicyNo },
                                            { "ayasompo_productcode", productCode },
                                            { "ayasompo_classcode", classCode },
                                            { "ayasompo_vehicleno", renewal.RiskName },
                                            { "ayasompo_accountcode", renewal.AccountCode },
                                            { "ayasompo_accounthandlername", accountHandlerName },
                                            { "ayasompo_phnorenewal", renewal.PhoneNumber },
                                            { "ayasompo_renewalpaymentamount", Convert.ToDecimal(renewal.RenewalPremium) },
                                            { "ayasompo_policyperiod", month },
                                            { "ayasompo_debitnote", renewal.InvoiceNo },
                                            { "ayasompo_renewalstatus", 1 },
                                            { "ayasompo_renewalnotice", renewal.Smsflag },
                                            {"ayasompo_tempdebitnote", renewal.InvoiceNo },
                                            {"ayasompo_renewalbranch", renewal.Branch },
                                            {"ayasompo_intermediarytype", renewal.IntermediaryType },
                                            {"ayasompo_renewalagentcode", renewalagentcode },
                                            {"ayasompo_renewalagentname", renewalagentname },
                                            {"ayasompo_responsibleparty", renewal.ResponsibleParty },
                                            {"ayasompo_renewalexpirydate", Convert.ToDateTime(renewal.ExpiryDate).Date },
                                            {"ayasompo_siamount", siamount },
                                            {"ayasompo_renewalfromdate", Convert.ToDateTime(renewal.FromDate).Date  },
                                            {"ayasompo_renewaltodate", Convert.ToDateTime(renewal.ToDate).Date },
                                            {"ayasompo_claimloading", claim_loading },
                                            {"ayasompo_noclaimbonus", no_claim_bonus }
                                        };

                                        #endregion

                                        responebody = cRMAPIService.CreateRenewal(HttpMethod.Post, body, renewalrequesturl);

                                        if (responebody != "-1")
                                        {
                                            CreateRenewalTimeLineActivity(siamount, Convert.ToDecimal(renewal.RenewalPremium), renewal.InvoiceNo, no_claim_bonus, claim_loading);
                                        }

                                        return responebody;
                                    }
                                }
                            }
                        }

                    }
                }
                else
                {
                    return responebody;
                }

                return responebody;
            }
            catch (Exception ex)
            {
                return responebody;
            }
        }

        private async Task<AccountCodeInfo> GetAccountCodeByPolicyNo(string invoiceNo)
        {
            AccountCodeInfo accountCodeInfos = new AccountCodeInfo();
            try
            {
                string invoiceNumber = Convert.ToBase64String(Encoding.UTF8.GetBytes(invoiceNo));
                var responseTask = await _client.GetAsync($"/customer/GetAccountCodeByPolicyNo/{invoiceNumber}");
                if (responseTask.IsSuccessStatusCode)
                {
                    //newCustomerInfos = await responseTask.Content.ReadAsAsync<List<NewCustomerInfo>>();
                    //newCustomerInfos = await responseTask.Content.ReadAsStreamAsync<List<NewCustomerInfo>>();

                    string jsonData = await responseTask.Content.ReadAsStringAsync();
                    accountCodeInfos = JsonConvert.DeserializeObject<AccountCodeInfo>(jsonData);
                    return accountCodeInfos;
                }
                else
                {
                    return accountCodeInfos;
                }
            }
            catch (Exception ex)
            {
                return accountCodeInfos;
            }
        }

        private void CreateRenewalTimeLineActivity(decimal siamount, decimal renewalPremium, string invoiceNo, decimal no_claim_bonus, decimal claim_loading)
        {
            try
            {
                RenewalCaseInvoiceInfoResponse renewalCaseInvoiceInfoResponse = new RenewalCaseInvoiceInfoResponse();
                renewalCaseInvoiceInfoResponse = GetRenewalInvoiceNoByInvoiceNo(invoiceNo);

                if (renewalCaseInvoiceInfoResponse.value.Count() > 0)
                {
                    string incidentid = "";
                    foreach (var custccode in renewalCaseInvoiceInfoResponse.value)
                    {
                        incidentid = custccode.incidentid;

                        Guid incidentID = Guid.Parse(incidentid);
                        if (incidentID != null)
                        {
                            string url = "ayasompo_customtimelines";
                            string desp = "SI Amount - " + Convert.ToDecimal(siamount).ToString("N")
                                           + ",Premium Amount - " + Convert.ToDecimal(renewalPremium).ToString("N")
                                           + ",Invoice No - " + invoiceNo
                                           + ",No Claim Bonus - " + Convert.ToDecimal(no_claim_bonus).ToString("N")
                                           + ",Claim Loading - " + Convert.ToDecimal(claim_loading).ToString("N");
                            var body = new JObject();
                            body = new JObject
                               {
                                  { "subject" , "Renewal Case Created with initial invoice information at " + DateTime.Now },
                                  { "description", desp },
                                  { "regardingobjectid_incident@odata.bind", "/incidents("+ incidentid +")" }
                               };

                            cRMAPIService.CreateRenewalInvoiceActivities(HttpMethod.Post, body, url);

                        }

                    }

                }
            }
            catch (Exception ex)
            {

            }
        }

        #endregion

        #region Renewal Fail Case 
        public IActionResult RenewalFailCase()
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }

            PrepareDataForRenewalReg();

            return View();
        }

        //[HttpPost]
        public IActionResult GetRenewalFailCase(filterRenewalFailCase filterRenewalData)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            PrepareDataForRenewalReg();
            try
            {
                if (filterRenewalData.filterFromDate == null || filterRenewalData.filterToDate == null)
                {
                    TempData["Message"] = "Invalid request data";
                    return View("RenewalFailCase");
                }

                filterRenewalFailCase filterRenewalFailCase = new filterRenewalFailCase();
                List<SMSRenewalViewModel> lstSMSRenewalViewModels = new List<SMSRenewalViewModel>();
                lstSMSRenewalViewModels = dataService.GetRenewalFailCaseByFilter(filterRenewalData);

                //filterRenewalFailCase.lstSentSMS = (from sms in _db.SentSms
                //                                    join view in lstSMSRenewalViewModels on sms.PolicyNo equals view.InvoiceNo
                //                                    where Convert.ToDateTime(sms.SentDate).Date >= filterRenewalData.filterFromDate.Date
                //                                    where Convert.ToDateTime(sms.SentDate).Date <= filterRenewalData.filterToDate.Date
                //                                    where sms.CrmrenewalCreateFlag == false
                //                                    select new SMSRenewalViewModel
                //                                    {
                //                                        Id = view.Id,
                //                                        smsId = sms.Id,
                //                                        PhoneNumber = view.PhoneNumber,
                //                                        CustomerName = view.CustomerName,
                //                                        InvoiceNo = view.InvoiceNo,
                //                                        RiskName = view.RiskName,
                //                                        ExpiryDate = view.ExpiryDate,
                //                                        RenewalPremium = view.RenewalPremium,
                //                                        Product = view.Product,
                //                                        Class = view.Class,
                //                                        FromDate = view.FromDate,
                //                                        ToDate = view.ToDate,
                //                                        Link = view.Link,
                //                                        CusCode = view.CusCode,
                //                                        Month = view.Month,
                //                                        PersonInCharge = view.PersonInCharge,
                //                                        ResponsibleParty = view.ResponsibleParty,
                //                                        PolicyNo = view.PolicyNo,
                //                                        Branch = view.Branch,
                //                                        IntermediaryType = view.IntermediaryType,
                //                                        AgentCode = view.AgentCode,
                //                                        AgentName = view.AgentName,
                //                                        AccountCode = view.AccountCode,
                //                                        AuthorizedCode = view.AuthorizedCode,
                //                                        AuthorizedName = view.AuthorizedName,
                //                                    }).ToList();
                
                HttpContext.Session.SetComplexData("lstRenewalFailCaseList", lstSMSRenewalViewModels);
                ViewBag.lstSMSRenewalViewModels = lstSMSRenewalViewModels;
                
                return View("RenewalFailCase", filterRenewalFailCase);
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        public async Task<IActionResult> RecreateFailCase()
        {
            try
            {
                PrepareDataForRenewalReg();
                if (!CheckLoginUser())
                    return RedirectToAction("Login", "Account");

                if (HttpContext.Session.GetObjectFromJson<List<SMSRenewalViewModel>>("lstRenewalFailCaseList") != null)
                {
                    List<SMSRenewalViewModel> lstSMSRenewalViewModels = new List<SMSRenewalViewModel>();
                    lstSMSRenewalViewModels = HttpContext.Session.GetObjectFromJson<List<SMSRenewalViewModel>>("lstRenewalFailCaseList");

                    int count = await RecreateRenewalFailCaseAsync(lstSMSRenewalViewModels);
                    if(count > 0)
                    {
                        TempData["Message"] = "Re-create renewal " + count + " case created successfully.";
                    }
                    else
                    {
                        TempData["Message"] = "Re-create renewal case cannot created successfully." ;
                    }
                }
                else
                {
                    TempData["Message"] = "Re-create renewal fail prcess.";
                }
                return View("RenewalFailCase");
            }
            catch (Exception ex)
            {
                return View("RenewalFailCase");
            }
        }

        private async Task<int> RecreateRenewalFailCaseAsync(List<SMSRenewalViewModel> lstSMSRenewalViewModels)
        {
            try
            {
                List<SentSms> lstSentSMS = new List<SentSms>();
               
                Category smsCategory = new Category();
                smsCategory = _db.Category.Where(c => c.Description == "Renewal Notice(EN)").FirstOrDefault();
                List<ReCreateRenewalFailCase> lstRereateRenewalFailCases = new List<ReCreateRenewalFailCase>();
                foreach (var renewal in lstSMSRenewalViewModels)
                {
                    SentSms sentSms = new SentSms();
                    
                    #region Create Renewal Case 
                    if (smsCategory.RenewalFlag == true)
                    {
                        ReCreateRenewalFailCase reCreate = new ReCreateRenewalFailCase();
                        //reCreate.smsId = renewal.smsId;
                        reCreate.Id = renewal.Id;
                        reCreate.Class = renewal.Class;
                        reCreate.PolicyNo = renewal.InvoiceNo;

                        RenewalCaseInvoiceInfoResponse renewalCaseInvoiceInfoResponse = new RenewalCaseInvoiceInfoResponse();
                        renewalCaseInvoiceInfoResponse = GetRenewalInvoiceNoByInvoiceNo(renewal.InvoiceNo);

                        if (renewalCaseInvoiceInfoResponse.value.Count() > 0)
                        {
                            reCreate.isCRMRenewalCreateFlag = true;
                        }
                        else
                        {
                            string CreateRenewalCaseRetMsg = await CreateRenewalCaseAsync(renewal);
                            if (CreateRenewalCaseRetMsg != "1")
                            {
                                reCreate.isCRMRenewalCreateFlag = false;
                            }
                            else
                            {
                                reCreate.isCRMRenewalCreateFlag = true;
                            }

                        }
                        
                        lstRereateRenewalFailCases.Add(reCreate);
                    }
                    #endregion
                    
                }

                #region Update SentSMS and Update Case Table 

                UpdateRenewalFailCase(lstRereateRenewalFailCases);
                var reRenewalfailcount = lstRereateRenewalFailCases.Where(f => f.isCRMRenewalCreateFlag == true).ToList().Count();
                return reRenewalfailcount;

                #endregion
            }
            catch (Exception ex)
            {
                return -1;
                
            }
        }

        private void UpdateRenewalFailCase(List<ReCreateRenewalFailCase> lstRereateRenewalFailCases)
        {
            try
            {
                if (lstRereateRenewalFailCases.Count() > 0)
                {
                    foreach (var failCase in lstRereateRenewalFailCases)
                    {
                        SentSms sentSms = new SentSms();
                        sentSms = _db.SentSms.Where(x => x.PolicyNo == failCase.PolicyNo).FirstOrDefault();
                        if (sentSms != null)
                        {
                            sentSms.CrmrenewalCreateFlag = failCase.isCRMRenewalCreateFlag;
                            _db.Update(sentSms);
                            _db.SaveChanges();
                        }

                        if (failCase.Class == "MOTOR INSURANCE" && failCase.isCRMRenewalCreateFlag == true)
                        {
                            MotorRenewalRegister motorRenewalRegister = new MotorRenewalRegister();
                            motorRenewalRegister = _db.MotorRenewalRegister.Where(m => m.Id == failCase.Id).FirstOrDefault();
                            motorRenewalRegister.CrmrenewalCreateFlag = failCase.isCRMRenewalCreateFlag;
                            motorRenewalRegister.CrmrenewalCreatedDate = DateTime.Now.Date;
                            _db.Entry(motorRenewalRegister).State = EntityState.Modified;
                            _db.SaveChanges();
                        }
                        else if (failCase.Class == "FIRE INSURANCE" && failCase.isCRMRenewalCreateFlag == true)
                        {
                            FireRenewalRegister fireRenewalRegister = new FireRenewalRegister();
                            fireRenewalRegister = _db.FireRenewalRegister.Where(m => m.Id == failCase.Id).FirstOrDefault();
                            fireRenewalRegister.CrmrenewalCreateFlag = failCase.isCRMRenewalCreateFlag;
                            fireRenewalRegister.CrmrenewalCreatedDate = DateTime.Now.Date;
                            _db.Entry(fireRenewalRegister).State = EntityState.Modified;
                            _db.SaveChanges();
                        }
                        else if (failCase.Class == "HEALTH INSURANCE" && failCase.isCRMRenewalCreateFlag == true)
                        {
                            HealthRenewalRegister healthRenewalRegister = new HealthRenewalRegister();
                            healthRenewalRegister = _db.HealthRenewalRegister.Where(m => m.Id == failCase.Id).FirstOrDefault();
                            healthRenewalRegister.CrmrenewalCreateFlag = failCase.isCRMRenewalCreateFlag;
                            healthRenewalRegister.CrmrenewalCreatedDate = DateTime.Now.Date;
                            _db.Entry(healthRenewalRegister).State = EntityState.Modified;
                            _db.SaveChanges();
                        }
                        else if (failCase.Class == "PERSONAL ACCIDENT INSURANCE" && failCase.isCRMRenewalCreateFlag == true)
                        {
                            ParenewalRegister parenewalRegister = new ParenewalRegister();
                            parenewalRegister = _db.ParenewalRegister.Where(m => m.Id == failCase.Id).FirstOrDefault();
                            parenewalRegister.CrmrenewalCreateFlag = failCase.isCRMRenewalCreateFlag;
                            parenewalRegister.CrmrenewalCreatedDate = DateTime.Now.Date;
                            _db.Entry(parenewalRegister).State = EntityState.Modified;
                            _db.SaveChanges();
                        }
                        else if (failCase.Class == "ENGINEERING" && failCase.isCRMRenewalCreateFlag == true)
                        {
                            EngineeringRenewalRegister engineeringRenewalRegister = new EngineeringRenewalRegister();
                            engineeringRenewalRegister = _db.EngineeringRenewalRegister.Where(m => m.Id == failCase.Id).FirstOrDefault();
                            engineeringRenewalRegister.CrmrenewalCreateFlag = failCase.isCRMRenewalCreateFlag;
                            engineeringRenewalRegister.CrmrenewalCreatedDate = DateTime.Now.Date;
                            _db.Entry(engineeringRenewalRegister).State = EntityState.Modified;
                            _db.SaveChanges();
                        }
                        else if (failCase.Class == "PROPERTY" && failCase.isCRMRenewalCreateFlag == true)
                        {
                            PropertyRenewalRegister propertyRenewalRegister = new PropertyRenewalRegister();
                            propertyRenewalRegister = _db.PropertyRenewalRegister.Where(m => m.Id == failCase.Id).FirstOrDefault();
                            propertyRenewalRegister.CrmrenewalCreateFlag = failCase.isCRMRenewalCreateFlag;
                            propertyRenewalRegister.CrmrenewalCreatedDate = DateTime.Now.Date;
                            _db.Entry(propertyRenewalRegister).State = EntityState.Modified;
                            _db.SaveChanges();
                        }

                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        #region Excel Export For Renewal FailCase
        public IActionResult ExportRenewalFailCase()
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                List<SMSRenewalViewModel> lstSMSRenewalViewModels = new List<SMSRenewalViewModel>();
                lstSMSRenewalViewModels = HttpContext.Session.GetObjectFromJson<List<SMSRenewalViewModel>>("lstRenewalFailCaseList");

                if (lstSMSRenewalViewModels == null)
                {
                    TempData["Message"] = "Invalid request data";
                    return RedirectToAction("RenewalFailCase");
                }

                var columnHeader = new List<string>
                {
                    //"SMS ID",
                    "Phone Number",
                    "Customer Name",
                    "Invoice No",
                    "Risk Name",
                    "Expired Date",
                    "Renewal Premium",
                    "Product",
                    "From Date",
                    "To Date",
                    "Link",
                    "Customer Code",
                    "Month",
                    "PersonInCharge",
                    "Responsiable Party",
                    "Policy Number",
                    "Branch",
                    "Intermediary Type",
                    "Agent Code",
                    "Agent Name",
                    "Account Code",
                    "Authorized Code",
                    "Authorized Name"
                };
                var fileContent = ExportExcelForRenewalFailCaseReport(lstSMSRenewalViewModels, columnHeader, "Renewal Fail Case Report");
                return File(fileContent, "application/ms-excel", "RenewalFailCase.xlsx");
            }
        }

        public byte[] ExportExcelForRenewalFailCaseReport(List<SMSRenewalViewModel> lst, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 30;
                using (var cells = worksheet.Cells[1, 1, 1, 60])
                    cells.Style.Font.Bold = true;

                for (int i = 0; i < columnHeader.Count(); i++)
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];


                var j = 2;
                var count = 1;
                foreach (var item in lst)
                {
                    //worksheet.Cells["A" + j].Value = item.smsId;
                    worksheet.Cells["A" + j].Value = item.PhoneNumber;
                    worksheet.Cells["B" + j].Value = item.CustomerName;
                    worksheet.Cells["C" + j].Value = item.InvoiceNo;
                    worksheet.Cells["D" + j].Value = item.RiskName;
                    worksheet.Cells["E" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ExpiryDate);
                    worksheet.Cells["F" + j].Value = item.RenewalPremium;
                    worksheet.Cells["G" + j].Value = item.Class;
                    worksheet.Cells["H" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.FromDate);
                    worksheet.Cells["I" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ToDate);
                    worksheet.Cells["J" + j].Value = item.Link;
                    worksheet.Cells["K" + j].Value = item.CusCode;
                    worksheet.Cells["L" + j].Value = item.Month;
                    worksheet.Cells["M" + j].Value = item.PersonInCharge;
                    worksheet.Cells["N" + j].Value = item.ResponsibleParty;
                    worksheet.Cells["O" + j].Value = item.PolicyNo;
                    worksheet.Cells["P" + j].Value = item.Branch;
                    worksheet.Cells["Q" + j].Value = item.IntermediaryType;
                    worksheet.Cells["R" + j].Value = item.AgentCode;
                    worksheet.Cells["S" + j].Value = item.AgentName;
                    worksheet.Cells["T" + j].Value = item.AccountCode;
                    worksheet.Cells["U" + j].Value = item.AuthorizedCode;
                    worksheet.Cells["V" + j].Value = item.AuthorizedName;
                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }
        #endregion

        #endregion

        #region RenewalCaseCreateWithSMS

        public IActionResult UploadRenewalCaseCreateWithoutSMS()
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }
            return View();
        }

        public IActionResult RenewalCaseCreateWithoutSMS()
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }
            try
            {
                //List<TempRenewalCaseCreate> lstTempRenewalCaseCreate = new List<TempRenewalCaseCreate>();
                //lstTempRenewalCaseCreate = _db.TempRenewalCaseCreate.ToList();

                List<TempRenewalCaseCreate> tempRenewalCaseCreates = new List<TempRenewalCaseCreate>();
                if (HttpContext.Session.GetObjectFromJson<List<TempRenewalCaseCreate>>("lstTempRenewalCaseCreate") != null)
                {
                    tempRenewalCaseCreates = HttpContext.Session.GetObjectFromJson<List<TempRenewalCaseCreate>>("lstTempRenewalCaseCreate");
                }
                ViewBag.lstTempRenewalCaseCreate = tempRenewalCaseCreates;

            }
            catch(Exception ex)
            {

            }
            return View();
        }

        [HttpPost]
        public IActionResult UploadRenewalCaseCreateWithoutSMS(IFormFile reportfile)
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }

            try
            {
                string folderName = "ImportedFiles";
                string webRootPath = _env.WebRootPath;
                string newPath = Path.Combine(webRootPath, folderName);
                
                if (!Directory.Exists(newPath))// Crate New Directory if not exist as per the path
                {
                    Directory.CreateDirectory(newPath);
                }
                else
                {
                    // Delete Files from Directory
                    System.IO.DirectoryInfo di = new DirectoryInfo(newPath);
                    foreach (FileInfo filesDelete in di.GetFiles())
                    {
                        filesDelete.Delete();
                    }// End Deleting files form directories
                    Directory.CreateDirectory(newPath);
                }
                var fiName = Guid.NewGuid().ToString() + Path.GetExtension(reportfile.FileName);
                using (var fileStream = new FileStream(Path.Combine(newPath, fiName), FileMode.Create))
                {
                    reportfile.CopyTo(fileStream);
                }
                // Get uploaded file path with root
                string rootFolder = _env.WebRootPath;
                string fileName = @"ImportedFiles/" + fiName;
                FileInfo file = new FileInfo(Path.Combine(rootFolder, fileName));

                using (ExcelPackage package = new ExcelPackage(file))
                {
                    ExcelWorksheet workSheet = package.Workbook.Worksheets[1];
                    int totalRows = workSheet.Dimension.Rows;
                    List<TempRenewalCaseCreate> lstTempRenewalCaseCreate = new List<TempRenewalCaseCreate>();
                    for (int i = 2; i <= totalRows; i++)
                    {
                        TempRenewalCaseCreate tempRenewalCaseCreate = new TempRenewalCaseCreate();
                        tempRenewalCaseCreate.Id = Guid.NewGuid();

                        tempRenewalCaseCreate.InvoiceNo = (workSheet.Cells[i, 3].Value == null) ? "" : workSheet.Cells[i, 3].Value.ToString();
                        tempRenewalCaseCreate.Class = (workSheet.Cells[i, 7].Value == null) ? "" : workSheet.Cells[i, 7].Value.ToString();
                        if (tempRenewalCaseCreate.InvoiceNo != null)
                        {
                            if(tempRenewalCaseCreate.Class == "MOTOR INSURANCE")
                            {
                                MotorRenewalRegister motorRenewalRegister = new MotorRenewalRegister();
                                motorRenewalRegister = _db.MotorRenewalRegister.Where(m => m.InvoiceNo == tempRenewalCaseCreate.InvoiceNo).FirstOrDefault();
                                if (motorRenewalRegister != null)
                                {
                                    tempRenewalCaseCreate.RenewalRegisterId = motorRenewalRegister.Id;
                                    tempRenewalCaseCreate.Product = motorRenewalRegister.Product;
                                }
                            }
                            else if(tempRenewalCaseCreate.Class == "FIRE INSURANCE")
                            {
                                FireRenewalRegister fireRenewalRegister = new FireRenewalRegister();
                                fireRenewalRegister = _db.FireRenewalRegister.Where(f => f.InvoiceNo == tempRenewalCaseCreate.InvoiceNo).FirstOrDefault();
                                if(fireRenewalRegister != null)
                                {
                                    tempRenewalCaseCreate.RenewalRegisterId = fireRenewalRegister.Id;
                                    tempRenewalCaseCreate.Product = fireRenewalRegister.Product;
                                }
                            }
                            else if(tempRenewalCaseCreate.Class == "HEALTH INSURANCE")
                            {
                                HealthRenewalRegister healthRenewalRegister = new HealthRenewalRegister();
                                healthRenewalRegister = _db.HealthRenewalRegister.Where(m => m.InvoiceNo == tempRenewalCaseCreate.InvoiceNo).FirstOrDefault();
                                if(healthRenewalRegister != null)
                                {
                                    tempRenewalCaseCreate.RenewalRegisterId = healthRenewalRegister.Id;
                                    tempRenewalCaseCreate.Product = healthRenewalRegister.Product;
                                }
                            }
                            else if(tempRenewalCaseCreate.Class == "PERSONAL ACCIDENT INSURANCE")
                            {
                                ParenewalRegister parenewalRegister = new ParenewalRegister();
                                parenewalRegister = _db.ParenewalRegister.Where(m => m.InvoiceNo == tempRenewalCaseCreate.InvoiceNo).FirstOrDefault();
                                if(parenewalRegister != null)
                                {
                                    tempRenewalCaseCreate.RenewalRegisterId = parenewalRegister.Id;
                                    tempRenewalCaseCreate.Product = parenewalRegister.Product;
                                }
                            }
                            else if(tempRenewalCaseCreate.Class == "ENGINEERING")
                            {
                                EngineeringRenewalRegister engineeringRenewalRegister = new EngineeringRenewalRegister();
                                engineeringRenewalRegister = _db.EngineeringRenewalRegister.Where(m => m.InvoiceNo == tempRenewalCaseCreate.InvoiceNo).FirstOrDefault();
                                if(engineeringRenewalRegister != null)
                                {
                                    tempRenewalCaseCreate.RenewalRegisterId = engineeringRenewalRegister.Id;
                                    tempRenewalCaseCreate.Product = engineeringRenewalRegister.Product;
                                }
                            }
                            else if(tempRenewalCaseCreate.Class == "PROPERTY")
                            {
                                PropertyRenewalRegister propertyRenewalRegister = new PropertyRenewalRegister();
                                propertyRenewalRegister = _db.PropertyRenewalRegister.Where(m => m.InvoiceNo == tempRenewalCaseCreate.InvoiceNo).FirstOrDefault();
                                if(propertyRenewalRegister != null)
                                {
                                    tempRenewalCaseCreate.RenewalRegisterId = propertyRenewalRegister.Id;
                                    tempRenewalCaseCreate.Product = propertyRenewalRegister.Product;
                                }
                            }
                        }

                        tempRenewalCaseCreate.PhoneNumber = (workSheet.Cells[i, 1].Value == null) ? "" : workSheet.Cells[i, 1].Value.ToString();
                        tempRenewalCaseCreate.CustomerName = (workSheet.Cells[i, 2].Value == null) ? "" : workSheet.Cells[i, 2].Value.ToString();
                        tempRenewalCaseCreate.RiskName = (workSheet.Cells[i, 4].Value == null) ? "" : workSheet.Cells[i, 4].Value.ToString();

                        tempRenewalCaseCreate.ExpiryDate = (workSheet.Cells[i, 5].Value == null) ? (DateTime?)null : Convert.ToDateTime(workSheet.Cells[i, 5].Value);
                        tempRenewalCaseCreate.RenewalPremium = (workSheet.Cells[i, 6].Value == null) ? "" : workSheet.Cells[i, 6].Value.ToString();
                        
                        tempRenewalCaseCreate.FromDate = (workSheet.Cells[i, 8].Value == null) ? (DateTime?)null : Convert.ToDateTime(workSheet.Cells[i, 8].Value);
                        tempRenewalCaseCreate.ToDate = (workSheet.Cells[i, 9].Value == null) ? (DateTime?)null : Convert.ToDateTime(workSheet.Cells[i, 9].Value);

                        tempRenewalCaseCreate.Link = (workSheet.Cells[i, 10].Value == null) ? "" : workSheet.Cells[i, 10].Value.ToString();
                        tempRenewalCaseCreate.CusCode = (workSheet.Cells[i, 11].Value == null) ? "" : workSheet.Cells[i, 11].Value.ToString();
                        tempRenewalCaseCreate.Month = (workSheet.Cells[i, 12].Value == null) ? "" : workSheet.Cells[i, 12].Value.ToString();
                        tempRenewalCaseCreate.PersonInCharge = (workSheet.Cells[i, 13].Value == null) ? "" : workSheet.Cells[i, 13].Value.ToString();
                        tempRenewalCaseCreate.ResponsibleParty = (workSheet.Cells[i, 14].Value == null) ? "" : workSheet.Cells[i, 14].Value.ToString();

                        tempRenewalCaseCreate.PolicyNo = (workSheet.Cells[i, 15].Value == null) ? "" : workSheet.Cells[i, 15].Value.ToString();
                        tempRenewalCaseCreate.Branch = (workSheet.Cells[i, 16].Value == null) ? "" : workSheet.Cells[i, 16].Value.ToString();
                        tempRenewalCaseCreate.IntermediaryType = (workSheet.Cells[i, 17].Value == null) ? "" : workSheet.Cells[i, 17].Value.ToString();
                        tempRenewalCaseCreate.AgentCode = (workSheet.Cells[i, 18].Value == null) ? "" : workSheet.Cells[i, 18].Value.ToString();
                        tempRenewalCaseCreate.AgentName = (workSheet.Cells[i, 19].Value == null) ? "" : workSheet.Cells[i, 19].Value.ToString();

                        tempRenewalCaseCreate.AccountCode = (workSheet.Cells[i, 20].Value == null) ? "" : workSheet.Cells[i, 20].Value.ToString();
                        tempRenewalCaseCreate.AuthorizedCode = (workSheet.Cells[i, 21].Value == null) ? "" : workSheet.Cells[i, 21].Value.ToString();
                        tempRenewalCaseCreate.AuthorizedName = (workSheet.Cells[i, 22].Value == null) ? "" : workSheet.Cells[i, 22].Value.ToString();
                        tempRenewalCaseCreate.CreatedDate = DateTime.Now.Date;

                        //TempRenewalCaseCreate caseCreate = new TempRenewalCaseCreate();
                        var delcaseCreate = _db.TempRenewalCaseCreate.Where(t => t.InvoiceNo == tempRenewalCaseCreate.InvoiceNo && t.PolicyNo == tempRenewalCaseCreate.PolicyNo);
                        if(delcaseCreate != null)
                        {
                            _db.TempRenewalCaseCreate.RemoveRange(delcaseCreate);
                            _db.SaveChanges();
                        }

                        lstTempRenewalCaseCreate.Add(tempRenewalCaseCreate);
                    }

                    _db.TempRenewalCaseCreate.AddRange(lstTempRenewalCaseCreate);
                    _db.SaveChanges();

                    HttpContext.Session.SetObjectAsJson("lstTempRenewalCaseCreate", null);
                    HttpContext.Session.SetObjectAsJson("lstTempRenewalCaseCreate", lstTempRenewalCaseCreate);
                    TempData["Message"] = "Import Data Successfully.";
                    return RedirectToAction("RenewalCaseCreateWithoutSMS");
                }
            }
            catch (Exception ex)
            {
                TempData["Message"] = ex.Message.ToString();
                return RedirectToAction("UploadRenewalCaseCreateWithoutSMS");
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateCustomRenewalCase(bool chkSentSMS = false)
        {
            try
            {
                bool isCustom = true;
                if (HttpContext.Session.GetObjectFromJson<List<TempRenewalCaseCreate>>("lstTempRenewalCaseCreate") != null)
                {
                    List<TempRenewalCaseCreate> tempRenewalCaseCreates = new List<TempRenewalCaseCreate>();
                    tempRenewalCaseCreates = HttpContext.Session.GetObjectFromJson<List<TempRenewalCaseCreate>>("lstTempRenewalCaseCreate");

                    List<SMSRenewalViewModel> lstSMSRenewalViewModels = new List<SMSRenewalViewModel>();
                    foreach (var renewalcase in tempRenewalCaseCreates)
                    {
                        SMSRenewalViewModel smsViewModel = new SMSRenewalViewModel();
                        smsViewModel.temprenewalcaseId = renewalcase.Id;
                        smsViewModel.renewalregisterId = renewalcase.RenewalRegisterId;
                        smsViewModel.PhoneNumber = renewalcase.PhoneNumber;
                        smsViewModel.CustomerName = renewalcase.CustomerName;
                        smsViewModel.InvoiceNo = renewalcase.InvoiceNo;
                        smsViewModel.RiskName = renewalcase.RiskName;
                        smsViewModel.ExpiryDate = renewalcase.ExpiryDate;
                        smsViewModel.RenewalPremium = Convert.ToDecimal(renewalcase.RenewalPremium);
                        smsViewModel.Class = renewalcase.Class;
                        smsViewModel.Product = renewalcase.Product;
                        smsViewModel.FromDate = renewalcase.FromDate;
                        smsViewModel.ToDate = renewalcase.ToDate;
                        smsViewModel.Link = renewalcase.Link;
                        smsViewModel.CusCode = renewalcase.CusCode;
                        smsViewModel.Month = renewalcase.Month;
                        smsViewModel.PersonInCharge = renewalcase.PersonInCharge;
                        smsViewModel.ResponsibleParty = renewalcase.ResponsibleParty;
                        smsViewModel.PolicyNo = renewalcase.PolicyNo;
                        smsViewModel.Branch = renewalcase.Branch;
                        smsViewModel.IntermediaryType = renewalcase.IntermediaryType;
                        smsViewModel.AgentCode = renewalcase.AgentCode;
                        smsViewModel.AgentName = renewalcase.AgentName;
                        smsViewModel.AccountCode = renewalcase.AccountCode;
                        smsViewModel.AuthorizedCode = renewalcase.AuthorizedCode;
                        smsViewModel.AuthorizedName = renewalcase.AuthorizedName;
                        lstSMSRenewalViewModels.Add(smsViewModel);
                    }

                    var message = (from c in _db.Category
                                   where c.RenewalFlag == true
                                   && c.Description == "Renewal Notice(EN)"
                                   select c.MsgTemplate).FirstOrDefault();

                    await SendSMSRenewalNoticeAsync(lstSMSRenewalViewModels, message, chkSentSMS, isCustom);
                }

                List<TempRenewalCaseCreate> lstTempRenewalCaseCreate = new List<TempRenewalCaseCreate>();
                lstTempRenewalCaseCreate = _db.TempRenewalCaseCreate.ToList();
                ViewBag.lstTempRenewalCaseCreate = lstTempRenewalCaseCreate;

                return View("RenewalCaseCreateWithoutSMS");
            }
            catch (Exception ex)
            {
                return View("RenewalCaseCreateWithoutSMS");
            }
            
        }

        #endregion

        #region NewBusinessCSAT

        public IActionResult SendNewBussinessCSATSMS()
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }
            BindData();
            GetSurveySMS();

            return View();
        }
        
        public JsonResult GetSMSByCategoryId(Guid CategoryId)
        {
            try
            {
                Category CategoryList = new Category();
                if (CategoryId != Guid.Empty)
                {
                    CategoryList = _db.Category.Where(x => x.Id == CategoryId).OrderBy(a => a.Description).FirstOrDefault();
                }
                return Json(CategoryList);
            }
            catch(Exception ex)
            {
                return Json("-1");
            }
        }

        [HttpPost]
        public IActionResult UploadNewBussinessCSATSMS(IFormFile reportfile, SurveySms surveySmsModel)
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }
            try
            {
                #region UploadData

                string folderName = "ImportedFiles";
                string webRootPath = _env.WebRootPath;
                string newPath = Path.Combine(webRootPath, folderName);

                if (!Directory.Exists(newPath))// Crate New Directory if not exist as per the path
                {
                    Directory.CreateDirectory(newPath);
                }
                else
                {
                    // Delete Files from Directory
                    System.IO.DirectoryInfo di = new DirectoryInfo(newPath);
                    foreach (FileInfo filesDelete in di.GetFiles())
                    {
                        filesDelete.Delete();
                    }// End Deleting files form directories
                    Directory.CreateDirectory(newPath);
                }
                var fiName = Guid.NewGuid().ToString() + Path.GetExtension(reportfile.FileName);
                using (var fileStream = new FileStream(Path.Combine(newPath, fiName), FileMode.Create))
                {
                    reportfile.CopyTo(fileStream);
                }
                // Get uploaded file path with root
                string rootFolder = _env.WebRootPath;
                string fileName = @"ImportedFiles/" + fiName;
                FileInfo file = new FileInfo(Path.Combine(rootFolder, fileName));
                using (ExcelPackage package = new ExcelPackage(file))
                {
                    ExcelWorksheet workSheet = package.Workbook.Worksheets[1];
                    int totalRows = workSheet.Dimension.Rows;

                    //List<SurveySms> lstSurveySms = new List<SurveySms>();

                    for (int i = 2; i <= totalRows; i++)
                    {

                        decimal lenmessge = surveySmsModel.Message.Length;
                        string Smscount = Convert.ToString(Math.Ceiling(lenmessge / 70));
                        Smscount = Smscount == "0" ? "1" : Smscount;

                        SurveySms surveySms = new SurveySms();
                        string policyno = (workSheet.Cells[i, 15].Value == null) ? "" : workSheet.Cells[i, 15].Value.ToString();
                        surveySms = _db.SurveySms.Where(s => s.PolicyNo == policyno).FirstOrDefault();

                        if(surveySms != null)
                        {
                            surveySms.PhoneNumber = (workSheet.Cells[i, 1].Value == null) ? "" : workSheet.Cells[i, 1].Value.ToString();
                            surveySms.CustomerName = (workSheet.Cells[i, 2].Value == null) ? "" : workSheet.Cells[i, 2].Value.ToString();
                            surveySms.InvoiceNo = (workSheet.Cells[i, 3].Value == null) ? "" : workSheet.Cells[i, 3].Value.ToString();
                            surveySms.RiskName = (workSheet.Cells[i, 4].Value == null) ? "" : workSheet.Cells[i, 4].Value.ToString();

                            surveySms.ExpiryDate = (workSheet.Cells[i, 5].Value == null) ? (DateTime?)null : Convert.ToDateTime(workSheet.Cells[i, 5].Value);
                            surveySms.RenewalPremiun = (workSheet.Cells[i, 6].Value == null) ? 0 : Convert.ToDecimal(workSheet.Cells[i, 6].Value);
                            surveySms.Product = (workSheet.Cells[i, 7].Value == null) ? "" : workSheet.Cells[i, 7].Value.ToString();
                            surveySms.FromDate = (workSheet.Cells[i, 8].Value == null) ? (DateTime?)null : Convert.ToDateTime(workSheet.Cells[i, 8].Value);
                            surveySms.ToDate = (workSheet.Cells[i, 9].Value == null) ? (DateTime?)null : Convert.ToDateTime(workSheet.Cells[i, 9].Value);

                            surveySms.Link = (workSheet.Cells[i, 10].Value == null) ? "" : workSheet.Cells[i, 10].Value.ToString();
                            surveySms.CusCode = (workSheet.Cells[i, 11].Value == null) ? "" : workSheet.Cells[i, 11].Value.ToString();
                            surveySms.Month = (workSheet.Cells[i, 12].Value == null) ? "" : workSheet.Cells[i, 12].Value.ToString();
                            surveySms.PersonInCharge = (workSheet.Cells[i, 13].Value == null) ? "" : workSheet.Cells[i, 13].Value.ToString();
                            surveySms.ResponsibleParty = (workSheet.Cells[i, 14].Value == null) ? "" : workSheet.Cells[i, 14].Value.ToString();

                            surveySms.PolicyNo = (workSheet.Cells[i, 15].Value == null) ? "" : workSheet.Cells[i, 15].Value.ToString();
                            surveySms.Branch = (workSheet.Cells[i, 16].Value == null) ? "" : workSheet.Cells[i, 16].Value.ToString();
                            surveySms.IntermediaryType = (workSheet.Cells[i, 17].Value == null) ? "" : workSheet.Cells[i, 17].Value.ToString();
                            surveySms.AgentCode = (workSheet.Cells[i, 18].Value == null) ? "" : workSheet.Cells[i, 18].Value.ToString();
                            surveySms.AgentName = (workSheet.Cells[i, 19].Value == null) ? "" : workSheet.Cells[i, 19].Value.ToString();

                            surveySms.AccountCode = (workSheet.Cells[i, 20].Value == null) ? "" : workSheet.Cells[i, 20].Value.ToString();
                            surveySms.AuthorizedCode = (workSheet.Cells[i, 21].Value == null) ? "" : workSheet.Cells[i, 21].Value.ToString();
                            surveySms.AuthorizedName = (workSheet.Cells[i, 22].Value == null) ? "" : workSheet.Cells[i, 22].Value.ToString();
                            surveySms.UploadedDate = DateTime.Now.Date;
                            surveySms.SysId = Guid.Parse(HttpContext.Session.GetString("userId"));
                            surveySms.CategoryId = surveySmsModel.CategoryId;

                            #region Prepare for SMS template
                            string smsmessage = string.Empty;
                            smsmessage = surveySmsModel.Message;

                            if (surveySms.Product != null)
                            {
                                smsmessage = smsmessage.Replace("@Product", surveySms.Product);
                            }
                            else
                            {
                                smsmessage = smsmessage.Replace("@Product", "-");
                            }

                            if (surveySms.PolicyNo != null)
                            {
                                smsmessage = smsmessage.Replace("@PolicyNo", "Policy No : " + surveySms.PolicyNo);
                            }
                            else
                            {
                                smsmessage = smsmessage.Replace("@PolicyNo", "Policy No : " + "-");
                            }

                            if (surveySms.FromDate != null)
                            {
                                smsmessage = smsmessage.Replace("@FromDate", string.Format("{0:dd/MMM/yyyy}", surveySms.FromDate));
                            }
                            else
                            {
                                smsmessage = smsmessage.Replace("@FromDate", string.Format("{0:dd/MMM/yyyy}", "-1"));
                            }

                            if (surveySms.ToDate != null)
                            {
                                smsmessage = smsmessage.Replace("@ToDate", string.Format("{0:dd/MMM/yyyy}", surveySms.ToDate));
                            }
                            else
                            {
                                smsmessage = smsmessage.Replace("@ToDate", string.Format("{0:dd/MMM/yyyy}", "-1"));
                            }

                            if (surveySms.Link != null)
                            {
                                smsmessage = smsmessage.Replace("@Link", surveySms.Link);
                            }
                            else
                            {
                                smsmessage = smsmessage.Replace("@Link", "-");
                            }

                            if (surveySms.InvoiceNo != null)
                            {
                                smsmessage = smsmessage.Replace("@InvoiceNo", "Invoice No :" + surveySms.InvoiceNo);
                            }
                            else
                            {
                                smsmessage = smsmessage.Replace("@InvoiceNo", "Invoice No : " + "-");
                            }

                            if (surveySms.RiskName != null)
                            {
                                smsmessage = smsmessage.Replace("@VehicleNo", "Vehicle No :" + surveySms.RiskName);
                            }
                            else
                            {
                                smsmessage = smsmessage.Replace("@VehicleNo", "Vehicle No : " + "-");
                            }

                            if (surveySms.ExpiryDate != null)
                            {
                                smsmessage = smsmessage.Replace("@ExpiryDate", "ExpiryDate :" + string.Format("{0:dd/MMM/yyyy}", surveySms.ExpiryDate));
                            }
                            else
                            {
                                smsmessage = smsmessage.Replace("@ExpiryDate", "ExpiryDate : " + string.Format("{0:dd/MMM/yyyy}", "-1"));
                            }

                            #endregion
                            surveySms.Message = smsmessage;
                            surveySms.Smscount = Smscount;
                            surveySms.PhNoFlag = true;
                            //surveySms.ConfirmCSAT = false;

                            _db.SurveySms.Update(surveySms);
                            _db.SaveChanges();
                        }
                        else if(policyno != "")
                        {
                            SurveySms newSurveySms = new SurveySms();

                            newSurveySms.Id = Guid.NewGuid();
                            newSurveySms.PhoneNumber = (workSheet.Cells[i, 1].Value == null) ? "" : workSheet.Cells[i, 1].Value.ToString();
                            newSurveySms.CustomerName = (workSheet.Cells[i, 2].Value == null) ? "" : workSheet.Cells[i, 2].Value.ToString();
                            newSurveySms.InvoiceNo = (workSheet.Cells[i, 3].Value == null) ? "" : workSheet.Cells[i, 3].Value.ToString();
                            newSurveySms.RiskName = (workSheet.Cells[i, 4].Value == null) ? "" : workSheet.Cells[i, 4].Value.ToString();

                            newSurveySms.ExpiryDate = (workSheet.Cells[i, 5].Value == null) ? (DateTime?)null : Convert.ToDateTime(workSheet.Cells[i, 5].Value);
                            newSurveySms.RenewalPremiun = (workSheet.Cells[i, 6].Value == null) ? 0 : Convert.ToDecimal(workSheet.Cells[i, 6].Value);
                            newSurveySms.Product = (workSheet.Cells[i, 7].Value == null) ? "" : workSheet.Cells[i, 7].Value.ToString();
                            newSurveySms.FromDate = (workSheet.Cells[i, 8].Value == null) ? (DateTime?)null : Convert.ToDateTime(workSheet.Cells[i, 8].Value);
                            newSurveySms.ToDate = (workSheet.Cells[i, 9].Value == null) ? (DateTime?)null : Convert.ToDateTime(workSheet.Cells[i, 9].Value);

                            newSurveySms.Link = (workSheet.Cells[i, 10].Value == null) ? "" : workSheet.Cells[i, 10].Value.ToString();
                            newSurveySms.CusCode = (workSheet.Cells[i, 11].Value == null) ? "" : workSheet.Cells[i, 11].Value.ToString();
                            newSurveySms.Month = (workSheet.Cells[i, 12].Value == null) ? "" : workSheet.Cells[i, 12].Value.ToString();
                            newSurveySms.PersonInCharge = (workSheet.Cells[i, 13].Value == null) ? "" : workSheet.Cells[i, 13].Value.ToString();
                            newSurveySms.ResponsibleParty = (workSheet.Cells[i, 14].Value == null) ? "" : workSheet.Cells[i, 14].Value.ToString();

                            newSurveySms.PolicyNo = (workSheet.Cells[i, 15].Value == null) ? "" : workSheet.Cells[i, 15].Value.ToString();
                            newSurveySms.Branch = (workSheet.Cells[i, 16].Value == null) ? "" : workSheet.Cells[i, 16].Value.ToString();
                            newSurveySms.IntermediaryType = (workSheet.Cells[i, 17].Value == null) ? "" : workSheet.Cells[i, 17].Value.ToString();
                            newSurveySms.AgentCode = (workSheet.Cells[i, 18].Value == null) ? "" : workSheet.Cells[i, 18].Value.ToString();
                            newSurveySms.AgentName = (workSheet.Cells[i, 19].Value == null) ? "" : workSheet.Cells[i, 19].Value.ToString();

                            newSurveySms.AccountCode = (workSheet.Cells[i, 20].Value == null) ? "" : workSheet.Cells[i, 20].Value.ToString();
                            newSurveySms.AuthorizedCode = (workSheet.Cells[i, 21].Value == null) ? "" : workSheet.Cells[i, 21].Value.ToString();
                            newSurveySms.AuthorizedName = (workSheet.Cells[i, 22].Value == null) ? "" : workSheet.Cells[i, 22].Value.ToString();
                            newSurveySms.UploadedDate = DateTime.Now.Date;
                            newSurveySms.SysId = Guid.Parse(HttpContext.Session.GetString("userId"));
                            newSurveySms.CategoryId = surveySmsModel.CategoryId;

                            #region Prepare for SMS template
                            string smsmessage = string.Empty;
                            smsmessage = surveySmsModel.Message;

                            if (newSurveySms.Product != null)
                            {
                                smsmessage = smsmessage.Replace("@Product", newSurveySms.Product);
                            }
                            else
                            {
                                smsmessage = smsmessage.Replace("@Product", "-");
                            }

                            if (newSurveySms.PolicyNo != null)
                            {
                                smsmessage = smsmessage.Replace("@PolicyNo", "Policy No : " + newSurveySms.PolicyNo);
                            }
                            else
                            {
                                smsmessage = smsmessage.Replace("@PolicyNo", "Policy No : " + "-");
                            }

                            if (newSurveySms.FromDate != null)
                            {
                                smsmessage = smsmessage.Replace("@FromDate", string.Format("{0:dd/MMM/yyyy}", newSurveySms.FromDate));
                            }
                            else
                            {
                                smsmessage = smsmessage.Replace("@FromDate", string.Format("{0:dd/MMM/yyyy}", "-1"));
                            }

                            if (newSurveySms.ToDate != null)
                            {
                                smsmessage = smsmessage.Replace("@ToDate", string.Format("{0:dd/MMM/yyyy}", newSurveySms.ToDate));
                            }
                            else
                            {
                                smsmessage = smsmessage.Replace("@ToDate", string.Format("{0:dd/MMM/yyyy}", "-1"));
                            }

                            if (newSurveySms.Link != null)
                            {
                                smsmessage = smsmessage.Replace("@Link", newSurveySms.Link);
                            }
                            else
                            {
                                smsmessage = smsmessage.Replace("@Link", "-");
                            }

                            if (newSurveySms.InvoiceNo != null)
                            {
                                smsmessage = smsmessage.Replace("@InvoiceNo", "Invoice No :" + newSurveySms.InvoiceNo);
                            }
                            else
                            {
                                smsmessage = smsmessage.Replace("@InvoiceNo", "Invoice No : " + "-");
                            }

                            if (newSurveySms.RiskName != null)
                            {
                                smsmessage = smsmessage.Replace("@VehicleNo", "Vehicle No :" + newSurveySms.RiskName);
                            }
                            else
                            {
                                smsmessage = smsmessage.Replace("@VehicleNo", "Vehicle No : " + "-");
                            }

                            if (newSurveySms.ExpiryDate != null)
                            {
                                smsmessage = smsmessage.Replace("@ExpiryDate", "ExpiryDate :" + string.Format("{0:dd/MMM/yyyy}", newSurveySms.ExpiryDate));
                            }
                            else
                            {
                                smsmessage = smsmessage.Replace("@ExpiryDate", "ExpiryDate : " + string.Format("{0:dd/MMM/yyyy}", "-1"));
                            }

                            #endregion

                            newSurveySms.Message = smsmessage;
                            newSurveySms.Smscount = Smscount;
                            newSurveySms.ConfirmCSAT = false;

                            _db.SurveySms.Add(newSurveySms);
                            _db.SaveChanges();
                        }
                        //lstSurveySms.Add(surveySms);

                    }

                    //_db.SurveySms.AddRange(lstSurveySms);
                    //_db.SaveChanges();

                    //HttpContext.Session.SetObjectAsJson("lstSurveySms", null);
                    //HttpContext.Session.SetObjectAsJson("lstSurveySms", lstSurveySms);

                    //List<SurveySms> lstSurveySMS = new List<SurveySms>();
                    //if (HttpContext.Session.GetObjectFromJson<List<SurveySms>>("lstSurveySms") != null)
                    //{
                    //    lstSurveySMS = HttpContext.Session.GetObjectFromJson<List<SurveySms>>("lstSurveySms");
                    //}
                    //ViewBag.lstSurveySMS = lstSurveySMS;

                    GetSurveySMS();

                    TempData["Message"] = "Import Data Successfully.";
                    return RedirectToAction("SendNewBussinessCSATSMS");
                }

                #endregion

            }
            catch(Exception ex)
            {
                TempData["Message"] = ex.Message.ToString();
                return RedirectToAction("SendNewBussinessCSATSMS");
            }
        }

        private void GetSurveySMS()
        {
            try
            {
                List<SurveySMSViewModel> surveySMSViewModels = new List<SurveySMSViewModel>();
                surveySMSViewModels = (from s in _db.SurveySms
                                         join c in _db.Category on s.CategoryId equals c.Id
                                         where s.UploadedDate == DateTime.Now.Date
                                         where s.PolicyNo != ""
                                         orderby s.UploadedDate descending
                                         select new SurveySMSViewModel
                                         {
                                             Id = s.Id,
                                             UploadedDate = s.UploadedDate,
                                             PhoneNumber = s.PhoneNumber,
                                             CusCode = s.CusCode,
                                             CustomerName = s.CustomerName,
                                             InvoiceNo = s.InvoiceNo,
                                             RiskName = s.RiskName,
                                             ExpiryDate = s.ExpiryDate,
                                             RenewalPremiun = s.RenewalPremiun,
                                             Product = s.Product,
                                             FromDate = s.FromDate,
                                             ToDate = s.ToDate,
                                             Link = s.Link,
                                             Month = s.Month,
                                             PersonInCharge = s.PersonInCharge,
                                             ResponsibleParty = s.ResponsibleParty,
                                             PolicyNo = s.PolicyNo,
                                             Branch = s.Branch,
                                             IntermediaryType = s.IntermediaryType,
                                             AgentCode = s.AgentCode,
                                             AgentName = s.AgentName,
                                             AccountCode = s.AccountCode,
                                             AuthorizedCode = s.AuthorizedCode,
                                             AuthorizedName = s.AuthorizedName,
                                             CategoryId = s.CategoryId,
                                             CategoryDescription = c.Description,
                                             Message = s.Message
                                         }).ToList();
                ViewBag.SurveySMSList = surveySMSViewModels;
                HttpContext.Session.SetObjectAsJson("lstSurveySms", null);
                HttpContext.Session.SetObjectAsJson("lstSurveySms", surveySMSViewModels);
            }
            catch(Exception ex)
            {

            }
        }

        public JsonResult UpdatePhNoFlag(string id)
        {
            try
            {
                SurveySms surveySms = new SurveySms();
                surveySms = _db.SurveySms.Where(s => s.Id == Guid.Parse(id)).FirstOrDefault();
                surveySms.PhNoFlag = false;
                _db.Update(surveySms);
                _db.SaveChanges();
                return Json("1");
            }
            catch (Exception ex)
            {
                return Json("-1");
            }

        }

        #region ExcelExport
        public IActionResult ExportCSATSurveySMS()
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                List<SurveySMSViewModel> lstSurveySms = new List<SurveySMSViewModel>();
                lstSurveySms = HttpContext.Session.GetObjectFromJson<List<SurveySMSViewModel>>("lstSurveySms");

                var columnHeader = new List<string>
                {
                    "Phone Number",
                    "Customer Name",
                    "Invoice No",
                    "Risk Name",
                    "Expiry Date",
                    "Renewal Premium",
                    "Product",
                    "From Date",
                    "To Date",
                    "Link",
                    "Customer Code",
                    "Month",
                    "For Person in charge(PIC)",
                    "Responsible Party",
                    "Policy No",
                    "Branch",
                    "Intermediary Type",
                    "Agent Code",
                    "Agent Name",
                    "Account Code",
                    "Authorized Code",
                    "Authorized Name"
                };
                var fileContent = ExportExcelForSurveySMSReport(lstSurveySms, columnHeader, "CSAT Survey SMS");
                return File(fileContent, "application/ms-excel", "CSATSurveySMS.xlsx");
            }
        }

        public byte[] ExportExcelForSurveySMSReport(List<SurveySMSViewModel> lst, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 30;
                using (var cells = worksheet.Cells[1, 1, 1, 60])
                    cells.Style.Font.Bold = true;

                for (int i = 0; i < columnHeader.Count(); i++)
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];


                var j = 2;
                var count = 1;
                foreach (var item in lst)
                {
                    worksheet.Cells["A" + j].Value = item.PhoneNumber;
                    worksheet.Cells["B" + j].Value = item.CustomerName;
                    worksheet.Cells["C" + j].Value = item.InvoiceNo;
                    worksheet.Cells["D" + j].Value = item.RiskName;
                    worksheet.Cells["E" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ExpiryDate);

                    worksheet.Cells["F" + j].Value = item.RenewalPremiun;
                    worksheet.Cells["G" + j].Value = item.Product;
                    worksheet.Cells["H" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.FromDate);
                    worksheet.Cells["I" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ToDate);
                    worksheet.Cells["J" + j].Value = item.Link;

                    worksheet.Cells["K" + j].Value = item.CusCode;
                    worksheet.Cells["L" + j].Value = item.Month;
                    worksheet.Cells["M" + j].Value = item.PersonInCharge;
                    worksheet.Cells["N" + j].Value = item.ResponsibleParty;
                    worksheet.Cells["O" + j].Value = item.PolicyNo;

                    worksheet.Cells["P" + j].Value = item.Branch;
                    worksheet.Cells["Q" + j].Value = item.IntermediaryType;
                    worksheet.Cells["R" + j].Value = item.AgentCode;
                    worksheet.Cells["S" + j].Value = item.AgentName;
                    worksheet.Cells["T" + j].Value = item.AccountCode;

                    worksheet.Cells["U" + j].Value = item.AuthorizedCode;
                    worksheet.Cells["V" + j].Value = item.AuthorizedName;

                    SurveySms surveySms = new SurveySms();
                    surveySms = _db.SurveySms.Where(s => s.Id == item.Id).FirstOrDefault();
                    if (surveySms.PhNoFlag == false)
                    {
                        using (ExcelRange Rng = worksheet.Cells[j, 1, j, 22])
                        {
                            Rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            Rng.Style.Fill.BackgroundColor.SetColor(Color.Coral);
                        }
                    }
                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        #endregion

        public IActionResult ConfirmCSATSMS()
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                try
                {
                    List<SurveySMSViewModel> lstSurveySms = new List<SurveySMSViewModel>();
                    lstSurveySms = HttpContext.Session.GetObjectFromJson<List<SurveySMSViewModel>>("lstSurveySms");

                    SurveySms surveySms = new SurveySms();
                    for(int i = 0; i<lstSurveySms.Count(); i++)
                    {
                        surveySms = _db.SurveySms.Where(s => s.Id == lstSurveySms[i].Id).FirstOrDefault();
                        surveySms.ConfirmCSAT = true;
                        _db.SurveySms.Update(surveySms);
                        _db.SaveChanges();
                    }

                    GetSurveySMS();

                    TempData["Message"] = "Successfully Confirmed. These will sent at 7:00PM.";
                    return RedirectToAction("SendNewBussinessCSATSMS");

                }
                catch (Exception ex)
                {
                    return RedirectToAction("SendNewBussinessCSATSMS");
                }
            }
        }

        public IActionResult DeleteSurveySMSById(Guid surveyId)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");
            SurveySms surveySms = new SurveySms();
            try
            {
                surveySms = _db.SurveySms.Where(m => m.Id == surveyId).FirstOrDefault();
                _db.Entry(surveySms).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                _db.SaveChanges();
                BindData();

                TempData["Message"] = "Delete Success";
                return RedirectToAction("SendNewBussinessCSATSMS");
            }
            catch(Exception ex)
            {
                BindData();
                TempData["Message"] = "Delete Fail";
                return RedirectToAction("SendNewBussinessCSATSMS");
            }
        }
        #endregion

        #region RenewalNoSMSCase 
        public IActionResult RenewalNoSMSCase()
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }

            PrepareDataForRenewalReg();
            try
            {
                filterRenewalNoSMSCase filterRenewalNoSMSCase = new filterRenewalNoSMSCase();
                List<SMSRenewalViewModel> lstSMSRenewalViewModels = new List<SMSRenewalViewModel>();
                lstSMSRenewalViewModels = dataService.GetRenewalNoSMSCase();

                HttpContext.Session.SetComplexData("lstNoSMSRenewalViewModels", lstSMSRenewalViewModels);
                ViewBag.lstSMSRenewalViewModels = lstSMSRenewalViewModels;

                return View();
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        //[HttpPost]
        public IActionResult GetRenewalNoSMSCase(filterRenewalNoSMSCase filterNoSMSData)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");
            PrepareDataForRenewalReg();
            try
            {
                filterRenewalNoSMSCase filterRenewalNoSMSCase = new filterRenewalNoSMSCase();
                List<SMSRenewalViewModel> lstSMSRenewalViewModels = new List<SMSRenewalViewModel>();

                if (filterNoSMSData.filterFromDate != null && filterNoSMSData.filterToDate != null)
                {
                    lstSMSRenewalViewModels = dataService.GetRenewalNoSMSCaseByFilter(filterNoSMSData);

                }
                HttpContext.Session.SetComplexData("lstNoSMSRenewalViewModels", lstSMSRenewalViewModels);
                ViewBag.lstSMSRenewalViewModels = lstSMSRenewalViewModels;

                return View("RenewalNoSMSCase", filterNoSMSData);

            }
            catch(Exception ex)
            {
                return View("RenewalNoSMSCase", filterNoSMSData);
            }
        }

        public IActionResult ExportRenewalNoSMSCase()
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                List<SMSRenewalViewModel> lstSMSRenewalViewModels = new List<SMSRenewalViewModel>();
                lstSMSRenewalViewModels = HttpContext.Session.GetObjectFromJson<List<SMSRenewalViewModel>>("lstNoSMSRenewalViewModels");

                if (lstSMSRenewalViewModels == null)
                {
                    TempData["Message"] = "Invalid request data";
                    return RedirectToAction("RenewalNoSMSCase");
                }

                var columnHeader = new List<string>
                {
                    "Phone Number",
                    "Customer Name",
                    "Invoice No",
                    "Risk Name",
                    "Expired Date",
                    "Renewal Premium",
                    "Product",
                    "From Date",
                    "To Date",
                    "Link",
                    "Customer Code",
                    "Month",
                    "PersonInCharge",
                    "Responsiable Party",
                    "Policy Number",
                    "Branch",
                    "Intermediary Type",
                    "Agent Code",
                    "Agent Name",
                    "Account Code",
                    "Authorized Code",
                    "Authorized Name"
                };
                var fileContent = ExportExcelForRenewalNoSMSCaseReport(lstSMSRenewalViewModels, columnHeader, "Renewal No SMS Case");
                return File(fileContent, "application/ms-excel", "RenewalNoSMSCase.xlsx");
            }
        }

        public byte[] ExportExcelForRenewalNoSMSCaseReport(List<SMSRenewalViewModel> lst, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 30;
                using (var cells = worksheet.Cells[1, 1, 1, 60])
                    cells.Style.Font.Bold = true;

                for (int i = 0; i < columnHeader.Count(); i++)
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];


                var j = 2;
                var count = 1;
                foreach (var item in lst)
                {
                    worksheet.Cells["A" + j].Value = item.PhoneNumber;
                    worksheet.Cells["B" + j].Value = item.CustomerName;
                    worksheet.Cells["C" + j].Value = item.InvoiceNo;
                    worksheet.Cells["D" + j].Value = item.RiskName;
                    worksheet.Cells["E" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ExpiryDate);
                    worksheet.Cells["F" + j].Value = item.RenewalPremium;
                    worksheet.Cells["G" + j].Value = item.Class;
                    worksheet.Cells["H" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.FromDate);
                    worksheet.Cells["I" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ToDate);
                    worksheet.Cells["J" + j].Value = item.Link;
                    worksheet.Cells["K" + j].Value = item.CusCode;
                    worksheet.Cells["L" + j].Value = item.Month;
                    worksheet.Cells["M" + j].Value = item.PersonInCharge;
                    worksheet.Cells["N" + j].Value = item.ResponsibleParty;
                    worksheet.Cells["O" + j].Value = item.PolicyNo;
                    worksheet.Cells["P" + j].Value = item.Branch;
                    worksheet.Cells["Q" + j].Value = item.IntermediaryType;
                    worksheet.Cells["R" + j].Value = item.AgentCode;
                    worksheet.Cells["S" + j].Value = item.AgentName;
                    worksheet.Cells["T" + j].Value = item.AccountCode;
                    worksheet.Cells["U" + j].Value = item.AuthorizedCode;
                    worksheet.Cells["V" + j].Value = item.AuthorizedName;
                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }
        #endregion

        #region SendSMS
        public IActionResult SendSMS()
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }

            BindData();
            return View("SendSMS");
        }

        [HttpPost]
        public async Task<IActionResult> SendSms(SentSms model)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            BindData();

            try
            {
                string PhoneNumber = model.PhoneNumber.Substring(1, model.PhoneNumber.Length - 1); //Remove 'zero' from phoneNumber
                PhoneNumber = PhoneNumber.Insert(0, "+95");// Add '+95' to phoneNumber

                decimal lenmessge = model.Message.Length;
                string Smscount = Convert.ToString(Math.Ceiling(lenmessge / 70));//Returns the smallest integral value that is greater than or equal to the specified decimal number.                                                                                   
                model.Smscount = Smscount == "0" ? "1" : Smscount;

                string retMsg = "SendSMS Successful";

                Category smsCategory = new Category();
                smsCategory = _db.Category.Where(c => c.Id == model.CategoryId).FirstOrDefault();

                if (smsCategory.Status == true && model.CusCode == null)
                {
                    TempData["Message"] = "Please Fill Cus Code !";
                    return View();
                }

                if (smsCategory.Status == true)
                {
                    string CRMActivityRetMsg = await CreateCRMActivityAsync(model.CusCode, smsCategory.Description);
                    if (CRMActivityRetMsg != "1")
                    {
                        model.ActivitiesFlag = false;
                        retMsg = "Sent SMS fail because of CRM activity cannot set. Please check the customer code.";
                    }
                    else
                    {
                        model.ActivitiesFlag = true;
                        SmsService.InvokeService(PhoneNumber, model.Message);
                    }
                }
                else
                {
                    model.ActivitiesFlag = false;
                    SmsService.InvokeService(PhoneNumber, model.Message);
                }

                SaveSMSData(model);
                TempData["Message"] = retMsg;
                return RedirectToAction("SendSMS");
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Sending in Error.";
                return RedirectToAction("SendSMS");
            }
        }

        private void SaveSMSData(SentSms model)
        {
            try
            {
                SentSms sentSMS = new SentSms();
                sentSMS.Id = Guid.NewGuid();
                sentSMS.PhoneNumber = model.PhoneNumber;
                sentSMS.UserName = model.UserName;
                sentSMS.SysId = Guid.Parse(HttpContext.Session.GetString("userId"));
                sentSMS.Message = model.Message;
                sentSMS.SentDate = DateTime.Now;
                sentSMS.Smscount = model.Smscount;
                sentSMS.CategoryId = model.CategoryId;
                sentSMS.CusCode = model.CusCode;
                sentSMS.ActivitiesFlag = model.ActivitiesFlag;
                _db.SentSms.Add(sentSMS);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        #endregion

        #region SendBulkSMS
        public IActionResult SendBulkSms()
        {
            if (!CheckLoginUser())
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                BindData();
                return View();
            }
        }

        [HttpPost]
        public async Task<IActionResult> UploadExcelBulkSMS(IList<IFormFile> files, SentSms sentSms)
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }
            try
            {
                string fileExt = null;
                var supportedTypes = new[] { ".xlsx" };

                foreach (var file in files)
                    fileExt = Path.GetExtension(file.FileName);

                if (!supportedTypes.Contains(fileExt))
                {
                    TempData["Message"] = "Uploaded File Is InValid - Only Upload Excel file with right format.";
                    return RedirectToAction("SendBulkSms");
                }

                string folderName = "ImportedFiles";
                string webRootPath = _env.WebRootPath;
                string newPath = Path.Combine(webRootPath, folderName);

                if (!Directory.Exists(newPath))// Crate New Directory if not exist as per the path
                {
                    Directory.CreateDirectory(newPath);
                }
                else
                {
                    // Delete Files from Directory
                    System.IO.DirectoryInfo di = new DirectoryInfo(newPath);
                    foreach (FileInfo filesDelete in di.GetFiles())
                    {
                        filesDelete.Delete();
                    }// End Deleting files form directories
                    Directory.CreateDirectory(newPath);
                }
                var fiName = @"ExcelImport.xlsx"; //add overwrite file name
                foreach (IFormFile source in files)
                {
                    using (var fileStream = new FileStream(Path.Combine(newPath, fiName), FileMode.Create))
                    {
                        source.CopyTo(fileStream);
                    }
                }
                // Get uploaded file path with root
                string rootFolder = _env.WebRootPath;
                string fileName = @"ImportedFiles/" + fiName;
                var ep = new ExcelPackage(new FileInfo(Path.Combine(rootFolder, fileName)));
                var ws = ep.Workbook.Worksheets["Sheet1"];

                BulkSMS model = new BulkSMS();

                var PhoneNumberList = new List<string>();
                var UserList = new List<string>();

                //For Dynamic Message
                var InvoiceNoList = new List<string>();
                var VehicleNoList = new List<string>();
                var ExpiryDateList = new List<DateTime>();
                var RenewalPremiumList = new List<decimal>();
                var ProductList = new List<string>();
                var FromDateList = new List<DateTime>();
                var ToDateList = new List<DateTime>();
                var LinkList = new List<string>();
                var CusCodeList = new List<string>();
                var MonthList = new List<string>();
                var PICList = new List<string>();
                var ResponsiblePartyList = new List<string>();
                var PolicyNoList = new List<string>();
                var AccountCodeList = new List<string>();

                List<BulkSMSDetail> BulkSMSDetailList = new List<BulkSMSDetail>();

                for (int rw = 2; rw <= ws.Dimension.End.Row; rw++)
                {
                    BulkSMSDetail bulkSMSDetail = new BulkSMSDetail();

                    if (ws.Cells[rw, 2].Value != null)
                    {
                        string UserName = ws.Cells[rw, 2].Value.ToString();
                        UserList.Add(UserName);

                        bulkSMSDetail.User = UserName;
                    }
                    if (ws.Cells[rw, 3].Value != null)
                    {
                        string InvoiceNo = ws.Cells[rw, 3].Value.ToString();
                        InvoiceNoList.Add(InvoiceNo);

                        bulkSMSDetail.InvoiceNo = InvoiceNo;
                    }
                    if (ws.Cells[rw, 4].Value != null)
                    {
                        string VehicleNo = ws.Cells[rw, 4].Value.ToString();
                        VehicleNoList.Add(VehicleNo);

                        bulkSMSDetail.VehicleNo = VehicleNo;
                    }
                    if (ws.Cells[rw, 5].Value != null)
                    {
                        DateTime ExpiryDate = Convert.ToDateTime(ws.Cells[rw, 5].Value.ToString());
                        ExpiryDateList.Add(ExpiryDate);

                        bulkSMSDetail.ExpiryDate = ExpiryDate;
                    }
                    if (ws.Cells[rw, 6].Value != null)
                    {
                        decimal RenewalPremium = Convert.ToDecimal(ws.Cells[rw, 6].Value.ToString());
                        RenewalPremiumList.Add(RenewalPremium);

                        bulkSMSDetail.RenewalPremium = RenewalPremium;
                    }
                    if (ws.Cells[rw, 7].Value != null)
                    {
                        string Product = ws.Cells[rw, 7].Value.ToString();
                        ProductList.Add(Product);

                        bulkSMSDetail.Product = Product;
                    }
                    if (ws.Cells[rw, 8].Value != null)
                    {
                        DateTime FromDate = Convert.ToDateTime(ws.Cells[rw, 8].Value.ToString());
                        FromDateList.Add(FromDate);

                        bulkSMSDetail.FromDate = FromDate;
                    }
                    if (ws.Cells[rw, 9].Value != null)
                    {
                        DateTime ToDate = Convert.ToDateTime(ws.Cells[rw, 9].Value.ToString());
                        ToDateList.Add(ToDate);

                        bulkSMSDetail.ToDate = ToDate;
                    }
                    if (ws.Cells[rw, 10].Value != null)
                    {
                        string Link = ws.Cells[rw, 10].Value.ToString();
                        LinkList.Add(Link);

                        bulkSMSDetail.Link = Link;
                    }
                    if (ws.Cells[rw, 11].Value != null)
                    {
                        string Cuscode = ws.Cells[rw, 11].Value.ToString();
                        CusCodeList.Add(Cuscode);

                        bulkSMSDetail.CusCode = Cuscode;
                    }
                    if (ws.Cells[rw, 12].Value != null)
                    {
                        string Month = ws.Cells[rw, 12].Value.ToString();
                        MonthList.Add(Month);

                        bulkSMSDetail.Month = Month;
                    }
                    else
                    {
                        string Month = "";
                        MonthList.Add(Month);

                        bulkSMSDetail.Month = Month;
                    }

                    if (ws.Cells[rw, 13].Value != null)
                    {
                        string PIC = ws.Cells[rw, 13].Value.ToString();
                        PICList.Add(PIC);

                        bulkSMSDetail.PIC = PIC;
                    }

                    string ResponsibleParty = string.Empty;
                    if (ws.Cells[rw, 14].Value != null)
                    {
                        ResponsibleParty = ws.Cells[rw, 14].Value.ToString();
                        ResponsiblePartyList.Add(ResponsibleParty);

                        bulkSMSDetail.ResponsibleParty = ResponsibleParty;
                    }
                    else
                    {
                        ResponsiblePartyList.Add(ResponsibleParty);
                    }

                    if (ws.Cells[rw, 15].Value != null)
                    {
                        string PolicyNo = ws.Cells[rw, 15].Value.ToString();
                        PolicyNoList.Add(PolicyNo);

                        bulkSMSDetail.PolicyNo = PolicyNo;
                    }

                    string AccountCodeStr = string.Empty;
                    if (ws.Cells[rw, 20].Value != null)
                    {
                        string AccountCode = ws.Cells[rw, 20].Value.ToString();
                        AccountCodeList.Add(AccountCode);
                        bulkSMSDetail.AccountCode = AccountCode;
                    }
                    else
                    {
                        AccountCodeList.Add(AccountCodeStr);
                    }

                    if (ws.Cells[rw, 1].Value == null)
                    {
                        if (bulkSMSDetail.CusCode != null && bulkSMSDetail.InvoiceNo != null)
                        {
                            #region Create RenewalCase
                            SMSRenewalViewModel renewal = new SMSRenewalViewModel();
                            renewal.PhoneNumber = bulkSMSDetail.PhoneNo;
                            renewal.CustomerName = bulkSMSDetail.User;
                            renewal.InvoiceNo = bulkSMSDetail.InvoiceNo;
                            renewal.RiskName = bulkSMSDetail.VehicleNo;
                            renewal.ExpiryDate = Convert.ToDateTime(bulkSMSDetail.ExpiryDate);
                            renewal.FromDate = Convert.ToDateTime(bulkSMSDetail.FromDate);
                            renewal.ToDate = Convert.ToDateTime(bulkSMSDetail.ToDate);
                            renewal.RenewalPremium = Convert.ToDecimal(bulkSMSDetail.RenewalPremium);
                            renewal.Class = bulkSMSDetail.Product;
                            renewal.Link = bulkSMSDetail.Link;
                            renewal.CusCode = bulkSMSDetail.CusCode;
                            renewal.Month = bulkSMSDetail.Month;
                            renewal.PersonInCharge = bulkSMSDetail.PIC;
                            renewal.ResponsibleParty = bulkSMSDetail.ResponsibleParty;
                            renewal.PolicyNo = bulkSMSDetail.PolicyNo;
                            if(ws.Cells[rw, 16].Value != null)
                            {
                                renewal.Branch = ws.Cells[rw, 16].Value.ToString();
                            }
                            if (ws.Cells[rw, 17].Value != null)
                            {
                                renewal.IntermediaryType = ws.Cells[rw, 17].Value.ToString();
                            }
                            if (ws.Cells[rw, 18].Value != null)
                            {
                                renewal.AgentCode = ws.Cells[rw, 18].Value.ToString();
                            }
                            if (ws.Cells[rw, 19].Value != null)
                            {
                                renewal.AgentName = ws.Cells[rw, 19].Value.ToString();
                            }
                            if (ws.Cells[rw, 21].Value != null)
                            {
                                renewal.AuthorizedCode = ws.Cells[rw, 21].Value.ToString();
                            }
                            if (ws.Cells[rw, 22].Value != null)
                            {
                                renewal.AuthorizedName = ws.Cells[rw, 22].Value.ToString();
                            }
                            renewal.AccountCode = bulkSMSDetail.AccountCode;
                            RenewalCaseInvoiceInfoResponse renewalCaseInvoiceInfoResponse = new RenewalCaseInvoiceInfoResponse();
                            renewalCaseInvoiceInfoResponse = GetRenewalInvoiceNoByInvoiceNo(renewal.InvoiceNo);
                            if (renewalCaseInvoiceInfoResponse.value.Count() == 0)
                            {
                                string CreateRenewalCaseRetMsg = await CreateRenewalCaseAsync(renewal);
                            }

                            #endregion

                            string PhoneNo = null;
                           PhoneNumberList.Add(PhoneNo);
                        }
                        else
                        {
                            continue;
                        }
                    }
                    if (ws.Cells[rw, 1].Value != null)
                    {
                        string PhoneNo = ws.Cells[rw, 1].Value.ToString();
                        PhoneNo = PhoneNo.Substring(1, PhoneNo.Length - 1); //Remove 'zero' from phoneNumber
                        PhoneNo = PhoneNo.Insert(0, "+95");// Add '+95' to phoneNumber
                        PhoneNumberList.Add(PhoneNo);

                        bulkSMSDetail.PhoneNo = PhoneNo;
                    }

                    BulkSMSDetailList.Add(bulkSMSDetail);
                }

                HttpContext.Session.SetComplexData("BulkSMSDetail", BulkSMSDetailList);
                model.BulkSMSDetail = BulkSMSDetailList;
                model.Message = sentSms.Message;
                model.UserList = UserList;
                model.PhoneNoList = PhoneNumberList;
                model.InvoiceNoList = InvoiceNoList;
                model.VehicleNoList = VehicleNoList;
                model.ExpiryDateList = ExpiryDateList;
                model.RenewalPremiumList = RenewalPremiumList;
                model.ProductList = ProductList;
                model.FromDateList = FromDateList;
                model.ToDateList = ToDateList;
                model.LinkList = LinkList;
                model.CusCodeList = CusCodeList;
                model.MonthList = MonthList;
                model.PICList = PICList;
                model.ResponsiblePartyList = ResponsiblePartyList;
                model.PolicyNoList = PolicyNoList;
                model.AccountCodeList = AccountCodeList;
                model.CategoryId = sentSms.CategoryId;
                return View(model);
            }
            catch (Exception ex)
            {
                TempData["Message"] = ex.Message.ToString();
                return RedirectToAction("SendBulkSms");
            }
        }

        [HttpPost]
        public async Task<IActionResult> SendBulkSms(List<string> PhoneNoList, List<string> UserList, string Message, Guid CategoryId, List<string> InvoiceNoList, List<string> VehicleNoList, List<DateTime> ExpiryDateList, List<decimal> RenewalPremiumList, List<string> ProductList, List<DateTime> FromDateList, List<DateTime> ToDateList, List<string> LinkList, List<string> CusCodeList, List<string> MonthList, List<string> PICList, List<string> ResponsiblePartyList, List<string> PolicyNoList)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            BindData();

            decimal lenmessge = Message.Length;
            string Smscount = Convert.ToString(Math.Ceiling(lenmessge / 70));
            Smscount = Smscount == "0" ? "1" : Smscount;

            try
            {
                BulkSMS bulkSMS = new BulkSMS();

                if (InvoiceNoList.Count() > 0)
                {
                    bulkSMS.InvoiceNoList = InvoiceNoList;
                }
                else
                {
                    bulkSMS.PolicyNoList = PolicyNoList;
                }
                bulkSMS.VehicleNoList = VehicleNoList;
                bulkSMS.ExpiryDateList = ExpiryDateList;
                bulkSMS.RenewalPremiumList = RenewalPremiumList;
                bulkSMS.ProductList = ProductList;
                bulkSMS.FromDateList = FromDateList;
                bulkSMS.ToDateList = ToDateList;
                bulkSMS.LinkList = LinkList;
                bulkSMS.CusCodeList = CusCodeList;
                bulkSMS.MonthList = MonthList;
                bulkSMS.PICList = PICList;
                bulkSMS.ResponsiblePartyList = ResponsiblePartyList;

                List<BulkSMSDetail> BulkSMSDetail = new List<BulkSMSDetail>();
                BulkSMSDetail = HttpContext.Session.GetComplexData<List<BulkSMSDetail>>("BulkSMSDetail");

                List<bool> isCRMActivity = new List<bool>();
                //List<bool> isCRMCreateRenewal = new List<bool>();
                List<bool> SMSFlag = new List<bool>();
                int SmsSuccess = 0;
                int SmsFail = 0;
                Category smsCategory = new Category();
                smsCategory = _db.Category.Where(c => c.Id == CategoryId).FirstOrDefault();


                foreach (BulkSMSDetail bulk in BulkSMSDetail)
                {
                    if (smsCategory.Status == true)
                    {
                        if (bulk.CusCode != null || bulk.CusCode != "")
                        {
                            string CRMActivityRetMsg = await CreateCRMActivityAsync(bulk.CusCode, smsCategory.Description);
                            if (CRMActivityRetMsg != "1")
                            {
                                isCRMActivity.Add(false);
                                SMSFlag.Add(false);
                                SmsFail++;
                                continue;
                            }
                            else
                            {
                                isCRMActivity.Add(true);
                            }
                        }
                        else
                        {
                            isCRMActivity.Add(false);
                            SMSFlag.Add(false);
                            SmsFail++;
                            continue;
                        }
                    }

                    // Send SMS Function
                    string smsmessage = string.Empty;
                    smsmessage = Message;

                    if (bulk.InvoiceNo != null)
                    {
                        smsmessage = smsmessage.Replace("@InvoiceNo", "Invoice No : " + bulk.InvoiceNo);
                    }
                    if (bulk.VehicleNo != null)
                    {
                        smsmessage = smsmessage.Replace("@RiskName", "Risk Name : " + bulk.VehicleNo);
                    }
                    if (bulk.ExpiryDate != null)
                    {
                        smsmessage = smsmessage.Replace("@ExpiryDate", "Policy Expiry Date : " + string.Format("{0:dd/MMM/yyyy}", bulk.ExpiryDate));
                    }
                    if (bulk.RenewalPremium != null)
                    {
                        smsmessage = smsmessage.Replace("@RenewalPremium", "Renewal Premium : " + Convert.ToDecimal(bulk.RenewalPremium).ToString("#,##0"));
                    }
                    if (bulk.Product != null)
                    {
                        smsmessage = smsmessage.Replace("@Product", bulk.Product);
                    }
                    if (bulk.FromDate != null)
                    {
                        smsmessage = smsmessage.Replace("@FromDate", string.Format("{0:dd/MMM/yyyy}", bulk.FromDate));
                    }
                    if (bulk.ToDate != null)
                    {
                        smsmessage = smsmessage.Replace("@ToDate", string.Format("{0:dd/MMM/yyyy}", bulk.ToDate));
                    }
                    if (bulk.Link != null)
                    {
                        smsmessage = smsmessage.Replace("@Link", bulk.Link);
                    }
                    if (bulk.CusCode != null)
                    {
                        smsmessage = smsmessage.Replace("@CusCode", bulk.CusCode);
                    }
                    if (bulk.Month != null)
                    {
                        smsmessage = smsmessage.Replace("@Month", bulk.Month);
                    }
                    if (bulk.PIC != null)
                    {
                        smsmessage = smsmessage.Replace("@PIC", bulk.PIC);
                    }
                    if (bulk.ResponsibleParty != null)
                    {
                        smsmessage = smsmessage.Replace("@ResponsibleParty", bulk.ResponsibleParty);
                    }
                    if (bulk.PolicyNo != null)
                    {
                        smsmessage = smsmessage.Replace("@PolicyNo", "Policy No : " + bulk.PolicyNo);
                    }
                    if (bulk.PhoneNo != null)
                    {
                        smsmessage = smsmessage.Replace("@PhoneNo", bulk.PhoneNo);
                    }
                    if (bulk.User != null)
                    {
                        smsmessage = smsmessage.Replace("@CustomerName", bulk.User);
                    }

                    bool smsFlag = false;
                    if (bulk.PhoneNo != null)
                    {
                        smsFlag = SmsService.InvokeServiceForSendSmsBulkRequest(bulk.PhoneNo, smsmessage);
                        if (smsFlag != true)
                        {
                            SMSFlag.Add(false);
                        }
                        else
                        {
                            SmsSuccess++;
                            SMSFlag.Add(true);
                        }
                    }

                }

                SaveBulkSMSData(PhoneNoList, UserList, Message, Smscount, CategoryId, bulkSMS, SMSFlag, isCRMActivity);
                TempData["Message"] = "Total Bulk SMS sent = " + SmsSuccess + " success and " + SmsFail + " fail.";
                return RedirectToAction("SendBulkSms");

            }
            catch (Exception ex)
            {
                TempData["Message"] = "Sending in Error.";
                return RedirectToAction("SendBulkSms");
            }
        }

        private void SaveBulkSMSData(List<string> PhoneNoList, List<string> UserList, string Message, string Smscount, Guid CategoryId, BulkSMS bulkSMS, List<bool> SMSFlag, List<bool> isCRMActivity)
        {
            SentSms model = new SentSms();

            for (int i = 0; i < PhoneNoList.Count; i++)
            {
                model.Id = Guid.NewGuid();
                if (PhoneNoList[i] != null)
                {
                    model.PhoneNumber = PhoneNoList[i];
                }
                else
                {
                    model.PhoneNumber = "";
                }

                model.UserName = UserList[i];
                model.SysId = Guid.Parse(HttpContext.Session.GetString("userId"));
                model.Message = Message;
                model.SentDate = DateTime.Now;
                model.Smscount = Smscount;
                model.CategoryId = CategoryId;
                if (SMSFlag.Count > 0)
                {
                    if (SMSFlag[i] == true) model.Smsflag = "1";
                    else model.Smsflag = "0";
                }
                if (isCRMActivity.Count > 0)
                {
                    model.ActivitiesFlag = isCRMActivity[i];
                }
                if (bulkSMS.PolicyNoList.Count > 0)
                {
                    model.PolicyNo = bulkSMS.PolicyNoList[i];
                }
                if (bulkSMS.VehicleNoList.Count > 0)
                {
                    model.VehicleNo = bulkSMS.VehicleNoList[i];
                }
                if (bulkSMS.ExpiryDateList.Count > 0)
                {
                    model.ExpireDate = bulkSMS.ExpiryDateList[i];
                }
                if (bulkSMS.RenewalPremiumList.Count > 0)
                {
                    model.RenewalPremium = bulkSMS.RenewalPremiumList[i];
                }
                if (bulkSMS.ProductList.Count > 0)
                {
                    model.Product = bulkSMS.ProductList[i];
                }
                if (bulkSMS.FromDateList.Count > 0)
                {
                    model.FromDate = bulkSMS.FromDateList[i];
                }
                if (bulkSMS.ToDateList.Count > 0)
                {
                    model.ToDate = bulkSMS.ToDateList[i];
                }
                if (bulkSMS.LinkList.Count > 0)
                {
                    model.Link = bulkSMS.LinkList[i];
                }
                if (bulkSMS.CusCodeList.Count > 0)
                {
                    model.CusCode = bulkSMS.CusCodeList[i];
                }
                if (bulkSMS.MonthList.Count > 0)
                {
                    model.Month = bulkSMS.MonthList[i];
                }
                if (bulkSMS.PICList.Count > 0)
                {
                    model.Pic = bulkSMS.PICList[i];
                }
                if (bulkSMS.ResponsiblePartyList.Count > 0)
                {
                    model.ResponsibleParty = bulkSMS.ResponsiblePartyList[i];
                }

                _db.SentSms.Add(model);
                _db.SaveChanges();
            }
        }
        #endregion

        #region Resend Bulk SMS

        public IActionResult SendBulkSmsActivity()
        {
            if (!CheckLoginUser())
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                BindData();
                return View();
            }
        }

        [HttpPost]
        public IActionResult UploadExcelActivity(IList<IFormFile> files, SentSms sentSms)
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }
            try
            {
                string fileExt = null;
                var supportedTypes = new[] { ".xlsx" };

                foreach (var file in files)
                    fileExt = Path.GetExtension(file.FileName);

                if (!supportedTypes.Contains(fileExt))
                {
                    TempData["Message"] = "Uploaded File Is InValid - Only Upload Excel file with right format.";
                    return RedirectToAction("SendBulkSms");
                }

                string folderName = "ImportedFiles";
                string webRootPath = _env.WebRootPath;
                string newPath = Path.Combine(webRootPath, folderName);

                if (!Directory.Exists(newPath))// Crate New Directory if not exist as per the path
                {
                    Directory.CreateDirectory(newPath);
                }
                else
                {
                    // Delete Files from Directory
                    System.IO.DirectoryInfo di = new DirectoryInfo(newPath);
                    foreach (FileInfo filesDelete in di.GetFiles())
                    {
                        filesDelete.Delete();
                    }// End Deleting files form directories
                    Directory.CreateDirectory(newPath);
                }
                var fiName = @"CRMActivity.xlsx"; //add overwrite file name
                foreach (IFormFile source in files)
                {
                    using (var fileStream = new FileStream(Path.Combine(newPath, fiName), FileMode.Create))
                    {
                        source.CopyTo(fileStream);
                    }
                }
                // Get uploaded file path with root
                string rootFolder = _env.WebRootPath;
                string fileName = @"ImportedFiles/" + fiName;
                var ep = new ExcelPackage(new FileInfo(Path.Combine(rootFolder, fileName)));
                var ws = ep.Workbook.Worksheets["Sheet1"];

                BulkSMS model = new BulkSMS();

                var PhoneNumberList = new List<string>();
                var UserList = new List<string>();

                //For Dynamic Message
                var PolicyNoList = new List<string>();
                var VehicleNoList = new List<string>();
                var ExpiryDateList = new List<DateTime>();
                var RenewalPremiumList = new List<decimal>();
                var ProductList = new List<string>();
                var FromDateList = new List<DateTime>();
                var ToDateList = new List<DateTime>();
                var LinkList = new List<string>();
                var CusCodeList = new List<string>();
                var IdList = new List<Guid>();

                List<BulkSMSDetail> BulkSMSDetailList = new List<BulkSMSDetail>();

                for (int rw = 2; rw <= ws.Dimension.End.Row; rw++)
                {
                    BulkSMSDetail bulkSMSDetail = new BulkSMSDetail();

                    if (ws.Cells[rw, 1].Value == null)
                    {
                        break;
                    }

                    if (ws.Cells[rw, 1].Value != null)
                    {
                        Guid SendId = Guid.Parse(ws.Cells[rw, 1].Value.ToString());
                        IdList.Add(SendId);

                        bulkSMSDetail.Id = SendId;
                    }
                    if (ws.Cells[rw, 3].Value != null)
                    {
                        string Cuscode = ws.Cells[rw, 3].Value.ToString();
                        CusCodeList.Add(Cuscode);

                        bulkSMSDetail.CusCode = Cuscode;
                    }
                    if (ws.Cells[rw, 4].Value != null)
                    {
                        string UserName = ws.Cells[rw, 4].Value.ToString();
                        UserList.Add(UserName);

                        bulkSMSDetail.User = UserName;
                    }
                    if (ws.Cells[rw, 5].Value != null)
                    {
                        string PhoneNo = ws.Cells[rw, 5].Value.ToString();
                        PhoneNo = PhoneNo.Substring(1, PhoneNo.Length - 1); //Remove 'zero' from phoneNumber
                        PhoneNo = PhoneNo.Insert(0, "+95");// Add '+95' to phoneNumber
                        PhoneNumberList.Add(PhoneNo);

                        bulkSMSDetail.PhoneNo = PhoneNo;
                    }
                    if (ws.Cells[rw, 6].Value != null)
                    {
                        string PolicyNo = ws.Cells[rw, 6].Value.ToString();
                        PolicyNoList.Add(PolicyNo);

                        bulkSMSDetail.PolicyNo = PolicyNo;
                    }
                    if (ws.Cells[rw, 7].Value != null)
                    {
                        string VehicleNo = ws.Cells[rw, 7].Value.ToString();
                        VehicleNoList.Add(VehicleNo);

                        bulkSMSDetail.VehicleNo = VehicleNo;
                    }
                    if (ws.Cells[rw, 8].Value != null)
                    {
                        DateTime ExpiryDate = Convert.ToDateTime(ws.Cells[rw, 8].Value.ToString());
                        ExpiryDateList.Add(ExpiryDate);

                        bulkSMSDetail.ExpiryDate = ExpiryDate;
                    }
                    if (ws.Cells[rw, 9].Value != null)
                    {
                        decimal RenewalPremium = Convert.ToDecimal(ws.Cells[rw, 9].Value.ToString());
                        RenewalPremiumList.Add(RenewalPremium);

                        bulkSMSDetail.RenewalPremium = RenewalPremium;
                    }
                    if (ws.Cells[rw, 10].Value != null)
                    {
                        string Product = ws.Cells[rw, 10].Value.ToString();
                        ProductList.Add(Product);

                        bulkSMSDetail.Product = Product;
                    }
                    if (ws.Cells[rw, 11].Value != null)
                    {
                        DateTime FromDate = Convert.ToDateTime(ws.Cells[rw, 11].Value.ToString());
                        FromDateList.Add(FromDate);

                        bulkSMSDetail.FromDate = FromDate;
                    }
                    if (ws.Cells[rw, 12].Value != null)
                    {
                        DateTime ToDate = Convert.ToDateTime(ws.Cells[rw, 12].Value.ToString());
                        ToDateList.Add(ToDate);

                        bulkSMSDetail.ToDate = ToDate;
                    }
                    if (ws.Cells[rw, 13].Value != null)
                    {
                        string Link = ws.Cells[rw, 13].Value.ToString();
                        LinkList.Add(Link);

                        bulkSMSDetail.Link = Link;
                    }

                    BulkSMSDetailList.Add(bulkSMSDetail);
                }

                HttpContext.Session.SetComplexData("BulkSMSDetail", BulkSMSDetailList);
                model.BulkSMSDetail = BulkSMSDetailList;
                model.Message = sentSms.Message;
                model.UserList = UserList;
                model.PhoneNoList = PhoneNumberList;
                model.PolicyNoList = PolicyNoList;
                model.VehicleNoList = VehicleNoList;
                model.ExpiryDateList = ExpiryDateList;
                model.RenewalPremiumList = RenewalPremiumList;
                model.ProductList = ProductList;
                model.FromDateList = FromDateList;
                model.ToDateList = ToDateList;
                model.LinkList = LinkList;
                model.CusCodeList = CusCodeList;
                model.Id = IdList;
                model.CategoryId = sentSms.CategoryId;
                return View(model);
            }
            catch (Exception ex)
            {
                TempData["Message"] = ex.Message.ToString();
                return RedirectToAction("SendBulkSmsActivity");
            }
        }

        [HttpPost]
        public async Task<IActionResult> SendBulkSmsActivity(List<string> PhoneNoList, List<string> UserList, string Message, Guid CategoryId, List<string> PolicyNoList, List<string> VehicleNoList, List<DateTime> ExpiryDateList, List<decimal> RenewalPremiumList, List<string> ProductList, List<DateTime> FromDateList, List<DateTime> ToDateList, List<string> LinkList, List<string> CusCodeList, List<Guid> Id)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            BindData();

            decimal lenmessge = Message.Length;
            string Smscount = Convert.ToString(Math.Ceiling(lenmessge / 70));
            Smscount = Smscount == "0" ? "1" : Smscount;

            try
            {
                BulkSMS bulkSMS = new BulkSMS();
                bulkSMS.PolicyNoList = PolicyNoList;
                bulkSMS.VehicleNoList = VehicleNoList;
                bulkSMS.ExpiryDateList = ExpiryDateList;
                bulkSMS.RenewalPremiumList = RenewalPremiumList;
                bulkSMS.ProductList = ProductList;
                bulkSMS.FromDateList = FromDateList;
                bulkSMS.ToDateList = ToDateList;
                bulkSMS.LinkList = LinkList;
                bulkSMS.CusCodeList = CusCodeList;

                List<BulkSMSDetail> BulkSMSDetail = new List<BulkSMSDetail>();
                BulkSMSDetail = HttpContext.Session.GetComplexData<List<BulkSMSDetail>>("BulkSMSDetail");

                List<bool> isCRMActivity = new List<bool>();
                List<bool> SMSFlag = new List<bool>();
                int SmsSuccess = 0;
                int SmsFail = 0;

                foreach (BulkSMSDetail bulk in BulkSMSDetail)
                {
                    Category smsCategory = new Category();
                    smsCategory = _db.Category.Where(c => c.Id == CategoryId).FirstOrDefault();

                    if (smsCategory.Status == true)
                    {
                        if (bulk.CusCode != null || bulk.CusCode != "")
                        {
                            string CRMActivityRetMsg = await CreateCRMActivityAsync(bulk.CusCode, smsCategory.Description);
                            if (CRMActivityRetMsg != "1")
                            {
                                isCRMActivity.Add(false);
                                SMSFlag.Add(false);
                                SmsFail++;
                                continue;
                            }
                            else
                            {
                                isCRMActivity.Add(true);
                            }
                        }
                        else
                        {
                            isCRMActivity.Add(false);
                            SMSFlag.Add(false);
                            SmsFail++;
                            continue;
                        }
                    }

                    // Send SMS Function
                    string smsmessage = string.Empty;
                    smsmessage = Message;

                    if (bulk.InvoiceNo != null)
                    {
                        smsmessage = smsmessage.Replace("@InvoiceNo", "Invoice No : " + bulk.InvoiceNo);
                    }
                    if (bulk.VehicleNo != null)
                    {
                        smsmessage = smsmessage.Replace("@RiskName", "Risk Name : " + bulk.VehicleNo);
                    }
                    if (bulk.ExpiryDate != null)
                    {
                        smsmessage = smsmessage.Replace("@ExpiryDate", "Policy Expiry Date : " + string.Format("{0:dd/MMM/yyyy}", bulk.ExpiryDate));
                    }
                    if (bulk.RenewalPremium != null)
                    {
                        smsmessage = smsmessage.Replace("@RenewalPremium", "Renewal Premium : " + Convert.ToDecimal(bulk.RenewalPremium).ToString("#,##0"));
                    }
                    if (bulk.Product != null)
                    {
                        smsmessage = smsmessage.Replace("@Product", bulk.Product);
                    }
                    if (bulk.FromDate != null)
                    {
                        smsmessage = smsmessage.Replace("@FromDate", string.Format("{0:dd/MMM/yyyy}", bulk.FromDate));
                    }
                    if (bulk.ToDate != null)
                    {
                        smsmessage = smsmessage.Replace("@ToDate", string.Format("{0:dd/MMM/yyyy}", bulk.ToDate));
                    }
                    if (bulk.Link != null)
                    {
                        smsmessage = smsmessage.Replace("@Link", bulk.Link);
                    }
                    if (bulk.CusCode != null)
                    {
                        smsmessage = smsmessage.Replace("@CusCode", bulk.CusCode);
                    }
                    if (bulk.Month != null)
                    {
                        smsmessage = smsmessage.Replace("@Month", bulk.Month);
                    }
                    if (bulk.PIC != null)
                    {
                        smsmessage = smsmessage.Replace("@PIC", bulk.PIC);
                    }
                    if (bulk.ResponsibleParty != null)
                    {
                        smsmessage = smsmessage.Replace("@ResponsibleParty", bulk.ResponsibleParty);
                    }
                    if (bulk.PolicyNo != null)
                    {
                        smsmessage = smsmessage.Replace("@PolicyNo", "Policy No : " + bulk.PolicyNo);
                    }
                    if (bulk.PhoneNo != null)
                    {
                        smsmessage = smsmessage.Replace("@PhoneNo", bulk.PhoneNo);
                    }
                    if (bulk.User != null)
                    {
                        smsmessage = smsmessage.Replace("@CustomerName", bulk.User);
                    }

                    bool smsFlag = false;
                    if (bulk.PhoneNo != null)
                    {
                        smsFlag = SmsService.InvokeServiceForSendSmsBulkRequest(bulk.PhoneNo, smsmessage);
                        if (smsFlag != true)
                        {
                            SMSFlag.Add(false);
                        }
                        else
                        {
                            SMSFlag.Add(true);
                            SmsSuccess++;
                        }
                    }

                }

                UpdateBulkSMSData(bulkSMS, SMSFlag, isCRMActivity, Id);
                TempData["Message"] = "Total Bulk SMS sent = " + SmsSuccess + " success and " + SmsFail + " fail.";
                return RedirectToAction("SendBulkSmsActivity");

            }
            catch (Exception ex)
            {
                TempData["Message"] = "Sending in Error.";
                return RedirectToAction("SendBulkSmsActivity");
            }
        }

        void UpdateBulkSMSData(BulkSMS bulkSMS, List<bool> SMSFlag, List<bool> isCRMActivity, List<Guid> Id)
        {
            SentSms model = new SentSms();

            for (int i = 0; i < Id.Count; i++)
            {

                model = _db.SentSms.Where(r => r.Id == Id[i]).FirstOrDefault();
                model.ActivitiesFlag = isCRMActivity[i];
                if (SMSFlag[i] == true) model.Smsflag = "1";
                else model.Smsflag = "0";

                if (bulkSMS.CusCodeList.Count > 0)
                {
                    model.CusCode = bulkSMS.CusCodeList[i];
                }

                _db.SentSms.Update(model);
                _db.SaveChanges();
            }
        }

        #endregion

        #region Excel Import Format
        [HttpGet]
        [Route("Export")]
        public IActionResult Export()
        {
            string sWebRootFolder = _env.WebRootPath;
            string sFileName = @"ExcelImport.xlsx";
            string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
            FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            if (file.Exists)
            {
                file.Delete();
                file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            }
            using (ExcelPackage package = new ExcelPackage(file))
            {
                // add a new worksheet to the empty workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Sheet1");
                //First add the headers
                worksheet.DefaultColWidth = 20;
                using (var cells = worksheet.Cells[1, 1, 1, 60])
                    cells.Style.Font.Bold = true;

                worksheet.Cells[1, 1].Value = "Phone Number";
                worksheet.Cells[1, 2].Value = "Customer Name";
                worksheet.Cells[1, 3].Value = "Invoice No";
                worksheet.Cells[1, 4].Value = "Risk Name";
                worksheet.Cells[1, 5].Value = "Expiry Date";
                worksheet.Cells[1, 6].Value = "Renewal Premium";
                worksheet.Cells[1, 7].Value = "Product";
                worksheet.Cells[1, 8].Value = "From Date";
                worksheet.Cells[1, 9].Value = "To Date";
                worksheet.Cells[1, 10].Value = "Link";
                worksheet.Cells[1, 11].Value = "CusCode";
                worksheet.Cells[1, 12].Value = "Month";
                worksheet.Cells[1, 13].Value = "For Person in charge(PIC)";
                worksheet.Cells[1, 14].Value = "Responsible Party";
                worksheet.Cells[1, 15].Value = "Policy No";
                worksheet.Cells[1, 16].Value = "Branch";
                worksheet.Cells[1, 17].Value = "Intermediary Type";
                worksheet.Cells[1, 18].Value = "Agent Code";
                worksheet.Cells[1, 19].Value = "Agent Name";
                worksheet.Cells[1, 20].Value = "Account Code";
                worksheet.Cells[1, 21].Value = "Authorized Code";
                worksheet.Cells[1, 22].Value = "Authorized Name";

                package.Save(); //Save the workbook.
            }
            var result = PhysicalFile(Path.Combine(sWebRootFolder, sFileName), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

            Response.Headers["Content-Disposition"] = new ContentDispositionHeaderValue("attachment")
            {
                FileName = file.Name
            }.ToString();

            return result;

        }
        #endregion
    }
}