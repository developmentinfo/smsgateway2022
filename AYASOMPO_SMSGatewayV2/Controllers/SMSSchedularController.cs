﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using AYASOMPO_SMSGatewayV2.Models;
using AYASOMPO_SMSGatewayV2.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AYASOMPO_SMSGatewayV2.Controllers
{
    public class SMSSchedularController : Controller
    {
        private readonly MainDBContext _db;
        //private IHostingEnvironment _env;
        public ISession session;
        public IConfigurationRoot Configuration { get; }
        SmsService SmsService = new SmsService();
        CRMAPIService cRMAPIService = new CRMAPIService();
        private HttpClient _client;
        private HttpClient client;

        Survey_DBContext _Surveydb = new Survey_DBContext();

        private const string Alphabet = "23456789bcdfghjkmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ-_";
        private static readonly int Base = Alphabet.Length;

        public SMSSchedularController()
        {
            var builder = new ConfigurationBuilder()
             .SetBasePath(Directory.GetCurrentDirectory())
             .AddJsonFile("appsettings.json");
            Configuration = builder.Build();

            _client = new HttpClient();
            _db = new MainDBContext();
            string url = Configuration["CoreMiddleware:IPAddress"].ToString();
            _client.BaseAddress = new Uri(Configuration["CoreMiddleware:IPAddress"].ToString());
            _client.DefaultRequestHeaders.Clear();
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            client = new HttpClient();
            string _url = Configuration["InternalMiddleware:IPAddress"].ToString();
            client.BaseAddress = new Uri(Configuration["InternalMiddleware:IPAddress"].ToString());
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<IActionResult> Index()
        {
            SchedulerLog scheduler = new SchedulerLog();
            scheduler.Id = Guid.NewGuid();
            scheduler.JobId = Guid.NewGuid();
            scheduler.Datetime = DateTime.Now.Date;
            await StartJob(scheduler);
            return View();
        }

        //internal async Task<bool> StartJob(SchedulerLog scheduler)
        //{
        //    bool flag = true;
        //    await SendCreateRenewalCSATSMS(scheduler,true);
        //    return flag;
        //}

        internal async Task<bool> StartJob(SchedulerLog scheduler)
        {
            bool flag = true;
            await PolicyReceiptTemplateReportDataSchedular(scheduler);
            return flag;
        }

        private async Task<bool> SendNewBusinessCSATSMS(SchedulerLog scheduler, CoreInfo output, bool chkSentSMS)
        {
            bool flag = false;
            int successCountSMS = 0;
            int failCountSMS = 0;
            try
            {
                SentSms sentSms = new SentSms();
                Category smsCategory = new Category();

                smsCategory = _db.Category.Where(c => c.Id == output.CategoryId).FirstOrDefault();
                sentSms.CategoryId = output.CategoryId;
                sentSms.Channel = output.Category;

                Guid SysId = _db.SysUser.Where(m => m.Name == "Schedular User").Select(m => m.Id).FirstOrDefault();
                sentSms.SysId = SysId;

                decimal lenmessge = smsCategory.MsgTemplate.Length;
                string Smscount = Convert.ToString(Math.Ceiling(lenmessge / 70));
                sentSms.Smscount = Smscount == "0" ? "1" : Smscount;

                foreach (var policy_data in output.lst)
                {
                    #region SentSMS

                    #region Prepare for SMS template
                    string smsmessage = string.Empty;
                    smsmessage = smsCategory.MsgTemplate;

                    if (policy_data.Class != null)
                    {
                        smsmessage = smsmessage.Replace("@Product", policy_data.Class);
                    }
                    else
                    {
                        smsmessage = smsmessage.Replace("@Product", "-");
                    }

                    if (policy_data.Policy_No != null)
                    {
                        smsmessage = smsmessage.Replace("@PolicyNo", "Policy No : " + policy_data.Policy_No);
                    }
                    else
                    {
                        smsmessage = smsmessage.Replace("@PolicyNo", "Policy No : " + "-");
                    }

                    if (policy_data.Period_From != null)
                    {
                        smsmessage = smsmessage.Replace("@FromDate", string.Format("{0:dd/MMM/yyyy}", policy_data.Period_From));
                    }
                    else
                    {
                        smsmessage = smsmessage.Replace("@FromDate", string.Format("{0:dd/MMM/yyyy}", "-1"));
                    }

                    if (policy_data.Period_To != null)
                    {
                        smsmessage = smsmessage.Replace("@ToDate", string.Format("{0:dd/MMM/yyyy}", policy_data.Period_To));
                    }
                    else
                    {
                        smsmessage = smsmessage.Replace("@ToDate", string.Format("{0:dd/MMM/yyyy}", "-1"));
                    }

                    if (policy_data.Link != null)
                    {
                        smsmessage = smsmessage.Replace("@Link", policy_data.Link);
                    }
                    else
                    {
                        smsmessage = smsmessage.Replace("@Link", "-");
                    }

                    if (policy_data.Invoice_No != null)
                    {
                        smsmessage = smsmessage.Replace("@InvoiceNo", "Invoice No :" + policy_data.Invoice_No);
                    }
                    else
                    {
                        smsmessage = smsmessage.Replace("@InvoiceNo", "Invoice No : " + "-");
                    }

                    if (policy_data.Vehicle_No != null)
                    {
                        smsmessage = smsmessage.Replace("@VehicleNo", "Vehicle No :" + policy_data.Vehicle_No);
                    }
                    else
                    {
                        smsmessage = smsmessage.Replace("@VehicleNo", "Vehicle No : " + "-");
                    }

                    if (policy_data.Expiry_Date != null)
                    {
                        smsmessage = smsmessage.Replace("@ExpiryDate", "ExpiryDate :" + string.Format("{0:dd/MMM/yyyy}", policy_data.Expiry_Date));
                    }
                    else
                    {
                        smsmessage = smsmessage.Replace("@ExpiryDate", "ExpiryDate : " + string.Format("{0:dd/MMM/yyyy}", "-1"));
                    }
                    sentSms.Message = smsmessage;

                    #endregion

                    bool smsFlag = false;
                    bool validity = false;
                    if (chkSentSMS == true)
                    {
                        if (policy_data.Phone_No != null)
                        {
                            string PhoneNo = policy_data.Phone_No.ToString();

                            #region Check PhoneNo

                            if (PhoneNo.StartsWith("0920") || PhoneNo.StartsWith("0921") || PhoneNo.StartsWith("0922") || PhoneNo.StartsWith("0923") || PhoneNo.StartsWith("0924")
                                || PhoneNo.StartsWith("0950") || PhoneNo.StartsWith("0951") || PhoneNo.StartsWith("0952") || PhoneNo.StartsWith("0953") || PhoneNo.StartsWith("0954")
                                || PhoneNo.StartsWith("0955") || PhoneNo.StartsWith("0956") || PhoneNo.StartsWith("0983") || PhoneNo.StartsWith("0985") || PhoneNo.StartsWith("0986")
                                || PhoneNo.StartsWith("0987"))
                            {
                                if (PhoneNo.Length == 9) validity = true;
                                else validity = false;
                            }
                            else if (PhoneNo.StartsWith("0941") || PhoneNo.StartsWith("0943") || PhoneNo.StartsWith("0947") || PhoneNo.StartsWith("0949") || PhoneNo.StartsWith("0973") || PhoneNo.StartsWith("0991"))
                            {
                                if (PhoneNo.Length == 10) validity = true;
                                else validity = false;
                            }
                            else if (PhoneNo.StartsWith("0925") || PhoneNo.StartsWith("0926") || PhoneNo.StartsWith("0940") || PhoneNo.StartsWith("0942") || PhoneNo.StartsWith("0944")
                                || PhoneNo.StartsWith("0945") || PhoneNo.StartsWith("0948") || PhoneNo.StartsWith("0988") || PhoneNo.StartsWith("0989"))
                            {
                                if (PhoneNo.Length == 11) validity = true;
                                else validity = false;
                            }
                            else if (PhoneNo.StartsWith("0994") || PhoneNo.StartsWith("0995") || PhoneNo.StartsWith("0996") || PhoneNo.StartsWith("0997") || PhoneNo.StartsWith("0998") || PhoneNo.StartsWith("0999"))
                            {
                                if (PhoneNo.Length == 11) validity = true;
                                else validity = false;
                            }
                            else if (PhoneNo.StartsWith("0930") || PhoneNo.StartsWith("0931") || PhoneNo.StartsWith("0932") || PhoneNo.StartsWith("0933") || PhoneNo.StartsWith("0936"))
                            {
                                if (PhoneNo.Length == 10) validity = true;
                                else validity = false;
                            }
                            else if (PhoneNo.StartsWith("0934") || PhoneNo.StartsWith("0935"))
                            {
                                if (PhoneNo.Length == 11) validity = true;
                                else validity = false;
                            }
                            else if (PhoneNo.StartsWith("0974") || PhoneNo.StartsWith("0975") || PhoneNo.StartsWith("0976") || PhoneNo.StartsWith("0977") || PhoneNo.StartsWith("0978") || PhoneNo.StartsWith("0979"))
                            {
                                if (PhoneNo.Length == 11) validity = true;
                                else validity = false;
                            }
                            else if (PhoneNo.StartsWith("0965") || PhoneNo.StartsWith("0966") || PhoneNo.StartsWith("0967") || PhoneNo.StartsWith("0968") || PhoneNo.StartsWith("0969"))
                            {
                                if (PhoneNo.Length == 11) validity = true;
                                else validity = false;
                            }
                            #endregion

                            if (validity == true)
                            {
                                //Create Renewal Case Activity
                                if (smsCategory.Status == true)
                                {
                                    if (policy_data.Cus_Code != null || policy_data.Cus_Code != "")
                                    {
                                        string CRMActivityRetMsg = await CreateCRMActivityAsync(policy_data.Cus_Code, smsCategory.Description);
                                        if (CRMActivityRetMsg != "1")
                                        {
                                            sentSms.ActivitiesFlag = false;
                                        }
                                        else
                                        {
                                            sentSms.ActivitiesFlag = true;
                                        }
                                    }
                                    else
                                    {
                                        sentSms.ActivitiesFlag = false;
                                    }
                                }

                                PhoneNo = PhoneNo.Substring(1, PhoneNo.Length - 1); //Remove 'zero' from phoneNumber
                                PhoneNo = PhoneNo.Insert(0, "+95");// Add '+95' to phoneNumber
                                smsFlag = SmsService.InvokeServiceForSendSmsBulkRequest(PhoneNo, smsmessage);
                            }
                        }
                        if (smsFlag == true)
                        {
                            sentSms.Smsflag = "1";
                            successCountSMS += 1;
                        }
                        else
                        {
                            sentSms.Smsflag = "0";
                            failCountSMS += 1;
                        }
                    }

                    #endregion

                    #region Save SentSMS
                    if (smsFlag == true && validity == true)
                    {
                        SaveSentSMS(policy_data, sentSms);
                    }
                    else
                    {
                        SaveSurveySMSFailCase(policy_data, sentSms);
                    }

                    #endregion
                }

                return flag = true;
            }
            catch(Exception ex)
            {
                return flag;
            }
        }

        private void SaveSentSMS(CoreInfo policyInfo, SentSms sentSms)
        {
            try
            {
                SentSms sentSmsModel = new SentSms();
                sentSmsModel.Id = Guid.NewGuid();
                sentSmsModel.PhoneNumber = policyInfo.Phone_No;
                sentSmsModel.UserName = policyInfo.Cus_Name;
                sentSmsModel.SysId = sentSms.SysId;
                sentSmsModel.Message = sentSms.Message;
                sentSmsModel.SentDate = DateTime.Now;
                sentSmsModel.Channel = sentSms.Channel;
                sentSmsModel.VehicleNo = policyInfo.Vehicle_No;
                sentSmsModel.Smsflag = sentSms.Smsflag;
                sentSmsModel.Smscount = sentSms.Smscount;
                sentSmsModel.CategoryId = sentSms.CategoryId;
                sentSmsModel.PolicyNo = policyInfo.Policy_No;
                sentSmsModel.ExpireDate = policyInfo.Expiry_Date;
                sentSmsModel.RenewalPremium = Convert.ToDecimal(policyInfo.Premium);
                sentSmsModel.Product = policyInfo.Class;
                sentSmsModel.FromDate = policyInfo.Period_From;
                sentSmsModel.ToDate = policyInfo.Period_To;
                sentSmsModel.Link = policyInfo.Link;
                sentSmsModel.CusCode = policyInfo.Cus_Code;
                sentSmsModel.ActivitiesFlag = sentSms.ActivitiesFlag;

                _db.Entry(sentSmsModel).State = EntityState.Added;
                _db.SaveChanges();

            }
            catch(Exception ex)
            {

            }
        }

        private void SaveSurveySMSFailCase(CoreInfo policyInfo, SentSms sentSms)
        {
            try
            {
                SurveySms surveySmsModel = new SurveySms();
                surveySmsModel.Id = Guid.NewGuid();
                surveySmsModel.PhoneNumber = policyInfo.Phone_No;
                surveySmsModel.CustomerName = policyInfo.Cus_Name;
                surveySmsModel.InvoiceNo = policyInfo.Invoice_No;
                surveySmsModel.RiskName = policyInfo.Vehicle_No;
                surveySmsModel.ExpiryDate = policyInfo.Expiry_Date;
                surveySmsModel.RenewalPremiun = Convert.ToDecimal(policyInfo.Premium);
                surveySmsModel.Product = policyInfo.Class;
                surveySmsModel.FromDate = policyInfo.Period_From;
                surveySmsModel.ToDate = policyInfo.Period_To;
                surveySmsModel.Link = policyInfo.Link;
                surveySmsModel.CusCode = policyInfo.Cus_Code;
                surveySmsModel.PolicyNo = policyInfo.Policy_No;
                surveySmsModel.Branch = policyInfo.Branch;
                surveySmsModel.IntermediaryType = policyInfo.Intermediary_Type;
                surveySmsModel.AgentCode = policyInfo.Agent_Code;
                surveySmsModel.AgentName = policyInfo.Agent_Name;
                surveySmsModel.AccountCode = policyInfo.Account_Code;
                surveySmsModel.AuthorizedCode = policyInfo.Authorized_code;
                surveySmsModel.AuthorizedName = policyInfo.Authorized_name;
                surveySmsModel.UploadedDate = DateTime.Now.Date;
                surveySmsModel.SysId = sentSms.SysId;
                surveySmsModel.CategoryId = sentSms.CategoryId;
                surveySmsModel.Message = sentSms.Message;
                surveySmsModel.Smscount = sentSms.Smscount;

                _db.Entry(surveySmsModel).State = EntityState.Added;
                _db.SaveChanges();

            }
            catch (Exception ex)
            {

            }
        }

        //private void DeleteTempSurveySMSData(SurveySms renewal)
        //{
        //    try
        //    {
        //        SurveySms surveySms = new SurveySms();
        //        surveySms = _db.SurveySms.Where(x => x.Id == renewal.Id).FirstOrDefault();
        //        if (surveySms != null)
        //        {
        //            _db.Entry(surveySms).State = EntityState.Deleted;
        //            _db.SaveChanges();
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}

        #region Customer timeline activity
        private async Task<string> CreateCRMActivityAsync(string cusCode, string description)
        {
            var body = new JObject();
            string responebody = "-1";
            ValidateCustomerResponse validateCustomerResponse = new ValidateCustomerResponse();

            List<NewCustomerInfo> lstcustInfo = new List<NewCustomerInfo>();
            lstcustInfo = await GetNewCustomerInfoByCusCode(cusCode);

            if (lstcustInfo.Count() > 0)
            {
                foreach (var lst in lstcustInfo)
                {
                    if (lst.cus_type == "I")
                    {
                        validateCustomerResponse = GetCRMContactsCustomerByCusCode(cusCode);
                        if (validateCustomerResponse.value != null)
                        {
                            if (validateCustomerResponse.value.Count() > 0)
                            {
                                string contactid = "";
                                foreach (var custccode in validateCustomerResponse.value)
                                {
                                    contactid = custccode.contactid;
                                }
                                Guid contactId = Guid.Parse(contactid);
                                if (contactId != null)
                                {
                                    string activityrequesturl = "ayasompo_contactcustomactivities";
                                    body = new JObject
                                    {
                                         { "subject", "New Business CSAT Survey SMS" },
                                         { "description", "Sending New Business CSAT Survey SMS to customer at " + DateTime.Now},
                                         { "regardingobjectid_contact@odata.bind", "/contacts("+ contactid +")" },
                                    };

                                    responebody = cRMAPIService.CreateActivities(HttpMethod.Post, body, activityrequesturl);
                                }
                            }
                        }
                    }
                    else
                    {
                        ValidateCorpCustomerResponse validateCorpCustomerResponse = new ValidateCorpCustomerResponse();
                        validateCorpCustomerResponse = GetCRMAccountCustomerByCusCode(cusCode);

                        if (validateCorpCustomerResponse.value != null)
                        {
                            if (validateCorpCustomerResponse.value.Count() > 0)
                            {
                                string contactid = "";
                                foreach (var custccode in validateCorpCustomerResponse.value)
                                {
                                    contactid = custccode.accountid;
                                }
                                Guid contactId = Guid.Parse(contactid);
                                if (contactId != null)
                                {
                                    string activityrequesturl = "ayasompo_contactcustomactivities";
                                    body = new JObject
                                    {
                                         { "subject", "New Business CSAT Survey SMS" },
                                         { "description", "Sending New Business CSAT Survey SMS to customer at " + DateTime.Now},
                                         { "regardingobjectid_account@odata.bind", "/accounts("+ contactid +")" },
                                    };

                                    responebody = cRMAPIService.CreateActivities(HttpMethod.Post, body, activityrequesturl);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                return responebody;
            }

            return responebody;
        }

        private ValidateCustomerResponse GetCRMContactsCustomerByCusCode(string cuscode)
        {
            ValidateCustomerResponse validateCustomerResponse = new ValidateCustomerResponse();
            string responseBody = "";
            var body = new JObject();
            string url = "";

            if (cuscode != null)
            {
                url = "contacts?$select=fullname&$filter=ayasompo_customercode eq '" + cuscode + "'";
                responseBody = cRMAPIService.CRMGetCustomerByCustomerCode(HttpMethod.Get, body = null, url);
                validateCustomerResponse = JsonConvert.DeserializeObject<ValidateCustomerResponse>(responseBody);
            }
            return validateCustomerResponse;
        }

        private ValidateCorpCustomerResponse GetCRMAccountCustomerByCusCode(string cuscode)
        {
            ValidateCorpCustomerResponse validateCorpCustomerResponse = new ValidateCorpCustomerResponse();
            string responseBody = "";
            var body = new JObject();
            string url = "";

            if (cuscode != null)
            {
                url = "accounts?$select=name&$filter=ayasompo_code eq '" + cuscode + "'";
                responseBody = cRMAPIService.CRMGetCustomerByCustomerCode(HttpMethod.Get, body = null, url);
                validateCorpCustomerResponse = JsonConvert.DeserializeObject<ValidateCorpCustomerResponse>(responseBody);

            }
            return validateCorpCustomerResponse;
        }

        private async Task<List<NewCustomerInfo>> GetNewCustomerInfoByCusCode(string cuscode)
        {
            List<NewCustomerInfo> newCustomerInfos = new List<NewCustomerInfo>();
            try
            {
                var responseTask = await _client.GetAsync($"/customer/GetNewCustomerByCusCode/{cuscode}");
                if (responseTask.IsSuccessStatusCode)
                {
                    //newCustomerInfos = await responseTask.Content.ReadAsAsync<List<NewCustomerInfo>>();
                    //newCustomerInfos = await responseTask.Content.ReadAsStreamAsync<List<NewCustomerInfo>>();

                    string jsonData = await responseTask.Content.ReadAsStringAsync();
                    newCustomerInfos = JsonConvert.DeserializeObject<List<NewCustomerInfo>>(jsonData);
                    return newCustomerInfos;
                }
                else
                {
                    return newCustomerInfos = new List<NewCustomerInfo>();
                }
            }
            catch (Exception ex)
            {
                return newCustomerInfos = new List<NewCustomerInfo>();
            }
        }
        #endregion

        #region Policy Receipt Template Report

        public CoreInfo CoreInfoReceiptData(CoreInfo info)
        {
            CoreInfo output = new CoreInfo();

            List<CoreInfo> coreInfos = new List<CoreInfo>();
            //session = HttpContext.Session;
            //session.SetString("From", info.from);
            //session.SetString("To", info.to);

            output.CategoryId = info.CategoryId;
            output.Category = _db.Category.Where(c => c.Id == info.CategoryId).Select(c => c.Description).FirstOrDefault();

            var responseTask = client.PostAsJsonAsync<CoreInfo>($"/CoreReport/GetPolicyReceiptTemplate", info);
            responseTask.Wait();
            var message = responseTask.Result;
            message.EnsureSuccessStatusCode();
            if (message.IsSuccessStatusCode)
            {
                #region Log
                Log log = new Log();
                log.Id = Guid.NewGuid();
                log.LogTitle = "Start Job";
                log.Logdetail = "Start Job Detail";
                log.LogTime = DateTime.Now;
                _db.Entry(log).State = EntityState.Added;
                _db.SaveChanges();
                #endregion

                var jsonData = message.Content.ReadAsStringAsync();
                List<CoreInfo> lstreportinfo = new List<CoreInfo>();
                lstreportinfo = JsonConvert.DeserializeObject<List<CoreInfo>>(jsonData.Result);

                List<CoreInfo> lstCoreInfos = new List<CoreInfo>();
                foreach (CoreInfo reportinfo in lstreportinfo)
                {
                    string cleartext = "customercode=" + reportinfo.Cus_Code + "&policyno=" + reportinfo.Policy_No + "&customerphoneno=" + reportinfo.Phone_No;
                    string encode = Encrypt(cleartext);

                    if (output.Category == "New Business CSAT(EN)")
                    {
                        CoreAccountCode coreAccountCode = new CoreAccountCode();
                        coreAccountCode = _db.CoreAccountCode.Where(m => m.AccountCode == reportinfo.Account_Code).FirstOrDefault();
                        if(coreAccountCode == null)
                        {
                            //For Live
                            reportinfo.Link = @"https://cxsurvey.ayasompo.com/survey/newbusiness?source=" + encode;
                            string shortlink = Create(reportinfo.Link, @"https://cxsurvey.ayasompo.com/link/qkw?pa=");

                            #region For UAT
                            //reportinfo.Link = @"https://uatsurvey.ayasompo.com:9012/survey/newbusiness?source=" + encode;
                            //string shortlink = Create(reportinfo.Link, @"https://uatsurvey.ayasompo.com:9012/link/QKW?pa=");
                            #endregion

                            reportinfo.Link = shortlink;

                            lstCoreInfos.Add(reportinfo);
                        }
                    }
                }
                output.lst = lstCoreInfos;

                //SaveSurveyTempSms(lstCoreInfos);
            }

            return output;
        }

        private async Task<bool> PolicyReceiptTemplateReportDataSchedular(SchedulerLog scheduler)
        {
            bool flag = false;
            try
            {
                CoreInfo output = new CoreInfo();
                CoreInfo info = new CoreInfo();

                info.CategoryId = _db.Category.Where(m => m.Description == "New Business CSAT(EN)").Select(m => m.Id).FirstOrDefault();
                info.from = DateTime.Now.Date.AddDays(-1).ToString();
                info.to = DateTime.Now.Date.AddDays(-1).ToString();
                info.filter = "receiptdate";
                info.Credit_term = "NO";
                info.Transaction_type = "N";
                output = CoreInfoReceiptData(info);

                await SendNewBusinessCSATSMS(scheduler, output, true);
                return flag = true;
            }
            catch(Exception ex)
            {
                return flag;
            }

        }

        public static string Encrypt(string clearText)
        {
            string Encryptkey = "65777383"; // ASCII code for AMIS 
            string EncryptionKey = Encryptkey;
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        [ValidateAntiForgeryToken]
        public string Create(string originalUrl, string shortlink)
        {
            if (ModelState.IsValid)
            {
                int Id = _Surveydb.ShortUrls.Select(x => x.Id).LastOrDefault();
                Id++;
                string link = string.Empty;

                link = Encode(Id);
                link = shortlink + link;

                var shortUrls = new ShortUrls
                {
                    OriginalUrl = originalUrl,
                    ShortUrl = link,
                    Id = Id
                };

                _Surveydb.Entry(shortUrls).State = EntityState.Added;
                _Surveydb.SaveChanges();
                return link;
            }
            else
            {
                return "";
            }
        }

        public static string Encode(int num)
        {
            //num = 123456789;
            var sb = new StringBuilder();
            while (num > 0)
            {

                sb.Insert(0, Alphabet.ElementAt(num % Base));
                num = num / Base;
            }
            return sb.ToString();
        }

        //private void SaveSurveyTempSms(List<CoreInfo> coreInfos)
        //{
        //    foreach (CoreInfo policyInfo in coreInfos)
        //    {
        //        SurveySms surveySmsModel = new SurveySms();
        //        surveySmsModel.Id = Guid.NewGuid();
        //        surveySmsModel.PhoneNumber = policyInfo.Phone_No;
        //        surveySmsModel.CustomerName = policyInfo.Cus_Name;
        //        surveySmsModel.InvoiceNo = policyInfo.Invoice_No;
        //        surveySmsModel.RiskName = policyInfo.Vehicle_No;
        //        surveySmsModel.ExpiryDate = policyInfo.Expiry_Date;
        //        surveySmsModel.RenewalPremiun = Convert.ToDecimal(policyInfo.Premium);
        //        surveySmsModel.Product = policyInfo.Class;
        //        surveySmsModel.FromDate = policyInfo.Period_From;
        //        surveySmsModel.ToDate = policyInfo.Period_To;
        //        surveySmsModel.Link = policyInfo.Link;
        //        surveySmsModel.CusCode = policyInfo.Cus_Code;
        //        surveySmsModel.PolicyNo = policyInfo.Policy_No;
        //        surveySmsModel.Branch = policyInfo.Branch;
        //        surveySmsModel.IntermediaryType = policyInfo.Intermediary_Type;
        //        surveySmsModel.AgentCode = policyInfo.Agent_Code;
        //        surveySmsModel.AgentName = policyInfo.Agent_Name;
        //        surveySmsModel.AccountCode = policyInfo.Account_Code;
        //        surveySmsModel.AuthorizedCode = policyInfo.Authorized_code;
        //        surveySmsModel.AuthorizedName = policyInfo.Authorized_name;
        //        surveySmsModel.UploadedDate = DateTime.Now.Date;
        //        Guid SysId = _db.SysUser.Where(m => m.Name == "Schedular User").Select(m => m.Id).FirstOrDefault();
        //        surveySmsModel.SysId = SysId;
        //        surveySmsModel.CategoryId = Guid.Parse("E130B05B-D2C8-49A8-9A1E-3C0911F29214");
        //        //surveySmsModel.Message = sentSms.Message;
        //        //surveySmsModel.Smscount = sentSms.Smscount;

        //        _db.Entry(surveySmsModel).State = EntityState.Added;
        //        _db.SaveChanges();
        //    }
        //}

        #endregion

    }
}