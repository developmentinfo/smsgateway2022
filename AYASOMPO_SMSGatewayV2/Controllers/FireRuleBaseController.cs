﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using AYASOMPO_SMSGatewayV2.Models;
using AYASOMPO_SMSGatewayV2.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Rewrite.Internal.ApacheModRewrite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Rotativa.AspNetCore;
using static AYASOMPO_SMSGatewayV2.Models.CustomerInfoViewModel;
using static AYASOMPO_SMSGatewayV2.Models.DefaultPolicyAnswerResponse;
using static AYASOMPO_SMSGatewayV2.Models.FireProposalViewModel;

namespace AYASOMPO_SMSGatewayV2.Controllers
{
    public class FireRuleBaseController : Controller
    {
        IILAPIservices IILAPIservices = new IILAPIservices();
        List<QuestionRule> lstQuestionRule = new List<QuestionRule>();
        MiddlewareAPIService _middlewareAPI = new MiddlewareAPIService();
        MainDBContext _context = new MainDBContext();
        private IHostingEnvironment _env;
        private IConfiguration _configuration;
        public IConfiguration Configuration { get; set; }
        private HttpClient _client;
        FireRuleValidation ruleValidation = new FireRuleValidation();
        public string sqlCon { get; set; }
        public ISession session;

        #region static master list
        List<MasterDataViewModel> lstOccupancy = new List<MasterDataViewModel>();
        List<MasterDataViewModel> lstRoof = new List<MasterDataViewModel>();
        List<MasterDataViewModel> lstWalls = new List<MasterDataViewModel>();
        List<MasterDataViewModel> lstColumns = new List<MasterDataViewModel>();
        List<MasterDataViewModel> lstFloor = new List<MasterDataViewModel>();

        List<MasterDataViewModel> lstABCType = new List<MasterDataViewModel>();
        List<MasterDataViewModel> lstCO2Type = new List<MasterDataViewModel>();
        List<MasterDataViewModel> lstExtinguisher = new List<MasterDataViewModel>();
        List<MasterDataViewModel> lstWeightOfFire = new List<MasterDataViewModel>();
        List<MasterDataViewModel> lstRequiredNumberOfExtinguisher = new List<MasterDataViewModel>();

        List<MasterDataViewModel> lstHoseReel = new List<MasterDataViewModel>();
        List<MasterDataViewModel> lstHydrant = new List<MasterDataViewModel>();
        List<MasterDataViewModel> lstTypeFireTank = new List<MasterDataViewModel>();
        List<MasterDataViewModel> lstNumberOfFireTank = new List<MasterDataViewModel>();
        List<MasterDataViewModel> lstSecurityGuardsData = new List<MasterDataViewModel>();

        List<MasterDataViewModel> lstSecurityGuardsDutyShiftData = new List<MasterDataViewModel>();
        List<MasterDataViewModel> lstSecurityGuardsDutyHourData = new List<MasterDataViewModel>();
        List<MasterDataViewModel> lstNumberOfCCTVData = new List<MasterDataViewModel>();
        List<ProductClassViewModel> lstProductClassViewModels = new List<ProductClassViewModel>();
        List<InventoryItemByPCodeViewModel> lstInventoryItemByPCodeViewModels = new List<InventoryItemByPCodeViewModel>();

        List<AgentCodeMaster> lstAgentCode = new List<AgentCodeMaster>();
        List<AccountCodeMaster> lstAccountCode = new List<AccountCodeMaster>();
        List<IntemediaryViewModel> lstIntemediaryViewModels = new List<IntemediaryViewModel>();
        List<MasterDataViewModel> lstClassOfBuilding = new List<MasterDataViewModel>();


        #endregion

        public enum Operator
        {
            Equal,
            NotEqual,
            AllowStatus,
            String,
            Number
        }

        public enum PriorityLevel
        {
            level1 = 1,
            level2 = 2,
            level3 = 3,
            level4 = 4,
            level5 = 5,

            level6 = 6,
            level7 = 7,
            level8 = 8,
            level9 = 9,
            level10 = 10,

            level11 = 11,
            level12 = 12,
            level13 = 13,
            level14 = 14,
            level15 = 15,

            level16 = 16,
            level17 = 17,
            level18 = 18,
            level19 = 19,
            level20 = 20,


            level21 = 21,
            level22 = 22,
            level23 = 23,
            level24 = 24,
            level25 = 25,

            level26 = 26,
            level27 = 27,
            level28 = 28,
            level29 = 29,
            level30 = 30,

            level31 = 31,
            level32 = 32,
            level33 = 33,
            level34 = 34,
            level35 = 35,

            level36 = 36,
            level37 = 37,
            level38 = 38,
            level39 = 39,
            level40 = 40,

            level41 = 41,
            level42 = 42,
            level43 = 43,
            level44 = 44,
            level45 = 45,

            level46 = 46,
            level47 = 47,
            level48 = 48,
            level49 = 49,
            level50 = 50

        }

        public enum TransactionType
        {
            New,
            Renewal
        }

        public FireRuleBaseController(IHostingEnvironment env, IConfiguration configuration)
        {
            _env = env;
            _configuration = configuration;
            _client = new HttpClient();

            var builder = new ConfigurationBuilder()
             .SetBasePath(Directory.GetCurrentDirectory())
             .AddJsonFile("appsettings.json");
            Configuration = builder.Build();

            sqlCon = Configuration["ConnectionStrings:DefaultConnection"].ToString();

        }

        public IActionResult Test()
        {
            return View();
        }

       
        public async Task<IActionResult> FireUW()
        {
            PrepareData();
            await PrepareDataMasterDataAsync();
            FireUWViewModel fireUWViewModel = new FireUWViewModel();
            if (HttpContext.Session.GetObjectFromJson<List<RiskLocation>>("lstRiskLocations") != null)
            {
                List<RiskLocation> lstRiskLocations = new List<RiskLocation>();
                lstRiskLocations = HttpContext.Session.GetObjectFromJson<List<RiskLocation>>("lstRiskLocations");

                if (lstRiskLocations.Count() > 0)
                {
                    fireUWViewModel.lstRiskLocations = lstRiskLocations;
                    //fireUWViewModel.riskCount = lstRiskLocations.Count();
                    var risk = (from d in lstRiskLocations
                                orderby d.RiskName
                                select d.RiskName).ToList().Distinct();
                    decimal tsi = 0;
                    tsi = Convert.ToDecimal(lstRiskLocations.Sum(t => t.RiskSumInsured));
                    fireUWViewModel.TotalSumInsured = tsi;

                    fireUWViewModel.riskCount = (from d in lstRiskLocations
                                                 orderby d.RiskName
                                                 select d.RiskName).ToList().Distinct().Count();
                    
                }
            }
            return View(fireUWViewModel);
        }

        public async Task<ActionResult> EditFireUW(string refNo)
        {
            PrepareData();
            await PrepareDataMasterDataAsync();
            FireRuleProposalViewModel fireRuleProposalViewModel = new FireRuleProposalViewModel();
            fireRuleProposalViewModel = GetFireProposalrelatedData(refNo);
            fireRuleProposalViewModel.riskCount = fireRuleProposalViewModel.fireRiskdetails.Count;

            return View(fireRuleProposalViewModel);
        }

        #region Post to UW Request
        public ActionResult PostToUW(string refNo)
        {
            PrepareData();
            RequestDetailViewModel requestDetailViewModel = new RequestDetailViewModel();
            requestDetailViewModel.ReferenceNo = refNo;
            requestDetailViewModel.lstFireRequireddocumentsetting = _context.FireRequireddocumentsetting.ToList();
            return View(requestDetailViewModel);
        }

        public ActionResult SummaryRequestView(string refNo)
        {

            RequestDetailViewModel requestDetailViewModel = new RequestDetailViewModel();
            if (refNo != null)
            {

            }

            return View();
        }

        public ActionResult SaveRequestDetail(RequestDetailViewModel requestDetailViewModel)
        {
            try
            {
                Requestdetail requestdetail = new Requestdetail();
                if (requestDetailViewModel.ReferenceNo != null)
                {
                    UwPerson uwPerson = new UwPerson();
                    requestdetail.Id = Guid.NewGuid();
                    requestdetail.ProposalReferenceNo = requestDetailViewModel.ReferenceNo;
                    requestdetail.UwpersonId = requestDetailViewModel.UwpersonId;
                    if (requestdetail.UwpersonId != null)
                    {
                        uwPerson = _context.UwPerson.Where(uw => uw.Id == requestdetail.UwpersonId).FirstOrDefault();
                    }
                    requestdetail.UwpersonEmail = uwPerson.PersonEmail;
                    requestdetail.RequestedDate = DateTime.Now;
                    requestdetail.Status = "pending";
                    requestdetail.Buremark = requestDetailViewModel.Buremark;

                    _context.Entry(requestdetail).State = EntityState.Added;
                    _context.SaveChanges();

                    #region Save Files to RequiredDocuments Upload and Approval Form

                    FireRequireddocuments fireRequireddocuments = new FireRequireddocuments();
                    fireRequireddocuments = ConvertApprovalFileToByte(requestDetailViewModel.ReferenceNo);

                    if (fireRequireddocuments != null)
                    {
                        fireRequireddocuments.Id = Guid.NewGuid();
                        fireRequireddocuments.ProposalReferenceNo = requestDetailViewModel.ReferenceNo;
                        fireRequireddocuments.requestdetailId = requestdetail.Id;

                        _context.Entry(fireRequireddocuments).State = EntityState.Added;
                        _context.SaveChanges();


                    }


                    List<FireRequireddocuments> lstFireRequireddocuments = new List<FireRequireddocuments>();

                    if (HttpContext.Session.GetObjectFromJson<List<FireRequireddocuments>>("lstFireRequireddocuments") != null)
                    {
                        lstFireRequireddocuments = HttpContext.Session.GetObjectFromJson<List<FireRequireddocuments>>("lstFireRequireddocuments");
                        if (lstFireRequireddocuments.Count() > 0)
                        {
                            foreach (var fileatt in lstFireRequireddocuments)
                            {
                                FireRequireddocuments requireddocuments = new FireRequireddocuments();
                                requireddocuments.Id = Guid.NewGuid();
                                requireddocuments.requestdetailId = requestdetail.Id;
                                requireddocuments.ProposalReferenceNo = requestDetailViewModel.ReferenceNo;
                                requireddocuments.DocumentName = fileatt.DocumentName;
                                requireddocuments.DocumentFile = fileatt.DocumentFile;
                                requireddocuments.UploadedFileName = fileatt.UploadedFileName;
                                _context.Entry(requireddocuments).State = EntityState.Added;
                                _context.SaveChanges();
                            }

                            #region Delete Local File
                            //string uploadpath = _env.WebRootPath;
                            //string dest_path = Path.Combine(uploadpath, "Attachment/" + requestDetailViewModel.ReferenceNo + "/");
                            //DeleteLocalFile(dest_path);
                            #endregion
                        }
                    }
                    #endregion

                }
            }
            catch (Exception ex)
            {

            }

            return RedirectToAction("SummaryRequestView", new { refNo = requestDetailViewModel.ReferenceNo });
        }

        private FireRequireddocuments ConvertApprovalFileToByte(String ReferenceNo)
        {
            FireRequireddocuments fireRequireddocuments = new FireRequireddocuments();
            FireRuleProposalViewModel fireRuleProposalViewModel = new FireRuleProposalViewModel();
            try
            {
                if (ReferenceNo != null)
                {
                    fireRuleProposalViewModel = GetFireProposalrelatedData(ReferenceNo);
                    List<RuleResultViewModel> lstSummaryRuleResult = new List<RuleResultViewModel>();
                    if (HttpContext.Session.GetObjectFromJson<List<FireRequireddocuments>>("lstSummaryRuleResult") != null)
                    {
                        lstSummaryRuleResult = HttpContext.Session.GetObjectFromJson<List<RuleResultViewModel>>("lstSummaryRuleResult");
                    }
                    fireRuleProposalViewModel.lstRuleResultViewModel = lstSummaryRuleResult;

                    if (fireRuleProposalViewModel != null)
                    {
                        var path = Path.Combine(_env.WebRootPath, "Attachment\\" + ReferenceNo + "\\");

                        //var filepath = Path.Combine("C:\\DRSUpload\\" + refNo);
                        if (!System.IO.Directory.Exists(path))
                        {
                            System.IO.Directory.CreateDirectory(path);
                        }

                        string filename = "ApprovalForm.pdf";
                        var filepath = Path.Combine(path + filename);
                        var ApprovalFormPdf = new ViewAsPdf("ApprovalFormPdf", fireRuleProposalViewModel)
                        {
                            FileName = "ApprovalForm.pdf",
                            SaveOnServerPath = Path.Combine(path + filename)
                        };
                        //return RedirectToAction("ApprovalFormPdf", fireRuleProposalViewModel);
                        ApprovalFormPdf.BuildFile(this.ControllerContext);


                        #region Prepare for file byte
                        byte[] fileBytes = null;

                        System.IO.FileStream fs = new System.IO.FileStream(filepath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                        System.IO.BinaryReader binaryReader = new System.IO.BinaryReader(fs);
                        long byteLength = new System.IO.FileInfo(filepath).Length;
                        fileBytes = binaryReader.ReadBytes((Int32)byteLength);

                        fireRequireddocuments.DocumentName = filename;
                        fireRequireddocuments.DocumentFile = fileBytes;
                        fireRequireddocuments.UploadedFileName = filename;
                        return fireRequireddocuments;

                        #endregion

                    }
                }
                return fireRequireddocuments;
            }
            catch (Exception ex)
            {
                return fireRequireddocuments;
            }
        }

        [HttpPost]
        public JsonResult PostAttachment()
        {
            session = HttpContext.Session;
            List<String> ImageList = new List<String>();
            try
            {
                var files = HttpContext.Request.Form.Files;
                string attachmentName = HttpContext.Request.Form["fileName"].ToString();
                if (files != null)
                {
                    //string refNo = session.GetString("refNo");
                    foreach (var file in files)
                    {
                        if (file.Length > 0)
                        {
                            var fileName = file.FileName;

                            byte[] fileBytes = null;

                            using (var memoryStream = new MemoryStream())
                            {
                                BinaryReader reader = new BinaryReader(file.OpenReadStream());
                                fileBytes = reader.ReadBytes((int)file.Length);
                            }

                            List<FireRequireddocuments> lstFireRequireddocuments = new List<FireRequireddocuments>();

                            if (HttpContext.Session.GetObjectFromJson<List<FireRequireddocuments>>("lstFireRequireddocuments") != null)
                            {
                                lstFireRequireddocuments = HttpContext.Session.GetObjectFromJson<List<FireRequireddocuments>>("lstFireRequireddocuments");
                            }

                            if (lstFireRequireddocuments.Count() > 0)
                            {
                                FireRequireddocuments FireDocs = new FireRequireddocuments();

                                FireDocs = lstFireRequireddocuments.Where(fatt => fatt.DocumentName == attachmentName).FirstOrDefault();
                                if (FireDocs != null)
                                {
                                    lstFireRequireddocuments.Remove(FireDocs);
                                }

                            }

                            FireRequireddocuments fireRequireddocuments = new FireRequireddocuments();
                            fireRequireddocuments.DocumentName = attachmentName;
                            fireRequireddocuments.DocumentFile = fileBytes;
                            fireRequireddocuments.UploadedFileName = fileName;

                            lstFireRequireddocuments.Add(fireRequireddocuments);
                            HttpContext.Session.SetObjectAsJson("lstFireRequireddocuments", null);
                            HttpContext.Session.SetObjectAsJson("lstFireRequireddocuments", lstFireRequireddocuments);

                        }
                    }

                    return Json("OK");
                }
                else
                {
                    return Json("-1");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public IActionResult ApprovalFormPdf(FireRuleProposalViewModel fireRuleProposalViewModel)
        {

            return View(fireRuleProposalViewModel);
        }


        #endregion

        #region Run the Rule
        public async Task<IActionResult> RunRuleResult(string refNo)
        {
            List<RuleResultViewModel> lstRuleResultViewModel = new List<RuleResultViewModel>();
            List<RuleResultViewModel> lstSummaryRuleResult = new List<RuleResultViewModel>();
            FireRuleProposalViewModel fireRuleProposalViewModel = new FireRuleProposalViewModel();
           
            if(refNo != null)
            {
                fireRuleProposalViewModel = GetFireProposalrelatedData(refNo);

                lstRuleResultViewModel = await RunFireRuleAsync(refNo , fireRuleProposalViewModel);
                lstSummaryRuleResult = ruleValidation.CheckingRules(lstRuleResultViewModel, fireRuleProposalViewModel);
                lstSummaryRuleResult = ruleValidation.CheckingManualRules(lstRuleResultViewModel ,lstSummaryRuleResult, fireRuleProposalViewModel);
                if(lstSummaryRuleResult.Count() > 0)
                {
                    ViewBag.lstSummaryRuleResult = lstSummaryRuleResult;
                    HttpContext.Session.SetObjectAsJson("lstSummaryRuleResult", null);
                    HttpContext.Session.SetObjectAsJson("lstSummaryRuleResult", lstSummaryRuleResult);
                    return View(fireRuleProposalViewModel);                    
                }
            }
            else
            {
                return RedirectToAction("FireUW");
            }
            return View(fireRuleProposalViewModel);
        }

        private async Task<List<RuleResultViewModel>> RunFireRuleAsync(string refNo, FireRuleProposalViewModel fireRuleProposalViewModel)
        {
            List<RuleResultViewModel> lstRuleResultViewModel = new List<RuleResultViewModel>();
            try
            {
                if(fireRuleProposalViewModel != null)
                {
                    DefaultPolicyAnswerResponse defaultPolicyAnswerResponse = new DefaultPolicyAnswerResponse();
                    DefaultPolicyAnswerRequest request = new DefaultPolicyAnswerRequest();
                    request.customerCode = fireRuleProposalViewModel.CusCode;
                    request.productCode = fireRuleProposalViewModel.ProductCode;
                    request.type = "policy";

                    defaultPolicyAnswerResponse = IILAPIservices.GetDefaultPolicyAnswerObj(request);
                    if(defaultPolicyAnswerResponse != null)
                    {
                        
                        lstRuleResultViewModel = await IILAPIservices.CreateProposalRequestAsync(defaultPolicyAnswerResponse, fireRuleProposalViewModel);
                        return lstRuleResultViewModel;
                    }
                }
                return lstRuleResultViewModel;
            }
            catch(Exception ex) {
                return lstRuleResultViewModel;
            }
        }

        private FireRuleProposalViewModel GetFireProposalrelatedData(string refNo)
        {
            FireRuleProposalViewModel fireRuleProposalViewModel = new FireRuleProposalViewModel();
            try
            {
                RuleProposal ruleProposal = new RuleProposal();
                List<FireRiskdetail> lstFireRiskdetail = new List<FireRiskdetail>();
                ruleProposal = _context.RuleProposal.Where(r => r.ReferenceNo == refNo).FirstOrDefault();

                if(ruleProposal != null)
                {
                    #region Rule Proposal
                    fireRuleProposalViewModel.Id = ruleProposal.Id;
                    fireRuleProposalViewModel.ReferenceNo = ruleProposal.ReferenceNo;
                    fireRuleProposalViewModel.Class = ruleProposal.Class;
                    fireRuleProposalViewModel.ProductCode = ruleProposal.ProductCode;
                    fireRuleProposalViewModel.IssueDate = ruleProposal.IssueDate;

                    fireRuleProposalViewModel.TransactionType = ruleProposal.TransactionType;
                    fireRuleProposalViewModel.CusCode = ruleProposal.CusCode;
                    fireRuleProposalViewModel.CusName = ruleProposal.CusName;
                    fireRuleProposalViewModel.IntermediaryCode = ruleProposal.IntermediaryCode;
                    fireRuleProposalViewModel.IntermediaryName = ruleProposal.IntermediaryName;

                    fireRuleProposalViewModel.AgentCode = ruleProposal.AgentCode;
                    fireRuleProposalViewModel.AgentName = ruleProposal.AgentName;
                    fireRuleProposalViewModel.AccountCode = ruleProposal.AccountCode;
                    fireRuleProposalViewModel.AccountUserCode = ruleProposal.AccountUserCode;
                    fireRuleProposalViewModel.AccountHandlerCode = ruleProposal.AccountHandlerCode;

                    fireRuleProposalViewModel.TotalSumInsured = ruleProposal.TotalSumInsured;
                    fireRuleProposalViewModel.Premium = ruleProposal.Premium;
                    fireRuleProposalViewModel.PolicyStartDate = ruleProposal.PolicyStartDate;
                    fireRuleProposalViewModel.PolicyEndDate = ruleProposal.PolicyEndDate;
                    fireRuleProposalViewModel.Uwstatus = "pending";

                    fireRuleProposalViewModel.Version = 1;
                    fireRuleProposalViewModel.createdby = ruleProposal.createdby;
                    fireRuleProposalViewModel.InsuredMailingAddress = ruleProposal.InsuredMailingAddress;
                    #endregion

                    #region Risk Detail
                    fireRuleProposalViewModel.fireRiskdetails = new List<FireRiskdetailView>();
                    lstFireRiskdetail = _context.FireRiskdetail.Where(risk => risk.ProposalReferenceno == refNo).ToList();
                    foreach (var risk in lstFireRiskdetail)
                    {
                        FireRiskdetailView fireRiskdetailView = new FireRiskdetailView();

                        fireRiskdetailView.Id = risk.Id;
                        fireRiskdetailView.ProposalId = risk.ProposalId;
                        fireRiskdetailView.ProposalReferenceno = risk.ProposalReferenceno;
                        fireRiskdetailView.RiskName = risk.RiskName;
                        fireRiskdetailView.RiskSumInsured = risk.RiskSumInsured;
                        fireRiskdetailView.RiskLocation = risk.RiskLocation;

                        fireRiskdetailView.Occupancy = risk.Occupancy;
                        fireRiskdetailView.OccupancyCode = risk.OccupancyCode;
                        fireRiskdetailView.Currency = risk.Currency;
                        fireRiskdetailView.BuildingNo = risk.BuildingNo;
                        fireRiskdetailView.Street = risk.Street;
                        fireRiskdetailView.WardCode = risk.WardCode;

                        fireRiskdetailView.WardDescription = risk.WardDescription;
                        fireRiskdetailView.TownCode = risk.TownCode;
                        fireRiskdetailView.TownDescription = risk.TownDescription;
                        fireRiskdetailView.TownshipCode = risk.TownshipCode;
                        fireRiskdetailView.TownshipDescription = risk.TownshipDescription;
                        fireRiskdetailView.DistrictCode = risk.DistrictCode;
                        fireRiskdetailView.DistrictDescription = risk.DistrictDescription;
                        fireRiskdetailView.RegionCode = risk.RegionCode;
                        fireRiskdetailView.RegionDescription = risk.RegionDescription;

                        fireRiskdetailView.constructiondetail = _context.FireConstruction.Where(c => c.ProposalReferenceno == fireRuleProposalViewModel.ReferenceNo && c.RiskId == risk.Id).FirstOrDefault();
                        fireRiskdetailView.feainformation = _context.FireFeaInfo.Where(c => c.ProposalReferenceno == fireRuleProposalViewModel.ReferenceNo && c.RiskId == risk.Id).FirstOrDefault();
                        fireRiskdetailView.firewatertank = _context.FireFirewatertank.Where(c => c.ProposalReferenceno == fireRuleProposalViewModel.ReferenceNo && c.RiskId == risk.Id).FirstOrDefault();
                        fireRiskdetailView.firesecurity = _context.FireSecurity.Where(c => c.ProposalReferenceno == fireRuleProposalViewModel.ReferenceNo && c.RiskId == risk.Id).FirstOrDefault();
                        fireRiskdetailView.fireinventory = _context.FireInventoryinfo.Where(c => c.ProposalReferenceno == fireRuleProposalViewModel.ReferenceNo && c.RiskId == risk.Id).ToList();
                        fireRiskdetailView.firecoverage = _context.FireCoverageinfo.Where(c => c.ProposalReferenceno == fireRuleProposalViewModel.ReferenceNo && c.RiskId == risk.Id).FirstOrDefault();


                        fireRuleProposalViewModel.fireRiskdetails.Add(fireRiskdetailView);  

                    }
                    #endregion
                }
                return fireRuleProposalViewModel;
            }
            catch(Exception ex)
            {
                return fireRuleProposalViewModel;
            }
        }

        #endregion

        #region Save Fire Proposal

        public async Task<JsonResult> SaveFireProposal([FromBody]FireProposalViewModel fireProposal)
        {
            try
            {
                session = HttpContext.Session;

                if (!CheckLoginUser())
                    return Json("-1");

                if (fireProposal.fireRiskDetailViews.Count > 0)
                {
                    string refNo = "";
                    if (session.GetString("userId") != null)
                    {
                        fireProposal.createdby = session.GetString("userId").ToString();
                    }
                    if(fireProposal.Id == null)
                    {
                        refNo = await SaveFireRuleProposalAsync(fireProposal);
                    }
                    else
                    {
                        refNo = await EditFireRuleProposalAsync(fireProposal);
                    }
                    
                    return Json(refNo);
                }
                else
                {
                    return Json("-1");
                }
            }
            catch(Exception ex)
            {
                return Json(ex.Message.ToString());
            }
            
        }

        public bool CheckLoginUser()
        {
            if (HttpContext.Session.GetString("userId") == null)
                return false;
            return true;
        }
        private async Task<string> SaveFireRuleProposalAsync(FireProposalViewModel fireProposal)
        {
            String flag = "-1";
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    #region Rule Proposal
                    if (lstAccountCode.Count == 0)
                    {
                        lstAccountCode = await _middlewareAPI.GetAccountCode();
                    }

                    RuleProposal ruleProposal = new RuleProposal();
                    ruleProposal.Id = Guid.NewGuid();
                    ruleProposal.ReferenceNo = GetReferenceNo("rulefire");
                    ruleProposal.Class = fireProposal.Class;
                    ruleProposal.ProductCode = fireProposal.ProductCode;
                    ruleProposal.IssueDate = DateTime.Now;

                    ruleProposal.TransactionType = fireProposal.TransactionType;
                    ruleProposal.CusCode = fireProposal.CusCode;
                    ruleProposal.CusName = fireProposal.CusName;
                    ruleProposal.InsuredMailingAddress = fireProposal.MailingAddress;
                    ruleProposal.IntermediaryCode = fireProposal.IntermediaryCode;
                    ruleProposal.IntermediaryName = fireProposal.IntermediaryName;

                    ruleProposal.AgentCode = fireProposal.AgentCode;
                    ruleProposal.AgentName = fireProposal.AgentName;
                    ruleProposal.AccountCode = fireProposal.AccountCode;
                    ruleProposal.AccountUserCode = lstAccountCode.Where(acc => acc.sfc_code == fireProposal.AccountCode).Select(a => a.sfc_username).FirstOrDefault();
                    ruleProposal.AccountHandlerCode = fireProposal.AccountHandlerCode;

                    ruleProposal.TotalSumInsured = Convert.ToDecimal(fireProposal.TotalSumInsured);
                    ruleProposal.Premium = string.IsNullOrEmpty(fireProposal.Premium) ? 0 : Convert.ToDecimal(fireProposal.Premium);
                    ruleProposal.PolicyStartDate = Convert.ToDateTime(fireProposal.PolicyStartDate);
                    ruleProposal.PolicyEndDate = Convert.ToDateTime(fireProposal.PolicyEndDate);
                    ruleProposal.Uwstatus = "pending";

                    ruleProposal.Version = 1;
                    ruleProposal.createdby = fireProposal.createdby;

                    _context.Entry(ruleProposal).State = EntityState.Added;
                    _context.SaveChanges();

                    #endregion


                    foreach (var riskdetail in fireProposal.fireRiskDetailViews)
                    {

                        if (lstOccupancy.Count == 0)
                        {
                            lstOccupancy = await _middlewareAPI.GetBuildingOccupationData();
                        }

                        #region Fire Risk Detail
                        FireRiskdetail fireRiskdetail = new FireRiskdetail();
                        LocationDetials locationDetials = new LocationDetials();

                        fireRiskdetail.Id = Guid.NewGuid();
                        fireRiskdetail.ProposalId = ruleProposal.Id;
                        fireRiskdetail.ProposalReferenceno = ruleProposal.ReferenceNo;
                        fireRiskdetail.RiskName = riskdetail.RiskName;
                        fireRiskdetail.RiskSumInsured = Convert.ToDecimal(riskdetail.RiskSumInsured);
                        fireRiskdetail.RiskLocation = riskdetail.RiskLocation;

                        fireRiskdetail.Occupancy = riskdetail.Occupancy;
                        fireRiskdetail.OccupancyCode = lstOccupancy.Where(occ => occ.RFT_DESCRIPTION == riskdetail.Occupancy).Select(o => o.RFT_CODE).FirstOrDefault();
                        fireRiskdetail.Currency = riskdetail.Currency;
                        fireRiskdetail.BuildingNo = riskdetail.BuildingNo;
                        fireRiskdetail.Street = riskdetail.Street;
                        fireRiskdetail.WardCode = riskdetail.WardCode;

                        locationDetials = await GetLocationDetailByWardAsync(fireRiskdetail.WardCode);
                        if (locationDetials != null)
                        {
                            fireRiskdetail.WardDescription = locationDetials.ward_description;
                            fireRiskdetail.TownCode = locationDetials.town_code;
                            fireRiskdetail.TownDescription = locationDetials.town_description;
                            fireRiskdetail.TownshipCode = locationDetials.township_code;
                            fireRiskdetail.TownshipDescription = locationDetials.town_description;
                            fireRiskdetail.DistrictCode = locationDetials.district_code;
                            fireRiskdetail.DistrictDescription = locationDetials.district_description;
                            fireRiskdetail.RegionCode = locationDetials.region_code;
                            fireRiskdetail.RegionDescription = locationDetials.region_description;
                        }

                        _context.Entry(fireRiskdetail).State = EntityState.Added;
                        _context.SaveChanges();
                        #endregion

                        #region Fire Construction
                        if (riskdetail.constructiondetail != null)
                        {
                            FireConstruction fireConstruction = new FireConstruction();
                            fireConstruction.Id = Guid.NewGuid();
                            fireConstruction.RiskId = fireRiskdetail.Id;
                            fireConstruction.ProposalReferenceno = ruleProposal.ReferenceNo;
                            fireConstruction.AreaOfBuilding = string.IsNullOrEmpty(riskdetail.constructiondetail.areaOfBuilding) ? 0 : Convert.ToInt32(riskdetail.constructiondetail.areaOfBuilding);
                            fireConstruction.Roof = riskdetail.constructiondetail.roof;

                            fireConstruction.RoofDescription = riskdetail.constructiondetail.roofDescription;
                            fireConstruction.Walls = riskdetail.constructiondetail.walls;
                            fireConstruction.WallDescription = riskdetail.constructiondetail.wallDescription;
                            fireConstruction.Columns = riskdetail.constructiondetail.columns;
                            fireConstruction.ColumnDescription = riskdetail.constructiondetail.columnDescription;

                            fireConstruction.Floor = riskdetail.constructiondetail.floor;
                            fireConstruction.FloorDescription = riskdetail.constructiondetail.floorDescription;
                            fireConstruction.ConstructionClass = riskdetail.constructiondetail.constructionClass;
                            fireConstruction.ConstructionDescription = riskdetail.constructiondetail.constructionDescription;
                            fireConstruction.LifeOfBuilding = string.IsNullOrEmpty(riskdetail.constructiondetail.lifeOfBuilding) ? 0 : Convert.ToInt32(riskdetail.constructiondetail.lifeOfBuilding);

                            fireConstruction.NumberOfStorey = string.IsNullOrEmpty(riskdetail.constructiondetail.numberOfStorey) ? 0 : Convert.ToInt32(riskdetail.constructiondetail.numberOfStorey);
                            fireConstruction.ProposedSI = string.IsNullOrEmpty(riskdetail.constructiondetail.proposedSI) ? 0 : Convert.ToDecimal(riskdetail.constructiondetail.proposedSI);
                            fireConstruction.ProposedPAE = string.IsNullOrEmpty(riskdetail.constructiondetail.proposedPAE) ? 0 : Convert.ToDecimal(riskdetail.constructiondetail.proposedPAE);
                            fireConstruction.LimitedSI = string.IsNullOrEmpty(riskdetail.constructiondetail.limitedSI) ? 0 : Convert.ToDecimal(riskdetail.constructiondetail.limitedSI);
                            fireConstruction.LimitedPAE = string.IsNullOrEmpty(riskdetail.constructiondetail.limitedPAE) ? 0 : Convert.ToDecimal(riskdetail.constructiondetail.limitedPAE);

                            _context.Entry(fireConstruction).State = EntityState.Added;
                            _context.SaveChanges();

                        }

                        #endregion

                        #region Fire FEA Information
                        if (riskdetail.feainformation != null)
                        {
                            FireFeaInfo fireFeaInfo = new FireFeaInfo();
                            fireFeaInfo.Id = Guid.NewGuid();
                            fireFeaInfo.RiskId = fireRiskdetail.Id;
                            fireFeaInfo.ProposalReferenceno = ruleProposal.ReferenceNo;
                            fireFeaInfo.NumberofAbctype = riskdetail.feainformation.numberofAbctype;
                            fireFeaInfo.NumberofAbctypeDesp = riskdetail.feainformation.numberofAbctypeDesp;

                            fireFeaInfo.NumberOfCo2type = riskdetail.feainformation.numberOfCo2type;
                            fireFeaInfo.NumberOfCo2typeDesp = riskdetail.feainformation.numberOfCo2typeDesp;
                            fireFeaInfo.NumberOfOtherExtinguisher = riskdetail.feainformation.numberOfOtherExtinguisher;
                            fireFeaInfo.NumberOfOtherExtinguisherDesp = riskdetail.feainformation.numberOfOtherExtinguisherDesp;
                            fireFeaInfo.WeightOfFireExtinguisher = riskdetail.feainformation.weightOfFireExtinguisher;

                            fireFeaInfo.WeightOfFireExtinguisherDesp = riskdetail.feainformation.weightOfFireExtinguisherDesp;
                            fireFeaInfo.RequiredNumberOfFireExtinguisher = riskdetail.feainformation.requiredNumberOfFireExtinguisher;
                            fireFeaInfo.RequiredNumberOfFireExtinguisherDesp = riskdetail.feainformation.requiredNumberOfFireExtinguisherDesp;
                            fireFeaInfo.NumberOfHoseReel = riskdetail.feainformation.numberOfHoseReel;
                            fireFeaInfo.NumberOfHoseReelDesp = riskdetail.feainformation.numberOfHoseReelDesp;

                            fireFeaInfo.NumberOfHydrant = riskdetail.feainformation.numberOfHydrant;
                            fireFeaInfo.NumberOfHydrantDesp = riskdetail.feainformation.numberOfHydrantDesp;
                            //fireFeaInfo.ProposedNumberOfFireExtinguisher = Convert.ToInt32(fireFeaInfo.NumberofAbctype) + Convert.ToInt32(fireFeaInfo.NumberOfCo2type) + Convert.ToInt32(fireFeaInfo.NumberOfOtherExtinguisher);

                            _context.Entry(fireFeaInfo).State = EntityState.Added;
                            _context.SaveChanges();

                        }
                        #endregion

                        #region Firewatertank
                        if (riskdetail.firewatertank != null)
                        {
                            FireFirewatertank firewatertank = new FireFirewatertank();
                            firewatertank.Id = Guid.NewGuid();
                            firewatertank.RiskId = fireRiskdetail.Id;
                            firewatertank.ProposalReferenceno = ruleProposal.ReferenceNo;
                            firewatertank.NumberofFireTank = riskdetail.firewatertank.numberofFireTank;
                            firewatertank.TypeofFireTank = riskdetail.firewatertank.typeofFireTank;

                            firewatertank.CapacityofFireTank = riskdetail.firewatertank.capacityofFireTank;
                            firewatertank.NumberofFireTankDesp = riskdetail.firewatertank.numberofFireTankDesp;
                            firewatertank.TypeofFireTankDesp = riskdetail.firewatertank.typeofFireTankDesp;

                            _context.Entry(firewatertank).State = EntityState.Added;
                            _context.SaveChanges();

                        }
                        #endregion

                        #region Security Guard 
                        FireSecurity fireSecurity = new FireSecurity();
                        fireSecurity.Id = Guid.NewGuid();
                        fireSecurity.RiskId = fireRiskdetail.Id;
                        fireSecurity.ProposalReferenceno = ruleProposal.ReferenceNo;
                        fireSecurity.NumberofSecurityGuards = riskdetail.firesecurity.numberofSecurityGuards;
                        fireSecurity.NumberofSecurityGuardsDesp = riskdetail.firesecurity.numberofSecurityGuardsDesp;

                        fireSecurity.SecurityGuardsDutyShift = riskdetail.firesecurity.securityGuardsDutyShift;
                        fireSecurity.SecurityGuardsDutyShiftDesp = riskdetail.firesecurity.securityGuardsDutyShiftDesp;
                        fireSecurity.SecurityGuardsDutyPerHour = riskdetail.firesecurity.securityGuardsDutyPerHour;
                        fireSecurity.SecurityGuardsDutyPerHourDesp = riskdetail.firesecurity.securityGuardsDutyPerHourDesp;
                        fireSecurity.NumberofCctvcamera = riskdetail.firesecurity.numberofCctvcamera;

                        fireSecurity.NumberofCCTVCameraDesp = riskdetail.firesecurity.numberofCCTVCameraDesp;
                        fireSecurity.Cctvbackdated = riskdetail.firesecurity.cctvbackdated;
                        fireSecurity.NumberofLabour = riskdetail.firesecurity.numberofLabour;
                        fireSecurity.NearestFireStation = riskdetail.firesecurity.nearestFireStation;
                        fireSecurity.Geocoordinate = riskdetail.firesecurity.geocoordinate;

                        fireSecurity.Losswithin5Years = riskdetail.firesecurity.losswithin5Years;
                        fireSecurity.ReferralPoints = riskdetail.firesecurity.referralPoints;

                        _context.Entry(fireSecurity).State = EntityState.Added;
                        _context.SaveChanges();
                        #endregion

                        #region Inventory
                        foreach (var item in riskdetail.inventory)
                        {
                            if (lstInventoryItemByPCodeViewModels.Count() == 0)
                            {
                                lstInventoryItemByPCodeViewModels = await _middlewareAPI.GetInventoryData("FFI");
                            }

                            InventoryItemByPCodeViewModel inventoryItem = new InventoryItemByPCodeViewModel();
                            var itemDesp = item.itemDescription;
                            inventoryItem = lstInventoryItemByPCodeViewModels.Where(i => i.ITM_DESCRIPTION == itemDesp.Trim()).FirstOrDefault();

                            FireInventoryinfo fireInventoryinfo = new FireInventoryinfo();
                            fireInventoryinfo.Id = Guid.NewGuid();
                            fireInventoryinfo.RiskId = fireRiskdetail.Id;
                            fireInventoryinfo.ProposalReferenceno = ruleProposal.ReferenceNo;
                            fireInventoryinfo.ItemCode = inventoryItem.ITM_CODE;
                            fireInventoryinfo.ItemDescription = item.itemDescription;
                            fireInventoryinfo.DeclaredAmount = string.IsNullOrEmpty(item.declaredAmount) ? 0 : Convert.ToDecimal(item.declaredAmount);
                            _context.Entry(fireInventoryinfo).State = EntityState.Added;
                            _context.SaveChanges();
                        }

                        #endregion

                        #region Coverage
                        FireCoverageinfo fireCoverageinfo = new FireCoverageinfo();
                        fireCoverageinfo.Id = Guid.NewGuid();
                        fireCoverageinfo.RiskId = fireRiskdetail.Id;
                        fireCoverageinfo.ProposalReferenceno = ruleProposal.ReferenceNo;
                        fireCoverageinfo.RiotStrikeAndMaliciousDamage = riskdetail.coverage.riotStrikeAndMaliciousDamage;
                        fireCoverageinfo.AircraftDamage = riskdetail.coverage.aircraftDamage;

                        fireCoverageinfo.ImpactDamage = riskdetail.coverage.impactDamage;
                        fireCoverageinfo.SubsidenceAndLandslide = riskdetail.coverage.subsidenceAndLandslide;
                        fireCoverageinfo.EarthQuakeFireFireAndShockDamage = riskdetail.coverage.earthQuakeFireFireAndShockDamage;
                        fireCoverageinfo.Explosion = riskdetail.coverage.explosion;
                        fireCoverageinfo.SpontaneousCombustion = riskdetail.coverage.spontaneousCombustion;

                        fireCoverageinfo.StormTyphoonHurricaneTempest = riskdetail.coverage.stormTyphoonHurricaneTempest;
                        fireCoverageinfo.Burglary = riskdetail.coverage.burglary;
                        fireCoverageinfo.WarRisk = riskdetail.coverage.warRisk;
                        fireCoverageinfo.FloodAndInundation = riskdetail.coverage.floodAndInundation;
                        fireCoverageinfo.BasicCover = riskdetail.coverage.basicCover;

                        _context.Entry(fireCoverageinfo).State = EntityState.Added;
                        _context.SaveChanges();

                        #endregion
                    }

                    transaction.Commit();

                    return flag = ruleProposal.ReferenceNo;


                }
                catch(Exception ex)
                {
                    transaction.Rollback();
                    return flag = "-1";
                }
                    
            }

            
        }

        private async Task<string> EditFireRuleProposalAsync(FireProposalViewModel fireProposal)
        {
            String flag = "-1";
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    #region Rule Proposal
                    if (lstAccountCode.Count == 0)
                    {
                        lstAccountCode = await _middlewareAPI.GetAccountCode();
                    }

                    RuleProposal ruleProposal = new RuleProposal();
                    ruleProposal = _context.RuleProposal.Where(r => r.Id == Guid.Parse(fireProposal.Id)).FirstOrDefault();

                    ruleProposal.IssueDate = DateTime.Now;
                    ruleProposal.CusCode = fireProposal.CusCode;
                    ruleProposal.CusName = fireProposal.CusName;
                    ruleProposal.InsuredMailingAddress = fireProposal.MailingAddress;
                    ruleProposal.IntermediaryCode = fireProposal.IntermediaryCode;
                    ruleProposal.IntermediaryName = fireProposal.IntermediaryName;

                    ruleProposal.AgentCode = fireProposal.AgentCode;
                    ruleProposal.AgentName = fireProposal.AgentName;
                    ruleProposal.AccountCode = fireProposal.AccountCode;
                    ruleProposal.AccountUserCode = lstAccountCode.Where(acc => acc.sfc_code == fireProposal.AccountCode).Select(a => a.sfc_username).FirstOrDefault();
                    ruleProposal.AccountHandlerCode = fireProposal.AccountHandlerCode;

                    ruleProposal.TotalSumInsured = Convert.ToDecimal(fireProposal.TotalSumInsured);
                    ruleProposal.Premium = string.IsNullOrEmpty(fireProposal.Premium) ? 0 : Convert.ToDecimal(fireProposal.Premium);
                    ruleProposal.PolicyStartDate = Convert.ToDateTime(fireProposal.PolicyStartDate);
                    ruleProposal.PolicyEndDate = Convert.ToDateTime(fireProposal.PolicyEndDate);
                    ruleProposal.Uwstatus = "pending";

                    ruleProposal.Version = 1;
                    ruleProposal.createdby = fireProposal.createdby;

                    _context.Entry(ruleProposal).State = EntityState.Modified;
                    _context.SaveChanges();

                    #endregion


                    foreach (var riskdetail in fireProposal.fireRiskDetailViews)
                    {
                        if (lstOccupancy.Count == 0)
                        {
                            lstOccupancy = await _middlewareAPI.GetBuildingOccupationData();
                        }

                        #region Fire Risk Detail
                        FireRiskdetail fireRiskdetail = new FireRiskdetail();
                        LocationDetials locationDetials = new LocationDetials();
                        fireRiskdetail = _context.FireRiskdetail.Where(x => x.Id == Guid.Parse(riskdetail.Id)).FirstOrDefault();
                        
                        fireRiskdetail.ProposalReferenceno = ruleProposal.ReferenceNo;
                        fireRiskdetail.RiskName = riskdetail.RiskName;
                        fireRiskdetail.RiskSumInsured = Convert.ToDecimal(riskdetail.RiskSumInsured);
                        fireRiskdetail.RiskLocation = riskdetail.RiskLocation;

                        fireRiskdetail.Occupancy = riskdetail.Occupancy;
                        fireRiskdetail.OccupancyCode = lstOccupancy.Where(occ => occ.RFT_DESCRIPTION == riskdetail.Occupancy).Select(o => o.RFT_CODE).FirstOrDefault();
                        fireRiskdetail.Currency = riskdetail.Currency;
                        fireRiskdetail.BuildingNo = riskdetail.BuildingNo;
                        fireRiskdetail.Street = riskdetail.Street;
                        fireRiskdetail.WardCode = riskdetail.WardCode;

                        locationDetials = await GetLocationDetailByWardAsync(fireRiskdetail.WardCode);
                        if (locationDetials != null)
                        {
                            fireRiskdetail.WardDescription = locationDetials.ward_description;
                            fireRiskdetail.TownCode = locationDetials.town_code;
                            fireRiskdetail.TownDescription = locationDetials.town_description;
                            fireRiskdetail.TownshipCode = locationDetials.township_code;
                            fireRiskdetail.TownshipDescription = locationDetials.town_description;
                            fireRiskdetail.DistrictCode = locationDetials.district_code;
                            fireRiskdetail.DistrictDescription = locationDetials.district_description;
                            fireRiskdetail.RegionCode = locationDetials.region_code;
                            fireRiskdetail.RegionDescription = locationDetials.region_description;
                        }

                        _context.Entry(fireRiskdetail).State = EntityState.Modified;
                        _context.SaveChanges();
                        #endregion

                        #region Fire Construction
                        if (riskdetail.constructiondetail != null)
                        {
                            FireConstruction fireConstruction = new FireConstruction();
                            fireConstruction = _context.FireConstruction.Where(c => c.Id == Guid.Parse(riskdetail.constructiondetail.Id)).FirstOrDefault();

                            fireConstruction.RiskId = fireRiskdetail.Id;
                            fireConstruction.ProposalReferenceno = ruleProposal.ReferenceNo;
                            fireConstruction.AreaOfBuilding = string.IsNullOrEmpty(riskdetail.constructiondetail.areaOfBuilding) ? 0 : Convert.ToInt32(riskdetail.constructiondetail.areaOfBuilding);
                            fireConstruction.Roof = riskdetail.constructiondetail.roof;

                            fireConstruction.RoofDescription = riskdetail.constructiondetail.roofDescription;
                            fireConstruction.Walls = riskdetail.constructiondetail.walls;
                            fireConstruction.WallDescription = riskdetail.constructiondetail.wallDescription;
                            fireConstruction.Columns = riskdetail.constructiondetail.columns;
                            fireConstruction.ColumnDescription = riskdetail.constructiondetail.columnDescription;

                            fireConstruction.Floor = riskdetail.constructiondetail.floor;
                            fireConstruction.FloorDescription = riskdetail.constructiondetail.floorDescription;
                            fireConstruction.ConstructionClass = riskdetail.constructiondetail.constructionClass;
                            fireConstruction.ConstructionDescription = riskdetail.constructiondetail.constructionDescription;
                            fireConstruction.LifeOfBuilding = string.IsNullOrEmpty(riskdetail.constructiondetail.lifeOfBuilding) ? 0 : Convert.ToInt32(riskdetail.constructiondetail.lifeOfBuilding);

                            fireConstruction.NumberOfStorey = string.IsNullOrEmpty(riskdetail.constructiondetail.numberOfStorey) ? 0 : Convert.ToInt32(riskdetail.constructiondetail.numberOfStorey);
                            fireConstruction.ProposedSI = string.IsNullOrEmpty(riskdetail.constructiondetail.proposedSI) ? 0 : Convert.ToDecimal(riskdetail.constructiondetail.proposedSI);
                            fireConstruction.ProposedPAE = string.IsNullOrEmpty(riskdetail.constructiondetail.proposedPAE) ? 0 : Convert.ToDecimal(riskdetail.constructiondetail.proposedPAE);
                            fireConstruction.LimitedSI = string.IsNullOrEmpty(riskdetail.constructiondetail.limitedSI) ? 0 : Convert.ToDecimal(riskdetail.constructiondetail.limitedSI);
                            fireConstruction.LimitedPAE = string.IsNullOrEmpty(riskdetail.constructiondetail.limitedPAE) ? 0 : Convert.ToDecimal(riskdetail.constructiondetail.limitedPAE);

                            _context.Entry(fireConstruction).State = EntityState.Modified;
                            _context.SaveChanges();

                        }

                        #endregion

                        #region Fire FEA Information
                        if (riskdetail.feainformation != null)
                        {
                            FireFeaInfo fireFeaInfo = new FireFeaInfo();
                            fireFeaInfo = _context.FireFeaInfo.Where(fea => fea.Id == Guid.Parse(riskdetail.feainformation.Id)).FirstOrDefault();
                            
                            fireFeaInfo.RiskId = fireRiskdetail.Id;
                            fireFeaInfo.ProposalReferenceno = ruleProposal.ReferenceNo;
                            fireFeaInfo.NumberofAbctype = riskdetail.feainformation.numberofAbctype;
                            fireFeaInfo.NumberofAbctypeDesp = riskdetail.feainformation.numberofAbctypeDesp;

                            fireFeaInfo.NumberOfCo2type = riskdetail.feainformation.numberOfCo2type;
                            fireFeaInfo.NumberOfCo2typeDesp = riskdetail.feainformation.numberOfCo2typeDesp;
                            fireFeaInfo.NumberOfOtherExtinguisher = riskdetail.feainformation.numberOfOtherExtinguisher;
                            fireFeaInfo.NumberOfOtherExtinguisherDesp = riskdetail.feainformation.numberOfOtherExtinguisherDesp;
                            fireFeaInfo.WeightOfFireExtinguisher = riskdetail.feainformation.weightOfFireExtinguisher;

                            fireFeaInfo.WeightOfFireExtinguisherDesp = riskdetail.feainformation.weightOfFireExtinguisherDesp;
                            fireFeaInfo.RequiredNumberOfFireExtinguisher = riskdetail.feainformation.requiredNumberOfFireExtinguisher;
                            fireFeaInfo.RequiredNumberOfFireExtinguisherDesp = riskdetail.feainformation.requiredNumberOfFireExtinguisherDesp;
                            fireFeaInfo.NumberOfHoseReel = riskdetail.feainformation.numberOfHoseReel;
                            fireFeaInfo.NumberOfHoseReelDesp = riskdetail.feainformation.numberOfHoseReelDesp;

                            fireFeaInfo.NumberOfHydrant = riskdetail.feainformation.numberOfHydrant;
                            fireFeaInfo.NumberOfHydrantDesp = riskdetail.feainformation.numberOfHydrantDesp;
                            //fireFeaInfo.ProposedNumberOfFireExtinguisher = Convert.ToInt32(fireFeaInfo.NumberofAbctype) + Convert.ToInt32(fireFeaInfo.NumberOfCo2type) + Convert.ToInt32(fireFeaInfo.NumberOfOtherExtinguisher);

                            _context.Entry(fireFeaInfo).State = EntityState.Modified;
                            _context.SaveChanges();

                        }
                        #endregion

                        #region Firewatertank
                        if (riskdetail.firewatertank != null)
                        {
                            FireFirewatertank firewatertank = new FireFirewatertank();
                            firewatertank = _context.FireFirewatertank.Where(fw => fw.Id == Guid.Parse(riskdetail.firewatertank.Id)).FirstOrDefault();
                            
                            firewatertank.RiskId = fireRiskdetail.Id;
                            firewatertank.ProposalReferenceno = ruleProposal.ReferenceNo;
                            firewatertank.NumberofFireTank = riskdetail.firewatertank.numberofFireTank;
                            firewatertank.TypeofFireTank = riskdetail.firewatertank.typeofFireTank;

                            firewatertank.CapacityofFireTank = riskdetail.firewatertank.capacityofFireTank;
                            firewatertank.NumberofFireTankDesp = riskdetail.firewatertank.numberofFireTankDesp;
                            firewatertank.TypeofFireTankDesp = riskdetail.firewatertank.typeofFireTankDesp;

                            _context.Entry(firewatertank).State = EntityState.Modified;
                            _context.SaveChanges();

                        }
                        #endregion

                        #region Security Guard 
                        FireSecurity fireSecurity = new FireSecurity();
                        fireSecurity = _context.FireSecurity.Where(s => s.Id == Guid.Parse(riskdetail.firesecurity.Id)).FirstOrDefault();

                        fireSecurity.RiskId = fireRiskdetail.Id;
                        fireSecurity.ProposalReferenceno = ruleProposal.ReferenceNo;
                        fireSecurity.NumberofSecurityGuards = riskdetail.firesecurity.numberofSecurityGuards;
                        fireSecurity.NumberofSecurityGuardsDesp = riskdetail.firesecurity.numberofSecurityGuardsDesp;

                        fireSecurity.SecurityGuardsDutyShift = riskdetail.firesecurity.securityGuardsDutyShift;
                        fireSecurity.SecurityGuardsDutyShiftDesp = riskdetail.firesecurity.securityGuardsDutyShiftDesp;
                        fireSecurity.SecurityGuardsDutyPerHour = riskdetail.firesecurity.securityGuardsDutyPerHour;
                        fireSecurity.SecurityGuardsDutyPerHourDesp = riskdetail.firesecurity.securityGuardsDutyPerHourDesp;
                        fireSecurity.NumberofCctvcamera = riskdetail.firesecurity.numberofCctvcamera;

                        fireSecurity.NumberofCCTVCameraDesp = riskdetail.firesecurity.numberofCCTVCameraDesp;
                        fireSecurity.Cctvbackdated = riskdetail.firesecurity.cctvbackdated;
                        fireSecurity.NumberofLabour = riskdetail.firesecurity.numberofLabour;
                        fireSecurity.NearestFireStation = riskdetail.firesecurity.nearestFireStation;
                        fireSecurity.Geocoordinate = riskdetail.firesecurity.geocoordinate;

                        fireSecurity.Losswithin5Years = riskdetail.firesecurity.losswithin5Years;
                        fireSecurity.ReferralPoints = riskdetail.firesecurity.referralPoints;

                        _context.Entry(fireSecurity).State = EntityState.Modified;
                        _context.SaveChanges();
                        #endregion

                        #region Inventory
                        foreach (var item in riskdetail.inventory)
                        {
                            if (lstInventoryItemByPCodeViewModels.Count() == 0)
                            {
                                lstInventoryItemByPCodeViewModels = await _middlewareAPI.GetInventoryData(ruleProposal.ProductCode);
                            }

                            InventoryItemByPCodeViewModel inventoryItem = new InventoryItemByPCodeViewModel();
                            
                            var itemDesp = item.itemDescription;
                            inventoryItem = lstInventoryItemByPCodeViewModels.Where(i => i.ITM_DESCRIPTION == itemDesp.Trim()).FirstOrDefault();

                            FireInventoryinfo fireInventoryinfo = new FireInventoryinfo();
                            fireInventoryinfo = _context.FireInventoryinfo.Where(inv => inv.Id == Guid.Parse(item.Id)).FirstOrDefault();

                            fireInventoryinfo.RiskId = fireRiskdetail.Id;
                            fireInventoryinfo.ProposalReferenceno = ruleProposal.ReferenceNo;
                            fireInventoryinfo.ItemCode = inventoryItem.ITM_CODE;
                            fireInventoryinfo.ItemDescription = item.itemDescription;
                            fireInventoryinfo.DeclaredAmount = string.IsNullOrEmpty(item.declaredAmount) ? 0 : Convert.ToDecimal(item.declaredAmount);
                            _context.Entry(fireInventoryinfo).State = EntityState.Modified;
                            _context.SaveChanges();
                        }

                        #endregion

                        #region Coverage
                        FireCoverageinfo fireCoverageinfo = new FireCoverageinfo();
                        fireCoverageinfo = _context.FireCoverageinfo.Where(c => c.Id == Guid.Parse(riskdetail.coverage.Id)).FirstOrDefault();

                        fireCoverageinfo.RiskId = fireRiskdetail.Id;
                        fireCoverageinfo.ProposalReferenceno = ruleProposal.ReferenceNo;
                        fireCoverageinfo.RiotStrikeAndMaliciousDamage = riskdetail.coverage.riotStrikeAndMaliciousDamage;
                        fireCoverageinfo.AircraftDamage = riskdetail.coverage.aircraftDamage;

                        fireCoverageinfo.ImpactDamage = riskdetail.coverage.impactDamage;
                        fireCoverageinfo.SubsidenceAndLandslide = riskdetail.coverage.subsidenceAndLandslide;
                        fireCoverageinfo.EarthQuakeFireFireAndShockDamage = riskdetail.coverage.earthQuakeFireFireAndShockDamage;
                        fireCoverageinfo.Explosion = riskdetail.coverage.explosion;
                        fireCoverageinfo.SpontaneousCombustion = riskdetail.coverage.spontaneousCombustion;

                        fireCoverageinfo.StormTyphoonHurricaneTempest = riskdetail.coverage.stormTyphoonHurricaneTempest;
                        fireCoverageinfo.Burglary = riskdetail.coverage.burglary;
                        fireCoverageinfo.WarRisk = riskdetail.coverage.warRisk;
                        fireCoverageinfo.FloodAndInundation = riskdetail.coverage.floodAndInundation;
                        fireCoverageinfo.BasicCover = riskdetail.coverage.basicCover;

                        _context.Entry(fireCoverageinfo).State = EntityState.Modified;
                        _context.SaveChanges();

                        #endregion
                    }

                    transaction.Commit();

                    return flag = ruleProposal.ReferenceNo;


                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return flag = "-1";
                }

            }


        }

        private string GetReferenceNo(string param)
        {
            AutoGenerate autogen = new AutoGenerate();
            string invCode = "";
            if (param == "rulefire")
            {
                autogen = _context.AutoGenerate.Where(x => x.Name == param).FirstOrDefault();
            }
            else
            {
                autogen = _context.AutoGenerate.Where(x => x.Name == param).FirstOrDefault();
            }

            invCode = autogen.Prefix;

            string serialNo = "";

            int invCodeLen = invCode.Length;
            int max = Convert.ToInt32(autogen.Maxcount);
            int cLen = autogen.Count.ToString().Length;

            int len = max - invCodeLen - cLen;
            for (int i = 0; i < len; i++)
            {
                serialNo += "0";
            }
            int count = Convert.ToInt32(autogen.Count + 1);

            invCode = invCode + serialNo + count;

            autogen.Count = count;

            _context.Entry(autogen).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            _context.SaveChanges();
            return invCode;
        }
        #endregion

        #region Risk Location Upload

        public async Task<ActionResult> Upload(RiskLocationViewModel riskLocationViewModel)
        {
            string fileName = null;

            foreach (IFormFile source in riskLocationViewModel.Files)
            {
                string uploadpath = _env.WebRootPath;
                string filefolder = DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString();
                string dest_path = Path.Combine(uploadpath, "ImportedFiles/" + filefolder + "/");

                if (!Directory.Exists(dest_path))
                {
                    Directory.CreateDirectory(dest_path);
                }

                string sourcefile = Path.GetFileName(source.FileName);
                string path = Path.Combine(dest_path, sourcefile);

                using (FileStream filestream = new FileStream(path, FileMode.Create))
                {
                    source.CopyTo(filestream);
                }

                string extension = Path.GetExtension(path);

                string conString = string.Empty;

                DataTable dataTable = new DataTable();

                dataTable = LocationDataTable(path, extension);

                DeleteLocalFile(dest_path);
                if (dataTable.Rows.Count > 0)
                {
                    List<RiskLocation> lstRiskLocation = new List<RiskLocation>();
                    lstRiskLocation = await ImportLocation(dataTable);

                    ViewBag.lstRiskLocation = lstRiskLocation;
                    return View("RiskLocationUpload");
                }
            }

            return Ok("-1");
        }

        public IActionResult RiskLocationUpload()
        {
            return View();
        }
        public async Task<List<RiskLocation>> ImportLocation(DataTable location)
        {
            int count = 0;
            List<RiskLocation> riskLocations = new List<RiskLocation>();
            try
            {
                if (location.Rows.Count > 0)
                {
                    foreach (DataRow row in location.Rows)
                    {
                        //if (location.Rows.IndexOf(row) != 0)
                        //{
                        //}

                        RiskLocation riskLocation = new RiskLocation();
                        riskLocation.Id = Guid.NewGuid();
                        riskLocation.WardCode = row["WardCode"].ToString();

                        WardCodeLocation wardCodeLocation = new WardCodeLocation();
                        wardCodeLocation = await _middlewareAPI.GetLocationByWardCodeAsync(riskLocation.WardCode);
                        if (wardCodeLocation != null)
                        {
                            riskLocation.RegionCode = wardCodeLocation.region_code;
                            riskLocation.RegionDescription = wardCodeLocation.region_description;
                            riskLocation.DistrictCode = wardCodeLocation.district_code;
                            riskLocation.DistrictDescription = wardCodeLocation.district_description;
                            riskLocation.TownshipCode = wardCodeLocation.township_code;
                            riskLocation.TownshipDescription = wardCodeLocation.township_description;
                            riskLocation.TownCode = wardCodeLocation.town_code;
                            riskLocation.TownDescription = wardCodeLocation.town_description;
                            riskLocation.WardCode = wardCodeLocation.ward_code;
                            riskLocation.WardDescription = wardCodeLocation.ward_description;

                            riskLocation.BuildingNo = row["Building No"].ToString();
                            riskLocation.Street = row["Street"].ToString();
                            riskLocation.Currency = row["Currency"].ToString();
                            riskLocation.RiskName = row["RiskName"].ToString();
                            riskLocation.RiskSumInsured = Convert.ToDecimal(row["Risk Sum Insured"]);
                            riskLocation.Occupancy = row["Occupancy"].ToString();
                            riskLocation.UploadedDate = DateTime.Now.Date;
                            riskLocation.LocationDetail = riskLocation.BuildingNo + "," +
                                                          riskLocation.Street + "," +
                                                          riskLocation.WardDescription + "," +
                                                          riskLocation.TownDescription + "," +
                                                          riskLocation.TownshipDescription + "," +
                                                          riskLocation.DistrictDescription + "," +
                                                          riskLocation.RegionDescription;

                            riskLocation.BuildingInventory = row["Inventory"].ToString();



                            riskLocations.Add(riskLocation);
                            _context.Entry(riskLocation).State = EntityState.Added;
                            _context.SaveChanges();

                        }
                        else
                        {
                            return riskLocations = new List<RiskLocation>();
                        }
                    }
                }

                HttpContext.Session.SetObjectAsJson("lstRiskLocations", riskLocations);
                return riskLocations;
            }
            catch (Exception ex)
            {
                return riskLocations;
            }
        }

        public DataTable LocationDataTable(string path, string extension)
        {
            DataTable datatable = new DataTable();

            try
            {
                string constr = "";
                switch (extension)
                {
                    case ".xls": //Excel 97-03.
                        constr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties='Excel 8.0;HDR=YES'";
                        break;
                    case ".xlsx": //Excel 07 and above.
                        constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties='Excel 8.0;HDR=YES'";
                        break;
                }


                constr = string.Format(constr, path);

                using (OleDbConnection connExcel = new OleDbConnection(constr))
                {
                    using (OleDbCommand cmdExcel = new OleDbCommand())
                    {
                        using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                        {
                            cmdExcel.Connection = connExcel;

                            //Get the name of First Sheet.
                            connExcel.Open();
                            DataTable dtExcelSchema;
                            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                            connExcel.Close();

                            //Read Data from First Sheet.
                            connExcel.Open();
                            cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                            odaExcel.SelectCommand = cmdExcel;
                            odaExcel.Fill(datatable);
                            connExcel.Close();

                        }
                    }
                }


            }
            catch (Exception ex)
            {

            }

            return datatable;
        }

        private void DeleteLocalFile(string path)
        {
            try
            {
                System.IO.DirectoryInfo di = new DirectoryInfo(path);

                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
            }
            catch (Exception ex)
            {

            }

        }

        #endregion

        #region Rule Setup
        public IActionResult FireRuleSetup()
        {
            DefaultPolicyAnswerResponse defaultPolicyAnswerResponse = new DefaultPolicyAnswerResponse();
            DefaultPolicyAnswerRequest request = new DefaultPolicyAnswerRequest();
            request.customerCode = "C00051224";
            request.productCode = "FFI";
            request.type = "policy";

            defaultPolicyAnswerResponse = IILAPIservices.GetDefaultPolicyAnswerObj(request);
            if (defaultPolicyAnswerResponse != null)
            {
                lstQuestionRule = GetListQuestionRule(defaultPolicyAnswerResponse);
                HttpContext.Session.SetObjectAsJson("lstQuestionRule", lstQuestionRule);
                if (lstQuestionRule != null)
                {
                    PrepareData();
                    ViewBag.RuleQuestion1 = new SelectList(lstQuestionRule, "questionid", "question");
                }
            }

            List<Rules> lstRules = new List<Rules>();
            lstRules = _context.Rules.ToList();
            ViewBag.lstRules = lstRules;

            return View();
        }
        public IActionResult DeleteRule(string Id)
        {
            try
            {
                Rules rule = new Rules();
                if(Id != null)
                {
                    rule = _context.Rules.Where(r => r.Id == Guid.Parse(Id)).FirstOrDefault();
                    if(rule != null)
                    {
                        _context.Entry(rule).State = EntityState.Deleted;
                        _context.SaveChanges();
                    }
                    else
                    {
                        TempData["Message"] = "There is no rule for delete.";
                    }
                }
                return RedirectToAction("FireRuleSetup");
            }
            catch(Exception ex) {
                return RedirectToAction("FireRuleSetup");
            }
        }
        
        public IActionResult SaveFireRule(RuleViewModel ruleViewModel)
        {
            string product = "FFI";
            try
            {
                if (HttpContext.Session.GetObjectFromJson<List<RiskLocation>>("lstQuestionRule") != null)
                {
                    List<QuestionRule> lstQuestionRule = new List<QuestionRule>();
                    lstQuestionRule = HttpContext.Session.GetObjectFromJson<List<QuestionRule>>("lstQuestionRule");

                    Rules rules = new Rules();
                    rules.Id = Guid.NewGuid();
                    rules.Product = product;
                    rules.Preference1 = ruleViewModel.QuestionId1;
                    rules.Preference1_Name = lstQuestionRule.Where(q => q.questionid == ruleViewModel.QuestionId1).Select(x => x.question).FirstOrDefault();
                    rules.Operator1 = ruleViewModel.Operator1;
                    if(ruleViewModel.Value1 == "Active")
                    {
                        rules.Value1 = "Y";
                    }
                    
                    rules.Preference2 = ruleViewModel.QuestionId2;
                    rules.Preference2_Name = lstQuestionRule.Where(q => q.questionid == ruleViewModel.QuestionId2).Select(x => x.question).FirstOrDefault();
                    rules.Operator2 = ruleViewModel.Operator2;
                    rules.Value2 = ruleViewModel.Value2;

                    rules.PriorityLevel = ruleViewModel.Priority;
                    rules.Action = ruleViewModel.Action;

                    _context.Entry(rules).State = EntityState.Added;
                    _context.SaveChanges();
                }

                return RedirectToAction("FireRuleSetup");
            }
            catch (Exception ex)
            {
                return RedirectToAction("FireRuleSetup");
            }

        }
        #endregion

        #region Common Functions
        private List<QuestionRule> GetListQuestionRule(DefaultPolicyAnswerResponse defaultPolicyAnswerResponse)
        {

            try
            {
                var risklist = (from x in defaultPolicyAnswerResponse.riskDetailDtos[0].riskDetails
                                select x).ToList();

                var policyAnswerDtos = (from p in defaultPolicyAnswerResponse.policyAnswerDtos
                                        select p).ToList();

                var locationDetails = (from x in defaultPolicyAnswerResponse.riskDetailDtos[0].locationDetails
                                       select x).ToList();

                var coverDetails = (from x in defaultPolicyAnswerResponse.riskDetailDtos[0].coverDetails
                                    select x).ToList();

                foreach (var risk in risklist)
                {
                    QuestionRule questionRule = new QuestionRule();
                    questionRule.questionid = risk.qid;
                    questionRule.question = risk.question;

                    lstQuestionRule.Add(questionRule);
                }

                foreach (var policy in policyAnswerDtos)
                {
                    QuestionRule questionRule = new QuestionRule();
                    questionRule.questionid = policy.qid;
                    questionRule.question = policy.question;

                    lstQuestionRule.Add(questionRule);
                }

                foreach (var location in locationDetails)
                {
                    QuestionRule questionRule = new QuestionRule();
                    questionRule.questionid = location.qid;
                    questionRule.question = location.question;

                    lstQuestionRule.Add(questionRule);
                }

                foreach (var cover in coverDetails)
                {
                    QuestionRule questionRule = new QuestionRule();
                    questionRule.questionid = cover.qid;
                    questionRule.question = cover.question;

                    lstQuestionRule.Add(questionRule);
                }

                QuestionRule questionTranTypeRule = new QuestionRule();
                questionTranTypeRule.questionid = "transactiontype";
                questionTranTypeRule.question = "Transaction Type";

                lstQuestionRule.Add(questionTranTypeRule);

                QuestionRule questionTranTypeRule2 = new QuestionRule();
                questionTranTypeRule2.questionid = "productcode";
                questionTranTypeRule2.question = "Product Code";

                lstQuestionRule.Add(questionTranTypeRule2);

                return lstQuestionRule;
            }
            catch (Exception ex)
            {
                return lstQuestionRule;
            }
        }

        public async Task PrepareDataMasterDataAsync()
        {
            try
            {
                lstOccupancy = await _middlewareAPI.GetBuildingOccupationData();
                lstRoof = await _middlewareAPI.GetRoofData();
                lstWalls = await _middlewareAPI.GetWallData();
                lstColumns = await _middlewareAPI.GetColumnData();
                lstFloor = await _middlewareAPI.GetFloorData();

                lstABCType = await _middlewareAPI.GetABCTypeData();
                lstCO2Type = await _middlewareAPI.GetCO2TypeData();
                lstExtinguisher = await _middlewareAPI.GetExtinguisherData();
                lstWeightOfFire = await _middlewareAPI.GetWeightOfFireData();
                lstRequiredNumberOfExtinguisher = await _middlewareAPI.GetRequiredNumberOfExtinguisherData();

                lstHoseReel = await _middlewareAPI.GetHoseReelData();
                lstHydrant = await _middlewareAPI.GetHydrantData();
                lstTypeFireTank = await _middlewareAPI.GetTypeFireTankData();
                lstNumberOfFireTank = await _middlewareAPI.GetNumberFireTankData();
                lstSecurityGuardsData = await _middlewareAPI.GetSecurityGuardsData();

                lstSecurityGuardsDutyShiftData = await _middlewareAPI.GetSecurityGuardsDutyShiftData();
                lstSecurityGuardsDutyHourData = await _middlewareAPI.GetSecurityGuardsDutyHourData();
                lstNumberOfCCTVData = await _middlewareAPI.GetCCTVCameraData();
                lstProductClassViewModels = await _middlewareAPI.GetProductClassData("FI");
                lstAgentCode = await _middlewareAPI.GetAgentCode();

                lstAccountCode = await _middlewareAPI.GetAccountCode();
                lstIntemediaryViewModels = await _middlewareAPI.GetIntemediary();
                lstInventoryItemByPCodeViewModels = await _middlewareAPI.GetInventoryData("FFI");
                lstClassOfBuilding = await _middlewareAPI.GetBuildingClassData();


                if (lstOccupancy.Count() > 0)
                {
                    ViewBag.OccupancyList = new SelectList(lstOccupancy, "RFT_DESCRIPTION", "RFT_DESCRIPTION");
                    ViewBag.RoofList = new SelectList(lstRoof, "RFT_CODE", "RFT_DESCRIPTION");
                    ViewBag.WallsList = new SelectList(lstWalls, "RFT_CODE", "RFT_DESCRIPTION");
                    ViewBag.ColumnsList = new SelectList(lstColumns, "RFT_CODE", "RFT_DESCRIPTION");
                    ViewBag.FloorList = new SelectList(lstFloor, "RFT_CODE", "RFT_DESCRIPTION");

                    ViewBag.ABCTypeList = new SelectList(lstABCType, "RFT_CODE", "RFT_DESCRIPTION");
                    ViewBag.CO2TypeList = new SelectList(lstCO2Type, "RFT_CODE", "RFT_DESCRIPTION");
                    ViewBag.ExtinguisherList = new SelectList(lstExtinguisher, "RFT_CODE", "RFT_DESCRIPTION");
                    ViewBag.WeightOfFireList = new SelectList(lstWeightOfFire, "RFT_CODE", "RFT_DESCRIPTION");
                    ViewBag.RequiredNumberOfExtinguisherList = new SelectList(lstRequiredNumberOfExtinguisher, "RFT_CODE", "RFT_DESCRIPTION");

                    ViewBag.HydrantList = new SelectList(lstHydrant, "RFT_CODE", "RFT_DESCRIPTION");
                    ViewBag.HoseReelList = new SelectList(lstHoseReel, "RFT_CODE", "RFT_DESCRIPTION");
                    ViewBag.TypeFireTankList = new SelectList(lstTypeFireTank, "RFT_CODE", "RFT_DESCRIPTION");
                    ViewBag.NumberOfFireTankList = new SelectList(lstNumberOfFireTank, "RFT_CODE", "RFT_DESCRIPTION");
                    ViewBag.SecurityGuardsDataList = new SelectList(lstSecurityGuardsData, "RFT_CODE", "RFT_DESCRIPTION");

                    ViewBag.SecurityGuardsDutyShiftDataList = new SelectList(lstSecurityGuardsDutyShiftData, "RFT_CODE", "RFT_DESCRIPTION");
                    ViewBag.SecurityGuardsDutyHourDataList = new SelectList(lstSecurityGuardsDutyHourData, "RFT_CODE", "RFT_DESCRIPTION");
                    ViewBag.NumberOfCCTVDataList = new SelectList(lstNumberOfCCTVData, "RFT_CODE", "RFT_DESCRIPTION");
                    ViewBag.ProductClass = new SelectList(lstProductClassViewModels, "PRD_CODE", "PRD_DESCRIPTION");
                    ViewBag.AgentCode = new SelectList(lstAgentCode, "sfc_code", "sfc_surname");

                    ViewBag.AccountCode = new SelectList(lstAccountCode, "sfc_code", "sfc_acc_code");
                    ViewBag.Intermediary = new SelectList(lstIntemediaryViewModels, "BSS_BSS_CODE", "BSS_BSS_DESC");
                    ViewBag.Inventory = new SelectList(lstInventoryItemByPCodeViewModels, "ITM_DESCRIPTION", "ITM_DESCRIPTION");
                    ViewBag.ClassOfBuilding = new SelectList(lstClassOfBuilding, "RFT_CODE", "RFT_DESCRIPTION");

                }
            }
            catch (Exception ex)
            {

            }
        }

        public void PrepareData()
        {
            var ccode = from Operator e in Enum.GetValues(typeof(Operator))
                        select new { Id = e, Operator = e.ToString() };

            ViewBag.OperatorList = new SelectList(ccode, "Operator", "Operator");

            var priority = from PriorityLevel e in Enum.GetValues(typeof(PriorityLevel))
                           select new { Id = e, PriorityLevel = e.ToString() };

            ViewBag.PriorityList = new SelectList(priority, "PriorityLevel", "PriorityLevel");

            var trantype = from TransactionType e in Enum.GetValues(typeof(TransactionType))
                           select new { Id = e, TransactionType = e.ToString() };

            ViewBag.TransactionType = new SelectList(trantype, "TransactionType", "TransactionType");

            List<RuleAction> lstRuleActions = new List<RuleAction>();
            lstRuleActions = _context.RuleAction.ToList();

            ViewBag.lstRuleActions = new SelectList(lstRuleActions, "ActionName", "ActionName");

            List<UwPerson> lstUWPerson = new List<UwPerson>();
            lstUWPerson = _context.UwPerson.ToList();
            ViewBag.lstUWPerson = new SelectList(lstUWPerson, "Id", "PersonName");

        }

        public JsonResult GetRegionList()
        {
            try
            {
                List<Region> lstRegion = new List<Region>();
                lstRegion = _context.Region.OrderBy(xx => xx.Name).ToList();
                // return Json(new SelectList(TownshipList, "Id","Description"));
                return Json(lstRegion);
            }
            catch (Exception ex)
            {
                return Json("-1");
            }

        }

        public async Task<JsonResult> GetCustomerInfo([FromBody] string cusCode)
        {
            LocationDetials locationDetials = new LocationDetials();
            CustomerInfoViewModel viewModel = new CustomerInfoViewModel();
            CustomerDataInfo customerDataInfo = new CustomerDataInfo();
            try
            {
                if(cusCode != null)
                {
                    viewModel = await IILAPIservices.GetCustomerInfoAsync(cusCode);
                    if(viewModel.CUSTOMERADDRESSES.Count() > 0)
                    {
                        CUSTOMERADDRESS cusAddress = new CUSTOMERADDRESS();
                        cusAddress = viewModel.CUSTOMERADDRESSES.Where(add => add.ADDEFAULTADDRESS == "Y").FirstOrDefault();
                        string wardCode = cusAddress.ADPOSTALCODE;
                        if (wardCode != null)
                        {
                            locationDetials = await IILAPIservices.GetCustomerAddressDetailAsync(wardCode);
                            if (locationDetials != null)
                            {
                                customerDataInfo.name = viewModel.SURNAME;
                                customerDataInfo.mailingaddress = cusAddress.ADLOCATIONDESCRIPTION +"," +
                                                                  locationDetials.town_description + "," + locationDetials.township_description + "," + locationDetials.district_description + "," + locationDetials.region_description;

                                return Json(customerDataInfo);
                            }
                        }
                        
                    }
                }
                return Json("-1");
            }
            catch (Exception ex)
            {
                return Json("-1");
            }

        }

        public async Task<LocationDetials> GetLocationDetailByWardAsync(string wardCode)
        {
            LocationDetials locationDetials = new LocationDetials();
            try
            {
                locationDetials = await IILAPIservices.GetCustomerAddressDetailAsync(wardCode);
                if(locationDetials != null)
                {
                    return locationDetials;
                }
                return locationDetials;
            }
            catch(Exception ex)
            {
                return locationDetials;
            }
        }
        public async Task<JsonResult> GetBuildingClass(string roof, string wall, string column, string floor, string lifeofBuilding)
        {
            MasterDataViewModel masterDataViewModel1 = new MasterDataViewModel();
            string rft_code = "-1";
            try
            {
                if (roof == null || roof == "")
                {
                    return Json("Roof should not be empty!");
                }
                else if (wall == null || wall == "")
                {
                    return Json("Wall should not be empty!");
                }
                else if (column == null || column == "")
                {
                    return Json("Column should not be empty!");
                }
                else if (floor == null || floor == "")
                {
                    return Json("Floor should not be empty!");
                }
                else
                {
                    masterDataViewModel1 = await ruleValidation.GetBuildingClass(roof, wall, column, floor);
                    if (masterDataViewModel1 != null)
                    {
                        rft_code = masterDataViewModel1.RFT_CODE;
                    }
                }

                string rft_desp = masterDataViewModel1.RFT_DESCRIPTION;

                //decimal pae = CalculatePAE(rft_desp, Convert.ToInt32(lifeofBuilding));
                return Json(rft_code);
            }
            catch (Exception ex)
            {
                return Json("-ex");
            }

        }

        public async Task<JsonResult> CalculatePAE(string classofBuilding, int lifetimeofBuilding, int totalAreaOfBuilding)
        {
            LimitedPAEViewModel limitedPAEViewModel = new LimitedPAEViewModel();
            int OneSqFtPAE = totalAreaOfBuilding;
            limitedPAEViewModel.paepersqft = OneSqFtPAE;
            decimal pae = 0;
            try
            {
                if(lstClassOfBuilding.Count() == 0)
                {
                    lstClassOfBuilding = await _middlewareAPI.GetBuildingClassData();
                }
                
                string buildingClass = lstClassOfBuilding.Where(c => c.RFT_CODE == classofBuilding).Select( s=> s.RFT_DESCRIPTION).FirstOrDefault();
                //J13 = Class of Building 
                //K13 = Building's life time
               
                if (buildingClass == "")
                {
                    return Json(pae);
                }
                else if (buildingClass == "1ST CLASS") // Class of Builing
                {

                    if (lifetimeofBuilding < 1) // if building lifetime under 1 year
                    {
                        pae = 30000 * lifetimeofBuilding;
                       
                    }
                    else if (lifetimeofBuilding < 6)
                    {
                        pae = 40000;
                       
                    }
                    else if (lifetimeofBuilding > 10)
                    {
                        pae = 30000;
                       
                    }
                    else
                    {
                        pae = 35000;
                       
                    }
                }
                else if (buildingClass == "2ND CLASS")
                {
                    if (lifetimeofBuilding < 1)
                    {
                        pae = 15000 * lifetimeofBuilding;
                        
                    }
                    else if (lifetimeofBuilding < 6)
                    {
                        pae = 25000;
                        
                    }
                    else if (lifetimeofBuilding > 10)
                    {
                        pae = 15000;
                        
                    }
                    else
                    {
                        pae = 20000;
                      
                    }
                }
                else if (buildingClass == "3RD CLASS")
                {
                    pae = 10000;
                   
                }
                else
                {
                    return Json(pae);
                }
                
                decimal limitedPAEVal = pae * OneSqFtPAE;
                limitedPAEViewModel.limitedpae = Convert.ToInt32(pae);
                limitedPAEViewModel.limitedvalue = Convert.ToDecimal(limitedPAEVal);

                return Json(limitedPAEViewModel);
            }
            catch (Exception ex)
            {
                return Json(limitedPAEViewModel);
            }
            
        }

       
        #endregion

        #region DRS Fire 
        public IActionResult drsfireproposal()
        {
            return View();
        }
        #endregion
    }
}