﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using AYASOMPO_SMSGatewayV2.Models;
using AYASOMPO_SMSGatewayV2.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;

namespace AYASOMPO_SMSGatewayV2.Controllers
{
    public class ReportController : Controller
    {
        MainDBContext _db = new MainDBContext();
        JWTDataContext _JWTDataContext = new JWTDataContext();
        private IHostingEnvironment _env;
        public ISession _session;
        Common common = new Common();
        DataServices dataService = new DataServices();
        SmsService SmsService = new SmsService();
        CRMAPIService cRMAPIService = new CRMAPIService();
        private HttpClient _client;
        public IConfigurationRoot Configuration { get; }

        Survey_DBContext _Surveydb = new Survey_DBContext();

        private const string Alphabet = "23456789bcdfghjkmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ-_";
        private static readonly int Base = Alphabet.Length;

        public ReportController(IHostingEnvironment env)
        {
            _env = env;

            var builder = new ConfigurationBuilder()
             .SetBasePath(Directory.GetCurrentDirectory())
             .AddJsonFile("appsettings.json");
            Configuration = builder.Build();

            _client = new HttpClient();
            string url = Configuration["InternalMiddleware:IPAddress"].ToString();
            _client.BaseAddress = new Uri(Configuration["InternalMiddleware:IPAddress"].ToString());
            _client.DefaultRequestHeaders.Clear();
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        #region Success Renewal Sent SMS

        public IActionResult RenewalSMSbyStaff()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            SentSmsVM model = new SentSmsVM();
            ViewBag.StaffList = new SelectList(_db.SysUser.OrderBy(d => d.Name).ToList(), "Id", "Name");

            //model.StaffList = GetStaffData();
            //model.DepartmentList = GetDeptList();

            return View(model);
        }

        //[HttpPost]
        public IActionResult SentSMSReportData(SentSmsVM model)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            SentSmsVM output = new SentSmsVM();
            if (model.FromDate == null && model.ToDate == null)
            {
                return RedirectToAction("RenewalSMSbyStaff");
            }
            else
            {
                output = SendSMSBySysId(model);
            }

            output.StaffList = GetStaffData();
            output.DepartmentList = GetDeptList();

            ViewBag.output = output;
            ViewBag.StaffList = new SelectList(_db.SysUser.Where(s => s.IsActive == true).OrderBy(d => d.Name).ToList(), "Id", "Name");
            HttpContext.Session.SetComplexData("lstSentSmsVM", output);
            return View("RenewalSMSbyStaff", output);
        }

        SentSmsVM SendSMSBySysId(SentSmsVM model)
        {
            SentSmsVM output = new SentSmsVM();
            try
            {
                //if (model.Status == "fail")
                //{
                //    model.ActivitiesFlag = false;
                //}
                //else
                //{
                //    model.ActivitiesFlag = true;
                //}
                //output.lst = (from u in _db.SysUser
                //              join sms in _db.SentSms on u.Id equals sms.SysId
                //              join d in _db.Department on u.Department equals d.Id
                //              //where d.IsDelete == false
                //              where u.Id == model.StaffId
                //              where sms.SentDate >= model.fromDate
                //              where sms.SentDate <= model.toDate.AddHours(23.50)
                //              where sms.ActivitiesFlag == model.ActivitiesFlag
                //              orderby sms.SentDate ascending
                //              select new SentSmsVM
                //              {
                //                  Id = sms.Id,
                //                  PhoneNumber = sms.PhoneNumber,
                //                  UserName = sms.UserName,
                //                  Message = sms.Message,
                //                  SentDate = sms.SentDate,
                //                  Smscount = sms.Smscount
                //              }).Distinct().ToList();

                if (model.StaffId != Guid.Empty)
                {
                    output.lst = (from sms in _db.SentSms
                                  where sms.SentDate >= model.fromDate
                                  where sms.SentDate <= model.toDate.AddHours(23.50)
                                  where sms.SysId == model.StaffId
                                  where sms.Channel == "RenewalNotice" &&  sms.Smsflag == "1"
                                  orderby sms.SentDate descending
                                  select new SentSmsVM
                                  {
                                      Id = sms.Id,
                                      PhoneNumber = sms.PhoneNumber,
                                      UserName = sms.UserName,
                                      Message = sms.Message,
                                      SentDate = sms.SentDate,
                                      Smscount = sms.Smscount,
                                      VehicleNo = sms.VehicleNo,
                                      PolicyNo = sms.PolicyNo,
                                      Category = sms.Channel
                                  }).Distinct().ToList();
                }
                else
                {
                    output.lst = (from sms in _db.SentSms
                                  where sms.SentDate >= model.fromDate
                                  where sms.SentDate <= model.toDate.AddHours(23.50)
                                  where sms.Channel == "RenewalNotice" && sms.Smsflag == "1"
                                  orderby sms.SentDate descending
                                  select new SentSmsVM
                                  {
                                      Id = sms.Id,
                                      PhoneNumber = sms.PhoneNumber,
                                      UserName = sms.UserName,
                                      Message = sms.Message,
                                      SentDate = sms.SentDate,
                                      Smscount = sms.Smscount,
                                      VehicleNo = sms.VehicleNo,
                                      PolicyNo = sms.PolicyNo,
                                      Category = sms.Channel
                                  }).Distinct().ToList();
                }

                output.StaffName = _db.SysUser.Where(u => u.Id == model.StaffId).Select(u => u.Name).FirstOrDefault();
                output.fromDate = model.fromDate;
                output.toDate = model.toDate;
                output.Status = model.Status;
                output.StaffId = model.StaffId;

                return output;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public IActionResult ExportReportBySysId(SentSmsVM model)
        {
            SentSmsVM VM = new SentSmsVM();
            List<SentSmsVM> lstSentSmsVM = new List<SentSmsVM>();
            if (HttpContext.Session.GetObjectFromJson<SentSmsVM>("lstSentSmsVM") != null)
            {
                VM = HttpContext.Session.GetObjectFromJson<SentSmsVM>("lstSentSmsVM");
                //VM.lst = lstSentSmsVM;
            }
            else
            {
                VM = SendSMSBySysId(model);
                if (VM == null)
                {
                    TempData["Message"] = "Invalid Request. Please preview data that you want to download.";
                    return RedirectToAction("RenewalSMSbyStaff");
                }
            }

            var columnHeader = new List<string>
            {
                "Sent Date/Time",
                "Receiver's Name",
                "Phone Number",
                "Vehicle No",
                "Invoice No",
                "Template",
                "Message"
            };

            var fileContent = ExportExcelBySysId(VM, columnHeader, "SentSMS ByStaff");
            return File(fileContent, "application/ms-excel", "Sent SMS Report  By Staff.xlsx");
        }

        public byte[] ExportExcelBySysId(SentSmsVM VM, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 20;

                worksheet.Cells[1, 1].Style.Font.Bold = true;
                worksheet.Cells[1, 1].Value = "Staff Name :";
                worksheet.Cells[1, 2].Value = VM.StaffName;

                worksheet.Cells[2, 1].Style.Font.Bold = true;
                worksheet.Cells[2, 1].Value = "From Date :";
                worksheet.Cells[2, 2].Value = string.Format("{0:dd/MM/yyyy}", VM.fromDate);

                worksheet.Cells[3, 1].Style.Font.Bold = true;
                worksheet.Cells[3, 1].Value = "To Date :";
                worksheet.Cells[3, 2].Value = string.Format("{0:dd/MM/yyyy}", VM.toDate);

                for (int i = 0; i < columnHeader.Count(); i++)
                {
                    worksheet.Cells[5, i + 1].Style.Font.Bold = true;
                    worksheet.Cells[5, i + 1].Value = columnHeader[i];
                }
                var j = 6;
                var count = 1;
                foreach (var item in VM.lst)
                {
                    worksheet.Cells["A" + j].Value = string.Format("{0:dd-MM-yyyy hh:mm:tt}", item.SentDate);
                    worksheet.Cells["B" + j].Value = item.UserName;
                    worksheet.Cells["C" + j].Value = item.PhoneNumber;
                    worksheet.Cells["D" + j].Value = item.VehicleNo;
                    worksheet.Cells["E" + j].Value = item.PolicyNo;
                    worksheet.Cells["F" + j].Value = item.Category;
                    worksheet.Cells["G" + j].Value = item.Message;

                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        #endregion

        #region Fail Renewal Sent SMS

        public IActionResult RenewalFailCase()
        {
            return View();
        }

        public IActionResult CustomRenewalFailCase()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            List<TempRenewalCaseCreate> lsttempRenewalCase = new List<TempRenewalCaseCreate>();
            //lsttempRenewalCase = (from x in _db.TempRenewalCaseCreate
            //                      where x.CreatedDate.Date >= renewalCaseViewModel.FromCreatedDate.Date
            //                      where x.CreatedDate.Date <= Convert.ToDateTime(renewalCaseViewModel.ToDate).Date
            //                      select x).ToList();

            lsttempRenewalCase = _db.TempRenewalCaseCreate.OrderByDescending(t => t.CreatedDate).ToList();

            ViewBag.lsttempRenewalCase = lsttempRenewalCase;
            HttpContext.Session.SetComplexData("lsttempRenewalCase", lsttempRenewalCase);
            return View();
        }

        public IActionResult GetCustomRenewalFailCase(TempRenewalCaseViewModel renewalCaseViewModel)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            List<TempRenewalCaseCreate> lsttempRenewalCase = new List<TempRenewalCaseCreate>();
            lsttempRenewalCase = (from x in _db.TempRenewalCaseCreate
                                  where x.CreatedDate.Date >= Convert.ToDateTime(renewalCaseViewModel.FromDate).Date
                                  where x.CreatedDate.Date <= Convert.ToDateTime(renewalCaseViewModel.ToDate).Date
                                  select x).ToList();

            ViewBag.lsttempRenewalCase = lsttempRenewalCase;
            HttpContext.Session.SetComplexData("lsttempRenewalCase", lsttempRenewalCase);
            return View("CustomRenewalFailCase");
        }

        public IActionResult ExportReportCustomerFailCase(SentSmsVM model)
        {
            TempRenewalCaseViewModel tempRenewalCaseViewModel = new TempRenewalCaseViewModel();
            List<TempRenewalCaseCreate> lstTempRenewalCaseViewModel = new List<TempRenewalCaseCreate>();
            if (HttpContext.Session.GetObjectFromJson<List<TempRenewalCaseCreate>>("lsttempRenewalCase") != null)
            {
                lstTempRenewalCaseViewModel = HttpContext.Session.GetObjectFromJson<List<TempRenewalCaseCreate>>("lsttempRenewalCase");
                //VM.lst = lstSentSmsVM;
            }
            else
            {
                lstTempRenewalCaseViewModel = _db.TempRenewalCaseCreate.OrderByDescending(t => t.CreatedDate).ToList(); ;
            }

            var columnHeader = new List<string>
            {
                "Phone Number",
                "Customer Name ",
                "Invoice No",
                "Risk Name",
                "Expiry Date",
                "Renewal Premium",
                "Product",
                "From Date",
                "To Date",
                "Link",
                "Customer Code",
                "Month",
                "Person In Charge",
                "Responsible Party",
                "Policy No",
                "Branch",
                "Intermediary Type",
                "Agent Code",
                "Agent Name",
                "Account Code",
                "Authorized Code",
                "Authorized Name",
                "Created Date"
            };

            var fileContent = ExportExcelByCustomFailCase(lstTempRenewalCaseViewModel, columnHeader, "Custom Fail Case");
            return File(fileContent, "application/ms-excel", "Custom Fail Case.xlsx");
        }

        private byte[] ExportExcelByCustomFailCase(List<TempRenewalCaseCreate> lstTempRenewalCaseViewModel, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 20;
                using (var cells = worksheet.Cells[1, 1, 1, 60])
                    cells.Style.Font.Bold = true;

                for (int i = 0; i < columnHeader.Count(); i++)
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];


                var j = 2;
                var count = 1;
                foreach (var item in lstTempRenewalCaseViewModel)
                {
                    worksheet.Cells["A" + j].Value = item.PhoneNumber;
                    worksheet.Cells["B" + j].Value = item.CustomerName;
                    worksheet.Cells["C" + j].Value = item.InvoiceNo;
                    worksheet.Cells["D" + j].Value = item.RiskName;
                    worksheet.Cells["E" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ExpiryDate);
                    worksheet.Cells["F" + j].Value = item.RenewalPremium;
                    worksheet.Cells["G" + j].Value = item.Product;
                    worksheet.Cells["H" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.FromDate);
                    worksheet.Cells["I" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ToDate);
                    worksheet.Cells["J" + j].Value = item.Link;
                    worksheet.Cells["K" + j].Value = item.CusCode;
                    worksheet.Cells["L" + j].Value = item.Month;
                    worksheet.Cells["M" + j].Value = item.PersonInCharge;
                    worksheet.Cells["N" + j].Value = item.ResponsibleParty;
                    worksheet.Cells["O" + j].Value = item.PolicyNo;
                    worksheet.Cells["P" + j].Value = item.Branch;
                    worksheet.Cells["Q" + j].Value = item.IntermediaryType;
                    worksheet.Cells["R" + j].Value = item.AgentCode;
                    worksheet.Cells["S" + j].Value = item.AgentName;
                    worksheet.Cells["T" + j].Value = item.AccountCode;
                    worksheet.Cells["U" + j].Value = item.AuthorizedCode;
                    worksheet.Cells["V" + j].Value = item.AuthorizedName;
                    worksheet.Cells["W" + j].Value = item.CreatedDate;
                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        #endregion

        #region NB_CSATSMS

        #region NewBussinessCSATSMSbyStaff
        public IActionResult NewBussinessCSATSMSbyStaff()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            SentSmsVM model = new SentSmsVM();

            ViewBag.StaffList = new SelectList(_db.SysUser.OrderBy(d => d.Name).ToList(), "Id", "Name");

            return View(model);
        }

        public IActionResult SentNewBussinessCSATSMSReportData(SentSmsVM model)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            SentSmsVM output = new SentSmsVM();

            if(model.FromDate == null && model.ToDate == null)
            {
                return RedirectToAction("NewBussinessCSATSMSbyStaff");
            }
            output = SendNewBussinessCSATSMSBySysId(model);

            ViewBag.output = output;
            ViewBag.StaffList = new SelectList(_db.SysUser.OrderBy(d => d.Name).ToList(), "Id", "Name");
            HttpContext.Session.SetComplexData("lstSentNewBussinessCSATSmsVM", output);
            return View("NewBussinessCSATSMSbyStaff", output);
        }

        SentSmsVM SendNewBussinessCSATSMSBySysId(SentSmsVM model)
        {
            SentSmsVM output = new SentSmsVM();

            try
            {
                if (model.StaffId != Guid.Empty)
                {
                    output.lst = (from sms in _db.SentSms
                                  where sms.SentDate >= model.fromDate
                                  where sms.SentDate <= model.toDate.AddHours(23.50)
                                  where sms.Channel == "New Business CSAT(EN)"
                                  where sms.SysId == model.StaffId
                                  orderby sms.SentDate ascending
                                  select new SentSmsVM
                                  {

                                      Id = sms.Id,
                                      PhoneNumber = sms.PhoneNumber,
                                      UserName = sms.UserName,
                                      Message = sms.Message,
                                      SentDate = sms.SentDate,
                                      Smscount = sms.Smscount
                                  }).Distinct().ToList();
                }
                else
                {
                    output.lst = (from sms in _db.SentSms
                                  where sms.SentDate >= model.fromDate
                                  where sms.SentDate <= model.toDate.AddHours(23.50)
                                  where sms.Channel == "New Business CSAT(EN)"
                                  orderby sms.SentDate ascending
                                  select new SentSmsVM
                                  {
                                      Id = sms.Id,
                                      PhoneNumber = sms.PhoneNumber,
                                      UserName = sms.UserName,
                                      Message = sms.Message,
                                      SentDate = sms.SentDate,
                                      Smscount = sms.Smscount
                                  }).Distinct().ToList();
                }

                output.StaffName = _db.SysUser.Where(u => u.Id == model.StaffId).Select(u => u.Name).FirstOrDefault();
                output.fromDate = model.fromDate;
                output.toDate = model.toDate;
                output.StaffId = model.StaffId;

                return output;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public IActionResult ExportNewBussinessCSATSMSReportBySysId(SentSmsVM model)
        {
            SentSmsVM VM = new SentSmsVM();
            List<SentSmsVM> lstSentSmsVM = new List<SentSmsVM>();
            if (HttpContext.Session.GetObjectFromJson<SentSmsVM>("lstSentNewBussinessCSATSmsVM") != null)
            {
                VM = HttpContext.Session.GetObjectFromJson<SentSmsVM>("lstSentNewBussinessCSATSmsVM");
                //VM.lst = lstSentSmsVM;
            }
            else
            {
                VM = SendNewBussinessCSATSMSBySysId(model);
                if (VM == null)
                {
                    TempData["Message"] = "Invalid Request. Please preview data that you want to download.";
                    return RedirectToAction("NewBussinessCSATSMSbyStaff");
                }
            }

            var columnHeader = new List<string>
            {
                "Sent Date/Time",
                "Receiver's Name",
                "Phone Number",
                "Message"
            };

            var fileContent = ExportNewBussinessCSATSMSBySysId(VM, columnHeader, "SentSMS ByStaff");
            return File(fileContent, "application/ms-excel", "Sent New Business CSAT SMS Report.xlsx");
        }

        public byte[] ExportNewBussinessCSATSMSBySysId(SentSmsVM VM, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 20;

                worksheet.Cells[1, 1].Style.Font.Bold = true;
                worksheet.Cells[1, 1].Value = "Staff Name :";
                worksheet.Cells[1, 2].Value = VM.StaffName;

                worksheet.Cells[2, 1].Style.Font.Bold = true;
                worksheet.Cells[2, 1].Value = "From Date :";
                worksheet.Cells[2, 2].Value = string.Format("{0:dd/MM/yyyy}", VM.fromDate);

                worksheet.Cells[3, 1].Style.Font.Bold = true;
                worksheet.Cells[3, 1].Value = "To Date :";
                worksheet.Cells[3, 2].Value = string.Format("{0:dd/MM/yyyy}", VM.toDate);

                for (int i = 0; i < columnHeader.Count(); i++)
                {
                    worksheet.Cells[5, i + 1].Style.Font.Bold = true;
                    worksheet.Cells[5, i + 1].Value = columnHeader[i];
                }
                var j = 6;
                var count = 1;
                foreach (var item in VM.lst)
                {
                    worksheet.Cells["A" + j].Value = string.Format("{0:dd-MM-yyyy hh:mm:tt}", item.SentDate);
                    worksheet.Cells["B" + j].Value = item.UserName;
                    worksheet.Cells["C" + j].Value = item.PhoneNumber;
                    worksheet.Cells["D" + j].Value = item.Message;

                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        #endregion

        #region NewBussinessCSATFailCase

        public IActionResult NewBusinessCSATFailCase()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            GetFailSurveySMS();
            return View();
        }

        private void GetFailSurveySMS()
        {
            try
            {
                List<SurveySMSViewModel> surveySMSViewModels = new List<SurveySMSViewModel>();
                surveySMSViewModels = (from s in _db.SurveySms
                                       join c in _db.Category on s.CategoryId equals c.Id
                                       //where s.UploadedDate != DateTime.Now.Date
                                       orderby s.UploadedDate descending
                                       select new SurveySMSViewModel
                                       {
                                           Id = s.Id,
                                           UploadedDate = s.UploadedDate,
                                           PhoneNumber = s.PhoneNumber,
                                           CusCode = s.CusCode,
                                           CustomerName = s.CustomerName,
                                           InvoiceNo = s.InvoiceNo,
                                           RiskName = s.RiskName,
                                           ExpiryDate = s.ExpiryDate,
                                           RenewalPremiun = s.RenewalPremiun,
                                           Product = s.Product,
                                           FromDate = s.FromDate,
                                           ToDate = s.ToDate,
                                           Link = s.Link,
                                           Month = s.Month,
                                           PersonInCharge = s.PersonInCharge,
                                           ResponsibleParty = s.ResponsibleParty,
                                           PolicyNo = s.PolicyNo,
                                           Branch = s.Branch,
                                           IntermediaryType = s.IntermediaryType,
                                           AgentCode = s.AgentCode,
                                           AgentName = s.AgentName,
                                           AccountCode = s.AccountCode,
                                           AuthorizedCode = s.AuthorizedCode,
                                           AuthorizedName = s.AuthorizedName,
                                           CategoryId = s.CategoryId,
                                           CategoryDescription = c.Description,
                                           Message = s.Message
                                       }).ToList();
                ViewBag.FailSurveySMSList = surveySMSViewModels;

                HttpContext.Session.SetObjectAsJson("lstFailSurveySms", null);
                HttpContext.Session.SetObjectAsJson("lstFailSurveySms", surveySMSViewModels);
            }
            catch (Exception ex)
            {

            }
        }

        public IActionResult GetNewBusinessCSATSMSFailCase(SurveySMSViewModel model)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");
            try
            {
                List<SurveySMSViewModel> lstFailSurveySMSCase = new List<SurveySMSViewModel>();
                lstFailSurveySMSCase = (from s in _db.SurveySms
                                        join c in _db.Category on s.CategoryId equals c.Id
                                        where s.UploadedDate >= model.FromUploadedDate
                                        where s.UploadedDate <= model.ToUploadedDate.AddHours(23.50)
                                        //where s.UploadedDate != DateTime.Now.Date
                                        orderby s.UploadedDate descending
                                        select new SurveySMSViewModel
                                        {
                                            Id = s.Id,
                                            UploadedDate = s.UploadedDate,
                                            PhoneNumber = s.PhoneNumber,
                                            CusCode = s.CusCode,
                                            CustomerName = s.CustomerName,
                                            InvoiceNo = s.InvoiceNo,
                                            RiskName = s.RiskName,
                                            ExpiryDate = s.ExpiryDate,
                                            RenewalPremiun = s.RenewalPremiun,
                                            Product = s.Product,
                                            FromDate = s.FromDate,
                                            ToDate = s.ToDate,
                                            Link = s.Link,
                                            Month = s.Month,
                                            PersonInCharge = s.PersonInCharge,
                                            ResponsibleParty = s.ResponsibleParty,
                                            PolicyNo = s.PolicyNo,
                                            Branch = s.Branch,
                                            IntermediaryType = s.IntermediaryType,
                                            AgentCode = s.AgentCode,
                                            AgentName = s.AgentName,
                                            AccountCode = s.AccountCode,
                                            AuthorizedCode = s.AuthorizedCode,
                                            AuthorizedName = s.AuthorizedName,
                                            CategoryId = s.CategoryId,
                                            CategoryDescription = c.Description,
                                            Message = s.Message
                                        }).ToList();
                ViewBag.FailSurveySMSList = lstFailSurveySMSCase;

                HttpContext.Session.SetObjectAsJson("lstFailSurveySms", null);
                HttpContext.Session.SetObjectAsJson("lstFailSurveySms", lstFailSurveySMSCase);
                return View("NewBusinessCSATFailCase");
            }
            catch(Exception ex)
            {
                TempData["Message"] = "Invalid Request.";
                return View("NewBusinessCSATFailCase");
            }
           
        }

        public async Task<IActionResult> SendNewBusinessCSATFailCase()
        {
            try
            {
                if (!CheckLoginUser())
                    return RedirectToAction("Login", "Account");
                if (HttpContext.Session.GetObjectFromJson<List<SurveySMSViewModel>>("lstFailSurveySms") != null)
                {
                    List<SurveySms> lstSurveyModels = new List<SurveySms>();
                    lstSurveyModels = _db.SurveySms.ToList();

                    await SendNewBusinessSurveySmsAsync(lstSurveyModels, true);

                    string SMSSuccess = HttpContext.Session.GetString("successCountSMS");
                    string SMSFail = HttpContext.Session.GetString("failCountSMS");
                    //string RenewalSuccess = HttpContext.Session.GetString("successCountRenewal");
                    //string RenewalFail = HttpContext.Session.GetString("failCountRenewal");
                    TempData["Message"] = "Total SMS sent = " + SMSSuccess + " success and " + SMSFail + " fail.";
                }
                else
                {
                    TempData["Message"] = "Error in sending SMS.";
                }
                return RedirectToAction("NewBusinessCSATFailCase");
            }
            catch (Exception ex)
            {
                return RedirectToAction("NewBusinessCSATFailCase");
            }
        }

        private async Task SendNewBusinessSurveySmsAsync(List<SurveySms> lstSurveyModels, bool chkSentSMS)
        {
            int successCountSMS = 0;
            int failCountSMS = 0;
            try
            {
                foreach (var surveySms in lstSurveyModels)
                {
                    SentSms sentSms = new SentSms();
                    Category smsCategory = new Category();

                    smsCategory = _db.Category.Where(c => c.Id == surveySms.CategoryId).FirstOrDefault();
                    sentSms.CategoryId = surveySms.CategoryId;
                    sentSms.Channel = smsCategory.Description;

                    string AccountCode = _db.CoreAccountCode.Where(m => m.AccountCode == surveySms.AccountCode).Select(m => m.AccountCode).FirstOrDefault();
                    if(AccountCode == null)
                    {
                        #region SentSMS
                        bool smsFlag = false;
                        bool validity = false;

                        if(chkSentSMS == true)
                        {
                            if(surveySms.PhoneNumber != null)
                            {
                                string PhoneNo = surveySms.PhoneNumber.ToString();

                                #region Check PhoneNo

                                if (PhoneNo.StartsWith("0920") || PhoneNo.StartsWith("0921") || PhoneNo.StartsWith("0922") || PhoneNo.StartsWith("0923") || PhoneNo.StartsWith("0924")
                                    || PhoneNo.StartsWith("0950") || PhoneNo.StartsWith("0951") || PhoneNo.StartsWith("0952") || PhoneNo.StartsWith("0953") || PhoneNo.StartsWith("0954")
                                    || PhoneNo.StartsWith("0955") || PhoneNo.StartsWith("0956") || PhoneNo.StartsWith("0983") || PhoneNo.StartsWith("0985") || PhoneNo.StartsWith("0986")
                                    || PhoneNo.StartsWith("0987"))
                                {
                                    if (PhoneNo.Length == 9) validity = true;
                                    else validity = false;
                                }
                                else if (PhoneNo.StartsWith("0941") || PhoneNo.StartsWith("0943") || PhoneNo.StartsWith("0947") || PhoneNo.StartsWith("0949") || PhoneNo.StartsWith("0973") || PhoneNo.StartsWith("0991"))
                                {
                                    if (PhoneNo.Length == 10) validity = true;
                                    else validity = false;
                                }
                                else if (PhoneNo.StartsWith("0925") || PhoneNo.StartsWith("0926") || PhoneNo.StartsWith("0940") || PhoneNo.StartsWith("0942") || PhoneNo.StartsWith("0944")
                                    || PhoneNo.StartsWith("0945") || PhoneNo.StartsWith("0948") || PhoneNo.StartsWith("0988") || PhoneNo.StartsWith("0989"))
                                {
                                    if (PhoneNo.Length == 11) validity = true;
                                    else validity = false;
                                }
                                else if (PhoneNo.StartsWith("0994") || PhoneNo.StartsWith("0995") || PhoneNo.StartsWith("0996") || PhoneNo.StartsWith("0997") || PhoneNo.StartsWith("0998") || PhoneNo.StartsWith("0999"))
                                {
                                    if (PhoneNo.Length == 11) validity = true;
                                    else validity = false;
                                }
                                else if (PhoneNo.StartsWith("0930") || PhoneNo.StartsWith("0931") || PhoneNo.StartsWith("0932") || PhoneNo.StartsWith("0933") || PhoneNo.StartsWith("0936"))
                                {
                                    if (PhoneNo.Length == 10) validity = true;
                                    else validity = false;
                                }
                                else if (PhoneNo.StartsWith("0934") || PhoneNo.StartsWith("0935"))
                                {
                                    if (PhoneNo.Length == 11) validity = true;
                                    else validity = false;
                                }
                                else if (PhoneNo.StartsWith("0974") || PhoneNo.StartsWith("0975") || PhoneNo.StartsWith("0976") || PhoneNo.StartsWith("0977") || PhoneNo.StartsWith("0978") || PhoneNo.StartsWith("0979"))
                                {
                                    if (PhoneNo.Length == 11) validity = true;
                                    else validity = false;
                                }
                                else if (PhoneNo.StartsWith("0965") || PhoneNo.StartsWith("0966") || PhoneNo.StartsWith("0967") || PhoneNo.StartsWith("0968") || PhoneNo.StartsWith("0969"))
                                {
                                    if (PhoneNo.Length == 11) validity = true;
                                    else validity = false;
                                }
                                #endregion

                                if (validity == true)
                                {
                                    //Create Renewal Case Activity
                                    if (smsCategory.Status == true)
                                    {
                                        if (surveySms.CusCode != null || surveySms.CusCode != "")
                                        {
                                            string CRMActivityRetMsg = await CreateCRMActivityAsync(surveySms.CusCode, smsCategory.Description);
                                            if (CRMActivityRetMsg != "1")
                                            {
                                                sentSms.ActivitiesFlag = false;
                                            }
                                            else
                                            {
                                                sentSms.ActivitiesFlag = true;
                                            }
                                        }
                                        else
                                        {
                                            sentSms.ActivitiesFlag = false;
                                        }
                                    }

                                    PhoneNo = PhoneNo.Substring(1, PhoneNo.Length - 1); //Remove 'zero' from phoneNumber
                                    PhoneNo = PhoneNo.Insert(0, "+95");// Add '+95' to phoneNumber
                                    smsFlag = SmsService.InvokeServiceForSendSmsBulkRequest(PhoneNo, surveySms.Message);
                                }
                            }
                            if(smsFlag == true)
                            {
                                sentSms.Smsflag = "1";
                                successCountSMS += 1;
                            }
                            else
                            {
                                sentSms.Smsflag = "0";
                                failCountSMS += 1;
                            }
                        }

                        #endregion

                        #region Save SentSMS
                        if (smsFlag == true && validity == true)
                        {
                            SaveSentSMS(surveySms, sentSms);
                            DeleteSurveySmsFailCase(surveySms);
                        }

                        #endregion
                    }
                    HttpContext.Session.SetString("successCountSMS", successCountSMS.ToString());
                    HttpContext.Session.SetString("failCountSMS", failCountSMS.ToString());
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void SaveSentSMS(SurveySms surveySmsModel, SentSms sentSms)
        {
            try
            {
                SentSms sentSmsModel = new SentSms();
                sentSmsModel.Id = Guid.NewGuid();
                sentSmsModel.PhoneNumber = surveySmsModel.PhoneNumber;
                sentSmsModel.UserName = surveySmsModel.CustomerName;
                sentSmsModel.SysId = Guid.Parse(HttpContext.Session.GetString("userId"));
                sentSmsModel.Message = surveySmsModel.Message;
                sentSmsModel.SentDate = DateTime.Now;
                sentSmsModel.Channel = sentSms.Channel;
                sentSmsModel.VehicleNo = surveySmsModel.RiskName;
                sentSmsModel.Smsflag = sentSms.Smsflag;
                sentSmsModel.Smscount = surveySmsModel.Smscount;
                sentSmsModel.CategoryId = sentSms.CategoryId;
                sentSmsModel.PolicyNo = surveySmsModel.PolicyNo;
                sentSmsModel.ExpireDate = surveySmsModel.ExpiryDate;
                sentSmsModel.RenewalPremium = surveySmsModel.RenewalPremiun;
                sentSmsModel.Product = surveySmsModel.Product;
                sentSmsModel.FromDate = surveySmsModel.FromDate;
                sentSmsModel.ToDate = surveySmsModel.ToDate;
                sentSmsModel.Link = surveySmsModel.Link;
                sentSmsModel.CusCode = surveySmsModel.CusCode;
                sentSmsModel.ActivitiesFlag = sentSms.ActivitiesFlag;

                _db.Entry(sentSmsModel).State = EntityState.Added;
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        private void DeleteSurveySmsFailCase(SurveySms surveySmsModel)
        {
            try
            {
                SurveySms model = new SurveySms();
                model = _db.SurveySms.Where(m => m.PolicyNo == surveySmsModel.PolicyNo).FirstOrDefault();
                _db.SurveySms.Remove(model);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        #region Customer timeline activity
        private async Task<string> CreateCRMActivityAsync(string cusCode, string description)
        {
            var body = new JObject();
            string responebody = "-1";
            ValidateCustomerResponse validateCustomerResponse = new ValidateCustomerResponse();

            List<NewCustomerInfo> lstcustInfo = new List<NewCustomerInfo>();
            lstcustInfo = await GetNewCustomerInfoByCusCode(cusCode);

            if (lstcustInfo.Count() > 0)
            {
                foreach (var lst in lstcustInfo)
                {
                    if (lst.cus_type == "I")
                    {
                        validateCustomerResponse = GetCRMContactsCustomerByCusCode(cusCode);
                        if (validateCustomerResponse.value != null)
                        {
                            if (validateCustomerResponse.value.Count() > 0)
                            {
                                string contactid = "";
                                foreach (var custccode in validateCustomerResponse.value)
                                {
                                    contactid = custccode.contactid;
                                }
                                Guid contactId = Guid.Parse(contactid);
                                if (contactId != null)
                                {
                                    string activityrequesturl = "ayasompo_contactcustomactivities";
                                    body = new JObject
                                    {
                                         { "subject", "New Business CSAT Survey SMS" },
                                         { "description", "Sending New Business CSAT Survey SMS to customer at " + DateTime.Now},
                                         { "regardingobjectid_contact@odata.bind", "/contacts("+ contactid +")" },
                                    };

                                    responebody = cRMAPIService.CreateActivities(HttpMethod.Post, body, activityrequesturl);
                                }
                            }
                        }
                    }
                    else
                    {
                        ValidateCorpCustomerResponse validateCorpCustomerResponse = new ValidateCorpCustomerResponse();
                        validateCorpCustomerResponse = GetCRMAccountCustomerByCusCode(cusCode);

                        if (validateCorpCustomerResponse.value != null)
                        {
                            if (validateCorpCustomerResponse.value.Count() > 0)
                            {
                                string contactid = "";
                                foreach (var custccode in validateCorpCustomerResponse.value)
                                {
                                    contactid = custccode.accountid;
                                }
                                Guid contactId = Guid.Parse(contactid);
                                if (contactId != null)
                                {
                                    string activityrequesturl = "ayasompo_contactcustomactivities";
                                    body = new JObject
                                    {
                                         { "subject", "New Business CSAT Survey SMS" },
                                         { "description", "Sending New Business CSAT Survey SMS to customer at " + DateTime.Now},
                                         { "regardingobjectid_account@odata.bind", "/accounts("+ contactid +")" },
                                    };

                                    responebody = cRMAPIService.CreateActivities(HttpMethod.Post, body, activityrequesturl);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                return responebody;
            }

            return responebody;
        }

        private ValidateCustomerResponse GetCRMContactsCustomerByCusCode(string cuscode)
        {
            ValidateCustomerResponse validateCustomerResponse = new ValidateCustomerResponse();
            string responseBody = "";
            var body = new JObject();
            string url = "";

            if (cuscode != null)
            {
                url = "contacts?$select=fullname&$filter=ayasompo_customercode eq '" + cuscode + "'";
                responseBody = cRMAPIService.CRMGetCustomerByCustomerCode(HttpMethod.Get, body = null, url);
                validateCustomerResponse = JsonConvert.DeserializeObject<ValidateCustomerResponse>(responseBody);
            }
            return validateCustomerResponse;
        }

        private ValidateCorpCustomerResponse GetCRMAccountCustomerByCusCode(string cuscode)
        {
            ValidateCorpCustomerResponse validateCorpCustomerResponse = new ValidateCorpCustomerResponse();
            string responseBody = "";
            var body = new JObject();
            string url = "";

            if (cuscode != null)
            {
                url = "accounts?$select=name&$filter=ayasompo_code eq '" + cuscode + "'";
                responseBody = cRMAPIService.CRMGetCustomerByCustomerCode(HttpMethod.Get, body = null, url);
                validateCorpCustomerResponse = JsonConvert.DeserializeObject<ValidateCorpCustomerResponse>(responseBody);

            }
            return validateCorpCustomerResponse;
        }

        private async Task<List<NewCustomerInfo>> GetNewCustomerInfoByCusCode(string cuscode)
        {
            List<NewCustomerInfo> newCustomerInfos = new List<NewCustomerInfo>();
            try
            {
                var responseTask = await _client.GetAsync($"/customer/GetNewCustomerByCusCode/{cuscode}");
                if (responseTask.IsSuccessStatusCode)
                {
                    //newCustomerInfos = await responseTask.Content.ReadAsAsync<List<NewCustomerInfo>>();
                    //newCustomerInfos = await responseTask.Content.ReadAsStreamAsync<List<NewCustomerInfo>>();

                    string jsonData = await responseTask.Content.ReadAsStringAsync();
                    newCustomerInfos = JsonConvert.DeserializeObject<List<NewCustomerInfo>>(jsonData);
                    return newCustomerInfos;
                }
                else
                {
                    return newCustomerInfos = new List<NewCustomerInfo>();
                }
            }
            catch (Exception ex)
            {
                return newCustomerInfos = new List<NewCustomerInfo>();
            }
        }
        #endregion

        public IActionResult ExportReportNewBusinessCSATFailCase()
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                List<SurveySMSViewModel> lstSurveySms = new List<SurveySMSViewModel>();
                lstSurveySms = HttpContext.Session.GetObjectFromJson<List<SurveySMSViewModel>>("lstFailSurveySms");

                var columnHeader = new List<string>
                {
                    "Phone Number",
                    "Customer Name",
                    "Invoice No",
                    "Risk Name",
                    "Expiry Date",
                    "Renewal Premium",
                    "Product",
                    "From Date",
                    "To Date",
                    "Link",
                    "Customer Code",
                    "Month",
                    "For Person in charge(PIC)",
                    "Responsible Party",
                    "Policy No",
                    "Branch",
                    "Intermediary Type",
                    "Agent Code",
                    "Agent Name",
                    "Account Code",
                    "Authorized Code",
                    "Authorized Name"
                };
                var fileContent = ExportExcelForFailCSATSurveySMSReport(lstSurveySms, columnHeader, "CSAT Survey SMS Fail Case");
                return File(fileContent, "application/ms-excel", "CSATSurveySMSFailCase.xlsx");
            }
        }

        public byte[] ExportExcelForFailCSATSurveySMSReport(List<SurveySMSViewModel> lst, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 30;
                using (var cells = worksheet.Cells[1, 1, 1, 60])
                    cells.Style.Font.Bold = true;

                for (int i = 0; i < columnHeader.Count(); i++)
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];


                var j = 2;
                var count = 1;
                foreach (var item in lst)
                {
                    worksheet.Cells["A" + j].Value = item.PhoneNumber;
                    worksheet.Cells["B" + j].Value = item.CustomerName;
                    worksheet.Cells["C" + j].Value = item.InvoiceNo;
                    worksheet.Cells["D" + j].Value = item.RiskName;
                    worksheet.Cells["E" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ExpiryDate);

                    worksheet.Cells["F" + j].Value = item.RenewalPremiun;
                    worksheet.Cells["G" + j].Value = item.Product;
                    worksheet.Cells["H" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.FromDate);
                    worksheet.Cells["I" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ToDate);
                    worksheet.Cells["J" + j].Value = item.Link;

                    worksheet.Cells["K" + j].Value = item.CusCode;
                    worksheet.Cells["L" + j].Value = item.Month;
                    worksheet.Cells["M" + j].Value = item.PersonInCharge;
                    worksheet.Cells["N" + j].Value = item.ResponsibleParty;
                    worksheet.Cells["O" + j].Value = item.PolicyNo;

                    worksheet.Cells["P" + j].Value = item.Branch;
                    worksheet.Cells["Q" + j].Value = item.IntermediaryType;
                    worksheet.Cells["R" + j].Value = item.AgentCode;
                    worksheet.Cells["S" + j].Value = item.AgentName;
                    worksheet.Cells["T" + j].Value = item.AccountCode;

                    worksheet.Cells["U" + j].Value = item.AuthorizedCode;
                    worksheet.Cells["V" + j].Value = item.AuthorizedName;

                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        public JsonResult UpdateNewBusinessSurveyPhoneNo(string phonenumber, string id)
        {
            try
            {
                SurveySms surveySms = new SurveySms();
                surveySms = _db.SurveySms.Where(m => m.Id == Guid.Parse(id)).FirstOrDefault();
                surveySms.PhoneNumber = phonenumber;
                _db.SurveySms.Update(surveySms);
                _db.SaveChanges();
                return Json("1");
            }
            catch (Exception ex)
            {
                return Json("-1");
            }
        }

        #endregion

        #endregion

        #region BulkSms
        public IActionResult BulkSmsByStaff()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            SentSmsVM model = new SentSmsVM();

            ViewBag.StaffList = new SelectList(_db.SysUser.OrderBy(d => d.Name).ToList(), "Id", "Name");
            ViewBag.CategoryList = new SelectList(_db.Category.OrderBy(c => c.Description).ToList(), "Id", "Description");

            return View(model);
        }

        public IActionResult SentBulkSMSReportData(SentSmsVM model)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            List<SentSmsVM> lstBulkSentSms = new List<SentSmsVM>();

            if(model.fromDate == null && model.toDate == null)
            {
                return RedirectToAction("BulkSmsByStaff");
            }
            else
            {
                lstBulkSentSms = GetBulkSmsByStaffSql(model);
            }
            ViewBag.output = lstBulkSentSms;
            ViewBag.StaffList = new SelectList(_db.SysUser.OrderBy(d => d.Name).ToList(), "Id", "Name");
            ViewBag.CategoryList = new SelectList(_db.Category.OrderBy(c => c.Description).ToList(), "Id", "Description");
            HttpContext.Session.SetComplexData("lstBulkSentSmsVM", lstBulkSentSms);

            return View("BulkSmsByStaff", model);
        }

        public List<SentSmsVM> GetBulkSmsByStaffSql(SentSmsVM filterData)
        {
            List<SentSmsVM> lstBulkSentSms = new List<SentSmsVM>();
            try
            {
                lstBulkSentSms = dataService.GetSentBulkSmsReport(filterData);
                return lstBulkSentSms;
            }
            catch(Exception ex)
            {
                return lstBulkSentSms;
            }
        }

        public IActionResult ExportBulkSmsReportBySysId(SentSmsVM model)
        {
            List<SentSmsVM> lstSentSmsVM = new List<SentSmsVM>();
            if (HttpContext.Session.GetObjectFromJson<List<SentSmsVM>>("lstBulkSentSmsVM") == null)
            {
                TempData["Message"] = "Invalid Request. Please preview data that you want to download.";
                return RedirectToAction("BulkSmsByStaff");
                //VM.lst = lstSentSmsVM;
            }
            else
            {
                lstSentSmsVM = HttpContext.Session.GetObjectFromJson<List<SentSmsVM>>("lstBulkSentSmsVM");
            }

            var columnHeader = new List<string>
            {
                "Sent Date/Time",
                "Receiver's Name",
                "Phone Number",
                "Vehicle No",
                "Invoice No",
                "Template",
                "Message"
            };

            var fileContent = ExportExcelBulkSMSBySysId(lstSentSmsVM, columnHeader, "BulkSMS ByStaff");
            return File(fileContent, "application/ms-excel", "Bulk SMS Report By Staff.xlsx");
        }

        public byte[] ExportExcelBulkSMSBySysId(List<SentSmsVM> lst, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 30;

                for (int i = 0; i < columnHeader.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Style.Font.Bold = true;
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];
                }
                var j = 2;
                var count = 1;
                foreach (var item in lst)
                {
                    worksheet.Cells["A" + j].Value = string.Format("{0:dd-MM-yyyy hh:mm:tt}", item.SentDate);
                    worksheet.Cells["B" + j].Value = item.UserName;
                    worksheet.Cells["C" + j].Value = item.PhoneNumber;
                    worksheet.Cells["D" + j].Value = item.VehicleNo;
                    worksheet.Cells["E" + j].Value = item.PolicyNo;
                    worksheet.Cells["F" + j].Value = item.Category;
                    worksheet.Cells["G" + j].Value = item.Message;

                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }
        #endregion

        #region Common Function
        public bool CheckLoginUser()
        {
            if (HttpContext.Session.GetString("userId") == null)
                return false;
            return true;
        }

        List<SysUser> GetStaffData()
        {
            List<SysUser> StaffList = new List<SysUser>();
            StaffList = (from u in _db.SysUser
                         where u.IsActive == true
                         orderby u.Name
                         select new SysUser
                         {
                             Id = u.Id,
                             Name = u.Name
                         }).ToList();
            return StaffList;


        }

        List<Department> GetDeptList()
        {
            List<Department> DeptList = new List<Department>();
            DeptList = (from u in _db.SysUser
                        join d in _db.Department on u.Department equals d.Id
                        where d.IsDelete == false
                        orderby d.Name
                        select new Department
                        {
                            Id = d.Id,
                            Name = d.Name
                        }).Distinct().ToList();
            return DeptList;
        }

        private void PrepareDataForRenewalReg()
        {
            try
            {
                List<AccountCodeInfoViewModel> lstAccountCode = new List<AccountCodeInfoViewModel>();
                ViewBag.ProductClass = new SelectList(_db.ProductClass.OrderBy(r => r.ClassCode).ToList(), "ClassCode", "ClassName");
                ViewBag.PICList = new SelectList(_JWTDataContext.CrmPersonInCharge.OrderBy(r => r.Picname).ToList(), "Picname", "Picname");

                lstAccountCode = dataService.GetAccountCodeList();
                ViewBag.AccountCode = new SelectList(lstAccountCode, "account_code", "account_code");
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

        #region RegisterRegisterList
        public IActionResult RenewalRegisterList(filterRenewalDataModel filterRenewalData)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            PrepareDataForRenewalReg();

            return View(filterRenewalData);

        }

        public IActionResult GetRenewalRegisterList(filterRenewalDataModel filterRenewalData)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            _session = HttpContext.Session;
            PrepareDataForRenewalReg();
            try
            {
                if (filterRenewalData.month == null || filterRenewalData.classname == null)
                {
                    TempData["Message"] = "Invalid request data";
                    return View("RenewalRegisterList", filterRenewalData);
                }

                if (filterRenewalData.classname == "FI")
                {
                    List<FireRenewalRegister> lstFireRenewalRegisters = new List<FireRenewalRegister>();
                    lstFireRenewalRegisters = GetFireRenewalRegisterSql(filterRenewalData);
                    ViewBag.lstFireRenewalRegisters = lstFireRenewalRegisters;
                    filterRenewalData.motortotalcount = lstFireRenewalRegisters.Count();
                    HttpContext.Session.SetObjectAsJson("lstFireRenewalRegistersReport", lstFireRenewalRegisters);
                }
                else if(filterRenewalData.classname == "MT")
                {
                    List<MotorRenewalRegister> lstMotorRenewalRegisters = new List<MotorRenewalRegister>();
                    lstMotorRenewalRegisters = GetMotorRenewalRegisterSql(filterRenewalData);
                    ViewBag.lstMotorRenewalRegisters = lstMotorRenewalRegisters;
                    HttpContext.Session.SetObjectAsJson("lstMotorRenewalRegistersReport", lstMotorRenewalRegisters);
                }
                else if(filterRenewalData.classname == "LF")
                {
                    List<HealthRenewalRegister> lstHealthenewalRegisters = new List<HealthRenewalRegister>();
                    lstHealthenewalRegisters = GetHealthRenewalRegisterSql(filterRenewalData);
                    ViewBag.lstHealthRenewalRegisters = lstHealthenewalRegisters;
                    HttpContext.Session.SetObjectAsJson("lstHealthRenewalRegistersReport", lstHealthenewalRegisters);
                }
                else if(filterRenewalData.classname == "MS")
                {
                    List<ParenewalRegister> lstPARenewalRegisters = new List<ParenewalRegister>();
                    lstPARenewalRegisters = GetPARenewalRegisterSql(filterRenewalData);
                    ViewBag.lstPARenewalRegisters = lstPARenewalRegisters;
                    HttpContext.Session.SetObjectAsJson("lstPARenewalRegistersReport", lstPARenewalRegisters);
                }
                else if(filterRenewalData.classname == "ENGG")
                {
                    List<EngineeringRenewalRegister> lstENGGRenewalRegisters = new List<EngineeringRenewalRegister>();
                    lstENGGRenewalRegisters = GetENGGRenewalRegisterSql(filterRenewalData);
                    ViewBag.lstENGGRenewalRegisters = lstENGGRenewalRegisters;
                    HttpContext.Session.SetObjectAsJson("lstENGGRenewalRegistersReport", lstENGGRenewalRegisters);
                }
                else if(filterRenewalData.classname == "PTY")
                {
                    List<PropertyRenewalRegister> lstPropertyRenewalRegisters = new List<PropertyRenewalRegister>();
                    lstPropertyRenewalRegisters = GetPropertyRenewalRegisterSql(filterRenewalData);
                    ViewBag.lstPropertyRenewalRegisters = lstPropertyRenewalRegisters;
                    HttpContext.Session.SetObjectAsJson("lstPropertyRegistersReport", lstPropertyRenewalRegisters);
                }

                return View("RenewalRegisterList", filterRenewalData);
            }
            catch (Exception ex)
            {
                return View("RenewalRegisterList", filterRenewalData);
            }
        }

        private List<MotorRenewalRegister> GetMotorRenewalRegisterSql(filterRenewalDataModel filterRenewalData)
        {
            List<MotorRenewalRegister> lstMotorRenewalRegisters = new List<MotorRenewalRegister>();
            try
            {
                lstMotorRenewalRegisters = dataService.GetMotorRenewalRegisterReport(filterRenewalData);
                return lstMotorRenewalRegisters;
            }
            catch (Exception ex)
            {
                return lstMotorRenewalRegisters;
            }
        }

        private List<FireRenewalRegister> GetFireRenewalRegisterSql(filterRenewalDataModel filterRenewalData)
        {
            List<FireRenewalRegister> lstFireRenewalRegisters = new List<FireRenewalRegister>();
            try
            {
                lstFireRenewalRegisters = dataService.GetFireRenewalRegisterDataReport(filterRenewalData);
                return lstFireRenewalRegisters;
            }
            catch (Exception ex)
            {
                return lstFireRenewalRegisters;
            }
        }

        private List<HealthRenewalRegister> GetHealthRenewalRegisterSql(filterRenewalDataModel filterRenewalData)
        {
            List<HealthRenewalRegister> lstHealthRenewalRegisters = new List<HealthRenewalRegister>();
            try
            {
                lstHealthRenewalRegisters = dataService.GetHealthRenewalRegisterDataReport(filterRenewalData);
                return lstHealthRenewalRegisters;
            }
            catch(Exception ex)
            {
                return lstHealthRenewalRegisters;
            }
        }

        private List<ParenewalRegister> GetPARenewalRegisterSql(filterRenewalDataModel filterRenewalData)
        {
            List<ParenewalRegister> lstPaRenewalRegisters = new List<ParenewalRegister>();
            try
            {
                lstPaRenewalRegisters = dataService.GetPARenewalRegisterDataReport(filterRenewalData);
                return lstPaRenewalRegisters;
            }
            catch(Exception ex)
            {
                return lstPaRenewalRegisters;
            }
        }

        private List<EngineeringRenewalRegister> GetENGGRenewalRegisterSql(filterRenewalDataModel filterRenewalData)
        {
            List<EngineeringRenewalRegister> lstENGGRenewalRegisters = new List<EngineeringRenewalRegister>();
            try
            {
                lstENGGRenewalRegisters = dataService.GetENGGRenewalRegisterDataReport(filterRenewalData);
                return lstENGGRenewalRegisters;
            }
            catch(Exception ex)
            {
                return lstENGGRenewalRegisters;
            }
        }

        private List<PropertyRenewalRegister> GetPropertyRenewalRegisterSql(filterRenewalDataModel filterRenewalData)
        {
            List<PropertyRenewalRegister> lstPropertyRenewalRegisters = new List<PropertyRenewalRegister>();
            try
            {
                lstPropertyRenewalRegisters = dataService.GetPropertyRenewalRegisterDataReport(filterRenewalData);
                return lstPropertyRenewalRegisters;
            }
            catch(Exception ex)
            {
                return lstPropertyRenewalRegisters;
            }
        }

        #region Excel Export
        public IActionResult ExportMotorRenewalRegisters()
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                List<MotorRenewalRegister> lstMotorRenewalRegisters = new List<MotorRenewalRegister>();
                lstMotorRenewalRegisters = HttpContext.Session.GetObjectFromJson<List<MotorRenewalRegister>>("lstMotorRenewalRegistersReport");

                var columnHeader = new List<string>
                {
                    "Customer Name",
                    "Phone Number",
                    "Approve Send SMS",
                    "UW Approval",
                    "Branch Authority",
                    "Agreed SI Amount",
                    "Invoice No",
                    "Policy Number",
                    "PersonInCharge",
                    "Responsible Party",
                    "Customer Code",
                    "Risk Name",
                    "Expired Date",
                    "From Date",
                    "To Date",
                    "Renewal Premium",
                    "Make",
                    "Model",
                    "Product",
                    "Branch",
                    "Account Code",
                    "Intermediary Type",
                    "Agent Code",
                    "Agent Name",
                    "Authorized Code",
                    "Authorized Name",
                    "Product Class",
                    "Currency",
                    "SumInsured",
                    "SI Increased Value",
                    "Renewal SI value",
                    "Previous Winscreen Value",
                    "Renewed Winscreen Value",
                    "Outstanding Amount",
                    "Excess Amount",
                    "Send SMS Date",
                    "Joint Member",
                    "Dominant Share Holder",
                    "Financial Interest",
                    "Branch Authority Date"
                };
                var fileContent = ExportExcelForMotorRenewalRegistersReport(lstMotorRenewalRegisters, columnHeader, "Motor Renewal Registers Report");
                return File(fileContent, "application/ms-excel", "MotorRenewalRegisters.xlsx");
            }
        }

        public byte[] ExportExcelForMotorRenewalRegistersReport(List<MotorRenewalRegister> lst, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 30;
                using (var cells = worksheet.Cells[1, 1, 1, 60])
                    cells.Style.Font.Bold = true;

                for (int i = 0; i < columnHeader.Count(); i++)
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];

                var j = 2;
                var count = 1;
                foreach (var item in lst)
                {
                    worksheet.Cells["A" + j].Value = item.CustomerName;
                    worksheet.Cells["B" + j].Value = item.PhoneNumber;
                    worksheet.Cells["C" + j].Value = item.ApproveSendSms;
                    worksheet.Cells["D" + j].Value = item.Uwapproval;
                    worksheet.Cells["E" + j].Value = item.BranchAuthority;
                    worksheet.Cells["F" + j].Value = string.Format("{0:n0}", item.AgreedSiamount);
                    worksheet.Cells["G" + j].Value = item.InvoiceNo;
                    worksheet.Cells["H" + j].Value = item.PolicyNo;
                    worksheet.Cells["I" + j].Value = item.PersonInCharge;
                    worksheet.Cells["J" + j].Value = item.ResponsibleParty;
                    worksheet.Cells["K" + j].Value = item.CusCode;
                    worksheet.Cells["L" + j].Value = item.RiskName;
                    worksheet.Cells["M" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ExpiryDate);
                    worksheet.Cells["N" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.FromDate);
                    worksheet.Cells["O" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ToDate);
                    worksheet.Cells["P" + j].Value = string.Format("{0:n0}", item.RenewalPremium);
                    worksheet.Cells["Q" + j].Value = item.Manufacturer;
                    worksheet.Cells["R" + j].Value = item.Car_Model;
                    worksheet.Cells["S" + j].Value = item.Product;
                    worksheet.Cells["T" + j].Value = item.Branch;
                    worksheet.Cells["U" + j].Value = item.AccountCode;
                    worksheet.Cells["V" + j].Value = item.IntermediaryType;
                    worksheet.Cells["W" + j].Value = item.AgentCode;
                    worksheet.Cells["X" + j].Value = item.AgentName;
                    worksheet.Cells["Y" + j].Value = item.AuthorizedCode;
                    worksheet.Cells["Z" + j].Value = item.AuthorizedName;
                    worksheet.Cells["AA" + j].Value = item.Class;
                    worksheet.Cells["AB" + j].Value = item.Currency;
                    worksheet.Cells["AC" + j].Value = string.Format("{0:n0}", item.SumInsured);
                    worksheet.Cells["AD" + j].Value = string.Format("{0:n0}", item.Sivalue20);
                    worksheet.Cells["AE" + j].Value = string.Format("{0:n0}", item.RenewedSivalue);
                    worksheet.Cells["AF" + j].Value = string.Format("{0:n0}", item.PreviousWindscreenValue);
                    worksheet.Cells["AG" + j].Value = string.Format("{0:n0}", item.RenewedWindscreenValue);
                    worksheet.Cells["AH" + j].Value = string.Format("{0:n0}", item.Outstanding_Amt);
                    worksheet.Cells["AI" + j].Value = string.Format("{0:n0}", item.Excess_Amt);
                    worksheet.Cells["AJ" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ApproveSendSmsdate);
                    worksheet.Cells["AK" + j].Value = item.JointMember;
                    worksheet.Cells["AL" + j].Value = item.DominantHolder;
                    worksheet.Cells["AM" + j].Value = item.Financial_Interest;
                    worksheet.Cells["AN" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.BranchAuthorityDate);
                    //using (ExcelRange Rng = worksheet.Cells[j, 1, j, 32])
                    //{
                    //    Rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    //    Rng.Style.Fill.BackgroundColor.SetColor(Color.Coral);
                    //}
                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        public IActionResult ExportFireRenewalRegisters()
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                List<FireRenewalRegister> lstFireRenewalRegisters = new List<FireRenewalRegister>();
                lstFireRenewalRegisters = HttpContext.Session.GetObjectFromJson<List<FireRenewalRegister>>("lstFireRenewalRegistersReport");

                var columnHeader = new List<string>
                {
                    "Customer Name",
                    "Phone Number",
                    "Approve Send SMS",
                    "UW Approval",
                    "Branch Authority",
                    "Agreed SI Amount",
                    "Invoice No",
                    "Policy Number",
                    "PersonInCharge",
                    "Responsible Party",
                    "Customer Code",
                    "Risk Name",
                    "Expired Date",
                    "From Date",
                    "To Date",
                    "Renewal Premium",
                    "Product",
                    "Branch",
                    "Account Code",
                    "Intermediary Type",
                    "Agent Code",
                    "Agent Name",
                    "Authorized Code",
                    "Authorized Name",
                    "Product Class",
                    "Currency",
                    "SumInsured",
                    "Detail Occupancy",
                    "Location Risk",
                    "Building Class",
                    "Loss Record",
                    "Coverage",
                    "Google Map/ Geo Coordinate",
                    "Number of ABC Type Extinguisher",
                    "Number of CO2 Type Extinguishers",
                    "Number of Other Extinguisher",
                    "Weight of Fire Extinguisher",
                    "No. of Fire Hose Reel",
                    "No. of Hydrant",
                    "Capacity of Fire Tank",
                    "Security Guards",
                    "Number of CCTV Camera",
                    "Sq M of the Building",
                    "Required No. of Extinguishers",
                    "Work in process if there is any",
                    "Send SMS Date",
                    "Joint Member",
                    "Dominant Share Holder",
                    "Number of Fire Tank",
                    "Type of Fire Tank",
                    "Outstanding Amount",
                    "Excess Amount",
                    "Financial Interest",
                    "Branch Authority Date"
                };
                var fileContent = ExportExcelForFireRenewalRegistersReport(lstFireRenewalRegisters, columnHeader, "Fire Renewal Registers Report");
                return File(fileContent, "application/ms-excel", "FireRenewalRegisters.xlsx");
            }
        }

        public byte[] ExportExcelForFireRenewalRegistersReport(List<FireRenewalRegister> lst, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 30;
                using (var cells = worksheet.Cells[1, 1, 1, 60])
                    cells.Style.Font.Bold = true;

                for (int i = 0; i < columnHeader.Count(); i++)
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];


                var j = 2;
                var count = 1;
                foreach (var item in lst)
                {
                    worksheet.Cells["A" + j].Value = item.CustomerName;
                    worksheet.Cells["B" + j].Value = item.PhoneNumber;
                    worksheet.Cells["C" + j].Value = item.ApproveSendSms;
                    worksheet.Cells["D" + j].Value = item.Uwapproval;
                    worksheet.Cells["E" + j].Value = item.BranchAuthority;
                    worksheet.Cells["F" + j].Value = string.Format("{0:n0}", item.AgreedSiamount);
                    worksheet.Cells["G" + j].Value = item.InvoiceNo;
                    worksheet.Cells["H" + j].Value = item.PolicyNo;
                    worksheet.Cells["I" + j].Value = item.PersonInCharge;
                    worksheet.Cells["J" + j].Value = item.ResponsibleParty;
                    worksheet.Cells["K" + j].Value = item.CusCode;
                    worksheet.Cells["L" + j].Value = item.RiskName;
                    worksheet.Cells["M" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ExpiryDate);
                    worksheet.Cells["N" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.FromDate);
                    worksheet.Cells["O" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ToDate);
                    worksheet.Cells["P" + j].Value = string.Format("{0:n0}", item.RenewalPremium);
                    worksheet.Cells["Q" + j].Value = item.Product;
                    worksheet.Cells["R" + j].Value = item.Branch;
                    worksheet.Cells["S" + j].Value = item.AccountCode;
                    worksheet.Cells["T" + j].Value = item.IntermediaryType;
                    worksheet.Cells["U" + j].Value = item.AgentCode;
                    worksheet.Cells["V" + j].Value = item.AgentName;
                    worksheet.Cells["W" + j].Value = item.AuthorizedCode;
                    worksheet.Cells["X" + j].Value = item.AuthorizedName;
                    worksheet.Cells["Y" + j].Value = item.Class;
                    worksheet.Cells["Z" + j].Value = item.Currency;
                    worksheet.Cells["AA" + j].Value = string.Format("{0:n0}", item.SumInsured);
                    worksheet.Cells["AB" + j].Value = string.Format("{0:n0}", item.DetailOccupancy);
                    worksheet.Cells["AC" + j].Value = string.Format("{0:n0}", item.LocationRisk);
                    worksheet.Cells["AD" + j].Value = string.Format("{0:n0}", item.BuildingClass);
                    worksheet.Cells["AE" + j].Value = item.LossRecord;
                    worksheet.Cells["AF" + j].Value = item.Coverage;
                    worksheet.Cells["AG" + j].Value = item.GoogleMap;
                    worksheet.Cells["AH" + j].Value = item.AbctypeExtinguisher;
                    worksheet.Cells["AI" + j].Value = item.Co2typeExtinguishers;
                    worksheet.Cells["AJ" + j].Value = item.OtherExtinguisher;
                    worksheet.Cells["AK" + j].Value = item.FireExtinguishersWeight;
                    worksheet.Cells["AL" + j].Value = item.NumberofFireHoseReel;
                    worksheet.Cells["AM" + j].Value = item.NumberofHydrant;
                    worksheet.Cells["AN" + j].Value = item.CapacityofFireTank;
                    worksheet.Cells["AO" + j].Value = item.SecurityGuards;
                    worksheet.Cells["AP" + j].Value = item.NumberofCctvcamera;
                    worksheet.Cells["AQ" + j].Value = item.SqMeterofBuilding;
                    worksheet.Cells["AR" + j].Value = item.RequiredExtinguishers;
                    worksheet.Cells["AS" + j].Value = item.WorkinProcess;
                    worksheet.Cells["AT" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ApproveSendSmsdate);
                    worksheet.Cells["AU" + j].Value = item.JointMember;
                    worksheet.Cells["AV" + j].Value = item.DominantHolder;
                    worksheet.Cells["AW" + j].Value = item.NumberofFireTank;
                    worksheet.Cells["AX" + j].Value = item.TypeofFireTank;
                    worksheet.Cells["AY" + j].Value = string.Format("{0:n0}", item.Outstanding_Amt);
                    worksheet.Cells["AZ" + j].Value = string.Format("{0:n0}", item.Excess_Amt);
                    worksheet.Cells["BA" + j].Value = item.Financial_Interest;
                    worksheet.Cells["BB" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.BranchAuthorityDate);
                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        public IActionResult ExportHealthRenewalRegisters()
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                List<HealthRenewalRegister> lstHealthRenewalRegisters = new List<HealthRenewalRegister>();
                lstHealthRenewalRegisters = HttpContext.Session.GetObjectFromJson<List<HealthRenewalRegister>>("lstHealthRenewalRegistersReport");

                var columnHeader = new List<string>
                {
                    "Customer Name",
                    "Phone Number",
                    "Approve Send SMS",
                    "UW Approval",
                    "Branch Authority",
                    "Agreed SI Amount",
                    "Invoice No",
                    "Policy Number",
                    "PersonInCharge",
                    "Responsible Party",
                    "Customer Code",
                    "Risk Name",
                    "Expired Date",
                    "From Date",
                    "To Date",
                    "Renewal Premium",
                    "Product",
                    "Branch",
                    "Account Code",
                    "Intermediary Type",
                    "Agent Code",
                    "Agent Name",
                    "Authorized Code",
                    "Authorized Name",
                    "Product Class",
                    "Installment Plan",
                    "Basic Coverage",
                    "Optional Coverage (1)",
                    "Optional Coverage (2)",
                    "NCB/Claim Loading",
                    "Outstanding Amount",
                    "Excess Amount",
                    "Financial Interest",
                    "Send SMS Date",
                    "Branch Authority Date"
                };
                var fileContent = ExportExcelForHealthRenewalRegistersReport(lstHealthRenewalRegisters, columnHeader, "Health Renewal Registers Report");
                return File(fileContent, "application/ms-excel", "HealthRenewalRegisters.xlsx");
            }
        }

        public byte[] ExportExcelForHealthRenewalRegistersReport(List<HealthRenewalRegister> lst, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 30;
                using (var cells = worksheet.Cells[1, 1, 1, 60])
                    cells.Style.Font.Bold = true;

                for (int i = 0; i < columnHeader.Count(); i++)
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];


                var j = 2;
                var count = 1;
                foreach (var item in lst)
                {
                    worksheet.Cells["A" + j].Value = item.CustomerName;
                    worksheet.Cells["B" + j].Value = item.PhoneNumber;
                    worksheet.Cells["C" + j].Value = item.ApproveSendSms;
                    worksheet.Cells["D" + j].Value = item.Uwapproval;
                    worksheet.Cells["E" + j].Value = item.BranchAuthority;
                    worksheet.Cells["F" + j].Value = string.Format("{0:n0}", item.AgreedSiamount);
                    worksheet.Cells["G" + j].Value = item.InvoiceNo;
                    worksheet.Cells["H" + j].Value = item.PolicyNo;
                    worksheet.Cells["I" + j].Value = item.PersonInCharge;
                    worksheet.Cells["J" + j].Value = item.ResponsibleParty;
                    worksheet.Cells["K" + j].Value = item.CusCode;
                    worksheet.Cells["L" + j].Value = item.RiskName;
                    worksheet.Cells["M" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ExpiryDate);
                    worksheet.Cells["N" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.FromDate);
                    worksheet.Cells["O" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ToDate);
                    worksheet.Cells["P" + j].Value = string.Format("{0:n0}", item.RenewalPremium);
                    worksheet.Cells["Q" + j].Value = item.Product;
                    worksheet.Cells["R" + j].Value = item.Branch;
                    worksheet.Cells["S" + j].Value = item.AccountCode;
                    worksheet.Cells["T" + j].Value = item.IntermediaryType;
                    worksheet.Cells["U" + j].Value = item.AgentCode;
                    worksheet.Cells["V" + j].Value = item.AgentName;
                    worksheet.Cells["W" + j].Value = item.AuthorizedCode;
                    worksheet.Cells["X" + j].Value = item.AuthorizedName;
                    worksheet.Cells["Y" + j].Value = item.Class;
                    worksheet.Cells["Z" + j].Value = item.InstallmentPlan;
                    worksheet.Cells["AA" + j].Value = item.BasicCoverage;
                    worksheet.Cells["AB" + j].Value = item.OptionalCoverage1;
                    worksheet.Cells["AC" + j].Value = item.OptionalCoverage2;
                    worksheet.Cells["AD" + j].Value = string.Format("{0:n0}", item.NcbclaimLoading);
                    worksheet.Cells["AE" + j].Value = string.Format("{0:n0}", item.Outstanding_Amt);
                    worksheet.Cells["AF" + j].Value = string.Format("{0:n0}", item.Excess_Amt);
                    worksheet.Cells["AG" + j].Value = item.Financial_Interest;
                    worksheet.Cells["AH" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ApproveSendSmsdate);
                    worksheet.Cells["AI" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.BranchAuthorityDate);
                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        public IActionResult ExportPARenewalRegisters()
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                List<ParenewalRegister> lstPARenewalRegisters = new List<ParenewalRegister>();
                lstPARenewalRegisters = HttpContext.Session.GetObjectFromJson<List<ParenewalRegister>>("lstPARenewalRegistersReport");

                var columnHeader = new List<string>
                {
                    "Customer Name",
                    "Phone Number",
                    "Approve Send SMS",
                    "UW Approval",
                    "Branch Authority",
                    "Agreed SI Amount",
                    "Invoice No",
                    "Policy Number",
                    "PersonInCharge",
                    "Responsible Party",
                    "Customer Code",
                    "Risk Name",
                    "Expired Date",
                    "From Date",
                    "To Date",
                    "Renewal Premium",
                    "Product",
                    "Branch",
                    "Account Code",
                    "Intermediary Type",
                    "Agent Code",
                    "Agent Name",
                    "Authorized Code",
                    "Authorized Name",
                    "Product Class",
                    "Installment Plan",
                    "Basic Coverage",
                    "Optional Coverage (1)",
                    "Optional Coverage (2)",
                    "NCB/Claim Loading",
                    "Outstanding Amount",
                    "Excess Amount",
                    "Financial Interest",
                    "Send SMS Date",
                    "Branch Authority Date"
                };
                var fileContent = ExportExcelForPARenewalRegistersReport(lstPARenewalRegisters, columnHeader, "PA Renewal Registers Report");
                return File(fileContent, "application/ms-excel", "PARenewalRegisters.xlsx");
            }
        }

        public byte[] ExportExcelForPARenewalRegistersReport(List<ParenewalRegister> lst, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 30;
                using (var cells = worksheet.Cells[1, 1, 1, 60])
                    cells.Style.Font.Bold = true;

                for (int i = 0; i < columnHeader.Count(); i++)
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];


                var j = 2;
                var count = 1;
                foreach (var item in lst)
                {
                    worksheet.Cells["A" + j].Value = item.CustomerName;
                    worksheet.Cells["B" + j].Value = item.PhoneNumber;
                    worksheet.Cells["C" + j].Value = item.ApproveSendSms;
                    worksheet.Cells["D" + j].Value = item.Uwapproval;
                    worksheet.Cells["E" + j].Value = item.BranchAuthority;
                    worksheet.Cells["F" + j].Value = string.Format("{0:n0}", item.AgreedSiamount);
                    worksheet.Cells["G" + j].Value = item.InvoiceNo;
                    worksheet.Cells["H" + j].Value = item.PolicyNo;
                    worksheet.Cells["I" + j].Value = item.PersonInCharge;
                    worksheet.Cells["J" + j].Value = item.ResponsibleParty;
                    worksheet.Cells["K" + j].Value = item.CusCode;
                    worksheet.Cells["L" + j].Value = item.RiskName;
                    worksheet.Cells["M" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ExpiryDate);
                    worksheet.Cells["N" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.FromDate);
                    worksheet.Cells["O" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ToDate);
                    worksheet.Cells["P" + j].Value = string.Format("{0:n0}", item.RenewalPremium);
                    worksheet.Cells["Q" + j].Value = item.Product;
                    worksheet.Cells["R" + j].Value = item.Branch;
                    worksheet.Cells["S" + j].Value = item.AccountCode;
                    worksheet.Cells["T" + j].Value = item.IntermediaryType;
                    worksheet.Cells["U" + j].Value = item.AgentCode;
                    worksheet.Cells["V" + j].Value = item.AgentName;
                    worksheet.Cells["W" + j].Value = item.AuthorizedCode;
                    worksheet.Cells["X" + j].Value = item.AuthorizedName;
                    worksheet.Cells["Y" + j].Value = item.Class;
                    worksheet.Cells["Z" + j].Value = item.InstallmentPlan;
                    worksheet.Cells["AA" + j].Value = item.BasicCoverage;
                    worksheet.Cells["AB" + j].Value = item.OptionalCoverage1;
                    worksheet.Cells["AC" + j].Value = item.OptionalCoverage2;
                    worksheet.Cells["AD" + j].Value = string.Format("{0:n0}", item.NcbclaimLoading);
                    worksheet.Cells["AE" + j].Value = string.Format("{0:n0}", item.Outstanding_Amt);
                    worksheet.Cells["AF" + j].Value = string.Format("{0:n0}", item.Excess_Amt);
                    worksheet.Cells["AG" + j].Value = item.Financial_Interest;
                    worksheet.Cells["AH" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ApproveSendSmsdate);
                    worksheet.Cells["AI" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.BranchAuthorityDate);
                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        public IActionResult ExportENGGRenewalRegisters()
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                List<EngineeringRenewalRegister> lstENGGRenewalRegisters = new List<EngineeringRenewalRegister>();
                lstENGGRenewalRegisters = HttpContext.Session.GetObjectFromJson<List<EngineeringRenewalRegister>>("lstENGGRenewalRegistersReport");

                var columnHeader = new List<string>
                {
                    "Customer Name",
                    "Phone Number",
                    "Approve Send SMS",
                    "UW Approval",
                    "Branch Authority",
                    "DMS Upload",
                    "Done/Pending",
                    "Agreed SI Amount",
                    "Invoice No",
                    "Policy Number",
                    "PersonInCharge",
                    "Responsible Party",
                    "Customer Code",
                    "Risk Name",
                    "Expired Date",
                    "From Date",
                    "To Date",
                    "Renewal Premium",
                    "Product",
                    "Branch",
                    "Account Code",
                    "Intermediary Type",
                    "Agent Code",
                    "Agent Name",
                    "Authorized Code",
                    "Authorized Name",
                    "Product Class",
                    "Currency",
                    "SumInsured",
                    "Detail Occupancy",
                    "Location Risk",
                    "Building Class",
                    "Loss Record",
                    "Coverage",
                    "Google Map/ Geo Coordinate",
                    "Number of ABC Type Extinguisher",
                    "Number of CO2 Type Extinguishers",
                    "Number of Other Extinguisher",
                    "Weight of Fire Extinguisher",
                    "No. of Fire Hose Reel",
                    "No. of Hydrant",
                    "No. of Fire Tank",
                    "Type of Fire Tank",
                    "Capacity of Fire Tank",
                    "Security Guards",
                    "Number of CCTV Camera",
                    "Sq M of the Building",
                    "Required No. of Extinguishers",
                    "Work in process if there is any",
                    "Outstanding Amount",
                    "Excess Amount",
                    "Financial Interest",
                    "Send SMS Date",
                    "Branch Authority Date"
                };
                var fileContent = ExportExcelForENGGRenewalRegistersReport(lstENGGRenewalRegisters, columnHeader, "Engineering Renewal Registers Report");
                return File(fileContent, "application/ms-excel", "EngineeringRenewalRegisters.xlsx");
            }
        }

        public byte[] ExportExcelForENGGRenewalRegistersReport(List<EngineeringRenewalRegister> lst, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 30;
                using (var cells = worksheet.Cells[1, 1, 1, 60])
                    cells.Style.Font.Bold = true;

                for (int i = 0; i < columnHeader.Count(); i++)
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];


                var j = 2;
                var count = 1;
                foreach (var item in lst)
                {
                    worksheet.Cells["A" + j].Value = item.CustomerName;
                    worksheet.Cells["B" + j].Value = item.PhoneNumber;
                    worksheet.Cells["C" + j].Value = item.ApproveSendSms;
                    worksheet.Cells["D" + j].Value = item.Uwapproval;
                    worksheet.Cells["E" + j].Value = item.BranchAuthority;
                    worksheet.Cells["F" + j].Value = item.Dmsupload;
                    worksheet.Cells["G" + j].Value = item.DonePending;
                    worksheet.Cells["H" + j].Value = string.Format("{0:n0}", item.AgreedSiamount);
                    worksheet.Cells["I" + j].Value = item.InvoiceNo;
                    worksheet.Cells["J" + j].Value = item.PolicyNo;
                    worksheet.Cells["K" + j].Value = item.PersonInCharge;
                    worksheet.Cells["L" + j].Value = item.ResponsibleParty;
                    worksheet.Cells["M" + j].Value = item.CusCode;
                    worksheet.Cells["N" + j].Value = item.RiskName;
                    worksheet.Cells["O" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ExpiryDate);
                    worksheet.Cells["P" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.FromDate);
                    worksheet.Cells["Q" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ToDate);
                    worksheet.Cells["R" + j].Value = string.Format("{0:n0}", item.RenewalPremium);
                    worksheet.Cells["S" + j].Value = item.Product;
                    worksheet.Cells["T" + j].Value = item.Branch;
                    worksheet.Cells["U" + j].Value = item.AccountCode;
                    worksheet.Cells["V" + j].Value = item.IntermediaryType;
                    worksheet.Cells["W" + j].Value = item.AgentCode;
                    worksheet.Cells["X" + j].Value = item.AgentName;
                    worksheet.Cells["Y" + j].Value = item.AuthorizedCode;
                    worksheet.Cells["Z" + j].Value = item.AuthorizedName;
                    worksheet.Cells["AA" + j].Value = item.Class;
                    worksheet.Cells["AB" + j].Value = item.Currency;
                    worksheet.Cells["AC" + j].Value = string.Format("{0:n0}", item.SumInsured);
                    worksheet.Cells["AD" + j].Value = string.Format("{0:n0}", item.DetailOccupancy);
                    worksheet.Cells["AE" + j].Value = string.Format("{0:n0}", item.LocationRisk);
                    worksheet.Cells["AF" + j].Value = string.Format("{0:n0}", item.BuildingClass);
                    worksheet.Cells["AG" + j].Value = item.LossRecord;
                    worksheet.Cells["AH" + j].Value = item.Coverage;
                    worksheet.Cells["AI" + j].Value = item.GoogleMap;
                    worksheet.Cells["AJ" + j].Value = item.AbctypeExtinguisher;
                    worksheet.Cells["AK" + j].Value = item.Co2typeExtinguishers;
                    worksheet.Cells["AL" + j].Value = item.OtherExtinguisher;
                    worksheet.Cells["AM" + j].Value = item.FireExtinguishersWeight;
                    worksheet.Cells["AN" + j].Value = item.NumberofFireHoseReel;
                    worksheet.Cells["AO" + j].Value = item.NumberofHydrant;
                    worksheet.Cells["AP" + j].Value = item.NumberofFireTank;
                    worksheet.Cells["AQ" + j].Value = item.TypeofFireTank;
                    worksheet.Cells["AR" + j].Value = item.CapacityofFireTank;
                    worksheet.Cells["AS" + j].Value = item.SecurityGuards;
                    worksheet.Cells["AT" + j].Value = item.NumberofCctvcamera;
                    worksheet.Cells["AU" + j].Value = item.SqMeterofBuilding;
                    worksheet.Cells["AV" + j].Value = item.RequiredExtinguishers;
                    worksheet.Cells["AW" + j].Value = item.WorkinProcess;
                    worksheet.Cells["AX" + j].Value = string.Format("{0:n0}", item.Outstanding_Amt);
                    worksheet.Cells["AY" + j].Value = string.Format("{0:n0}", item.Excess_Amt);
                    worksheet.Cells["AZ" + j].Value = item.Financial_Interest;
                    worksheet.Cells["BA" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ApproveSendSmsdate);
                    worksheet.Cells["BB" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.BranchAuthorityDate);
                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        public IActionResult ExportPropertyRenewalRegisters()
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                List<PropertyRenewalRegister> lstPropertyRenewalRegisters = new List<PropertyRenewalRegister>();
                lstPropertyRenewalRegisters = HttpContext.Session.GetObjectFromJson<List<PropertyRenewalRegister>>("lstPropertyRegistersReport");

                var columnHeader = new List<string>
                {
                    "Customer Name",
                    "Phone Number",
                    "Approve Send SMS",
                    "UW Approval",
                    "Branch Authority",
                    "DMS Upload",
                    "Done/Pending",
                    "Agreed SI Amount",
                    "Invoice No",
                    "Policy Number",
                    "PersonInCharge",
                    "Responsible Party",
                    "Customer Code",
                    "Risk Name",
                    "Expired Date",
                    "From Date",
                    "To Date",
                    "Renewal Premium",
                    "Product",
                    "Branch",
                    "Account Code",
                    "Intermediary Type",
                    "Agent Code",
                    "Agent Name",
                    "Authorized Code",
                    "Authorized Name",
                    "Product Class",
                    "Currency",
                    "SumInsured",
                    "Detail Occupancy",
                    "Location Risk",
                    "Building Class",
                    "Loss Record",
                    "Coverage",
                    "Google Map/ Geo Coordinate",
                    "Number of ABC Type Extinguisher",
                    "Number of CO2 Type Extinguishers",
                    "Number of Other Extinguisher",
                    "Weight of Fire Extinguisher",
                    "No. of Fire Hose Reel",
                    "No. of Hydrant",
                    "No. of Fire Tank",
                    "Type of Fire Tank",
                    "Capacity of Fire Tank",
                    "Security Guards",
                    "Number of CCTV Camera",
                    "Sq M of the Building",
                    "Required No. of Extinguishers",
                    "Work in process if there is any",
                    "Outstanding Amount",
                    "Excess Amount",
                    "Financial Interest",
                    "Send SMS Date",
                    "Branch Authority Date"
                };
                var fileContent = ExportExcelForPropertyRenewalRegistersReport(lstPropertyRenewalRegisters, columnHeader, "Property Renewal Registers Report");
                return File(fileContent, "application/ms-excel", "PropertyRenewalRegisters.xlsx");
            }
        }

        public byte[] ExportExcelForPropertyRenewalRegistersReport(List<PropertyRenewalRegister> lst, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 30;
                using (var cells = worksheet.Cells[1, 1, 1, 60])
                    cells.Style.Font.Bold = true;

                for (int i = 0; i < columnHeader.Count(); i++)
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];


                var j = 2;
                var count = 1;
                foreach (var item in lst)
                {
                    worksheet.Cells["A" + j].Value = item.CustomerName;
                    worksheet.Cells["B" + j].Value = item.PhoneNumber;
                    worksheet.Cells["C" + j].Value = item.ApproveSendSms;
                    worksheet.Cells["D" + j].Value = item.Uwapproval;
                    worksheet.Cells["E" + j].Value = item.BranchAuthority;
                    worksheet.Cells["F" + j].Value = item.Dmsupload;
                    worksheet.Cells["G" + j].Value = item.DonePending;
                    worksheet.Cells["H" + j].Value = string.Format("{0:n0}", item.AgreedSiamount);
                    worksheet.Cells["I" + j].Value = item.InvoiceNo;
                    worksheet.Cells["J" + j].Value = item.PolicyNo;
                    worksheet.Cells["K" + j].Value = item.PersonInCharge;
                    worksheet.Cells["L" + j].Value = item.ResponsibleParty;
                    worksheet.Cells["M" + j].Value = item.CusCode;
                    worksheet.Cells["N" + j].Value = item.RiskName;
                    worksheet.Cells["O" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ExpiryDate);
                    worksheet.Cells["P" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.FromDate);
                    worksheet.Cells["Q" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ToDate);
                    worksheet.Cells["R" + j].Value = string.Format("{0:n0}", item.RenewalPremium);
                    worksheet.Cells["S" + j].Value = item.Product;
                    worksheet.Cells["T" + j].Value = item.Branch;
                    worksheet.Cells["U" + j].Value = item.AccountCode;
                    worksheet.Cells["V" + j].Value = item.IntermediaryType;
                    worksheet.Cells["W" + j].Value = item.AgentCode;
                    worksheet.Cells["X" + j].Value = item.AgentName;
                    worksheet.Cells["Y" + j].Value = item.AuthorizedCode;
                    worksheet.Cells["Z" + j].Value = item.AuthorizedName;
                    worksheet.Cells["AA" + j].Value = item.Class;
                    worksheet.Cells["AB" + j].Value = item.Currency;
                    worksheet.Cells["AC" + j].Value = string.Format("{0:n0}", item.SumInsured);
                    worksheet.Cells["AD" + j].Value = string.Format("{0:n0}", item.DetailOccupancy);
                    worksheet.Cells["AE" + j].Value = string.Format("{0:n0}", item.LocationRisk);
                    worksheet.Cells["AF" + j].Value = string.Format("{0:n0}", item.BuildingClass);
                    worksheet.Cells["AG" + j].Value = item.LossRecord;
                    worksheet.Cells["AH" + j].Value = item.Coverage;
                    worksheet.Cells["AI" + j].Value = item.GoogleMap;
                    worksheet.Cells["AJ" + j].Value = item.AbctypeExtinguisher;
                    worksheet.Cells["AK" + j].Value = item.Co2typeExtinguishers;
                    worksheet.Cells["AL" + j].Value = item.OtherExtinguisher;
                    worksheet.Cells["AM" + j].Value = item.FireExtinguishersWeight;
                    worksheet.Cells["AN" + j].Value = item.NumberofFireHoseReel;
                    worksheet.Cells["AO" + j].Value = item.NumberofHydrant;
                    worksheet.Cells["AP" + j].Value = item.NumberofFireTank;
                    worksheet.Cells["AQ" + j].Value = item.TypeofFireTank;
                    worksheet.Cells["AR" + j].Value = item.CapacityofFireTank;
                    worksheet.Cells["AS" + j].Value = item.SecurityGuards;
                    worksheet.Cells["AT" + j].Value = item.NumberofCctvcamera;
                    worksheet.Cells["AU" + j].Value = item.SqMeterofBuilding;
                    worksheet.Cells["AV" + j].Value = item.RequiredExtinguishers;
                    worksheet.Cells["AW" + j].Value = item.WorkinProcess;
                    worksheet.Cells["AX" + j].Value = string.Format("{0:n0}", item.Outstanding_Amt);
                    worksheet.Cells["AY" + j].Value = string.Format("{0:n0}", item.Excess_Amt);
                    worksheet.Cells["AZ" + j].Value = item.Financial_Interest;
                    worksheet.Cells["BA" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ApproveSendSmsdate);
                    worksheet.Cells["BB" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.BranchAuthorityDate);
                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        #endregion

        #endregion

        #region Policy Receipt Template Report

        public IActionResult PolicyReceiptTemplateReport()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            CoreInfo model = new CoreInfo();
            PrepareDataForReceiptCoreReport();
            return View(model);
        }

        //Remove AccountCode
        //public CoreInfo CoreInfoReceiptData(CoreInfo info)
        //{
        //    CoreInfo output = new CoreInfo();

        //    List<CoreInfo> coreInfos = new List<CoreInfo>();
        //    _session = HttpContext.Session;
        //    _session.SetString("From", info.from);
        //    _session.SetString("To", info.to);

        //    if (info.ACC_CODE_LIST != null)
        //    {
        //        foreach (string acclist in info.ACC_CODE_LIST)
        //        {
        //            info.Account_Code += acclist + ",";
        //        }
        //        info.Account_Code = info.Account_Code.TrimEnd(',', ' ');
        //    }

        //    if (info.ClassLIST != null)
        //    {
        //        foreach (string cla in info.ClassLIST)
        //        {
        //            info.Class += cla + ",";
        //        }
        //        info.Class = info.Class.TrimEnd(',', ' ');
        //    }

        //    if (info.ProductLIST != null)
        //    {
        //        foreach (string product in info.ProductLIST)
        //        {
        //            info.Product += product + ",";
        //        }
        //        info.Product = info.Product.TrimEnd(',', ' ');
        //    }

        //    output.CategoryId = info.CategoryId;
        //    output.Category = _db.Category.Where(c => c.Id == info.CategoryId).Select(c => c.Description).FirstOrDefault();

        //    var responseTask = _client.PostAsJsonAsync<CoreInfo>($"/CoreReport/GetPolicyReceiptTemplate", info);
        //    responseTask.Wait();
        //    var message = responseTask.Result;
        //    message.EnsureSuccessStatusCode();
        //    if (message.IsSuccessStatusCode)
        //    {
        //        _session = HttpContext.Session;
        //        var jsonData = message.Content.ReadAsStringAsync();
        //        List<CoreInfo> lstreportinfo = new List<CoreInfo>();
        //        lstreportinfo = JsonConvert.DeserializeObject<List<CoreInfo>>(jsonData.Result);

        //        List<CoreInfo> lstCoreInfos = new List<CoreInfo>();
        //        foreach (CoreInfo reportinfo in lstreportinfo)
        //        {
        //            string cleartext = "customercode=" + reportinfo.Cus_Code + "&policyno=" + reportinfo.Policy_No + "&customerphoneno=" + reportinfo.Phone_No;
        //            string encode = Encrypt(cleartext);

        //            if (output.Category == "New Business CSAT(EN)")
        //            {
        //                CoreAccountCode coreAccountCode = new CoreAccountCode();
        //                coreAccountCode = _db.CoreAccountCode.Where(m => m.AccountCode == reportinfo.Account_Code).FirstOrDefault();
        //                if (coreAccountCode == null)
        //                {
        //                    //For Live
        //                    //reportinfo.Link = @"https://cxsurvey.ayasompo.com/survey/newbusiness?source=" + encode;
        //                    //string shortlink = Create(reportinfo.Link, @"https://cxsurvey.ayasompo.com/link/qkw?pa=");

        //                    #region For UAT
        //                    reportinfo.Link = @"https://uatsurvey.ayasompo.com:9012/survey/newbusiness?source=" + encode;
        //                    string shortlink = Create(reportinfo.Link, @"https://uatsurvey.ayasompo.com:9012/link/QKW?pa=");
        //                    #endregion

        //                    reportinfo.Link = shortlink;

        //                    lstCoreInfos.Add(reportinfo);
        //                }
        //            }
        //        }
        //        output.lst = lstCoreInfos;

        //        HttpContext.Session.SetComplexData("lstPolicyTemplateList", lstCoreInfos);
        //    }
        //    output.from = info.from;
        //    output.to = info.to;
        //    output.Class = info.Class;
        //    output.Product = info.Product;
        //    //output.Expiry_Date = Info.Expiry_Date;
        //    if (info.Transaction_type == "N")
        //    {
        //        output.Transaction_type = "New Business";
        //    }
        //    else if (info.Transaction_type == "R")
        //    {
        //        output.Transaction_type = "Renewal";
        //    }

        //    return output;
        //}

        public CoreInfo CoreInfoReceiptData(CoreInfo info)
        {
            CoreInfo output = new CoreInfo();

            List<CoreInfo> coreInfos = new List<CoreInfo>();
            _session = HttpContext.Session;
            _session.SetString("From", info.from);
            _session.SetString("To", info.to);

            if (info.ACC_CODE_LIST != null)
            {
                foreach (string acclist in info.ACC_CODE_LIST)
                {
                    info.Account_Code += acclist + ",";
                }
                info.Account_Code = info.Account_Code.TrimEnd(',', ' ');
            }

            if (info.ClassLIST != null)
            {
                foreach (string cla in info.ClassLIST)
                {
                    info.Class += cla + ",";
                }
                info.Class = info.Class.TrimEnd(',', ' ');
            }

            if (info.ProductLIST != null)
            {
                foreach (string product in info.ProductLIST)
                {
                    info.Product += product + ",";
                }
                info.Product = info.Product.TrimEnd(',', ' ');
            }

            output.CategoryId = info.CategoryId;
            output.Category = _db.Category.Where(c => c.Id == info.CategoryId).Select(c => c.Description).FirstOrDefault();

            var responseTask = _client.PostAsJsonAsync<CoreInfo>($"/CoreReport/GetPolicyReceiptTemplate", info);
            responseTask.Wait();
            var message = responseTask.Result;
            message.EnsureSuccessStatusCode();
            if (message.IsSuccessStatusCode)
            {
                _session = HttpContext.Session;
                var jsonData = message.Content.ReadAsStringAsync();
                List<CoreInfo> lstreportinfo = new List<CoreInfo>();
                lstreportinfo = JsonConvert.DeserializeObject<List<CoreInfo>>(jsonData.Result);

                foreach (CoreInfo reportinfo in lstreportinfo)
                {
                    string cleartext = "customercode=" + reportinfo.Cus_Code + "&policyno=" + reportinfo.Policy_No + "&customerphoneno=" + reportinfo.Phone_No;
                    string encode = Encrypt(cleartext);

                    if (output.Category == "New Business CSAT(EN)")
                    {
                        //For Live
                        reportinfo.Link = @"https://cxsurvey.ayasompo.com/survey/newbusiness?source=" + encode;
                        string shortlink = Create(reportinfo.Link, @"https://cxsurvey.ayasompo.com/link/qkw?pa=");

                        #region For UAT
                        //reportinfo.Link = @"https://uatsurvey.ayasompo.com:9012/survey/newbusiness?source=" + encode;
                        //string shortlink = Create(reportinfo.Link, @"https://uatsurvey.ayasompo.com:9012/link/QKW?pa=");
                        #endregion

                        #region UAT-Link

                        //reportinfo.Link = @"https://itravel.ayasompo.com/survey/newbusiness?source=" + encode;
                        //string shortlink = Create(reportinfo.Link, @"https://survey.ayasompo.com/link/qkw?pa=");


                        //string shortlink = Create(reportinfo.Link, @"http://localhost:53086/link/QKW?pa="); //local
                        #endregion

                        reportinfo.Link = shortlink;

                    }
                    //else if (output.Category == "Renewal CSAT(EN)")
                    //{
                    //    //For Live
                    //    //reportinfo.Link = @"https://cxsurvey.ayasompo.com/survey/renewal?source=" + encode;
                    //    //string shortlink = Create(reportinfo.Link, @"https://cxsurvey.ayasompo.com/link/qkw?pa=");

                    //    #region For UAT
                    //    reportinfo.Link = @"https://uatsurvey.ayasompo.com:9012/survey/renewal?source=" + encode;
                    //    string shortlink = Create(reportinfo.Link, @"https://uatsurvey.ayasompo.com:9012/link/QKW?pa="); //uat
                    //    #endregion

                    //    #region UAT-Link
                    //    //reportinfo.Link = @"https://itravel.ayasompo.com/survey/renewal?source=" + encode;
                    //    //string shortlink = Create(reportinfo.Link, @"https://survey.ayasompo.com/link/qkw?pa=");


                    //    //string shortlink = Create(reportinfo.Link, @"http://localhost:53086/link/QKW?pa="); //local
                    //    #endregion

                    //    reportinfo.Link = shortlink;
                    //}
                    //else if (output.Category == "Non-renewal CSAT(EN)")
                    //{
                    //    //For Live
                    //    //reportinfo.Link = @"https://cxsurvey.ayasompo.com/survey/nonrenewal?source=" + encode;
                    //    //string shortlink = Create(reportinfo.Link, @"https://cxsurvey.ayasompo.com/link/qkw?pa="); //Live

                    //    #region For UAT
                    //    reportinfo.Link = @"https://uatsurvey.ayasompo.com:9012/survey/nonrenewal?source=" + encode;
                    //    string shortlink = Create(reportinfo.Link, @"https://uatsurvey.ayasompo.com:9012/link/QKW?pa="); //uat
                    //    #endregion

                    //    #region UAR-Link
                    //    //reportinfo.Link = @"https://itravel.ayasompo.com/survey/nonrenewal?source=" + encode;
                    //    //string shortlink = Create(reportinfo.Link, @"https://survey.ayasompo.com/link/qkw?pa="); //Live
                    //    //string shortlink = Create(reportinfo.Link, @"http://localhost:53086/link/QKW?pa="); //local
                    //    #endregion

                    //    reportinfo.Link = shortlink;
                    //}

                }
                output.lst = lstreportinfo;

                HttpContext.Session.SetComplexData("lstPolicyTemplateList", lstreportinfo);
            }
            output.from = info.from;
            output.to = info.to;
            output.Class = info.Class;
            output.Product = info.Product;
            //output.Expiry_Date = Info.Expiry_Date;
            if (info.Transaction_type == "N")
            {
                output.Transaction_type = "New Business";
            }
            else if (info.Transaction_type == "R")
            {
                output.Transaction_type = "Renewal";
            }

            return output;
        }

        //[HttpPost]
        public IActionResult PolicyReceiptTemplateReportData(CoreInfo model)
        {
            CoreInfo output = new CoreInfo();
            output = CoreInfoReceiptData(model);
            return View(output);
        }

        public IActionResult ExportPolicyReceiptTemplateReport()
        {
            List<CoreInfo> data = new List<CoreInfo>();
            data = HttpContext.Session.GetComplexData<List<CoreInfo>>("lstPolicyTemplateList");
            var columnHeader = new List<string>
            {
                "Phone Number",
                "Customer Name",
                "Invoice No",
                "Risk Name",
                "Expiry Date",
                "Renewal Premium",
                "Product",
                "From Date",
                "To Date",
                "Link",
                "Cus Code",
                "Month",
                "For Person in charge(PIC)",
                "Responsible Party",
                "Policy No",
                "Branch",
                "Intermediary Type",
                "Agent Code",
                "Agent Name",
                "Account Code",
                "Authorized Code",
                "Authorized Name"
            };

            var fileContent = ExportExcelPolicyReceiptTemplate(data, columnHeader, "Sheet1");
            return File(fileContent, "application/ms-excel", "PolicyReceiptTemplateReport.xlsx");
        }

        public byte[] ExportExcelPolicyReceiptTemplate(List<CoreInfo> VM, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 20;


                using (var cells = worksheet.Cells[1, 1, 1, 22])
                    cells.Style.Font.Bold = true;

                for (int i = 0; i < columnHeader.Count(); i++)
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];

                var j = 2;
                var count = 1;
                foreach (var item in VM)
                {

                    //worksheet.Cells["A" + j].Style.Numberformat.Format = "#";

                    worksheet.Cells["A" + j].Value = item.Phone_No;
                    worksheet.Cells["B" + j].Value = item.Cus_Name;
                    worksheet.Cells["C" + j].Value = item.Invoice_No;
                    worksheet.Cells["D" + j].Value = item.Vehicle_No;
                    worksheet.Cells["E" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.Expiry_Date);
                    if (item.Premium != null)
                    {
                        worksheet.Cells["F" + j].Value = Convert.ToDecimal(item.Premium).ToString("#,##0");
                    }
                    else
                    {
                        worksheet.Cells["F" + j].Value = item.Premium;
                    }
                    worksheet.Cells["G" + j].Value = item.Class;
                    worksheet.Cells["H" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.Period_From);
                    worksheet.Cells["I" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.Period_To);
                    worksheet.Cells["J" + j].Value = item.Link;
                    worksheet.Cells["K" + j].Value = item.Cus_Code;
                    worksheet.Cells["L" + j].Value = "";
                    worksheet.Cells["M" + j].Value = "";
                    worksheet.Cells["N" + j].Value = "";
                    worksheet.Cells["O" + j].Value = item.Policy_No;
                    worksheet.Cells["P" + j].Value = item.Branch;
                    worksheet.Cells["Q" + j].Value = item.Intermediary_Type;
                    worksheet.Cells["R" + j].Value = item.Agent_Code;
                    worksheet.Cells["S" + j].Value = item.Agent_Name;
                    worksheet.Cells["T" + j].Value = item.Account_Code;
                    worksheet.Cells["U" + j].Value = item.Authorized_code;
                    worksheet.Cells["V" + j].Value = item.Authorized_name;

                    j++;
                    count++;
                }


                result = package.GetAsByteArray();
            }
            return result;
        }

        private void PrepareDataForReceiptCoreReport()
        {
            ViewBag.CategoryList = new SelectList(_db.Category.OrderBy(r => r.Description).ToList(), "Id", "Description");
            ViewBag.ClassList = new SelectList(_db.ProductClass.OrderBy(r => r.ClassCode).ToList(), "ClassName", "ClassName");
            AccountCodeList();

            List<SelectListItem> Product = new List<SelectListItem>();
            {
                Product.Add(new SelectListItem { Text = "Comprehensive Dual Purpose Car", Value = "MCH" });
                Product.Add(new SelectListItem { Text = "Comprehensive Commercial Fleet", Value = "MFC" });
                Product.Add(new SelectListItem { Text = "Comprehensive Private Fleet", Value = "MFP" });
                Product.Add(new SelectListItem { Text = "Comprehensive Commercial Car", Value = "MCC" });
                Product.Add(new SelectListItem { Text = "Comprehensive Private Car", Value = "MCP" });
                Product.Add(new SelectListItem { Text = "Comprehensive Private Car (USD)", Value = "MUP" });
                Product.Add(new SelectListItem { Text = "Comprehensive Dual Purpose Car (USD)", Value = "MUH" });
                Product.Add(new SelectListItem { Text = "Comprehensive Commercial Car (USD)", Value = "MUC" });
                Product.Add(new SelectListItem { Text = "Contractor's_all_risks_munichre", Value = "ECA" });
                Product.Add(new SelectListItem { Text = "Erection_all_risks_munichre", Value = "EEA" });
                Product.Add(new SelectListItem { Text = "Fire Insurance", Value = "FFI" });
                Product.Add(new SelectListItem { Text = "Fidelity Guarantee Insurance", Value = "FFG" });
                Product.Add(new SelectListItem { Text = "Cash In Transit Insurance", Value = "FCT" });
                Product.Add(new SelectListItem { Text = "Stock Declaration", Value = "FSD" });
                Product.Add(new SelectListItem { Text = "Cash In Safe Insurance", Value = "FCS" });
                Product.Add(new SelectListItem { Text = "Inactive Product - Travel_local - Individual", Value = "T20" });
                Product.Add(new SelectListItem { Text = "Micro Health Insurance", Value = "LHM" });
                Product.Add(new SelectListItem { Text = "Inactive Product - Sportsman Insurance", Value = "LSP" });
                Product.Add(new SelectListItem { Text = "Critical Illness Insurance", Value = "LHC" });
                Product.Add(new SelectListItem { Text = "Carrier Liability", Value = "LCL" });
                Product.Add(new SelectListItem { Text = "Employer's_liability", Value = "LEM" });
                Product.Add(new SelectListItem { Text = "Transport Operator's Liability", Value = "LTO" });
                Product.Add(new SelectListItem { Text = "Bailee's / Bailees's And Warehousemens' Liability", Value = "LBL" });
                Product.Add(new SelectListItem { Text = "Public Liability", Value = "LPC" });
                Product.Add(new SelectListItem { Text = "Product_liability", Value = "LPD" });
                Product.Add(new SelectListItem { Text = "Freight_forwarder_liability", Value = "LFF" });
                Product.Add(new SelectListItem { Text = "Third_party_liability", Value = "LTP" });
                Product.Add(new SelectListItem { Text = "Marine Cargo", Value = "MCG" });
                Product.Add(new SelectListItem { Text = "Marine Hull Insurance(Wooden)", Value = "MHW" });
                Product.Add(new SelectListItem { Text = "Oversea Marine Cargo Insurance (Ocean)", Value = "MCO" });
                Product.Add(new SelectListItem { Text = "Inland Transit Insurance", Value = "MIT" });
                Product.Add(new SelectListItem { Text = "Oversea Marine Cargo Insurance (Air)", Value = "MCA" });
                Product.Add(new SelectListItem { Text = "Marine Hull Insurance(Steel)", Value = "MHS" });
                Product.Add(new SelectListItem { Text = "Personal Accident Insurance", Value = "MPA" });
                Product.Add(new SelectListItem { Text = "Motor Cycle", Value = "MTC" });
                Product.Add(new SelectListItem { Text = "Property All Risk", Value = "PAR" });
                Product.Add(new SelectListItem { Text = "Industrial All Risks", Value = "PIA" });
                Product.Add(new SelectListItem { Text = "Express Coach Travel Insurance", Value = "TEC" });
            };

            List<SelectListItem> Filter = new List<SelectListItem>();
            {
                Filter.Add(new SelectListItem { Text = "Period To", Value = "periodto" });
                Filter.Add(new SelectListItem { Text = "Policy Date", Value = "poldate" });
                Filter.Add(new SelectListItem { Text = "Receipt Date", Value = "receiptdate" });
            };

            List<SelectListItem> TransactionType = new List<SelectListItem>();
            {
                TransactionType.Add(new SelectListItem { Text = "New Business", Value = "N" });
                TransactionType.Add(new SelectListItem { Text = "Renewal", Value = "R" });
                TransactionType.Add(new SelectListItem { Text = "Additional Endorsement", Value = "A" });
                TransactionType.Add(new SelectListItem { Text = "Special Endorsement", Value = "S" });
            };

            List<SelectListItem> YNConditon = new List<SelectListItem>();
            {
                YNConditon.Add(new SelectListItem { Text = "Yes", Value = "YES" });
                YNConditon.Add(new SelectListItem { Text = "No", Value = "NO" });
            };

            ViewBag.ProductList = Product;
            ViewBag.Filter = Filter;
            ViewBag.TransactionType = TransactionType;
            ViewBag.YNConditon = YNConditon;
        }

        public void AccountCodeList()
        {
            var accountcodeList = HttpContext.Session.GetComplexData<List<CoreInfo>>("ACCOUNTCODELIST");
            if (accountcodeList == null)
            {
                var responseTask = _client.GetAsync($"/CoreReport/GetAccountCode");
                responseTask.Wait();
                var message = responseTask.Result;
                var jsonData = message.Content.ReadAsStringAsync();
                accountcodeList = JsonConvert.DeserializeObject<List<CoreInfo>>(jsonData.Result);
                HttpContext.Session.SetComplexData("ACCOUNTCODELIST", accountcodeList);
            }
            ViewBag.AccountCodeList = new SelectList(accountcodeList, "SFC_ACC_CODE", "SFC_ACC_CODE");
        }

        public static string Encrypt(string clearText)
        {
            string Encryptkey = "65777383"; // ASCII code for AMIS 
            string EncryptionKey = Encryptkey;
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        public static string Decrypt(string cipherText)
        {
            //string Encryptkey = "65777383"; // ASCII code for AMIS
            string Encryptkey = "65777383"; // ASCII code for AMIS

            string EncryptionKey = Encryptkey;
            cipherText = cipherText.Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        [ValidateAntiForgeryToken]
        public string Create(string originalUrl, string shortlink)
        {
            if (ModelState.IsValid)
            {
                int Id = _Surveydb.ShortUrls.Select(x => x.Id).LastOrDefault();
                Id++;
                string link = string.Empty;

                link = Encode(Id);
                link = shortlink + link;

                var shortUrls = new ShortUrls
                {
                    OriginalUrl = originalUrl,
                    ShortUrl = link,
                    Id = Id
                };

                _Surveydb.Entry(shortUrls).State = EntityState.Added;
                _Surveydb.SaveChanges();
                return link;
            }
            else
            {
                return "";
            }
        }

        public static string Encode(int num)
        {
            //num = 123456789;
            var sb = new StringBuilder();
            while (num > 0)
            {

                sb.Insert(0, Alphabet.ElementAt(num % Base));
                num = num / Base;
            }
            return sb.ToString();
        }

        #endregion

        #region Renewal Decline FailCase
        public IActionResult RenewalDeclineFailCase()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            ViewBag.StaffList = new SelectList(_db.SysUser.Where(s => s.IsActive == true).OrderBy(d => d.Name).ToList(), "Id", "Name");
            return View();
        }

        private void GetRenwalDeclineFailSms()
        {
            try
            {
                SentSmsVM output = new SentSmsVM();

                output.lst = (from s in _db.SentSms
                              join c in _db.Category on s.CategoryId equals c.Id
                              where s.CategoryId == Guid.Parse("73C0DC6C-1249-4A8D-8D43-DA525A11327C")
                              where s.ActivitiesFlag == false
                              orderby s.SentDate descending
                              select new SentSmsVM
                              {
                                  Id = s.Id,
                                  PhoneNumber = s.PhoneNumber,
                                  UserName = s.UserName,
                                  CusCode = s.CusCode,
                                  PolicyNo = s.PolicyNo,
                                  VehicleNo = s.VehicleNo,
                                  Message = s.Message,
                                  SentDate = s.SentDate,
                                  Category = c.Description
                              }).Distinct().ToList();
                ViewBag.RenewalDeclineFailList = output.lst;

                HttpContext.Session.SetObjectAsJson("lstRenewalDeclineSms", null);
                HttpContext.Session.SetObjectAsJson("lstRenewalDeclineSms", output.lst);
            }
            catch (Exception ex)
            {

            }
        }

        public IActionResult GetRenewalDeclineFailCase(SentSmsVM model)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");
            try
            {
                List<SentSmsVM> lstRenewalDeclineFailSms = new List<SentSmsVM>();

                if (model.fromDate == null && model.toDate == null)
                {
                    return RedirectToAction("GetRenwalDeclineFailSms");
                }
                else
                {
                    lstRenewalDeclineFailSms = GetRenewalDeclineFailSmsByStaffSql(model);
                }
                ViewBag.RenewalDeclineFailList = lstRenewalDeclineFailSms;

                ViewBag.StaffList = new SelectList(_db.SysUser.Where(s => s.IsActive == true).OrderBy(d => d.Name).ToList(), "Id", "Name");

                HttpContext.Session.SetObjectAsJson("lstRenewalDeclineSms", null);
                HttpContext.Session.SetObjectAsJson("lstRenewalDeclineSms", lstRenewalDeclineFailSms);
                return View("RenewalDeclineFailCase");
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Invalid Request.";
                return View("RenewalDeclineFailCase");
            }
        }

        public List<SentSmsVM> GetRenewalDeclineFailSmsByStaffSql(SentSmsVM filterData)
        {
            List<SentSmsVM> lstRenewalDeclineFailSms = new List<SentSmsVM>();
            try
            {
                lstRenewalDeclineFailSms = dataService.GetRenewalDeclineFailSmsReport(filterData);
                return lstRenewalDeclineFailSms;
            }
            catch (Exception ex)
            {
                return lstRenewalDeclineFailSms;
            }
        }

        public IActionResult ExportRenewalDeclineFailCase()
        {
            if (CheckLoginUser() == false)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                List<SentSmsVM> lstDeclineFailSms = new List<SentSmsVM>();
                lstDeclineFailSms = HttpContext.Session.GetObjectFromJson<List<SentSmsVM>>("lstRenewalDeclineSms");

                var columnHeader = new List<string>
                {
                    "ID",
                    "Sent Date/Time",
                    "Customer Code",
                    "User Name",
                    "Phone Number",
                    "Policy No",
                    "Vehicle No",
                    "Expirty Date",
                    "Renewal Premium",
                    "Product",
                    "From Date",
                    "To Date",
                    "Link"
                };
                var fileContent = ExportExcelForRenewalDeclineSMSReport(lstDeclineFailSms, columnHeader, "Sheet1");
                return File(fileContent, "application/ms-excel", "RenewalDeclineSMSFailCase.xlsx");
            }
        }

        public byte[] ExportExcelForRenewalDeclineSMSReport(List<SentSmsVM> lst, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 30;

                for (int i = 0; i < columnHeader.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Style.Font.Bold = true;
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];
                }
                var j = 2;
                var count = 1;
                foreach (var item in lst)
                {
                    worksheet.Cells["A" + j].Value = item.Id;
                    worksheet.Cells["B" + j].Value = string.Format("{0:MM/dd/yyyy hh:mm:tt}", item.SentDate);
                    worksheet.Cells["C" + j].Value = item.CusCode;
                    worksheet.Cells["D" + j].Value = item.UserName;

                    string PhoneNo = item.PhoneNumber;
                    PhoneNo = PhoneNo.Substring(3, PhoneNo.Length - 3); //Remove '+95' from phoneNumber
                    PhoneNo = PhoneNo.Insert(0, "0");// Add '0' to phoneNumber

                    worksheet.Cells["E" + j].Value = PhoneNo;
                    worksheet.Cells["F" + j].Value = item.PolicyNo;
                    worksheet.Cells["G" + j].Value = item.VehicleNo;
                    worksheet.Cells["H" + j].Value = string.Format("{0:MM/dd/yyyy}", item.ExpireDate);
                    worksheet.Cells["I" + j].Value = item.RenewalPremium;
                    worksheet.Cells["J" + j].Value = item.Product;
                    worksheet.Cells["K" + j].Value = string.Format("{0:MM/dd/yyyy}", item.FromDate);
                    worksheet.Cells["L" + j].Value = string.Format("{0:MM/dd/yyyy}", item.ToDate);
                    worksheet.Cells["M" + j].Value = item.Link;

                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        #endregion

    }
}