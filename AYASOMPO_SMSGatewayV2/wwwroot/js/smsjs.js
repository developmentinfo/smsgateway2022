﻿$('document').ready(function () {
  
    $('#tblBU').on('change', '.ddlPIC', function () {
        var productclass = $('#classname').val();
        var row = $(this).closest('tr'); // get the row
        var id = row.find('.hdfId').val();
        var picname = row.find('.ddlPIC option:selected').text();
        if (id !== null) {
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: "/SMS/GetResponsiblePartyByPIC",
                data: { picname: picname, id: id, productclass: productclass },
                dataType: "Json",
                success: function (data) {
                    if (data != null) {
                        row.find('.txtresponsibleparty').val(data);
                    }
                    else if (data == "-1") {
                        alert("There is no responsiable party.");
                        row.find('.txtresponsibleparty').val("");
                    }
                },
                error: function ajaxError(response) {
                    alert("Error");
                }
            });
        }

    });

    $('#tblBU').on('change', '.ddlBranchAuthority', function () {
        var productclass = $('#classname').val();
        var row = $(this).closest('tr'); // get the row
        var id = row.find('.hdfId').val();
        var branchauthority = row.find('.ddlBranchAuthority option:selected').val();

        if (id !== null) {
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: "/SMS/UpdateBranchAuthority",
                data: { branchauthority: branchauthority, id: id, productclass: productclass },
                dataType: "Json",
                success: function (data) {
                    if (data != null) {

                    }
                    else if (data == "-1") {
                        alert("Authority can not save correctly.");
                    }
                },
                error: function ajaxError(response) {
                    alert("Error");
                }
            });
        }
    });

    $('#tblBU').on('change', '.ddlDMSUpload', function () {
        var productclass = $('#classname').val();
        var row = $(this).closest('tr'); // get the row
        var id = row.find('.hdfId').val();
        var dmsupload = row.find('.ddlDMSUpload option:selected').val();

        if (id !== null) {
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: "/SMS/UpdateDMSUpload",
                data: { dmsupload: dmsupload, id: id, productclass: productclass },
                dataType: "Json",
                success: function (data) {
                    if (data != null) {

                    }
                    else if (data == "-1") {
                        alert("Invalid");
                    }
                },
                error: function ajaxError(response) {
                    alert("Error");
                }
            });
        }
    });

    $('#tblBU').on('change', '.ddlDonePending', function () {
        var productclass = $('#classname').val();
        var row = $(this).closest('tr'); // get the row
        var id = row.find('.hdfId').val();
        var status = row.find('.ddlDonePending option:selected').val();

        if (id !== null) {
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: "/SMS/UpdateDonePendingStatus",
                data: { status: status, id: id, productclass: productclass },
                dataType: "Json",
                success: function (data) {
                    if (data != null) {

                    }
                    else if (data == "-1") {
                        alert("Invalid");
                    }
                },
                error: function ajaxError(response) {
                    alert("Error");
                }
            });
        }
    });

    $('#tblBU').on('click', '.lnkRemark', function () {
        var row = $(this).closest('tr'); // get the row
        var id = row.find('.hdfId').val();
        var productclass = row.find("td:eq(29)").html();
        var policyno = row.find("td:eq(10)").html();
        if (id !== null) {
            $('#hdfRenewalRemarkId').val(id);
            $('#hdfproduct_class').val(productclass);
            $('#txtpolicyno').val(policyno);
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: "/SMS/GetRenewalRemarkById",
                data: { id: id },
                dataType: "Json",
                success: function (data) {
                    if (data != null) {
                        $('#mdlMsg').modal('show');
                        $("#tblRemark  tbody").remove();
                        for (var i = 0; i < data.length; i++) {
                            var row = $('<tr><td>' + data[i].remarkDate + '</td><td>' + data[i].userName + '</td><td>' + data[i].remark + '</td></tr>');
                            $('#tblRemark').append(row);
                        }
                    }
                    else if (data == "-1") {
                        alert("Remark can not show correctly.");
                    }
                },
                error: function ajaxError(response) {
                    alert("Error");
                }
            });
        }
       // alert($('#hdfRenewalRemarkId').val());

       
    });

    $('#tblBU').on('click', '.lnkRemark1', function () {
        var row = $(this).closest('tr'); // get the row
        var id = row.find('.hdfId').val();
        var productclass = row.find("td:eq(27)").html();
        var policyno = row.find("td:eq(10)").html();
        if (id !== null) {
            $('#hdfRenewalRemarkId').val(id);
            $('#hdfproduct_class').val(productclass);
            $('#txtpolicyno').val(policyno);
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: "/SMS/GetRenewalRemarkById",
                data: { id: id },
                dataType: "Json",
                success: function (data) {
                    if (data != null) {
                        $('#mdlMsg').modal('show');
                        $("#tblRemark  tbody").remove();
                        for (var i = 0; i < data.length; i++) {
                            var row = $('<tr><td>' + data[i].remarkDate + '</td><td>' + data[i].userName + '</td><td>' + data[i].remark + '</td></tr>');
                            $('#tblRemark').append(row);
                        }
                    }
                    else if (data == "-1") {
                        alert("Remark can not show correctly.");
                    }
                },
                error: function ajaxError(response) {
                    alert("Error");
                }
            });
        }
        // alert($('#hdfRenewalRemarkId').val());


    });

    $('#tblBU').on('click', '.lnkRemark2', function () {
        var row = $(this).closest('tr'); // get the row
        var id = row.find('.hdfId').val();
        var productclass = row.find("td:eq(29)").html();
        var policyno = row.find("td:eq(12)").html();
        if (id !== null) {
            $('#hdfRenewalRemarkId').val(id);
            $('#hdfproduct_class').val(productclass);
            $('#txtpolicyno').val(policyno);
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: "/SMS/GetRenewalRemarkById",
                data: { id: id },
                dataType: "Json",
                success: function (data) {
                    if (data != null) {
                        $('#mdlMsg').modal('show');
                        $("#tblRemark  tbody").remove();
                        for (var i = 0; i < data.length; i++) {
                            var row = $('<tr><td>' + data[i].remarkDate + '</td><td>' + data[i].userName + '</td><td>' + data[i].remark + '</td></tr>');
                            $('#tblRemark').append(row);
                        }
                    }
                    else if (data == "-1") {
                        alert("Remark can not show correctly.");
                    }
                },
                error: function ajaxError(response) {
                    alert("Error");
                }
            });
        }
        // alert($('#hdfRenewalRemarkId').val());


    });

    $('#tblBU').on('keyup', '.txtagreedsiamount', function (event) {

        if (event.which >= 37 && event.which <= 40) return;

        var row = $(this).closest('tr');
        row.find('.txtagreedsiamount').val(function (index, value) {
            return value
                // Keep only digits and decimal points:
                .replace(/[^\d.]/g, "")
                // Remove duplicated decimal point, if one exists:
                .replace(/^(\d*\.)(.*)\.(.*)$/, '$1$2$3')
                // Keep only two digits past the decimal point:
                .replace(/\.(\d{2})\d+/, '.$1')
                // Add thousands separators:
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        });
    });

    $('#tblBU').on('change', '.txtagreedsiamount', function () {
        var productclass = $('#classname').val();
        var row = $(this).closest('tr'); // get the row
        var id = row.find('.hdfId').val();
        var agreedsiamount = row.find('.txtagreedsiamount').val();

        if (id !== null) {
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: "/SMS/UpdateAgreedSIAmount",
                data: { agreedsiamount: agreedsiamount, id: id, productclass: productclass },
                dataType: "Json",
                success: function (data) {
                    if (data != null) {

                    }
                    else if (data == "-1") {
                        alert("Agreed SI Amount can not save correctly.");
                    }
                },
                error: function ajaxError(response) {
                    alert("Error");
                }
            });
        }
    });

    $('#tblBU').on('click', '.btnInvoiceSync', function () {
        var productclass = $('#classname').val();
        var row = $(this).closest('tr'); // get the row
        var id = row.find('.hdfId').val();

        var strArray = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        //$('#mdlLoading').modal('show');
        $('#mdlLoading').modal({ backdrop: 'static', keyboard: false })
        if (id !== null) {
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: "/SMS/SyncRenewalInvoiceUpdate",
                data: {  id: id, productclass: productclass },
                dataType: "Json",
                success: function (data) {
                    if (data != null) {
                        //if (data.invoiceNo == null) {
                        //    alert("There is no generated invoice number.");
                        //}
                        if (productclass == "MT") {

                            row.find("td:eq(9)").html(data.invoiceNo);
                            row.find("td:eq(10)").html(data.policyNo);
                            row.find("td:eq(11)").html(data.phoneNumber);
                            row.find("td:eq(12)").html(data.cusCode);
                            row.find("td:eq(13)").html(data.customerName);

                            row.find("td:eq(14)").html(data.riskName);
                            //row.find("td:eq(14)").html(data.expiryDate);
                            var from_date = new Date(data.fromDate);
                            row.find("td:eq(16)").html('' + strArray[from_date.getMonth()] + '/' + (from_date.getDate() <= 9 ? '0' + from_date.getDate() : from_date.getDate()) + '/' + from_date.getFullYear().toString().slice(-2));
                            var to_date = new Date(data.toDate);
                            row.find("td:eq(17)").html('' + strArray[to_date.getMonth()] + '/' + (to_date.getDate() <= 9 ? '0' + to_date.getDate() : to_date.getDate()) + '/' + to_date.getFullYear().toString().slice(-2));

                            row.find("td:eq(18)").html(data.renewalPremium);
                            row.find("td:eq(19)").html(data.manufacturer);
                            row.find("td:eq(20)").html(data.car_Model);
                            row.find("td:eq(22)").html(data.branch);
                            row.find("td:eq(23)").html(data.accountCode);
                            row.find("td:eq(24)").html(data.intermediaryType);
                            row.find("td:eq(25)").html(data.agentCode);

                            row.find("td:eq(26)").html(data.agentName);
                            row.find("td:eq(27)").html(data.authorizedCode);
                            row.find("td:eq(28)").html(data.authorizedName);
                            row.find("td:eq(31)").html(data.sumInsured);
                            //row.find("td:eq(29)").html(data.sivalue20);
                            row.find("td:eq(33)").html(data.renewedSivalue);
                            row.find("td:eq(35)").html(data.renewedWindscreenValue);
                            row.find("td:eq(36)").html(data.outstanding_Amt);
                            row.find("td:eq(37)").html(data.excess_Amt);
                            row.find("td:eq(41)").html(data.financial_Interest);

                            //  $('#mdlLoading').modal('hide');
                            alert("Synced Successful.");
                        }
                        else if (productclass == "FI") {
                            row.find("td:eq(9)").html(data.invoiceNo);
                            row.find("td:eq(10)").html(data.policyNo);
                            row.find("td:eq(11)").html(data.phonenumber);
                            row.find("td:eq(13)").html(data.customerName);
                            row.find("td:eq(14)").html(data.riskName);
                            //row.find("td:eq(14)").html(data.expiryDate);

                            var from_date = new Date(data.fromDate);
                            row.find("td:eq(16)").html('' + strArray[from_date.getMonth()] + '/' + (from_date.getDate() <= 9 ? '0' + from_date.getDate() : from_date.getDate()) + '/' + from_date.getFullYear().toString().slice(-2));
                            var to_date = new Date(data.toDate);
                            row.find("td:eq(17)").html('' + strArray[to_date.getMonth()] + '/' + (to_date.getDate() <= 9 ? '0' + to_date.getDate() : to_date.getDate()) + '/' + to_date.getFullYear().toString().slice(-2));

                            row.find("td:eq(18)").html(data.renewalPremium);
                            row.find("td:eq(20)").html(data.branch);
                            row.find("td:eq(21)").html(data.accountCode);
                            row.find("td:eq(22)").html(data.intermediaryType);
                            row.find("td:eq(23)").html(data.agentCode);

                            row.find("td:eq(24)").html(data.agentName);
                            row.find("td:eq(25)").html(data.authorizedCode);
                            row.find("td:eq(26)").html(data.authorizedName);
                            row.find("td:eq(29)").html(data.sumInsured);
                            row.find("td:eq(30)").html(data.detailOccupancy);

                            row.find("td:eq(31)").html(data.locationRisk);
                            row.find("td:eq(32)").html(data.buildingClass);
                            row.find("td:eq(33)").html(data.lossRecord);
                            row.find("td:eq(34)").html(data.coverage);
                            row.find("td:eq(35)").html(data.googleMap);

                            row.find("td:eq(36)").html(data.abctypeExtinguisher);
                            row.find("td:eq(37)").html(data.co2typeExtinguishers);
                            row.find("td:eq(38)").html(data.otherExtinguisher);
                            row.find("td:eq(39)").html(data.fireExtinguishersWeight);
                            row.find("td:eq(40)").html(data.numberofFireHoseReel);

                            row.find("td:eq(41)").html(data.numberofHydrant);
                            row.find("td:eq(42)").html(data.capacityofFireTank);
                            row.find("td:eq(43)").html(data.securityGuards);
                            row.find("td:eq(44)").html(data.numberofCctvcamera);
                            row.find("td:eq(45)").html(data.sqMeterofBuilding);

                            row.find("td:eq(46)").html(data.requiredExtinguishers);
                            row.find("td:eq(47)").html(data.workinProcess);
                            row.find("td:eq(51)").html(data.numberofFireTank);
                            row.find("td:eq(52)").html(data.typeofFireTank);
                            row.find("td:eq(53)").html(data.outstanding_Amt);

                            row.find("td:eq(54)").html(data.excess_Amt);
                            row.find("td:eq(55)").html(data.financial_Interest);

                            alert("Synced Successful.");

                        }
                        else if (productclass == "LF") {
                            row.find("td:eq(9)").html(data.invoiceNo);
                            row.find("td:eq(10)").html(data.policyNo);
                            row.find("td:eq(11)").html(data.phoneNumber);
                            row.find("td:eq(12)").html(data.cusCode);
                            row.find("td:eq(13)").html(data.customerName);

                            row.find("td:eq(14)").html(data.riskName);
                            var from_date = new Date(data.fromDate);
                            row.find("td:eq(16)").html('' + strArray[from_date.getMonth()] + '/' + (from_date.getDate() <= 9 ? '0' + from_date.getDate() : from_date.getDate()) + '/' + from_date.getFullYear().toString().slice(-2));
                            var to_date = new Date(data.toDate);
                            row.find("td:eq(17)").html('' + strArray[to_date.getMonth()] + '/' + (to_date.getDate() <= 9 ? '0' + to_date.getDate() : to_date.getDate()) + '/' + to_date.getFullYear().toString().slice(-2));

                            row.find("td:eq(18)").html(data.renewalPremium);
                            row.find("td:eq(20)").html(data.branch);
                            row.find("td:eq(21)").html(data.accountCode);
                            row.find("td:eq(22)").html(data.intermediaryType);
                            row.find("td:eq(23)").html(data.agentCode);

                            row.find("td:eq(24)").html(data.agentName);
                            row.find("td:eq(25)").html(data.authorizedCode);
                            row.find("td:eq(26)").html(data.authorizedName);
                            row.find("td:eq(28)").html(data.installmentPlan);
                            row.find("td:eq(29)").html(data.basicCoverage);

                            row.find("td:eq(30)").html(data.optionalCoverage1);
                            row.find("td:eq(31)").html(data.optionalCoverage2);
                            row.find("td:eq(32)").html(data.ncbclaimLoading);
                            row.find("td:eq(33)").html(data.outstanding_Amt);
                            row.find("td:eq(34)").html(data.excess_Amt);
                            row.find("td:eq(35)").html(data.financial_Interest);

                            alert("Synced Successful.");
                        }
                        else if (productclass == "MS") {
                            row.find("td:eq(9)").html(data.invoiceNo);
                            row.find("td:eq(10)").html(data.policyNo);
                            row.find("td:eq(11)").html(data.phoneNumber);
                            row.find("td:eq(12)").html(data.cusCode);
                            row.find("td:eq(13)").html(data.customerName);

                            row.find("td:eq(14)").html(data.riskName);
                            var from_date = new Date(data.fromDate);
                            row.find("td:eq(16)").html('' + strArray[from_date.getMonth()] + '/' + (from_date.getDate() <= 9 ? '0' + from_date.getDate() : from_date.getDate()) + '/' + from_date.getFullYear().toString().slice(-2));
                            var to_date = new Date(data.toDate);
                            row.find("td:eq(17)").html('' + strArray[to_date.getMonth()] + '/' + (to_date.getDate() <= 9 ? '0' + to_date.getDate() : to_date.getDate()) + '/' + to_date.getFullYear().toString().slice(-2));

                            row.find("td:eq(18)").html(data.renewalPremium);
                            row.find("td:eq(20)").html(data.branch);
                            row.find("td:eq(21)").html(data.accountCode);
                            row.find("td:eq(22)").html(data.intermediaryType);
                            row.find("td:eq(23)").html(data.agentCode);

                            row.find("td:eq(24)").html(data.agentName);
                            row.find("td:eq(25)").html(data.authorizedCode);
                            row.find("td:eq(26)").html(data.authorizedName);
                            row.find("td:eq(28)").html(data.installmentPlan);
                            row.find("td:eq(29)").html(data.basicCoverage);

                            row.find("td:eq(30)").html(data.optionalCoverage1);
                            row.find("td:eq(31)").html(data.optionalCoverage2);
                            row.find("td:eq(32)").html(data.ncbclaimLoading);
                            row.find("td:eq(33)").html(data.outstanding_Amt);
                            row.find("td:eq(34)").html(data.excess_Amt);
                            row.find("td:eq(35)").html(data.financial_Interest);

                            alert("Synced Successful.");
                        }
                        else if (productclass == "ENGG") {
                            row.find("td:eq(11)").html(data.invoiceNo);
                            row.find("td:eq(12)").html(data.policyNo);
                            row.find("td:eq(13)").html(data.phonenumber);
                            row.find("td:eq(15)").html(data.customerName);
                            row.find("td:eq(16)").html(data.riskName);

                            var from_date = new Date(data.fromDate);
                            row.find("td:eq(18)").html('' + strArray[from_date.getMonth()] + '/' + (from_date.getDate() <= 9 ? '0' + from_date.getDate() : from_date.getDate()) + '/' + from_date.getFullYear().toString().slice(-2));
                            var to_date = new Date(data.toDate);
                            row.find("td:eq(19)").html('' + strArray[to_date.getMonth()] + '/' + (to_date.getDate() <= 9 ? '0' + to_date.getDate() : to_date.getDate()) + '/' + to_date.getFullYear().toString().slice(-2));

                            row.find("td:eq(20)").html(data.renewalPremium);
                            row.find("td:eq(22)").html(data.branch);
                            row.find("td:eq(23)").html(data.accountCode);
                            row.find("td:eq(24)").html(data.intermediaryType);
                            row.find("td:eq(25)").html(data.agentCode);

                            row.find("td:eq(26)").html(data.agentName);
                            row.find("td:eq(27)").html(data.authorizedCode);
                            row.find("td:eq(28)").html(data.authorizedName);
                            row.find("td:eq(31)").html(data.sumInsured);
                            row.find("td:eq(32)").html(data.detailOccupancy);

                            row.find("td:eq(33)").html(data.locationRisk);
                            row.find("td:eq(34)").html(data.buildingClass);
                            row.find("td:eq(35)").html(data.lossRecord);
                            row.find("td:eq(36)").html(data.coverage);
                            row.find("td:eq(37)").html(data.googleMap);

                            row.find("td:eq(38)").html(data.abctypeExtinguisher);
                            row.find("td:eq(39)").html(data.co2typeExtinguishers);
                            row.find("td:eq(40)").html(data.otherExtinguisher);
                            row.find("td:eq(41)").html(data.fireExtinguishersWeight);
                            row.find("td:eq(42)").html(data.numberofFireHoseReel);

                            row.find("td:eq(43)").html(data.numberofHydrant);
                            row.find("td:eq(44)").html(data.numberofFireTank);
                            row.find("td:eq(45)").html(data.typeofFireTank);
                            row.find("td:eq(46)").html(data.capacityofFireTank);
                            row.find("td:eq(47)").html(data.securityGuards);

                            row.find("td:eq(48)").html(data.numberofCctvcamera);
                            row.find("td:eq(49)").html(data.sqMeterofBuilding);
                            row.find("td:eq(50)").html(data.requiredExtinguishers);
                            row.find("td:eq(51)").html(data.workinProcess);
                            row.find("td:eq(52)").html(data.outstanding_Amt);
                            row.find("td:eq(53)").html(data.excess_Amt);
                            row.find("td:eq(54)").html(data.financial_Interest);

                            alert("Synced Successful.");
                        }
                        else if (productclass == "PTY") {
                            row.find("td:eq(11)").html(data.invoiceNo);
                            row.find("td:eq(12)").html(data.policyNo);
                            row.find("td:eq(13)").html(data.phonenumber);
                            row.find("td:eq(15)").html(data.customerName);
                            row.find("td:eq(16)").html(data.riskName);

                            var from_date = new Date(data.fromDate);
                            row.find("td:eq(18)").html('' + strArray[from_date.getMonth()] + '/' + (from_date.getDate() <= 9 ? '0' + from_date.getDate() : from_date.getDate()) + '/' + from_date.getFullYear().toString().slice(-2));
                            var to_date = new Date(data.toDate);
                            row.find("td:eq(19)").html('' + strArray[to_date.getMonth()] + '/' + (to_date.getDate() <= 9 ? '0' + to_date.getDate() : to_date.getDate()) + '/' + to_date.getFullYear().toString().slice(-2));

                            row.find("td:eq(20)").html(data.renewalPremium);
                            row.find("td:eq(22)").html(data.branch);
                            row.find("td:eq(23)").html(data.accountCode);
                            row.find("td:eq(24)").html(data.intermediaryType);
                            row.find("td:eq(25)").html(data.agentCode);

                            row.find("td:eq(26)").html(data.agentName);
                            row.find("td:eq(27)").html(data.authorizedCode);
                            row.find("td:eq(28)").html(data.authorizedName);
                            row.find("td:eq(31)").html(data.sumInsured);
                            row.find("td:eq(32)").html(data.detailOccupancy);

                            row.find("td:eq(33)").html(data.locationRisk);
                            row.find("td:eq(34)").html(data.buildingClass);
                            row.find("td:eq(35)").html(data.lossRecord);
                            row.find("td:eq(36)").html(data.coverage);
                            row.find("td:eq(37)").html(data.googleMap);

                            row.find("td:eq(38)").html(data.abctypeExtinguisher);
                            row.find("td:eq(39)").html(data.co2typeExtinguishers);
                            row.find("td:eq(40)").html(data.otherExtinguisher);
                            row.find("td:eq(41)").html(data.fireExtinguishersWeight);
                            row.find("td:eq(42)").html(data.numberofFireHoseReel);

                            row.find("td:eq(43)").html(data.numberofHydrant);
                            row.find("td:eq(44)").html(data.numberofFireTank);
                            row.find("td:eq(45)").html(data.typeofFireTank);
                            row.find("td:eq(46)").html(data.capacityofFireTank);
                            row.find("td:eq(47)").html(data.securityGuards);

                            row.find("td:eq(48)").html(data.numberofCctvcamera);
                            row.find("td:eq(49)").html(data.sqMeterofBuilding);
                            row.find("td:eq(50)").html(data.requiredExtinguishers);
                            row.find("td:eq(51)").html(data.workinProcess);
                            row.find("td:eq(52)").html(data.outstanding_Amt);

                            row.find("td:eq(53)").html(data.excess_Amt);
                            row.find("td:eq(54)").html(data.financial_Interest);

                            alert("Synced Successful.");
                        }
                        $('#mdlLoading').modal('hide');
                    }
                    else if (data == "-1") {
                        alert("There is no responsible party.");
                        row.find('.txtresponsibleparty').val("");
                        $('#mdlLoading').modal('hide');
                    }
                },
                error: function ajaxError(response) {
                    alert("Error");
                    $('#mdlLoading').modal('hide');
                }
            });
        }

    });

    $('#btnSaveRemark').click(function () {
        var id = $('#hdfRenewalRemarkId').val();
        var remark = $('#remark').val();
        var remarlinfo = {};
        remarlinfo.id = $('#hdfRenewalRemarkId').val();
        remarlinfo.remark = $('#remark').val();
        remarlinfo.productClass = $('#hdfproduct_class').val();
        if (id !== null) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/SMS/SaveRenewalRemarkById",
                data: JSON.stringify(remarlinfo),
                dataType: "Json",
                success: function (data) {
                    if (data != null) {
                        $('#mdlMsg').modal('show');
                        $("#tblRemark  tbody").remove();
                        for (var i = 0; i < data.length; i++) {
                            var row = $('<tr><td>' + data[i].remarkDate + '</td><td>' + data[i].userName + '</td><td>' + data[i].remark + '</td></tr>');
                            $('#tblRemark').append(row);
                        }

                        //Clear Remark TextArea
                        $('#remark').val("");
                    }
                    else if (data == "-1") {
                        alert("Remark can not save correctly.");
                    }
                },
                error: function ajaxError(response) {
                    alert("Error");
                }
            });
        }
    });

    $('#btnDoneRemark').click(function () {
        var id = $('#hdfRenewalRemarkId').val();
        var remarkinfo = {};
        remarkinfo.id = $('#hdfRenewalRemarkId').val();
        remarkinfo.productClass = $('#hdfproduct_class').val();
        if (id !== null) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/SMS/UpdateRenewalRemarkDone",
                data: JSON.stringify(remarkinfo),
                dataType: "Json",
                success: function (data) {
                    if (data != null) {
                        $('#mdlMsg').modal('show');
                        $("#tblRemark  tbody").remove();
                        for (var i = 0; i < data.length; i++) {
                            var row = $('<tr><td>' + data[i].remarkDate + '</td><td>' + data[i].userName + '</td><td>' + data[i].remark + '</td></tr>');
                            $('#tblRemark').append(row);
                        }
                    }
                    else if (data == "-1") {
                        alert("Remark can not close successfully.");
                    }
                },
                error: function ajaxError(response) {
                    alert("Error");
                }
            });
        }
    });

    $('.ddlChkApproveSendSMS').change(function () {
        var productclass = $('#classname').val();
        var row = $(this).closest('tr'); // get the row
        var ids = [];
        var phonenos = [];
        var approvesendsms = $('.ddlChkApproveSendSMS :selected').text();
        var approveSendSMSModel = {};

        var oTable = $('#tblOP').dataTable();
        var rowcollection = oTable.$("#smschk:checked", { "page": "all" });
        rowcollection.each(function (index, elem) {
            ids[index] = $(elem).val();
            //phonenos[index] = row.find('.txtphonenumber').val();
            phonenos[index] = $(this).closest("tr").find(".txtphonenumber").val();
            //Do something with 'checkbox_value'
        });

        approveSendSMSModel.ids = ids;
        approveSendSMSModel.approvesendsms = approvesendsms;
        approveSendSMSModel.productclass = productclass;
        approveSendSMSModel.phonenos = phonenos;

        //$('#mdlLoading').modal('show');
        $('#mdlLoading').modal({ backdrop: 'static', keyboard: false })

        if (ids.length > 0) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/SMS/UpdateChkApproveSendSMS",
                data: JSON.stringify(approveSendSMSModel),
                dataType: "Json",
                success: function (data) {
                    if (data != null) {
                        $('#mdlLoading').modal('hide');

                        if (approvesendsms == "Send_SMS") {
                            var renewalfailcase = ids.length - data;
                            alert("Renewal case " + data + " success and " + renewalfailcase + " fail.");
                        }
                        else {
                            alert("You have changed " + approvesendsms + " case successfully.");
                        }

                        if (!alert("Do you really want to refresh the page?"))
                            location.reload();
                    }
                    else if (data == "-1") {
                        alert("Approve Send SMS Status can not change correctly.");
                    }
                },
                error: function ajaxError(response) {
                    alert("Error");
                }
            });
        }
        
    });

    $('#tblOP').on('change', '.ddlApproveSendSMS', function () {
        var productclass = $('#classname').val();
        var row = $(this).closest('tr'); // get the row
        var id = row.find('.hdfId').val();
        var approvesendsms = row.find('.ddlApproveSendSMS option:selected').text();
        var phonenumber = row.find('.txtphonenumber').val();
        
        if (id !== null) {
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: "/SMS/UpdateApproveSendSMS",
                data: { approvesendsms: approvesendsms, id: id, productclass: productclass, phonenumber: phonenumber },
                dataType: "Json",
                success: function (data) {
                    if (data == "1") {

                        if (approvesendsms == "Send_SMS") {
                            alert("Renewal case has been successfully created");
                        }

                    }
                    else if (data == "-1" && approvesendsms == "Send_SMS") {
                        alert("Renewal case cannot created.");
                    }
                },
                error: function ajaxError(response) {
                    alert("Error");
                }
            });
        }

    });

    $('#tblOP').on('change', '.ddlUWApproval', function () {
        var productclass = $('#classname').val();
        var row = $(this).closest('tr'); // get the row
        var id = row.find('.hdfId').val();
        var uwapproval = row.find('.ddlUWApproval option:selected').val();

        if (id !== null) {
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: "/SMS/UpdateUWApproval",
                data: { uwapproval: uwapproval, id: id, productclass: productclass },
                dataType: "Json",
                success: function (data) {
                    if (data != null) {

                    }
                    else if (data == "-1") {
                        alert("Approval can not save correctly.");
                    }
                },
                error: function ajaxError(response) {
                    alert("Error");
                }
            });
        }

    });

    $('#tblOP').on('change', '.ddlBranchAuthority', function () {
        var productclass = $('#classname').val();
        var row = $(this).closest('tr'); // get the row
        var id = row.find('.hdfId').val();
        var branchauthority = row.find('.ddlBranchAuthority option:selected').val();

        if (id !== null) {
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: "/SMS/UpdateBranchAuthority",
                data: { branchauthority: branchauthority, id: id, productclass: productclass},
                dataType: "Json",
                success: function (data) {
                    if (data != null) {

                    }
                    else if (data == "-1") {
                        alert("Authority can not save correctly.");
                    }
                },
                error: function ajaxError(response) {
                    alert("Error");
                }
            });
        }

    });

    $('#tblOP').on('change', '.ddlDMSUpload', function () {
        var productclass = $('#classname').val();
        var row = $(this).closest('tr'); // get the row
        var id = row.find('.hdfId').val();
        var dmsupload = row.find('.ddlDMSUpload option:selected').val();

        if (id !== null) {
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: "/SMS/UpdateDMSUpload",
                data: { dmsupload: dmsupload, id: id, productclass: productclass },
                dataType: "Json",
                success: function (data) {
                    if (data != null) {

                    }
                    else if (data == "-1") {
                        alert("Invalid");
                    }
                },
                error: function ajaxError(response) {
                    alert("Error");
                }
            });
        }
    });

    $('#tblOP').on('change', '.ddlDonePending', function () {
        var productclass = $('#classname').val();
        var row = $(this).closest('tr'); // get the row
        var id = row.find('.hdfId').val();
        var status = row.find('.ddlDonePending option:selected').val();

        if (id !== null) {
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: "/SMS/UpdateDonePendingStatus",
                data: { status: status, id: id, productclass: productclass },
                dataType: "Json",
                success: function (data) {
                    if (data != null) {

                    }
                    else if (data == "-1") {
                        alert("Invalid");
                    }
                },
                error: function ajaxError(response) {
                    alert("Error");
                }
            });
        }
    });

    $('#tblOP').on('click', '.lnkRemark', function () {
        var row = $(this).closest('tr'); // get the row
        var id = row.find('.hdfId').val();
        var productclass = row.find("td:eq(28)").html();
        var policyno = row.find("td:eq(8)").html();
        
        if (id !== null) {
            $('#hdfRenewalRemarkId').val(id);
            $('#hdfproduct_class').val(productclass);
            $('#txtpolicyno').val(policyno);
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: "/SMS/GetRenewalRemarkById",
                data: { id: id },
                dataType: "Json",
                success: function (data) {
                    if (data != null) {
                        $('#mdlMsg').modal('show');
                        $("#tblRemark  tbody").remove();
                        for (var i = 0; i < data.length; i++) {
                            var row = $('<tr><td>' + data[i].remarkDate + '</td><td>' + data[i].userName + '</td><td>' + data[i].remark + '</td></tr>');
                            $('#tblRemark').append(row);
                        }
                    }
                    else if (data == "-1") {
                        alert("Remark can not show correctly.");
                    }
                },
                error: function ajaxError(response) {
                    alert("Error");
                }
            });
        }
        // alert($('#hdfRenewalRemarkId').val());


    });

    $('#tblOP').on('click', '.lnkRemark1', function () {
        var row = $(this).closest('tr'); // get the row
        var id = row.find('.hdfId').val();
        var productclass = row.find("td:eq(26)").html();
        var policyno = row.find("td:eq(8)").html();

        if (id !== null) {
            $('#hdfRenewalRemarkId').val(id);
            $('#hdfproduct_class').val(productclass);
            $('#txtpolicyno').val(policyno);
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: "/SMS/GetRenewalRemarkById",
                data: { id: id },
                dataType: "Json",
                success: function (data) {
                    if (data != null) {
                        $('#mdlMsg').modal('show');
                        $("#tblRemark  tbody").remove();
                        for (var i = 0; i < data.length; i++) {
                            var row = $('<tr><td>' + data[i].remarkDate + '</td><td>' + data[i].userName + '</td><td>' + data[i].remark + '</td></tr>');
                            $('#tblRemark').append(row);
                        }
                    }
                    else if (data == "-1") {
                        alert("Remark can not show correctly.");
                    }
                },
                error: function ajaxError(response) {
                    alert("Error");
                }
            });
        }
        // alert($('#hdfRenewalRemarkId').val());


    });

    $('#tblOP').on('click', '.lnkRemark2', function () {
        var row = $(this).closest('tr'); // get the row
        var id = row.find('.hdfId').val();
        var productclass = row.find("td:eq(28)").html();
        var policyno = row.find("td:eq(10)").html();

        if (id !== null) {
            $('#hdfRenewalRemarkId').val(id);
            $('#hdfproduct_class').val(productclass);
            $('#txtpolicyno').val(policyno);
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: "/SMS/GetRenewalRemarkById",
                data: { id: id },
                dataType: "Json",
                success: function (data) {
                    if (data != null) {
                        $('#mdlMsg').modal('show');
                        $("#tblRemark  tbody").remove();
                        for (var i = 0; i < data.length; i++) {
                            var row = $('<tr><td>' + data[i].remarkDate + '</td><td>' + data[i].userName + '</td><td>' + data[i].remark + '</td></tr>');
                            $('#tblRemark').append(row);
                        }
                    }
                    else if (data == "-1") {
                        alert("Remark can not show correctly.");
                    }
                },
                error: function ajaxError(response) {
                    alert("Error");
                }
            });
        }
        // alert($('#hdfRenewalRemarkId').val());


    });

    $('#btnAllSync').click(function () {
        var productclass = $('#classname').val();
        var strArray = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        var oTable = $('#tblBU').dataTable();
        var rowCount = oTable.$('tr').length;
       
        if (rowCount <= 0) {
            alert("Please select data and retrieve data first.");
            return false;
        }
        //$('#mdlLoading').modal('show');
        $('#mdlLoading').modal({ backdrop: 'static', keyboard: false })

        oTable.$("TR").each(function () {
            var row = $(this);
            var id = row.find('.hdfId').val();
            if (id !== null) {
                $.ajax({
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    url: "/SMS/SyncRenewalInvoiceUpdate",
                    data: { id: id, productclass: productclass },
                    dataType: "Json",
                    success: function (data) {
                        if (data != null) {
                            //if (data.invoiceNo == null) {
                            //    alert("There is no generated invoice number for policy number.");
                            //}
                            if (productclass == "MT") {
                                row.find("td:eq(9)").html(data.invoiceNo);
                                row.find("td:eq(10)").html(data.policyNo);
                                row.find("td:eq(11)").html(data.phoneNumber);
                                row.find("td:eq(12)").html(data.cusCode);
                                row.find("td:eq(13)").html(data.customerName);

                                row.find("td:eq(14)").html(data.riskName);
                                //row.find("td:eq(14)").html(data.expiryDate);
                                var from_date = new Date(data.fromDate);
                                row.find("td:eq(16)").html('' + strArray[from_date.getMonth()] + '/' + (from_date.getDate() <= 9 ? '0' + from_date.getDate() : from_date.getDate()) + '/' + from_date.getFullYear().toString().slice(-2));
                                var to_date = new Date(data.toDate);
                                row.find("td:eq(17)").html('' + strArray[to_date.getMonth()] + '/' + (to_date.getDate() <= 9 ? '0' + to_date.getDate() : to_date.getDate()) + '/' + to_date.getFullYear().toString().slice(-2));

                                row.find("td:eq(18)").html(data.renewalPremium);
                                row.find("td:eq(19)").html(data.manufacturer);
                                row.find("td:eq(20)").html(data.car_Model);
                                row.find("td:eq(22)").html(data.branch);
                                row.find("td:eq(23)").html(data.accountCode);
                                row.find("td:eq(24)").html(data.intermediaryType);
                                row.find("td:eq(25)").html(data.agentCode);

                                row.find("td:eq(26)").html(data.agentName);
                                row.find("td:eq(27)").html(data.authorizedCode);
                                row.find("td:eq(28)").html(data.authorizedName);
                                row.find("td:eq(31)").html(data.sumInsured);
                                //row.find("td:eq(29)").html(data.sivalue20);
                                row.find("td:eq(33)").html(data.renewedSivalue);
                                row.find("td:eq(35)").html(data.renewedWindscreenValue);
                                row.find("td:eq(36)").html(data.outstanding_Amt);
                                row.find("td:eq(37)").html(data.excess_Amt);
                                row.find("td:eq(41)").html(data.financial_Interest);
                            }
                            else if (productclass == "FI") {
                                row.find("td:eq(9)").html(data.invoiceNo);
                                row.find("td:eq(10)").html(data.policyNo);
                                row.find("td:eq(11)").html(data.phonenumber);
                                row.find("td:eq(13)").html(data.customerName);
                                row.find("td:eq(14)").html(data.riskName);
                                //row.find("td:eq(14)").html(data.expiryDate);

                                var from_date = new Date(data.fromDate);
                                row.find("td:eq(16)").html('' + strArray[from_date.getMonth()] + '/' + (from_date.getDate() <= 9 ? '0' + from_date.getDate() : from_date.getDate()) + '/' + from_date.getFullYear().toString().slice(-2));
                                var to_date = new Date(data.toDate);
                                row.find("td:eq(17)").html('' + strArray[to_date.getMonth()] + '/' + (to_date.getDate() <= 9 ? '0' + to_date.getDate() : to_date.getDate()) + '/' + to_date.getFullYear().toString().slice(-2));

                                row.find("td:eq(18)").html(data.renewalPremium);
                                row.find("td:eq(20)").html(data.branch);
                                row.find("td:eq(21)").html(data.accountCode);
                                row.find("td:eq(22)").html(data.intermediaryType);
                                row.find("td:eq(23)").html(data.agentCode);

                                row.find("td:eq(24)").html(data.agentName);
                                row.find("td:eq(25)").html(data.authorizedCode);
                                row.find("td:eq(26)").html(data.authorizedName);
                                row.find("td:eq(29)").html(data.sumInsured);
                                row.find("td:eq(30)").html(data.detailOccupancy);

                                row.find("td:eq(31)").html(data.locationRisk);
                                row.find("td:eq(32)").html(data.buildingClass);
                                row.find("td:eq(33)").html(data.lossRecord);
                                row.find("td:eq(34)").html(data.coverage);
                                row.find("td:eq(35)").html(data.googleMap);

                                row.find("td:eq(36)").html(data.abctypeExtinguisher);
                                row.find("td:eq(37)").html(data.co2typeExtinguishers);
                                row.find("td:eq(38)").html(data.otherExtinguisher);
                                row.find("td:eq(39)").html(data.fireExtinguishersWeight);
                                row.find("td:eq(40)").html(data.numberofFireHoseReel);

                                row.find("td:eq(41)").html(data.numberofHydrant);
                                row.find("td:eq(42)").html(data.capacityofFireTank);
                                row.find("td:eq(43)").html(data.securityGuards);
                                row.find("td:eq(44)").html(data.numberofCctvcamera);
                                row.find("td:eq(45)").html(data.sqMeterofBuilding);

                                row.find("td:eq(46)").html(data.requiredExtinguishers);
                                row.find("td:eq(47)").html(data.workinProcess);
                                row.find("td:eq(51)").html(data.numberofFireTank);
                                row.find("td:eq(52)").html(data.typeofFireTank);
                                row.find("td:eq(53)").html(data.outstanding_Amt);

                                row.find("td:eq(54)").html(data.excess_Amt);
                                row.find("td:eq(55)").html(data.financial_Interest);
                            }
                            else if (productclass == "LF") {
                                row.find("td:eq(9)").html(data.invoiceNo);
                                row.find("td:eq(10)").html(data.policyNo);
                                row.find("td:eq(11)").html(data.phoneNumber);
                                row.find("td:eq(12)").html(data.cusCode);
                                row.find("td:eq(13)").html(data.customerName);

                                row.find("td:eq(14)").html(data.riskName);
                                var from_date = new Date(data.fromDate);
                                row.find("td:eq(16)").html('' + strArray[from_date.getMonth()] + '/' + (from_date.getDate() <= 9 ? '0' + from_date.getDate() : from_date.getDate()) + '/' + from_date.getFullYear().toString().slice(-2));
                                var to_date = new Date(data.toDate);
                                row.find("td:eq(17)").html('' + strArray[to_date.getMonth()] + '/' + (to_date.getDate() <= 9 ? '0' + to_date.getDate() : to_date.getDate()) + '/' + to_date.getFullYear().toString().slice(-2));

                                row.find("td:eq(18)").html(data.renewalPremium);
                                row.find("td:eq(20)").html(data.branch);
                                row.find("td:eq(21)").html(data.accountCode);
                                row.find("td:eq(22)").html(data.intermediaryType);
                                row.find("td:eq(23)").html(data.agentCode);

                                row.find("td:eq(24)").html(data.agentName);
                                row.find("td:eq(25)").html(data.authorizedCode);
                                row.find("td:eq(26)").html(data.authorizedName);
                                row.find("td:eq(28)").html(data.installmentPlan);
                                row.find("td:eq(29)").html(data.basicCoverage);

                                row.find("td:eq(30)").html(data.optionalCoverage1);
                                row.find("td:eq(31)").html(data.optionalCoverage2);
                                row.find("td:eq(32)").html(data.ncbclaimLoading);
                                row.find("td:eq(33)").html(data.outstanding_Amt);
                                row.find("td:eq(34)").html(data.excess_Amt);
                                row.find("td:eq(35)").html(data.financial_Interest);
                            }
                            else if (productclass == "MS") {
                                row.find("td:eq(9)").html(data.invoiceNo);
                                row.find("td:eq(10)").html(data.policyNo);
                                row.find("td:eq(11)").html(data.phoneNumber);
                                row.find("td:eq(12)").html(data.cusCode);
                                row.find("td:eq(13)").html(data.customerName);

                                row.find("td:eq(14)").html(data.riskName);
                                var from_date = new Date(data.fromDate);
                                row.find("td:eq(16)").html('' + strArray[from_date.getMonth()] + '/' + (from_date.getDate() <= 9 ? '0' + from_date.getDate() : from_date.getDate()) + '/' + from_date.getFullYear().toString().slice(-2));
                                var to_date = new Date(data.toDate);
                                row.find("td:eq(17)").html('' + strArray[to_date.getMonth()] + '/' + (to_date.getDate() <= 9 ? '0' + to_date.getDate() : to_date.getDate()) + '/' + to_date.getFullYear().toString().slice(-2));

                                row.find("td:eq(18)").html(data.renewalPremium);
                                row.find("td:eq(20)").html(data.branch);
                                row.find("td:eq(21)").html(data.accountCode);
                                row.find("td:eq(22)").html(data.intermediaryType);
                                row.find("td:eq(23)").html(data.agentCode);

                                row.find("td:eq(24)").html(data.agentName);
                                row.find("td:eq(25)").html(data.authorizedCode);
                                row.find("td:eq(26)").html(data.authorizedName);
                                row.find("td:eq(28)").html(data.installmentPlan);
                                row.find("td:eq(29)").html(data.basicCoverage);

                                row.find("td:eq(30)").html(data.optionalCoverage1);
                                row.find("td:eq(31)").html(data.optionalCoverage2);
                                row.find("td:eq(32)").html(data.ncbclaimLoading);
                                row.find("td:eq(33)").html(data.outstanding_Amt);
                                row.find("td:eq(34)").html(data.excess_Amt);
                                row.find("td:eq(35)").html(data.financial_Interest);
                            }
                            else if (productclass == "ENGG") {
                                row.find("td:eq(11)").html(data.invoiceNo);
                                row.find("td:eq(12)").html(data.policyNo);
                                row.find("td:eq(13)").html(data.phonenumber);
                                row.find("td:eq(15)").html(data.customerName);
                                row.find("td:eq(16)").html(data.riskName);

                                var from_date = new Date(data.fromDate);
                                row.find("td:eq(18)").html('' + strArray[from_date.getMonth()] + '/' + (from_date.getDate() <= 9 ? '0' + from_date.getDate() : from_date.getDate()) + '/' + from_date.getFullYear().toString().slice(-2));
                                var to_date = new Date(data.toDate);
                                row.find("td:eq(19)").html('' + strArray[to_date.getMonth()] + '/' + (to_date.getDate() <= 9 ? '0' + to_date.getDate() : to_date.getDate()) + '/' + to_date.getFullYear().toString().slice(-2));

                                row.find("td:eq(20)").html(data.renewalPremium);
                                row.find("td:eq(22)").html(data.branch);
                                row.find("td:eq(23)").html(data.accountCode);
                                row.find("td:eq(24)").html(data.intermediaryType);
                                row.find("td:eq(25)").html(data.agentCode);

                                row.find("td:eq(26)").html(data.agentName);
                                row.find("td:eq(27)").html(data.authorizedCode);
                                row.find("td:eq(28)").html(data.authorizedName);
                                row.find("td:eq(31)").html(data.sumInsured);
                                row.find("td:eq(32)").html(data.detailOccupancy);

                                row.find("td:eq(33)").html(data.locationRisk);
                                row.find("td:eq(34)").html(data.buildingClass);
                                row.find("td:eq(35)").html(data.lossRecord);
                                row.find("td:eq(36)").html(data.coverage);
                                row.find("td:eq(37)").html(data.googleMap);

                                row.find("td:eq(38)").html(data.abctypeExtinguisher);
                                row.find("td:eq(39)").html(data.co2typeExtinguishers);
                                row.find("td:eq(40)").html(data.otherExtinguisher);
                                row.find("td:eq(41)").html(data.fireExtinguishersWeight);
                                row.find("td:eq(42)").html(data.numberofFireHoseReel);

                                row.find("td:eq(43)").html(data.numberofHydrant);
                                row.find("td:eq(44)").html(data.numberofFireTank);
                                row.find("td:eq(45)").html(data.typeofFireTank);
                                row.find("td:eq(46)").html(data.capacityofFireTank);
                                row.find("td:eq(47)").html(data.securityGuards);

                                row.find("td:eq(48)").html(data.numberofCctvcamera);
                                row.find("td:eq(49)").html(data.sqMeterofBuilding);
                                row.find("td:eq(50)").html(data.requiredExtinguishers);
                                row.find("td:eq(51)").html(data.workinProcess);
                                row.find("td:eq(52)").html(data.outstanding_Amt);

                                row.find("td:eq(53)").html(data.excess_Amt);
                                row.find("td:eq(54)").html(data.financial_Interest);
                            }
                            else if (productclass == "PTY") {
                                row.find("td:eq(11)").html(data.invoiceNo);
                                row.find("td:eq(12)").html(data.policyNo);
                                row.find("td:eq(13)").html(data.phonenumber);
                                row.find("td:eq(15)").html(data.customerName);
                                row.find("td:eq(16)").html(data.riskName);

                                var from_date = new Date(data.fromDate);
                                row.find("td:eq(18)").html('' + strArray[from_date.getMonth()] + '/' + (from_date.getDate() <= 9 ? '0' + from_date.getDate() : from_date.getDate()) + '/' + from_date.getFullYear().toString().slice(-2));
                                var to_date = new Date(data.toDate);
                                row.find("td:eq(19)").html('' + strArray[to_date.getMonth()] + '/' + (to_date.getDate() <= 9 ? '0' + to_date.getDate() : to_date.getDate()) + '/' + to_date.getFullYear().toString().slice(-2));

                                row.find("td:eq(20)").html(data.renewalPremium);
                                row.find("td:eq(22)").html(data.branch);
                                row.find("td:eq(23)").html(data.accountCode);
                                row.find("td:eq(24)").html(data.intermediaryType);
                                row.find("td:eq(25)").html(data.agentCode);

                                row.find("td:eq(26)").html(data.agentName);
                                row.find("td:eq(27)").html(data.authorizedCode);
                                row.find("td:eq(28)").html(data.authorizedName);
                                row.find("td:eq(31)").html(data.sumInsured);
                                row.find("td:eq(32)").html(data.detailOccupancy);

                                row.find("td:eq(33)").html(data.locationRisk);
                                row.find("td:eq(34)").html(data.buildingClass);
                                row.find("td:eq(35)").html(data.lossRecord);
                                row.find("td:eq(36)").html(data.coverage);
                                row.find("td:eq(37)").html(data.googleMap);

                                row.find("td:eq(38)").html(data.abctypeExtinguisher);
                                row.find("td:eq(39)").html(data.co2typeExtinguishers);
                                row.find("td:eq(40)").html(data.otherExtinguisher);
                                row.find("td:eq(41)").html(data.fireExtinguishersWeight);
                                row.find("td:eq(42)").html(data.numberofFireHoseReel);

                                row.find("td:eq(43)").html(data.numberofHydrant);
                                row.find("td:eq(44)").html(data.numberofFireTank);
                                row.find("td:eq(45)").html(data.typeofFireTank);
                                row.find("td:eq(46)").html(data.capacityofFireTank);
                                row.find("td:eq(47)").html(data.securityGuards);

                                row.find("td:eq(48)").html(data.numberofCctvcamera);
                                row.find("td:eq(49)").html(data.sqMeterofBuilding);
                                row.find("td:eq(50)").html(data.requiredExtinguishers);
                                row.find("td:eq(51)").html(data.workinProcess);
                                row.find("td:eq(52)").html(data.outstanding_Amt);

                                row.find("td:eq(53)").html(data.excess_Amt);
                                row.find("td:eq(54)").html(data.financial_Interest);
                            }
                            $('#mdlLoading').modal('hide');
                        }
                        else if (data == "-1") {
                            alert("There is no responsiable party.");
                            row.find('.txtresponsibleparty').val("");
                        }
                    },
                    error: function ajaxError(response) {
                        alert("Error");
                    }
                });
            }
        });


    });

    $('#btnOPAllSync').click(function () {
        var productclass = $('#classname').val();
        var strArray = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        var oTable = $('#tblOP').dataTable();
        var rowCount = oTable.$('tr').length;

        if (rowCount <= 0) {
            alert("Please select data and retrieve data first.");
            return false;
        }
        //$('#mdlLoading').modal('show');
        $('#mdlLoading').modal({ backdrop: 'static', keyboard: false })

        oTable.$("TR").each(function () {
            var row = $(this);
            var id = row.find('.hdfId').val();
            if (id !== null) {
                $.ajax({
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    url: "/SMS/SyncRenewalInvoiceUpdate",
                    data: { id: id, productclass: productclass },
                    dataType: "Json",
                    success: function (data) {
                        if (data != null) {
                            //if (data.invoiceNo == null) {
                            //    alert("There is no generated invoice number for policy number.");
                            //}
                            if (productclass == "MT") {
                                row.find("td:eq(7)").html(data.invoiceNo);
                                row.find("td:eq(8)").html(data.policyNo);
                                if (data.phoneNumber == null) {
                                    row.find("td:eq(2)").html('<input class="form-control txtphonenumber" type="text" id="phonenumber" asp-for="@motor.PhoneNumber" value="" onkeydown="keyupFunction()" style="width:150px !important;" />');
                                }
                                else {
                                    row.find("td:eq(2)").html('<input class="form-control txtphonenumber" type="text" id="phonenumber" asp-for="@motor.PhoneNumber" value="' + data.phoneNumber + '" onkeydown="keyupFunction()" style="width:150px !important;" />');
                                }
                                row.find("td:eq(11)").html(data.cusCode);
                                row.find("td:eq(12)").html(data.customerName);

                                row.find("td:eq(13)").html(data.riskName);

                                var from_date = new Date(data.fromDate);
                                row.find("td:eq(15)").html('' + strArray[from_date.getMonth()] + '/' + (from_date.getDate() <= 9 ? '0' + from_date.getDate() : from_date.getDate()) + '/' + from_date.getFullYear().toString().slice(-2));
                                var to_date = new Date(data.toDate);
                                row.find("td:eq(16)").html('' + strArray[to_date.getMonth()] + '/' + (to_date.getDate() <= 9 ? '0' + to_date.getDate() : to_date.getDate()) + '/' + to_date.getFullYear().toString().slice(-2));

                                row.find("td:eq(17)").html(data.renewalPremium);
                                row.find("td:eq(18)").html(data.manufacturer);
                                row.find("td:eq(19)").html(data.car_Model);
                                row.find("td:eq(21)").html(data.branch);

                                row.find("td:eq(22)").html(data.accountCode);
                                row.find("td:eq(23)").html(data.intermediaryType);
                                row.find("td:eq(24)").html(data.agentCode);
                                row.find("td:eq(25)").html(data.agentName);
                                row.find("td:eq(26)").html(data.authorizedCode);

                                row.find("td:eq(27)").html(data.authorizedName);
                                row.find("td:eq(30)").html(data.sumInsured);
                                //row.find("td:eq(28)").html(data.sivalue20);
                                row.find("td:eq(32)").html(data.renewedSivalue);
                                row.find("td:eq(34)").html(data.renewedWindscreenValue);
                                row.find("td:eq(35)").html(data.outstanding_Amt);
                                row.find("td:eq(36)").html(data.excess_Amt);
                                row.find("td:eq(40)").html(data.financial_Interest);
                            }
                            else if (productclass == "FI") {
                                row.find("td:eq(7)").html(data.invoiceNo);
                                if (data.phoneNumber == null) {
                                    row.find("td:eq(2)").html('<input class="form-control txtphonenumber" type="text" id="phonenumber" asp-for="@fire.PhoneNumber" value="" onkeydown="keyupFunction()" />');
                                }
                                else {
                                    row.find("td:eq(2)").html('<input class="form-control txtphonenumber" type="text" id="phonenumber" asp-for="@fire.PhoneNumber" value="' + data.phoneNumber + '" onkeydown="keyupFunction()" />');
                                }

                                row.find("td:eq(12)").html(data.customerName);
                                row.find("td:eq(13)").html(data.riskName);
                                //row.find("td:eq(13)").html(data.expiredDate);

                                var from_date = new Date(data.fromDate);
                                row.find("td:eq(15)").html('' + strArray[from_date.getMonth()] + '/' + (from_date.getDate() <= 9 ? '0' + from_date.getDate() : from_date.getDate()) + '/' + from_date.getFullYear().toString().slice(-2));
                                var to_date = new Date(data.toDate);
                                row.find("td:eq(16)").html('' + strArray[to_date.getMonth()] + '/' + (to_date.getDate() <= 9 ? '0' + to_date.getDate() : to_date.getDate()) + '/' + to_date.getFullYear().toString().slice(-2));

                                row.find("td:eq(17)").html(data.renewalPremium);
                                row.find("td:eq(19)").html(data.branch);
                                row.find("td:eq(20)").html(data.account_code);
                                row.find("td:eq(21)").html(data.intermediaryType);
                                row.find("td:eq(22)").html(data.agentCode);

                                row.find("td:eq(23)").html(data.agentName);
                                row.find("td:eq(24)").html(data.authorizedCode);
                                row.find("td:eq(25)").html(data.authorizedName);
                                row.find("td:eq(28)").html(data.sumInsured);
                                row.find("td:eq(29)").html(data.detailOccupancy);

                                row.find("td:eq(30)").html(data.locationRisk);
                                row.find("td:eq(31)").html(data.buildingClass);
                                row.find("td:eq(32)").html(data.lossRecord);
                                row.find("td:eq(33)").html(data.coverage);
                                row.find("td:eq(34)").html(data.googleMap);

                                row.find("td:eq(35)").html(data.abctypeExtinguisher);
                                row.find("td:eq(36)").html(data.co2typeExtinguishers);
                                row.find("td:eq(37)").html(data.otherExtinguisher);
                                row.find("td:eq(38)").html(data.fireextinguishersWeight);
                                row.find("td:eq(39)").html(data.numberofFireHoseReel);

                                row.find("td:eq(40)").html(data.numberofHydrant);
                                row.find("td:eq(41)").html(data.capacityofFireTank);
                                row.find("td:eq(42)").html(data.securityGuards);
                                row.find("td:eq(43)").html(data.numberofCctvcamera);
                                row.find("td:eq(44)").html(data.sqmeterofBuilding);

                                row.find("td:eq(45)").html(data.requiredExtinguishers);
                                row.find("td:eq(46)").html(data.workinProcess);
                                row.find("td:eq(50)").html(data.numberofFireTank);
                                row.find("td:eq(51)").html(data.typeofFireTank);
                                row.find("td:eq(52)").html(data.outstanding_Amt);

                                row.find("td:eq(53)").html(data.excess_Amt);
                                row.find("td:eq(54)").html(data.financial_Interest);
                            }
                            else if (productclass == "LF") {
                                row.find("td:eq(7)").html(data.invoiceNo);
                                row.find("td:eq(8)").html(data.policyNo);
                                if (data.phoneNumber == null) {
                                    row.find("td:eq(2)").html('<input class="form-control txtphonenumber" type="text" id="phonenumber" asp-for="@health.PhoneNumber" value="" onkeydown="keyupFunction()" style="width:150px !important;" />');
                                }
                                else {
                                    row.find("td:eq(2)").html('<input class="form-control txtphonenumber" type="text" id="phonenumber" asp-for="@health.PhoneNumber" value="' + data.phoneNumber + '" onkeydown="keyupFunction()" style="width:150px !important;" />');
                                }
                                row.find("td:eq(11)").html(data.cusCode);
                                row.find("td:eq(12)").html(data.customerName);

                                row.find("td:eq(13)").html(data.riskName);

                                var from_date = new Date(data.fromDate);
                                row.find("td:eq(15)").html('' + strArray[from_date.getMonth()] + '/' + (from_date.getDate() <= 9 ? '0' + from_date.getDate() : from_date.getDate()) + '/' + from_date.getFullYear().toString().slice(-2));
                                var to_date = new Date(data.toDate);
                                row.find("td:eq(16)").html('' + strArray[to_date.getMonth()] + '/' + (to_date.getDate() <= 9 ? '0' + to_date.getDate() : to_date.getDate()) + '/' + to_date.getFullYear().toString().slice(-2));

                                row.find("td:eq(17)").html(data.renewalPremium);
                                row.find("td:eq(19)").html(data.branch);

                                row.find("td:eq(20)").html(data.accountCode);
                                row.find("td:eq(21)").html(data.intermediaryType);
                                row.find("td:eq(22)").html(data.agentCode);
                                row.find("td:eq(23)").html(data.agentName);
                                row.find("td:eq(24)").html(data.authorizedCode);

                                row.find("td:eq(25)").html(data.authorizedName);
                                row.find("td:eq(27)").html(data.installmentPlan);
                                row.find("td:eq(28)").html(data.basicCoverage);
                                row.find("td:eq(29)").html(data.optionalCoverage1);
                                row.find("td:eq(30)").html(data.optionalCoverage2);
                                row.find("td:eq(31)").html(data.ncbclaimLoading);
                                row.find("td:eq(32)").html(data.outstanding_Amt);
                                row.find("td:eq(33)").html(data.excess_Amt);
                                row.find("td:eq(34)").html(data.financial_Interest);
                            }
                            else if (productclass == "MS") {
                                row.find("td:eq(7)").html(data.invoiceNo);
                                row.find("td:eq(8)").html(data.policyNo);
                                row.find("td:eq(2)").html('<input class="form-control txtphonenumber" type="text" id="phonenumber" asp-for="@pa.PhoneNumber" value="' + data.phoneNumber + '" onkeydown="keyupFunction()" style="width:150px !important;" />');
                                row.find("td:eq(11)").html(data.cusCode);
                                row.find("td:eq(12)").html(data.customerName);

                                row.find("td:eq(13)").html(data.riskName);

                                var from_date = new Date(data.fromDate);
                                row.find("td:eq(15)").html('' + strArray[from_date.getMonth()] + '/' + (from_date.getDate() <= 9 ? '0' + from_date.getDate() : from_date.getDate()) + '/' + from_date.getFullYear().toString().slice(-2));
                                var to_date = new Date(data.toDate);
                                row.find("td:eq(16)").html('' + strArray[to_date.getMonth()] + '/' + (to_date.getDate() <= 9 ? '0' + to_date.getDate() : to_date.getDate()) + '/' + to_date.getFullYear().toString().slice(-2));

                                row.find("td:eq(17)").html(data.renewalPremium);
                                row.find("td:eq(19)").html(data.branch);

                                row.find("td:eq(20)").html(data.accountCode);
                                row.find("td:eq(21)").html(data.intermediaryType);
                                row.find("td:eq(22)").html(data.agentCode);
                                row.find("td:eq(23)").html(data.agentName);
                                row.find("td:eq(24)").html(data.authorizedCode);

                                row.find("td:eq(25)").html(data.authorizedName);
                                row.find("td:eq(27)").html(data.installmentPlan);
                                row.find("td:eq(28)").html(data.basicCoverage);
                                row.find("td:eq(29)").html(data.optionalCoverage1);
                                row.find("td:eq(30)").html(data.optionalCoverage2);
                                row.find("td:eq(31)").html(data.ncbclaimLoading);
                                row.find("td:eq(32)").html(data.outstanding_Amt);
                                row.find("td:eq(33)").html(data.excess_Amt);
                                row.find("td:eq(34)").html(data.financial_Interest);
                            }
                            else if (productclass == "ENGG") {
                                row.find("td:eq(9)").html(data.invoiceNo);
                                if (data.phoneNumber == null) {
                                    row.find("td:eq(2)").html('<input class="form-control txtphonenumber" type="text" id="phonenumber" asp-for="@engg.PhoneNumber" value="" onkeydown="keyupFunction()" />');
                                }
                                else {
                                    row.find("td:eq(2)").html('<input class="form-control txtphonenumber" type="text" id="phonenumber" asp-for="@engg.PhoneNumber" value="' + data.phoneNumber + '" onkeydown="keyupFunction()" />');
                                }
                                row.find("td:eq(14)").html(data.customerName);
                                row.find("td:eq(15)").html(data.riskName);

                                var from_date = new Date(data.fromDate);
                                row.find("td:eq(17)").html('' + strArray[from_date.getMonth()] + '/' + (from_date.getDate() <= 9 ? '0' + from_date.getDate() : from_date.getDate()) + '/' + from_date.getFullYear().toString().slice(-2));
                                var to_date = new Date(data.toDate);
                                row.find("td:eq(18)").html('' + strArray[to_date.getMonth()] + '/' + (to_date.getDate() <= 9 ? '0' + to_date.getDate() : to_date.getDate()) + '/' + to_date.getFullYear().toString().slice(-2));

                                row.find("td:eq(19)").html(data.renewalPremium);
                                row.find("td:eq(21)").html(data.branch);
                                row.find("td:eq(22)").html(data.account_code);
                                row.find("td:eq(23)").html(data.intermediaryType);
                                row.find("td:eq(24)").html(data.agentCode);

                                row.find("td:eq(25)").html(data.agentName);
                                row.find("td:eq(26)").html(data.authorizedCode);
                                row.find("td:eq(27)").html(data.authorizedName);
                                row.find("td:eq(30)").html(data.sumInsured);
                                row.find("td:eq(31)").html(data.detailOccupancy);

                                row.find("td:eq(32)").html(data.locationRisk);
                                row.find("td:eq(33)").html(data.buildingClass);
                                row.find("td:eq(34)").html(data.lossRecord);
                                row.find("td:eq(35)").html(data.coverage);
                                row.find("td:eq(36)").html(data.googleMap);

                                row.find("td:eq(37)").html(data.abctypeExtinguisher);
                                row.find("td:eq(38)").html(data.co2typeExtinguishers);
                                row.find("td:eq(39)").html(data.otherExtinguisher);
                                row.find("td:eq(40)").html(data.fireextinguishersWeight);
                                row.find("td:eq(41)").html(data.numberofFireHoseReel);

                                row.find("td:eq(42)").html(data.numberofHydrant);
                                row.find("td:eq(43)").html(data.numberofFireTank);
                                row.find("td:eq(44)").html(data.typeofFireTank);
                                row.find("td:eq(45)").html(data.capacityofFireTank);
                                row.find("td:eq(46)").html(data.securityGuards);

                                row.find("td:eq(47)").html(data.numberofCctvcamera);
                                row.find("td:eq(48)").html(data.sqmeterofBuilding);
                                row.find("td:eq(49)").html(data.requiredExtinguishers);
                                row.find("td:eq(50)").html(data.workinProcess);
                                row.find("td:eq(51)").html(data.outstanding_Amt);
                                row.find("td:eq(52)").html(data.excess_Amt);
                                row.find("td:eq(53)").html(data.financial_Interest);
                            }
                            else if (productclass == "PTY") {
                                row.find("td:eq(9)").html(data.invoiceNo);
                                if (data.phoneNumber == null) {
                                    row.find("td:eq(2)").html('<input class="form-control txtphonenumber" type="text" id="phonenumber" asp-for="@engg.PhoneNumber" value="" onkeydown="keyupFunction()" />');
                                }
                                else {
                                    row.find("td:eq(2)").html('<input class="form-control txtphonenumber" type="text" id="phonenumber" asp-for="@engg.PhoneNumber" value="' + data.phoneNumber + '" onkeydown="keyupFunction()" />');
                                }
                                row.find("td:eq(14)").html(data.customerName);
                                row.find("td:eq(15)").html(data.riskName);

                                var from_date = new Date(data.fromDate);
                                row.find("td:eq(17)").html('' + strArray[from_date.getMonth()] + '/' + (from_date.getDate() <= 9 ? '0' + from_date.getDate() : from_date.getDate()) + '/' + from_date.getFullYear().toString().slice(-2));
                                var to_date = new Date(data.toDate);
                                row.find("td:eq(18)").html('' + strArray[to_date.getMonth()] + '/' + (to_date.getDate() <= 9 ? '0' + to_date.getDate() : to_date.getDate()) + '/' + to_date.getFullYear().toString().slice(-2));

                                row.find("td:eq(19)").html(data.renewalPremium);
                                row.find("td:eq(21)").html(data.branch);
                                row.find("td:eq(22)").html(data.account_code);
                                row.find("td:eq(23)").html(data.intermediaryType);
                                row.find("td:eq(24)").html(data.agentCode);

                                row.find("td:eq(25)").html(data.agentName);
                                row.find("td:eq(26)").html(data.authorizedCode);
                                row.find("td:eq(27)").html(data.authorizedName);
                                row.find("td:eq(30)").html(data.sumInsured);
                                row.find("td:eq(31)").html(data.detailOccupancy);

                                row.find("td:eq(32)").html(data.locationRisk);
                                row.find("td:eq(33)").html(data.buildingClass);
                                row.find("td:eq(34)").html(data.lossRecord);
                                row.find("td:eq(35)").html(data.coverage);
                                row.find("td:eq(36)").html(data.googleMap);

                                row.find("td:eq(37)").html(data.abctypeExtinguisher);
                                row.find("td:eq(38)").html(data.co2typeExtinguishers);
                                row.find("td:eq(39)").html(data.otherExtinguisher);
                                row.find("td:eq(40)").html(data.fireextinguishersWeight);
                                row.find("td:eq(41)").html(data.numberofFireHoseReel);

                                row.find("td:eq(42)").html(data.numberofHydrant);
                                row.find("td:eq(43)").html(data.numberofFireTank);
                                row.find("td:eq(44)").html(data.typeofFireTank);
                                row.find("td:eq(45)").html(data.capacityofFireTank);
                                row.find("td:eq(46)").html(data.securityGuards);

                                row.find("td:eq(47)").html(data.numberofCctvcamera);
                                row.find("td:eq(48)").html(data.sqmeterofBuilding);
                                row.find("td:eq(49)").html(data.requiredExtinguishers);
                                row.find("td:eq(50)").html(data.workinProcess);
                                row.find("td:eq(51)").html(data.outstanding_Amt);

                                row.find("td:eq(52)").html(data.excess_Amt);
                                row.find("td:eq(53)").html(data.financial_Interest);
                            }
                            $('#mdlLoading').modal('hide');
                        }
                        else if (data == "-1") {
                            alert("There is no responsiable party.");
                            //row.find('.txtresponsibleparty').val("");
                        }
                    },
                    error: function ajaxError(response) {
                        alert("Error");
                    }
                });
            }

        });


    });

    $('#tblOP').on('change', '.txtphonenumber', function () {
        var productclass = $('#classname').val();
        var row = $(this).closest('tr'); // get the row
        var id = row.find('.hdfId').val();
        var phonenumber = row.find('.txtphonenumber').val();

        if (id !== null) {
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: "/SMS/UpdatePhoneNo",
                data: { phonenumber: phonenumber, id: id, productclass: productclass },
                dataType: "Json",
                success: function (data) {
                    if (data != null) {

                    }
                    else if (data == "-1") {
                        alert("Phone Number can not save correctly.");
                    }
                },
                error: function ajaxError(response) {
                    alert("Error");
                }
            });
        }

    });

    $('#tblSMS').on('keyup', '.txtphonenumber', function (evt) {

        //if (event.which >= 37 && event.which <= 40) return;

        var row = $(this).closest('tr');
        row.find('.txtphonenumber').val(function (index, value) {

            return value
                // Keep only digits and decimal points:
                .replace(/[^0-9\.]/g, '');
        });

        return true;
    });

    $('#tblSMS').on('change', '.txtphonenumber', function () {
        var row = $(this).closest('tr'); // get the row
        var id = row.find('.hdfId').val();
        var phonenumber = row.find('.txtphonenumber').val();

        if (id !== null) {
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: "/Report/UpdateNewBusinessSurveyPhoneNo",
                data: { phonenumber: phonenumber, id: id},
                dataType: "Json",
                success: function (data) {
                    if (data != null) {

                    }
                    else if (data == "-1") {
                        alert("Phone Number can not save correctly.");
                    }
                },
                error: function ajaxError(response) {
                    alert("Error");
                }
            });
        }

    });

    $('#btnCheckPhNo').click(function () {

        //var rowCount = $('#tblSMS tr td').length;
        //if (rowCount <= 0) {
        //    alert("There is no data to check phone number. Please select data and retrieve data first.")
        //}
        var wrongphnocount = 0;

        var oTable = $('#tblSMS').dataTable();

        oTable.$("TR").each(function () {
            var row = $(this);
            var cphno = row.find('.hdfPhno').val();
            var id = row.find('.hdfId').val();

            if (cphno != null) {
                var expr;

                if (cphno.startsWith("093") || cphno.startsWith("0940") || cphno.startsWith("0942") || cphno.startsWith("0944") || cphno.startsWith("0945") || cphno.startsWith("0946") || cphno.startsWith("0948")
                    || cphno.startsWith("0965") || cphno.startsWith("0969") || cphno.startsWith("0970") || cphno.startsWith("0975") || cphno.startsWith("0976") || cphno.startsWith("0977") || cphno.startsWith("0978") || cphno.startsWith("0979")
                    || cphno.startsWith("098") || cphno.startsWith("0995") || cphno.startsWith("0996") || cphno.startsWith("0997") || cphno.startsWith("0998") || cphno.startsWith("0999")) {
                    //11
                    expr = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{5})$/;
                }
                else if (cphno.startsWith("0941") || cphno.startsWith("0943") || cphno.startsWith("0947") || cphno.startsWith("0949") || cphno.startsWith("0973") || cphno.startsWith("0991")) {
                    //10
                    expr = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
                }
                else if (cphno.startsWith("0925") || cphno.startsWith("0926") || cphno.startsWith("0966") || cphno.startsWith("0967")) {
                    //11
                    expr = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{5})$/;
                }
                else if (cphno.startsWith("092") || cphno.startsWith("095") || cphno.startsWith("0964") ) {
                    //9
                    expr = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})$/;
                }
                else {
                    //11
                    expr = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{5})$/;
                }

                //var element = document.getElementById("highlight");
                //element.classList.add("myStyle");
            }
            if (!expr.test(cphno)) {
                row.find('td').addClass('myStyle');

                wrongphnocount++;

                $.ajax({
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    url: "/SMS/UpdatePhNoFlag",
                    data: { id: id},
                    dataType: "Json",
                    success: function (data) {
                        if (data != null) {

                        }
                        else if (data == "-1") {
                            //alert("Phone Number can not save correctly.");
                        }
                    },
                    error: function ajaxError(response) {
                        alert("Error");
                    }
                });
            }

        });

        alert("There are " + wrongphnocount + " wrong phone numbers.")

    });

    $('#RenewalbtnCheckPhNo').click(function () {

        //var rowCount = $('#tblSMS tr td').length;
        //if (rowCount <= 0) {
        //    alert("There is no data to check phone number. Please select data and retrieve data first.")
        //}
        var wrongphnocount = 0;

        var oTable = $('#tblSMS').dataTable();

        oTable.$("TR").each(function () {
            var row = $(this);
            var cphno = row.find('.hdfPhno').val();
            var id = row.find('.hdfId').val();

            if (cphno != null) {
                var expr;

                if (cphno.startsWith("093") || cphno.startsWith("0940") || cphno.startsWith("0942") || cphno.startsWith("0944") || cphno.startsWith("0945") || cphno.startsWith("0946") || cphno.startsWith("0948")
                    || cphno.startsWith("0965") || cphno.startsWith("0969") || cphno.startsWith("0970") || cphno.startsWith("0975") || cphno.startsWith("0976") || cphno.startsWith("0977") || cphno.startsWith("0978") || cphno.startsWith("0979")
                    || cphno.startsWith("098") || cphno.startsWith("0995") || cphno.startsWith("0996") || cphno.startsWith("0997") || cphno.startsWith("0998") || cphno.startsWith("0999")) {
                    //11
                    expr = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{5})$/;
                }
                else if (cphno.startsWith("0941") || cphno.startsWith("0943") || cphno.startsWith("0947") || cphno.startsWith("0949") || cphno.startsWith("0973") || cphno.startsWith("0991")) {
                    //10
                    expr = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
                }
                else if (cphno.startsWith("0925") || cphno.startsWith("0926") || cphno.startsWith("0966") || cphno.startsWith("0967")) {
                    //11
                    expr = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{5})$/;
                }
                else if (cphno.startsWith("092") || cphno.startsWith("095") || cphno.startsWith("0964")) {
                    //9
                    expr = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})$/;
                }
                else {
                    //11
                    expr = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{5})$/;
                }

                //var element = document.getElementById("highlight");
                //element.classList.add("myStyle");
            }
            if (!expr.test(cphno)) {
                row.find('td').addClass('myStyle');

                wrongphnocount++;

                //$.ajax({
                //    type: "GET",
                //    contentType: "application/json; charset=utf-8",
                //    url: "/SMS/UpdatePhNoFlag",
                //    data: { id: id },
                //    dataType: "Json",
                //    success: function (data) {
                //        if (data != null) {

                //        }
                //        else if (data == "-1") {
                //            //alert("Phone Number can not save correctly.");
                //        }
                //    },
                //    error: function ajaxError(response) {
                //        alert("Error");
                //    }
                //});
            }

        });

        alert("There are " + wrongphnocount + " wrong phone numbers.")

    });

    $('#OPRenewalbtnCheckPhNo').click(function () {

        var wrongphnocount = 0;

        var oTable = $('#tblOP').dataTable();

        oTable.$("TR").each(function () {
            var row = $(this);
            var cphno = row.find('.hdfPhno').val();
            var id = row.find('.hdfId').val();

            if (cphno != null) {
                var expr;

                if (cphno.startsWith("093") || cphno.startsWith("0940") || cphno.startsWith("0942") || cphno.startsWith("0944") || cphno.startsWith("0945") || cphno.startsWith("0946") || cphno.startsWith("0948")
                    || cphno.startsWith("0965") || cphno.startsWith("0969") || cphno.startsWith("0970") || cphno.startsWith("0975") || cphno.startsWith("0976") || cphno.startsWith("0977") || cphno.startsWith("0978") || cphno.startsWith("0979")
                    || cphno.startsWith("098") || cphno.startsWith("0995") || cphno.startsWith("0996") || cphno.startsWith("0997") || cphno.startsWith("0998") || cphno.startsWith("0999")) {
                    //11
                    expr = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{5})$/;
                }
                else if (cphno.startsWith("0941") || cphno.startsWith("0943") || cphno.startsWith("0947") || cphno.startsWith("0949") || cphno.startsWith("0973") || cphno.startsWith("0991")) {
                    //10
                    expr = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
                }
                else if (cphno.startsWith("0925") || cphno.startsWith("0926") || cphno.startsWith("0966") || cphno.startsWith("0967")) {
                    //11
                    expr = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{5})$/;
                }
                else if (cphno.startsWith("092") || cphno.startsWith("095") || cphno.startsWith("0964")) {
                    //9
                    expr = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})$/;
                }
                else {
                    //11
                    expr = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{5})$/;
                }

                //var element = document.getElementById("highlight");
                //element.classList.add("myStyle");
            }
            if (!expr.test(cphno)) {
                row.find('td').addClass('myStyle');

                wrongphnocount++;

                //$.ajax({
                //    type: "GET",
                //    contentType: "application/json; charset=utf-8",
                //    url: "/SMS/UpdatePhNoFlag",
                //    data: { id: id },
                //    dataType: "Json",
                //    success: function (data) {
                //        if (data != null) {

                //        }
                //        else if (data == "-1") {
                //            //alert("Phone Number can not save correctly.");
                //        }
                //    },
                //    error: function ajaxError(response) {
                //        alert("Error");
                //    }
                //});
            }

        });

        alert("There are " + wrongphnocount + " wrong phone numbers.")

    });

    $('#ddlCategory').change(function () {
        var categoryid = $('#ddlCategory').val();
        if (categoryid != null) {
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: "/SMS/GetSMSByCategoryId",
                data: { CategoryId: categoryid },
                dataType: "Json",
                success: function (data) {
                    if (data != null) {
                        $('#ddlMsg').val(data.msgTemplate);
                    }
                },
                error: function ajaxError(response) {
                    alert("Error");
                }
            });
        }

    });

    $('#btnSync').click(function () {
        //$('#mdlLoading').modal('show');
        $('#mdlLoading').modal({ backdrop: 'static', keyboard: false })
    });

    $('#btnSentSMS').click(function () {
        //$('#mdlLoading').modal('show');
        $('#mdlLoading').modal({ backdrop: 'static', keyboard: false })
    });
    
    $('#btnReCreateFailCase').click(function () {
        //$('#mdlLoading').modal('show');
        $('#mdlLoading').modal({ backdrop: 'static', keyboard: false })
    });

    $('#btnReCreateFailCase').click(function () {
        //$('#mdlLoading').modal('show');
        $('#mdlLoading').modal({ backdrop: 'static', keyboard: false })
    });
    
    
});


