﻿$('document').ready(function () {

    var reqNumberofFireExtinguisher = 0;
    
    $('#btnRunRule').click(function () {
        var refNo = $('#txtReferenceNo').val();
        //if (refNo == "") {
        //    //alert("Please save the proposal first!");
        //    //return false;
        //}
        //else {
        //    document.location = '/FireRuleBase/RunRuleResult' + '?' + 'refNo='+ refNo;
        //}
       document.location = '/FireRuleBase/RunRuleResult' + '?' + 'refNo=' + refNo;
    });

    $('#btnEditProposal').click(function () {
        
    });
    
    $('#btnCustomerSearch').click(function () {
        var cusCode = $('#txtCustomerCode').val();
        if (cusCode != "") {

            $.ajax(
                {
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "/FireRuleBase/GetCustomerInfo",
                    data: JSON.stringify(cusCode),
                    dataType: "Json",
                    success: function (data) {
                        if (data != null) {                            
                            $('#txtInsuredName').val(data['name']);
                            $('#txtInsuredMailAddress').val(data['mailingaddress']);
                        }
                    }
                }
            );

        }
        else {
            alert("Please insert customer code.");
            $('#txtCustomerCode').focus();
            return false;
        }
    });

    $('#btnSaveRule').click(function () {
        /* alert($("#AgentCode option:selected").text());*/
        var refNo = $('#txtReferenceNo').val();

        //if (refNo != "") {
        //    alert("Already save the proposal.");
        //}

        var riskcount = $("#hdfRiskCount").val();
        var fireProposal = {};
        fireProposal.fireRiskDetailViews = [];
        fireProposal.Id = $("#hdfId").val(); 
        fireProposal.Class = "FI";
        fireProposal.ProductCode = $("#ProductCode").val();
        fireProposal.CusCode = $("#txtCustomerCode").val();
        fireProposal.CusName = $("#txtInsuredName").val();
        fireProposal.MailingAddress = $("#txtInsuredMailAddress").val();
        fireProposal.TotalSumInsured = $("#txtTotalSI").val();

        fireProposal.IssueDate = $("#IssueDate").val();
        fireProposal.TransactionType = $("#TransactionType").val();
        fireProposal.AgentCode = $("#AgentCode").val();
        fireProposal.AgentName = $("#AgentCode option:selected").text();
        fireProposal.AccountCode = $("#AccountCode").val();
        fireProposal.AccountHandlerCode = $("#AccountCode option:selected").text();
        
        fireProposal.IntermediaryCode = $("#IntermediaryCode").val();
        fireProposal.IntermediaryName = $("#IntermediaryCode option:selected").text();
        fireProposal.PolicyStartDate = $("#PolicyStartDate").val();
        fireProposal.PolicyEndDate = $("#PolicyEndDate").val();
        fireProposal.Premium = $("#Premium").val();
        
        var riskInfos = [];
        
        if (riskcount > 0) {
            for (var num = 1; num <= riskcount; num++) {

                var riskdetail = {};
                
                riskdetail.constructiondetail = {};
                riskdetail.feainformation = {};
                riskdetail.firewatertank = {};
                riskdetail.firesecurity = {};
                riskdetail.inventory = [];
                riskdetail.coverage = {};

                // Risk Detail
                riskdetail.Id = $("#hdfRiskId").val(); 
                riskdetail.RiskName = $("#txtRiskName_" + num).val();
                riskdetail.RiskSumInsured = $("#txtRiskSumInsured_" + num).val();
                riskdetail.RiskLocation = $("#txtLocationRisk_" + num).val();
                riskdetail.Occupancy = $("#ddlOccupancy_" + num).val();                
                riskdetail.Currency = "MMK";

                riskdetail.BuildingNo = $("#hdfBuildingNo_" + num).val();
                riskdetail.Street = $("#hdfStreet_" + num).val();
                riskdetail.WardCode = $("#hdfWardCode_" + num).val();
                // Risk Details

                // Construction

                riskdetail.constructiondetail.Id = $("#hdfConstructionId").val(); 
                riskdetail.constructiondetail.areaOfBuilding = $("#txtTotalAreaofBuilding_" + num).val();
                riskdetail.constructiondetail.roof = $("#ddlRoof_" + num).val();
                riskdetail.constructiondetail.roofDescription = $("#ddlRoof_" + num + " option:selected").text();
                riskdetail.constructiondetail.walls = $("#ddlWalls_" + num).val();
                riskdetail.constructiondetail.wallDescription = $("#ddlWalls_" + num + " option:selected").text();

                riskdetail.constructiondetail.columns = $("#ddlColumn_" + num).val();
                riskdetail.constructiondetail.columnDescription = $("#ddlColumn_" + num + " option:selected").text();
                riskdetail.constructiondetail.floor = $("#ddlFloor_" + num).val();
                riskdetail.constructiondetail.floorDescription = $("#ddlFloor_" + num + " option:selected").text();
                riskdetail.constructiondetail.constructionClass = $("#ddlTypeOfConstruction_" + num).val();

                riskdetail.constructiondetail.constructionDescription = $("#ddlTypeOfConstruction_" + num + " option:selected").text();
                riskdetail.constructiondetail.lifeOfBuilding = $("#txtLifeOfBuilding_" + num).val();
                riskdetail.constructiondetail.numberOfStorey = $("#txtNumberofStorey_" + num).val();
                riskdetail.constructiondetail.proposedSI = $("#txtProposeValue_" + num).val();
                riskdetail.constructiondetail.proposedPAE = $("#txtProposePAE_" + num).val();

                riskdetail.constructiondetail.limitedSI = $("#hdfLimitedSI_" + num).val();
                riskdetail.constructiondetail.limitedPAE = $("#hdfLimitedPAE_" + num).val();

                // Construction

                // FEA Information

                riskdetail.feainformation.Id = $("#hdfFeainformationId").val(); 
                riskdetail.feainformation.numberofAbctype = $("#ddlNumberofABCTypeExtinguisher_" + num).val();
                riskdetail.feainformation.numberofAbctypeDesp = $("#ddlNumberofABCTypeExtinguisher_" + num +" option:selected").text();
                riskdetail.feainformation.numberOfCo2type = $("#ddlNumberofCO2TypeExtinguisher_" + num).val(); 
                riskdetail.feainformation.numberOfCo2typeDesp = $("#ddlNumberofCO2TypeExtinguisher_" + num + " option:selected").text();
                riskdetail.feainformation.numberOfOtherExtinguisher = $("#ddlNumberofOtherExtinguisher_" + num).val();

                riskdetail.feainformation.numberOfOtherExtinguisherDesp = $("#ddlNumberofOtherExtinguisher_" + num + " option:selected").text();
                riskdetail.feainformation.weightOfFireExtinguisher = $("#ddlWeightofFireExtinguishers_" + num).val(); 
                riskdetail.feainformation.weightOfFireExtinguisherDesp = $("#ddlWeightofFireExtinguishers_" + num + " option:selected").text();
                riskdetail.feainformation.requiredNumberOfFireExtinguisher = $("#ddlRequiredNumberofFireExtinguishers_" + num).val(); 
                riskdetail.feainformation.requiredNumberOfFireExtinguisherDesp = $("#ddlRequiredNumberofFireExtinguishers_" + num + " option:selected").text();

                riskdetail.feainformation.numberOfHoseReel = $("#ddlNumberofHoseReel_" + num).val();
                riskdetail.feainformation.numberOfHoseReelDesp = $("#ddlNumberofHoseReel_" + num + " option:selected").text();
                riskdetail.feainformation.numberOfHydrant = $("#ddlNumberofHydrant_" + num).val(); 
                riskdetail.feainformation.numberOfHydrantDesp = $("#ddlNumberofHydrant_" + num + " option:selected").text();
                riskdetail.feainformation.proposedNumberOfFireExtinguisher = $("#hdfProposedNumberOfFireExtinguisher_" + num).val(); 
                // FEA Information

                // Fire Water Tank 
                riskdetail.firewatertank.Id = $("#hdfFirewatertankId").val(); 
                riskdetail.firewatertank.numberofFireTank = $("#ddlNumberofFireTank_" + num).val(); 
                riskdetail.firewatertank.typeofFireTank = $("#ddlTypeofFireTank_" + num).val(); 
                riskdetail.firewatertank.capacityofFireTank = $("#txtCapacityofFireTank_" + num).val(); 
                riskdetail.firewatertank.numberofFireTankDesp = $("#ddlNumberofFireTank_" + num + " option:selected").text();
                riskdetail.firewatertank.typeofFireTankDesp = $("#ddlTypeofFireTank_" + num + " option:selected").text();
                // Fire Water Tank

                // Fire Security
                riskdetail.firesecurity.Id = $("#hdfFiresecurityId").val(); 
                riskdetail.firesecurity.numberofSecurityGuards = $("#ddlNumberofSecurityGuards_" + num).val(); 
                riskdetail.firesecurity.numberofSecurityGuardsDesp = $("#ddlNumberofSecurityGuards_" + num + " option:selected").text();
                riskdetail.firesecurity.securityGuardsDutyShift = $("#ddlSecurityGuardsDutyShift_" + num).val(); 
                riskdetail.firesecurity.securityGuardsDutyShiftDesp = $("#ddlSecurityGuardsDutyShift_" + num + " option:selected").text();
                riskdetail.firesecurity.securityGuardsDutyPerHour = $("#ddlSecurityGuardsDutyPerHour_" + num).val(); 

                riskdetail.firesecurity.securityGuardsDutyPerHourDesp = $("#ddlSecurityGuardsDutyPerHour_" + num + " option:selected").text();
                riskdetail.firesecurity.numberofCctvcamera = $("#ddlNumberofCCTVCamera_" + num).val();
                riskdetail.firesecurity.numberofCCTVCameraDesp = $("#ddlNumberofCCTVCamera_" + num + " option:selected").text();
                riskdetail.firesecurity.cctvbackdated = $("#txtCCTVBackdated_" + num).val(); 
                riskdetail.firesecurity.numberofLabour = $("#txtNumberofLabour_" + num).val();

                riskdetail.firesecurity.nearestFireStation = $("#txtNearestFireStation_" + num).val();
                riskdetail.firesecurity.geocoordinate = $("#txtGoogleMapGEOcoordinate_" + num).val();
                riskdetail.firesecurity.losswithin5Years = $("#txtAnyLosswithin5Years_" + num).val();
                riskdetail.firesecurity.referralPoints = $("#txtReferralPoints_" + num).val(); 
                /// Fire Security

                // Fire Inventory                
                var table = document.getElementById("tblInventoryInfo_" + num);
                var rows = table.getElementsByTagName('tr');
                for (var i = 1; i < rows.length - 1; i++) {
                    var inventory = {};
                    inventory.itemCode = "";   
                    inventory.itemDescription = table.rows[i].cells[0].innerHTML;   
                    inventory.declaredAmount = table.rows[i].cells[1].innerHTML;
                    inventory.Id = table.rows[i].cells[3].innerHTML;
                    riskdetail.inventory.push(inventory);
                }
                // Fire Inventory

                // Fire Coverage
                riskdetail.coverage.Id = $("#hdfFirecoverageId").val(); ;
                riskdetail.coverage.basicCover = true;
                riskdetail.coverage.riotStrikeAndMaliciousDamage = $('#chkRSMD_' + num).is(':checked');
                riskdetail.coverage.aircraftDamage = $('#chkAD_' + num).is(':checked'); 
                riskdetail.coverage.impactDamage = $('#chkID_' + num).is(':checked');
                riskdetail.coverage.subsidenceAndLandslide = $('#chkSL_' + num).is(':checked');

                riskdetail.coverage.earthQuakeFireFireAndShockDamage = $('#chkEFSDC_' + num).is(':checked');
                riskdetail.coverage.explosion = $('#chkExplosion_' + num).is(':checked');
                riskdetail.coverage.spontaneousCombustion = $('#chkSC_' + num).is(':checked');
                riskdetail.coverage.stormTyphoonHurricaneTempest = $('#chkSTHT_' + num).is(':checked');
                riskdetail.coverage.burglary = $('#chkBurglary_' + num).is(':checked');

                riskdetail.coverage.warRisk = $('#chkWarRisk_' + num).is(':checked');
                riskdetail.coverage.floodAndInundation = $('#chkFloodInundation_' + num).is(':checked');
                // Fire Coverage

                // 
                fireProposal.fireRiskDetailViews.push(riskdetail);

            }
           
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/FireRuleBase/SaveFireProposal",
                data: JSON.stringify(fireProposal),
                dataType: "Json",
                success: function (data) {
                    if (data != "-1") {                      
                        $('#txtReferenceNo').val(data);
                    }
                    else if (data == "-1") {
                        alert("Error in saving process");
                    }
                    else {
                        /*alert(data);*/
                    }
                },
                error: function ajaxError(response) {
                    alert("Error");
                }
            });

        }
    });

    $('#txtTotalSI').change(function () {
        var tsi = $('#txtTotalSI').val();
        let tsival = parseFloat(tsi.replace(/,/g, ''));

        if (parseInt(tsival) > 500000000) {
            $('#mdlMsg').modal('show');
            $('#pMsg').html("This should be register as a IAR product.");

        }
    });

    //$('.btnRiskDetailAdd').on('click', function () {
    //    var $BOX_PANEL = $(this).closest('.x_panel'),
    //        $ICON = $(this).find('i'),
    //        $BOX_CONTENT = $BOX_PANEL.find('.x_content');

    //    // fix for some div with hardcoded fix class
    //    if ($BOX_PANEL.attr('style')) {
    //        $BOX_CONTENT.slideToggle(200, function () {
    //            $BOX_PANEL.removeAttr('style');
    //        });
    //    } else {
    //        $BOX_CONTENT.slideToggle(200);
    //        $BOX_PANEL.css('height', 'auto');
    //    }

    //    $ICON.toggleClass('fa-chevron-up fa-chevron-down');
    //});

});

function calculateProposePAE(id) {
    var proposeSI = document.getElementById(id).value;
    var index = id.substr(id.length - 1);
    var totalAreaOfBuilding = document.getElementById("txtTotalAreaofBuilding_" + index).value;
    let tproposeSI = parseFloat(proposeSI.replace(/,/g, ''));

    if (totalAreaOfBuilding == null || totalAreaOfBuilding == "NaN" || totalAreaOfBuilding == "underfined" || totalAreaOfBuilding == "") {
        alert("Please insert total area of building!");
        document.getElementById(id).value = "";
        return false;
    }

    if (proposeSI == null || proposeSI == "NaN" || proposeSI == "underfined" || proposeSI == "") {
        alert("Please insert propose SI!");
        
        return false;
    }

    if (totalAreaOfBuilding != null) {
        var proposePAE = tproposeSI / totalAreaOfBuilding;
       
        $("#txtProposePAE_" + index).val(Math.round(proposePAE));
       
    }
  
}

function getFileName(event) {
    var id = $(event.target).closest('.checkboxrow').find('input:checkbox').attr('id');
    var el = document.getElementById(id);

    document.getElementById(id).checked = true;
   
    var files = event.target.files;
    var formData = new FormData();
    if (files.length > 0) {
        for (var i = 0; i < files.length; i++) {
            formData.append("fileInput", files[i]);
            if (id == "branchauthority") {              
                formData.append('fileName', "branchauthority");                
            }
            if (id == "checklist") {               
                formData.append('fileName', "checklist");
            }
            if (id == "companyregisteration") {
                formData.append('fileName', "companyregisteration");
            }
            if (id == "comparison") {
                formData.append('fileName', "comparison");
            }
            if (id == "copyofinvoice") {
                formData.append('fileName', "copyofinvoice");
            }
            if (id == "copyofNRC") {
                formData.append('fileName', "copyofNRC");
            }
            if (id == "dominantshareholder") {
                formData.append('fileName', "dominantshareholder");
            }
            if (id == "endorsecancel") {
                $("#endorsecancel").val(true);
                formData.append('fileName', "endorsecancel");
            }
            if (id == "feainfo") {
                formData.append('fileName', "feainfo");
            }
            if (id == "ledger") {
                formData.append('fileName', "ledger");
            }
            if (id == "letterofproposalSI") {
                formData.append('fileName', "letterofproposalSI");
            }
            if (id == "listoffurniture") {
                formData.append('fileName', "listoffurniture");
            }
            if (id == "lossratio") {
                formData.append('fileName', "lossratio");
            }
            if (id == "ownercertificate") {
                formData.append('fileName', "ownercertificate");
            }
            if (id == "proposal") {
                formData.append('fileName', "proposal");
            }
            if (id == "risksurroundingphoto") {
                formData.append('fileName', "risksurroundingphoto");
            }
            if (id == "sketch") {
                formData.append('fileName', "sketch");
            }
            if (id == "underinsuranceletter") {
                formData.append('fileName', "underinsuranceletter");
            }
        }

        $.ajax({
            type: "POST",
            url: "/FireRuleBase/PostAttachment",
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
                
            }

        });

    }
 
   
    
  
}

function addInventory_click(clicked_id) {
    debugger;
    var index = clicked_id.substr(clicked_id.length - 1);
    

    //Reference the Name and Country TextBoxes.
    var ddlInventory = $("#ddlInventory_" + index).val();
    var txtDeclaredAmt = $("#txtDeclaredAmt_" + index).val();
  
    if (ddlInventory == null || ddlInventory == 'undefined' || ddlInventory == "") {
        alert('Data should not be empty!');
    }
    if (txtDeclaredAmt == null || txtDeclaredAmt == 'undefined' || txtDeclaredAmt == "") {
        alert('Data should not be empty!');
    }

    //Get the reference of the Table's TBODY element.
    var tBody = $("#tblInventoryInfo_" + index +" > TBODY")[0];
    //var rowId = uuidv4();
    //Add Row.
    var row = tBody.insertRow(-1);

    //Add Weight of Fire Extinguisher cell.
    var cell = $(row.insertCell(-1));
    cell.html(ddlInventory);

    //Add Type of Fire Extinguisher cell.
    cell = $(row.insertCell(-1));
    cell.html(txtDeclaredAmt);

   
    //Add Button cell.
    cell = $(row.insertCell(-1));
    var btnRemove = $("<button />");
    btnRemove.attr("type", "button");
    btnRemove.attr("id", "btnInventoryRemove_" + index);
    btnRemove.attr("class", "btn");    
    btnRemove.attr("onclick", "Remove(this);");
    //btnRemove.addClass("fas fa-remove"); 

    var icon = $("<i />");
    icon.attr("class", "fa fa-remove");
    btnRemove.append(icon);
    cell.append(btnRemove);

    //Clear the TextBoxes.
    $("#ddlInventory_" + index).val("");
    $("#txtDeclaredAmt_" + index).val("");
   
}

function Remove(button) {
    var id = button.id;
    var index = id.substr(id.length - 1);
    //Determine the reference of the Row using the Button.
    var row = $(button).closest("TR");
    var name = $("TD", row).eq(0).html();

    var table = $("#tblInventoryInfo_" + index)[0];
    //Delete the Table row using it's Index.
    table.deleteRow(row[0].rowIndex);

}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function getWardData() {
    debugger;
    //const xhr = new XMLHttpRequest();
    //xhr.open("GET", "https://uatecom.ayasompo.com:5005/api/UnderWritting/GetLocationDetailByWardCode/MMR02700001");
    //xhr.setRequestHeader("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjgzMGQwMmZmLWI3NzUtNDJiNy05MmJjLWVhMDI4YjU4MTZiYSIsIm5iZiI6MTY4MzE4NjQ5OSwiZXhwIjoxNjgzMTk3Mjk5LCJpYXQiOjE2ODMxODY0OTl9.nhGsHlO8s6HXzCd2v6Fz1TvIerOo8fnl5cADeHw3FPs");
    //xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
   
    //xhr.send();

    //xhr.onload = () => {
    //    if (xhr.readyState == 4 && xhr.status == 200) {
    //        console.log(JSON.parse(xhr.responseText));
    //        var addressData = JSON.parse(xhr.responseText);
           
    //        alert(addressData.town_description);
    //    } else {
    //        console.log(`Error: ${xhr.status}`);
    //    }
    //};

    //var request = new XMLHttpRequest();
    var apiUrl = "https://uatecom.ayasompo.com:5005/api/UnderWritting/GetLocationDetailByWardCode/MMR02700001";
    //request.open('GET', 'https://uatecom.ayasompo.com:5005/api/UnderWritting/GetLocationDetailByWardCode/MMR02700001');
    var req = new XMLHttpRequest();
    req.open("GET", apiUrl, true);
    req.setRequestHeader('Content-Type', 'application/json');
    req.setRequestHeader("Accept", "*/*");
    req.setRequestHeader("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjgzMGQwMmZmLWI3NzUtNDJiNy05MmJjLWVhMDI4YjU4MTZiYSIsIm5iZiI6MTY4MzE5NDM0MywiZXhwIjoxNjgzMjA1MTQzLCJpYXQiOjE2ODMxOTQzNDN9.NyE7wZgis4wkkO1fwEMi1JFnVv3jGZzd1PyNqTDejYc");

    req.send();

    req.onreadystatechange = function () {
        if (req.readyState == 4 && req.status == 200) {
            debugger;
            console.log(JSON.parse(req.responseText));
            var addressData = JSON.parse(req.responseText);
            alert(addressData.town_description);
        }
        else {
            alert(req.readyState);
        }
    };

   
    
}

function calculateSqlMeter(id) {
    
    var areaOftheBuilding = document.getElementById(id).value;
   
    var index = id.substr(id.length - 1);

    var sqlMeter = 0.092903;
    var sqlMeterresult = 0;
    var requiredFireExtenguisher = 0;
    if (areaOftheBuilding != null || areaOftheBuilding != '') {
        sqlMeterresult = sqlMeter * areaOftheBuilding;
        requiredFireExtenguisher = (sqlMeterresult - 150) / 200 + 2;

        reqNumberofFireExtinguisher = Math.round(requiredFireExtenguisher);

        let dd = document.getElementById("ddlRequiredNumberofFireExtinguishers_" + index);

        for (var i = 0; i < dd.options.length; i++) {
            if (dd.options[i].text == reqNumberofFireExtinguisher) {
                dd.options[i].setAttribute('selected', true);
                return;
            }
        }


    }
    else {
        alert("Please enter total area of the building!");
        return false;
    }
}

function OnGradeChanged(id) {
    //alert(id);
    //var index = id.substr(id.length - 1);
    //alert(index);

    //let dd = document.getElementById("ddlNumberofABCTypeExtinguisher_" + index);
    //alert(dd);
    //alert(dd.options[index].text);
}

function GetBuildingClass(id) {
    var index = id.substr(id.length - 1);
   
    var roofVal = document.getElementById("ddlRoof_" + index).value;
    var wallVal = document.getElementById("ddlWalls_" + index).value;
    var columnVal = document.getElementById("ddlColumn_" + index).value;
    var floorVal = document.getElementById("ddlFloor_" + index).value;
   
    $.ajax(
        {
            url: "/FireRuleBase/GetBuildingClass",
            data: { roof: roofVal, wall: wallVal, column: columnVal, floor: floorVal },
            contentType: "application/json; charset=utf-8",
            type: "GET",
            success: function (data) {
                if (data != null) {
                    let dd = document.getElementById("ddlTypeOfConstruction_" + index);         
                    for (var i = 0; i < dd.options.length; i++) {
                        if (dd.options[i].value == data) {
                            dd.options[i].setAttribute('selected', true);
                            return;
                        }
                    }
                }               
            }
        }    
    );

}

function CalculatePAE(id) {
    var index = id.substr(id.length - 1);
    var totalAreaOfBuilding = document.getElementById("txtTotalAreaofBuilding_" + index).value;
    var typeOfConstruction = document.getElementById("ddlTypeOfConstruction_" + index).value;
    var lifeofBuilding = document.getElementById("txtLifeOfBuilding_" + index).value;
    if (totalAreaOfBuilding == null || totalAreaOfBuilding == 'underfined') {
        alert("Please select the roof, column, wall and floor value!");
        return false;
    }

    if (typeOfConstruction == null || typeOfConstruction == 'underfined' || typeOfConstruction == "") {
        alert("Please select the roof, column, wall and floor value!");

        return false;
    }
    if (lifeofBuilding == null || lifeofBuilding == 'underfined' || lifeofBuilding == "") {
        alert("Please select the life of building value!");
        return false;
    }

    var tboby = $('#tblLimitedPAE_' + index + ' tbody');
    tboby.empty();

    
    $.ajax(
        {
            url: "/FireRuleBase/CalculatePAE",
            data: { classofBuilding: typeOfConstruction, lifetimeofBuilding: lifeofBuilding, totalAreaOfBuilding: totalAreaOfBuilding },
            contentType: "application/json; charset=utf-8",
            type: "GET",
            success: function (data) {
                if (data != null) {
                    var divLimitedPAE = document.getElementById('divLimitedPAE_' + index);
                    divLimitedPAE.style.display = 'block';

                    $("#hdfLimitedSI_" + index).val(data['limitedpae']);
                    $("#hdfLimitedPAE_" + index).val(data['limitedvalue']);

                    var tr = $('<tr></tr>')
                    tr.append('<td>' + data['limitedpae'] + '</td>')
                    tr.append('<td>' + data['limitedvalue'] + '</td>')

                    tboby.append(tr);

                }
            }
        }
    );

}

function GOECoordinate(id) {
    var index = id.substr(id.length - 1);
    
    $("#GeoModal").modal("show");
    var mapOptions = {
        center: new google.maps.LatLng(16.774160319891767, 96.16846909458707),
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var infoWindow = new google.maps.InfoWindow();
    var latlngbounds = new google.maps.LatLngBounds();
    var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
    google.maps.event.addListener(map, 'click', function (e) {
        // alert("Latitude: " + e.latLng.lat() + "\r\nLongitude: " + e.latLng.lng());

        document.getElementById("txtGoogleMapGEOcoordinate_" + index).value = e.latLng.lat() + "," + e.latLng.lng();
        $("#GeoModal").modal("hide");
    });
 
}

//function uploadFiles(inputId) {
//    var input = document.getElementById(inputId);
//    var files = input.files;
//    var formData = new FormData();

//    for (var i = 0; i !== files.length; i++) {
//        formData.append("files", files[i]);
//    }

//    $.ajax(
//        {
//            url: "/FireRuleBase/Upload",
//            data: formData,
//            processData: false,
//            contentType: false,
//            type: "POST",
//            success: function (data) {
//                //var count = data.length;
//                //$('#riskCount').val(count);
//                alert($('#riskCount').val());
//                alert(data.length);
//            }
//        }
//    );
//}
