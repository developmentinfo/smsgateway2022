﻿$('document').ready(function () {

    var dropdownlist = "ddlValue1";

    var divAdditionalRule = document.getElementById("divAdditionalRule");
    divAdditionalRule.style.display = 'none';

    var divTextBox2 = document.getElementById("divTextBox2");
    divTextBox2.style.display = 'none';

    $('#ddlQuestionId1').change(function () {

        var e = document.getElementById("ddlQuestionId1");
        var value = e.value;
        var text = e.options[e.selectedIndex].text;
        // OCCUPATION OF THE BUILDING
        if (value == "0HEAD1900000980") {
            // asyncFn();
            GetOccupationListDropDown();
        }

        // For RIOT STRIKE AND MALICIOUS DAMAGE
        if (value == "0HEAD1900000985" ) {
            // asyncFn();
            dropdownlist = "ddlValue1";
            GetStatusListDropDown(dropdownlist);
        }
        // AIRCRAFT DAMAGE
        if (value == "0HEAD1900000986") {
            dropdownlist = "ddlValue1";
            GetStatusListDropDown(dropdownlist);
        }
        // EARTH-QUAKE FIRE, FIRE AND SHOCK DMG CAUSED BY EQ
        if (value == "0HEAD1900000989") {
            dropdownlist = "ddlValue1";
            GetStatusListDropDown(dropdownlist);
        }
        // STORM, TYPHOON, HURRICANE, TEMPEST
        if (value == "0HEAD1900000992") {
            dropdownlist = "ddlValue1";
            GetStatusListDropDown(dropdownlist);
        }
        // FLOOD AND INUNDATION
        if (value == "0HEAD1900000995") {
            dropdownlist = "ddlValue1";
            GetStatusListDropDown(dropdownlist);
        }
        //SUBSIDENCE & LANDSLIDE
        if (value == "0HEAD1900000988") {
            dropdownlist = "ddlValue1";
            GetStatusListDropDown(dropdownlist);
        }
        // Burglary
        if (value == "0HEAD1900000993") {
            dropdownlist = "ddlValue1";
            GetStatusListDropDown(dropdownlist);
        }
        // War Risk
        if (value == "0HEAD1900000994") {
            dropdownlist = "ddlValue1";
            GetStatusListDropDown(dropdownlist);
        }
        
        // For Transaction Type
        if (value == "transactiontype") {
            // asyncFn();
            dropdownlist = "ddlValue1";
            GetTransactionListDropDown(dropdownlist);
        }
        // For STORM, TYPHOON, HURRICANE, TEMPEST
        if (value == "0HEAD1900000992") {
            dropdownlist = "ddlValue1";
            GetStatusListDropDown(dropdownlist);
        }

        if (value == "0HEAD1900000981") {
            dropdownlist = "ddlValue1";
            GetClassListDropDown(dropdownlist);
        }
        

        // Risk Sum Insured
        if (value == "0HEAD1900000969") {
            var divTextBox2 = document.getElementById("divTextBox2");
            divTextBox2.style.display = 'block';

            var divQuestion2 = document.getElementById("divQuestion2");
            divQuestion2.style.display = 'none';
        }
        else {
            var divTextBox2 = document.getElementById("divTextBox2");
            divTextBox2.style.display = 'none';

            var divQuestion2 = document.getElementById("divQuestion2");
            divQuestion2.style.display = 'block';

        }

    });

    $('#ddlQuestionId3').change(function () {
        var e = document.getElementById("ddlQuestionId3");
        var value = e.value;
        var text = e.options[e.selectedIndex].text;

        // For RIOT STRIKE AND MALICIOUS DAMAGE
        if (value == "0HEAD1900000985") {
            // asyncFn();
            dropdownlist = "ddlValue2";
            GetStatusListDropDown(ddlValue2);
        }
        // For Transaction Type
        if (value == "transactiontype") {
            // asyncFn();
            dropdownlist = "ddlValue2";
            GetTransactionListDropDown(dropdownlist);
        }
        if (value == "productcode") {
            // asyncFn();
            dropdownlist = "ddlValue2";
            GetTransactionListDropDown(dropdownlist);
        }
        // For Risk Location City 
        if (value == "0HEAD1900000953") {
            dropdownlist = "ddlValue2";
            GetRegionListDropDown(dropdownlist);
        }
        
    });

    $('#btnRuleAdd').click(function () {
        //$('#divAdditionalRule').
        var divAdditionalRule = document.getElementById("divAdditionalRule");
        if (divAdditionalRule.style.display == 'block') {
            divAdditionalRule.style.display = 'none';
        }
        else {
            divAdditionalRule.style.display = 'block';
        }
    });

    $('#ddlValue1').change(function () {
        var e = document.getElementById("ddlValue1");
        var value = e.value;

        $('#Value1').val(value);
       
    });
});

function GetRegionListDropDown() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/FireRuleBase/GetRegionList",
        dataType: "Json",
        success: function (data) {
            if (data != null) {
                $('#ddlValue2').empty().append($('<option value="0"></option>'));
                $.each(data, function (i, item) {
                    $('#ddlValue2').append($("<option></option>").val(item['name']).html(item['name']));
                });
            }
            else if (data == "-1") {
                alert("There is no responsiable party.");
            }
        },
        error: function ajaxError(response) {
            alert("Error");
        }
    });
}

function GetStatusListDropDown(dropdownlist) {
        var options = ["","Active"];

    if (dropdownlist == "ddlValue1") {
        $('#ddlValue1').empty();

            $.each(options, function (i, p) {
                $('#ddlValue1').append($('<option></option>').val(p).html(p));
            });
        }
        else {
        $('#ddlValue2').empty();
            $.each(options, function (i, p) {
                $('#ddlValue2').append($('<option></option>').val(p).html(p));
            });
        }
}

function GetTransactionListDropDown(dropdownlist) {
    var options = ["","NEW", "RENEW"];

    if (dropdownlist == "ddlValue1") {
        $('#ddlValue1').empty();

        $.each(options, function (i, p) {
            $('#ddlValue1').append($('<option></option>').val(p).html(p));
        });
    }
    else {
        $('#ddlValue2').empty();

        $.each(options, function (i, p) {
            $('#ddlValue2').append($('<option></option>').val(p).html(p));
        });
    }
    
}

function GetClassListDropDown(dropdownlist) {
    var options = ["", "3RD CLASS","4TH CLASS"];

    if (dropdownlist == "ddlValue1") {
        $('#ddlValue1').empty();

        $.each(options, function (i, p) {
            $('#ddlValue1').append($('<option></option>').val(p).html(p));
        });
    }
    else {
        $('#ddlValue2').empty();

        $.each(options, function (i, p) {
            $('#ddlValue2').append($('<option></option>').val(p).html(p));
        });
    }

}

GetClassListDropDown
function GetTransactionListDropDown(dropdownlist) {
    var options = ["", "FFI", "FSD", "FCS", "FCT", "FFG", "IAR","FAT"];

    if (dropdownlist == "ddlValue1") {
        $('#ddlValue1').empty();

        $.each(options, function (i, p) {
            $('#ddlValue1').append($('<option></option>').val(p).html(p));
        });
    }
    else {
        $('#ddlValue2').empty();

        $.each(options, function (i, p) {
            $('#ddlValue2').append($('<option></option>').val(p).html(p));
        });
    }

}
function GetOccupationListDropDown() {

    var userData = {};
    userData.username = "user1";
    userData.password = "user@123";
    //userData.username = "crmprod";
    //userData.password = "Crm123!@#";

    var apiUrl = "https://uatecom.ayasompo.com:5003/users/authenticate";
    //var apiUrl = "https://auth.ayasompo.com/users/authenticate";

    var req = new XMLHttpRequest();
    req.open("POST", apiUrl, true);
    req.setRequestHeader('Content-Type', 'application/json');
    req.setRequestHeader("Accept", "*/*");

    req.onreadystatechange = function () {
        if (req.readyState == 4 && req.status == 200) {
            debugger;
            req.onreadystatechange = null;

            var jsonResults = JSON.parse(req.response);
            if (jsonResults.access_token) {
                getOccupationDropDown("Bearer" + " " + jsonResults.access_token);
            }
            
        }
    };

    req.send(JSON.stringify(userData));
}

function getOccupationDropDown(access_token) {
    debugger;

    var apiUrl = "https://uatecom.ayasompo.com/MasterData/GetBuildingOccupationData";
    //var apiUrl = "https://auth.ayasompo.com/users/authenticate";

    var req = new XMLHttpRequest();
    req.open("GET", apiUrl, true);
    req.setRequestHeader('Content-Type', 'application/json');
    req.setRequestHeader("Accept", "*/*");
    req.setRequestHeader("Authorization", access_token);

    req.onreadystatechange = function () {
        if (req.readyState == 4 && req.status == 200) {
            debugger;
            req.onreadystatechange = null;
            var jsonResults = JSON.parse(req.response);
            var Occupancyoptions = jsonResults;
            $('#ddlValue1').empty();
           
            $.each(Occupancyoptions, function (i, p) {
                $('#ddlValue1').append($('<option></option>').val(p["RFT_DESCRIPTION"]).html(p["RFT_DESCRIPTION"]));
            });
        }
    };

    req.send();

   

}