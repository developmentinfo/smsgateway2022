﻿using AYASOMPO_SMSGatewayV2.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Services
{
    public class FireRuleValidation
    {
        MainDBContext _context = new MainDBContext();
        MiddlewareAPIService _middlewareAPI = new MiddlewareAPIService();
        public async Task<MasterDataViewModel> GetBuildingClass(string roof, string wall, string column, string floor)
        {
            MasterDataViewModel masterDataViewModel = new MasterDataViewModel();
            string rft_desp = "";
            string rft_code = "";
            int maxClass = 0;
            try
            {
                List<int> classArr = new List<int>();

                List<MasterDataViewModel> lstClassOfBuilding = new List<MasterDataViewModel>();

                lstClassOfBuilding = await _middlewareAPI.GetBuildingClassData();

                Roof roof1 = new Roof();
                Wall wall1 = new Wall();
                Column column1 = new Column();
                Floor floor1 = new Floor();

                int roofLevel = 0;
                int wallLevel = 0;
                int floorLevel = 0;
                int columnLevel = 0;

                roof1 = _context.Roof.Where(r => r.Code == roof).FirstOrDefault();
                wall1 = _context.Wall.Where(w => w.Code == wall).FirstOrDefault();
                column1 = _context.Column.Where(c => c.Code == column).FirstOrDefault();
                floor1 = _context.Floor.Where(f => f.Code == floor).FirstOrDefault();

                if (roof1 != null)
                {
                    roofLevel = Convert.ToInt32(roof1.Class);
                    classArr.Add(roofLevel);
                }
                if (wall1 != null)
                {
                    wallLevel = Convert.ToInt32(wall1.Class);
                    classArr.Add(wallLevel);
                }
                if (column1 != null)
                {
                    columnLevel = Convert.ToInt32(column1.Class);
                    classArr.Add(columnLevel);
                }
                if (floor1 != null)
                {
                    floorLevel = Convert.ToInt32(floor1.Class);
                    classArr.Add(floorLevel);
                }

                maxClass = classArr.Max();
                if (maxClass > 0)
                {
                    switch (maxClass)
                    {
                        case 1:
                            rft_desp = "1ST CLASS";
                            break;
                        case 2:
                            rft_desp = "2ND CLASS";
                            break;
                        case 3:
                            rft_desp = "3RD CLASS";
                            break;
                        case 4:
                            rft_desp = "4TH CLASS";
                            break;
                    }
                }

                masterDataViewModel = lstClassOfBuilding.Where(c => c.RFT_DESCRIPTION == rft_desp).FirstOrDefault();
                if (masterDataViewModel != null)
                {
                    return masterDataViewModel;
                }

                return masterDataViewModel;
            }  
            catch (Exception ex)
            {
                return masterDataViewModel;
            }
        }

        public List<RuleResultViewModel> CheckingRules(List<RuleResultViewModel> lstRuleResultViewModel, FireRuleProposalViewModel fireRuleProposalViewModel)
        {
            List<Rules> lstRules = new List<Rules>();
            List<RuleResultViewModel> lstRuleResults = new List<RuleResultViewModel>();
            try
            {
                if (lstRuleResultViewModel.Count() > 0)
                {
                    string qvalue = "";
                    foreach (var ruleQuestion in lstRuleResultViewModel)
                    {
                        RuleResultViewModel ruleResultViewModel = new RuleResultViewModel();
                        Rules rule1 = new Rules();


                        if (ruleQuestion.questionname == "OCCUPATION OF THE BUILDING")
                        {
                            qvalue = ruleQuestion.questionvalue.Substring(6);
                        }
                        else
                        {
                            qvalue = ruleQuestion.questionvalue;
                        }

                        rule1 = _context.Rules.Where(r => r.Preference1 == ruleQuestion.questionId && r.Value1.Contains(qvalue)).FirstOrDefault();

                        if (rule1 != null)
                        {
                            if (rule1.Preference2 != null)
                            {
                                if (rule1.Preference2 == "productcode")
                                {
                                    Rules rule2 = new Rules();
                                    rule2 = _context.Rules.Where(r => r.Preference1 == ruleQuestion.questionId && r.Value2.Contains(fireRuleProposalViewModel.ProductCode)).FirstOrDefault();
                                    if (rule2 != null)
                                    {
                                        ruleResultViewModel.questionId = ruleQuestion.questionId;
                                        ruleResultViewModel.questionvalue = ruleQuestion.questionvalue;
                                        ruleResultViewModel.questionname = ruleQuestion.questionname;
                                        ruleResultViewModel.riskName = ruleQuestion.riskName;
                                        ruleResultViewModel.action = rule1.Action;
                                        lstRuleResults.Add(ruleResultViewModel);
                                    }
                                }
                                else if (rule1.Preference2 == "transactiontype")
                                {
                                    Rules rule2 = new Rules();
                                    rule2 = _context.Rules.Where(r => r.Preference1 == ruleQuestion.questionId && r.Value2.Contains(fireRuleProposalViewModel.TransactionType)).FirstOrDefault();
                                    if (rule2 != null)
                                    {
                                        ruleResultViewModel.questionId = ruleQuestion.questionId;
                                        ruleResultViewModel.questionvalue = ruleQuestion.questionvalue;
                                        ruleResultViewModel.questionname = ruleQuestion.questionname;
                                        ruleResultViewModel.riskName = ruleQuestion.riskName;
                                        ruleResultViewModel.action = rule1.Action;
                                        lstRuleResults.Add(ruleResultViewModel);
                                    }
                                }
                                else
                                {
                                    RuleResultViewModel Pre2 = new RuleResultViewModel();
                                    Pre2 = lstRuleResultViewModel.Where(rq => rq.questionId == rule1.Value2).FirstOrDefault();
                                    if (Pre2 != null)
                                    {
                                        if (Pre2.questionvalue == rule1.Value2)
                                        {
                                            ruleResultViewModel.questionId = ruleQuestion.questionId;
                                            ruleResultViewModel.questionvalue = ruleQuestion.questionvalue;
                                            ruleResultViewModel.questionname = ruleQuestion.questionname;
                                            ruleResultViewModel.riskName = ruleQuestion.riskName;
                                            ruleResultViewModel.action = rule1.Action;
                                            lstRuleResults.Add(ruleResultViewModel);
                                        }
                                    }
                                }
                            }
                            else // there is no Preference Value 2
                            {
                                ruleResultViewModel.questionId = ruleQuestion.questionId;
                                ruleResultViewModel.questionvalue = ruleQuestion.questionvalue;
                                ruleResultViewModel.questionname = ruleQuestion.questionname;
                                ruleResultViewModel.riskName = ruleQuestion.riskName;
                                ruleResultViewModel.action = rule1.Action;
                                lstRuleResults.Add(ruleResultViewModel);
                            }

                        }

                    }
                }

                return lstRuleResults;
            }
            catch (Exception ex)
            {
                return lstRuleResults;
            }
        }

        public List<RuleResultViewModel> CheckingManualRules(List<RuleResultViewModel> lstRuleResultViewModel, List<RuleResultViewModel> lstSummaryRuleResult, FireRuleProposalViewModel fireRuleProposalViewModel)
        {
            try
            {
                RuleResultViewModel ruleResultViewModel = new RuleResultViewModel();
                // Checking Total SI 
                if (fireRuleProposalViewModel.TotalSumInsured >= 500000000)
                {
                    ruleResultViewModel = new RuleResultViewModel();
                    ruleResultViewModel.questionId = "Total Sum Insured";
                    ruleResultViewModel.questionvalue = fireRuleProposalViewModel.TotalSumInsured.ToString();
                    ruleResultViewModel.questionname = "Total Sum Insured";
                    ruleResultViewModel.riskName = "-";
                    ruleResultViewModel.action = "Consider with IAR Policy";
                    lstSummaryRuleResult.Add(ruleResultViewModel);
                }
                foreach (var risk in fireRuleProposalViewModel.fireRiskdetails)
                {
                    // Check Air Craft Damage
                    if(risk.firecoverage.AircraftDamage == true)
                    {
                        //string questionId = lstRuleResultViewModel.Where(q => q.questionId)
                        ruleResultViewModel = new RuleResultViewModel();
                        ruleResultViewModel.questionId = "AIRCRAFT DAMAGE";
                        ruleResultViewModel.questionvalue = "Y";
                        ruleResultViewModel.questionname = "AIRCRAFT DAMAGE";
                        ruleResultViewModel.riskName = risk.RiskName;
                        ruleResultViewModel.action = "Risk Location \n" + risk.RiskLocation +"\n"+ "Floor \n" + risk.constructiondetail.FloorDescription;
                        lstSummaryRuleResult.Add(ruleResultViewModel);
                    }

                    // Checking Burglary and Number of Security Guards 
                    if (risk.firecoverage.Burglary == true)
                    {
                        if(Convert.ToInt32(risk.firesecurity.NumberofSecurityGuardsDesp) <= 0)
                        {
                            //string questionId = lstRuleResultViewModel.Where(q => q.questionId)
                            ruleResultViewModel = new RuleResultViewModel();
                            ruleResultViewModel.questionId = "Number of Security Guards ";
                            ruleResultViewModel.questionvalue = risk.firesecurity.NumberofSecurityGuardsDesp;
                            ruleResultViewModel.questionname = "Number of Security Guards ";
                            ruleResultViewModel.riskName = risk.RiskName;
                            ruleResultViewModel.action = "Must have 24 hours security";
                            lstSummaryRuleResult.Add(ruleResultViewModel);
                        }
                       
                    }

                    // Checking Burglary and Security Guards Duty Shift
                    if (risk.firecoverage.Burglary == true)
                    {
                        if (risk.firesecurity.SecurityGuardsDutyShiftDesp == null)
                        {
                            //string questionId = lstRuleResultViewModel.Where(q => q.questionId)
                            ruleResultViewModel = new RuleResultViewModel();
                            ruleResultViewModel.questionId = "Security Guards Duty Shift";
                            ruleResultViewModel.questionvalue = "-";
                            ruleResultViewModel.questionname = "Security Guards Duty Shift";
                            ruleResultViewModel.riskName = risk.RiskName;
                            ruleResultViewModel.action = "Must have 24 hours security";
                            lstSummaryRuleResult.Add(ruleResultViewModel);
                        }

                    }

                    // Checking Burglary and Number of CCTV Camera
                    if (risk.firecoverage.Burglary == true)
                    {
                        if (Convert.ToInt32(risk.firesecurity.NumberofCCTVCameraDesp) <= 0)
                        {
                            //string questionId = lstRuleResultViewModel.Where(q => q.questionId)
                            ruleResultViewModel = new RuleResultViewModel();
                            ruleResultViewModel.questionId = "Number of CCTV Camera";
                            ruleResultViewModel.questionvalue = risk.firesecurity.NumberofCCTVCameraDesp;
                            ruleResultViewModel.questionname = "Number of CCTV Camera";
                            ruleResultViewModel.riskName = risk.RiskName;
                            ruleResultViewModel.action = "Must have CCTV record";
                            lstSummaryRuleResult.Add(ruleResultViewModel);
                        }

                    }

                    // Checking Required Number Of Fire Extinguishers
                    if (Convert.ToInt32(risk.feainformation.RequiredNumberOfFireExtinguisherDesp) > 0)
                    {
                        int abcType = string.IsNullOrEmpty(risk.feainformation.NumberofAbctypeDesp) ? 0 : Convert.ToInt32(risk.feainformation.NumberofAbctypeDesp);
                        int CO2Type = string.IsNullOrEmpty(risk.feainformation.NumberOfCo2typeDesp) ? 0 : Convert.ToInt32(risk.feainformation.NumberOfCo2typeDesp);
                        int otherFireType = string.IsNullOrEmpty(risk.feainformation.NumberOfOtherExtinguisherDesp) ? 0 : Convert.ToInt32(risk.feainformation.NumberOfOtherExtinguisherDesp);

                        int proposeNumber = abcType + CO2Type + otherFireType;
                        if(Convert.ToInt32(risk.feainformation.RequiredNumberOfFireExtinguisherDesp) < proposeNumber)
                        {
                            ruleResultViewModel = new RuleResultViewModel();
                            ruleResultViewModel.questionId = "Required Number Of Fire Extinguishers";
                            ruleResultViewModel.questionvalue = proposeNumber.ToString();
                            ruleResultViewModel.questionname = "Required Number Of Fire Extinguishers";
                            ruleResultViewModel.riskName = risk.RiskName;
                            ruleResultViewModel.action = "Install at least \"Value of Required Number of Fire Extinguishers\" (3kg, ABC Powder Type) per SqM.";
                            lstSummaryRuleResult.Add(ruleResultViewModel);
                        }

                        
                    }
                }

                return lstSummaryRuleResult;
            }
            catch (Exception ex)
            {
                return lstSummaryRuleResult;
            }
        }
    }
}
