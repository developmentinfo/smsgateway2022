﻿using AYASOMPO_SMSGatewayV2.Controllers;
using AYASOMPO_SMSGatewayV2.Models;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Services.SchedularService
{
    public class NotificationJob : IJob
    {
        SMSSchedularController smsSchedular = new SMSSchedularController();
        private readonly ILogger<NotificationJob> _logger;
        public NotificationJob(ILogger<NotificationJob> logger)
        {
            _logger = logger;
        }
        public Task Execute(IJobExecutionContext context)
        {
            SMSSchedularController smsSchedular = new SMSSchedularController();
            SchedulerLog schedulerLog = new SchedulerLog();
            try
            {
                schedulerLog.JobId = Guid.Parse(context.JobDetail.Key.Name);
                schedulerLog.JobName = context.JobDetail.Description;
                schedulerLog.Datetime = DateTime.Now.Date;
                schedulerLog.StartTime = DateTime.Now;
                smsSchedular.StartJob(schedulerLog).Wait();
            }
            catch (Exception ex)
            {
                return Task.CompletedTask;
            }
            return Task.CompletedTask;
        }
    }
}
