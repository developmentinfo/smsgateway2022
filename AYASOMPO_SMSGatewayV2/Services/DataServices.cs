﻿using AYASOMPO_SMSGatewayV2.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Services
{
    public class DataServices
    {
        public IConfiguration Configuration { get; set; }

        private SqlConnection connection = null;

        public DataServices()
        {
            var builder = new ConfigurationBuilder()
          .SetBasePath(Directory.GetCurrentDirectory())
          .AddJsonFile("appsettings.json");
            Configuration = builder.Build();

            connection = new SqlConnection(Configuration.GetConnectionString("DefaultConnection"));

        }
        
        private void CheckConnectionState()
        {
            try
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AccountCodeInfoViewModel> GetAccountCodeList()
        {
            DataSet ds = new DataSet();

            try
            {
                string query = "";

                query = "select AccountCode " +
                        "from FireRenewalRegister " +
                        "union " +
                        "select AccountCode " +
                        "from MotorRenewalRegister " +
                        "union " +
                        "select AccountCode " +
                        "from HealthRenewalRegister " +
                        "union " +
                        "select AccountCode " +
                        "from PARenewalRegister " +
                        "union " +
                        "select AccountCode " +
                        "from EngineeringRenewalRegister " +
                        "union " +
                        "select AccountCode " +
                        "from PropertyRenewalRegister " +
                        "group by AccountCode";

                CheckConnectionState();
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = connection;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds);
                    }
                }

                List<AccountCodeInfoViewModel> lstAccountCode = new List<AccountCodeInfoViewModel>();

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    AccountCodeInfoViewModel accountCode = new AccountCodeInfoViewModel();
                    accountCode.account_code = Convert.ToString(dr["AccountCode"]);
                    lstAccountCode.Add(accountCode);
                }

                return lstAccountCode;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                connection.Close();
            }
        }

        public List<MotorRenewalRegister> GetMotorRenewalListByFilter(filterRenewalDataModel filterRenewalDataModel)
        {
            DataSet ds = new DataSet();
            List<MotorRenewalRegister> lstMotorrenewal = new List<MotorRenewalRegister>();
            try
            {
                var firstDayOfMonth = new DateTime(filterRenewalDataModel.month.Year, filterRenewalDataModel.month.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                string accCodes = "";
                if (filterRenewalDataModel.AccountCodes != null)
                {
                    for (int i = 0; i < filterRenewalDataModel.AccountCodes.Count(); i++)
                    {
                        accCodes += "'"+filterRenewalDataModel.AccountCodes[i]+"',";
                    }
                    accCodes = accCodes.Substring(0, accCodes.Count() - 1);
                }

                string responsibleParties = "";
                if(filterRenewalDataModel.ResponsibleParties != null)
                {
                    for (int i = 0; i < filterRenewalDataModel.ResponsibleParties.Count(); i++)
                    {
                        responsibleParties += "'" + filterRenewalDataModel.ResponsibleParties[i] + "',";
                    }
                    responsibleParties = responsibleParties.Substring(0, responsibleParties.Count() - 1);
                }

                string query = "";
                query = "SELECT * FROM MotorRenewalRegister WHERE 1=1";
                if (filterRenewalDataModel.month != null && filterRenewalDataModel.month != null)
                {
                    query += " AND Convert(date, ExpiryDate) >= '" + Convert.ToDateTime(firstDayOfMonth).Date + "' AND Convert(date, ExpiryDate) <='" + Convert.ToDateTime(lastDayOfMonth).Date + "'";
                }
                if (accCodes != null && accCodes != String.Empty)
                {
                    query += " AND AccountCode IN (" + accCodes + ")";
                }
                if (responsibleParties != null && responsibleParties != String.Empty)
                {
                    query += " AND ResponsibleParty IN (" + responsibleParties + ")";
                }
                if(filterRenewalDataModel.approvesendsms != null)
                {
                    if(filterRenewalDataModel.approvesendsms == "Pending")
                    {
                        query += " AND (ApproveSendSMS = '" + filterRenewalDataModel.approvesendsms + "' OR ApproveSendSMS is null OR ApproveSendSMS = ' ')";
                    }
                   
                    else
                    {
                        query += " AND ApproveSendSMS = '" + filterRenewalDataModel.approvesendsms + "'";
                    }
                }
                if(filterRenewalDataModel.personincharge == "true")
                {
                    query += " AND PersonInCharge is not null";
                }
                else if(filterRenewalDataModel.personincharge == "false")
                {
                    query += " AND PersonInCharge is null";
                }
                if(filterRenewalDataModel.RemarkId == true)
                {
                    query += " AND RemarkId is not null";
                }
                else if(filterRenewalDataModel.RemarkId == false)
                {
                    query += " AND (RemarkId is null OR RemarkId = '')";
                }

                if(filterRenewalDataModel.approvesendsms == "No_SMS")
                {
                    query += "  AND SMSFlag='0' and CRMRenewalCreateFlag ='0' order by ExpiryDate desc";
                }
                else if(filterRenewalDataModel.approvesendsms == "Pending" || filterRenewalDataModel.approvesendsms == "Send_SMS")
                {
                    query += "  AND SMSFlag='0' order by ExpiryDate desc";
                }
                else
                {
                    query += " AND SMSFlag ='0' and CRMRenewalCreateFlag ='0' and (ApproveSendSMS != 'Send_SMS' OR ApproveSendSMS is null ) order by ExpiryDate desc";
                }
                

                CheckConnectionState();
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = connection;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds);
                    }
                }

                lstMotorrenewal = ConvertDataTable<MotorRenewalRegister>(ds.Tables[0]);
                return lstMotorrenewal;
            }
            catch (Exception ex)
            {
                return lstMotorrenewal;
            }
            finally
            {
                connection.Close();
            }
        }

        public List<FireRenewalRegister> GetFireRenewalListByFilter(filterRenewalDataModel filterRenewalDataModel)
        {
            DataSet ds = new DataSet();
            List<FireRenewalRegister> lstMotorrenewal = new List<FireRenewalRegister>();
            try
            {
                var firstDayOfMonth = new DateTime(filterRenewalDataModel.month.Year, filterRenewalDataModel.month.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

                string accCodes = "";
                if (filterRenewalDataModel.AccountCodes != null)
                {
                    for (int i = 0; i < filterRenewalDataModel.AccountCodes.Count(); i++)
                    {
                        accCodes += "'" + filterRenewalDataModel.AccountCodes[i] + "',";
                    }
                    accCodes = accCodes.Substring(0, accCodes.Count() - 1);
                }

                string responsibleParties = "";
                if (filterRenewalDataModel.ResponsibleParties != null)
                {
                    for (int i = 0; i < filterRenewalDataModel.ResponsibleParties.Count(); i++)
                    {
                        responsibleParties += "'" + filterRenewalDataModel.ResponsibleParties[i] + "',";
                    }
                    responsibleParties = responsibleParties.Substring(0, responsibleParties.Count() - 1);
                }

                string query = "";
                query = "SELECT * FROM FireRenewalRegister WHERE 1=1";
                if (filterRenewalDataModel.month != null && filterRenewalDataModel.month != null)
                {
                    query += " AND Convert(date, ExpiryDate) >= '" + Convert.ToDateTime(firstDayOfMonth).Date + "' AND Convert(date, ExpiryDate) <='" + Convert.ToDateTime(lastDayOfMonth).Date + "'";
                }
                if (accCodes != null && accCodes != String.Empty)
                {
                    query += " AND AccountCode IN (" + accCodes + ")";
                }
                if (responsibleParties != null && responsibleParties != String.Empty)
                {
                    query += " AND ResponsibleParty IN (" + responsibleParties + ")";
                }
                if (filterRenewalDataModel.approvesendsms != null)
                {
                    if (filterRenewalDataModel.approvesendsms == "Pending")
                    {
                        query += " AND (ApproveSendSMS = '" + filterRenewalDataModel.approvesendsms + "' OR ApproveSendSMS is null)";
                    }

                    else
                    {
                        query += " AND ApproveSendSMS = '" + filterRenewalDataModel.approvesendsms + "'";
                    }
                }
                if(filterRenewalDataModel.personincharge == "true")
                {
                    query += " AND PersonInCharge is not null";
                }
                else if(filterRenewalDataModel.personincharge == "false")
                {
                    query += " AND PersonInCharge is null";
                }
                if (filterRenewalDataModel.RemarkId == true)
                {
                    query += " AND RemarkId is not null";
                }
                else if (filterRenewalDataModel.RemarkId == false)
                {
                    query += " AND (RemarkId is null OR RemarkId = '')";
                }

                if (filterRenewalDataModel.approvesendsms == "No_SMS")
                {
                    query += "  AND SMSFlag='0' and CRMRenewalCreateFlag ='0' order by ExpiryDate desc";
                }
                else if (filterRenewalDataModel.approvesendsms == "Pending" || filterRenewalDataModel.approvesendsms == "Send_SMS")
                {
                    query += "  AND SMSFlag='0' order by ExpiryDate desc";
                }
                else
                {
                    query += " AND SMSFlag ='0' and CRMRenewalCreateFlag ='0' and (ApproveSendSMS != 'Send_SMS' OR ApproveSendSMS is null) order by ExpiryDate desc";
                }


                CheckConnectionState();
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = connection;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds);
                    }
                }

                lstMotorrenewal = ConvertDataTable<FireRenewalRegister>(ds.Tables[0]);
                return lstMotorrenewal;
            }
            catch (Exception ex)
            {
                return lstMotorrenewal;
            }
            finally
            {
                connection.Close();
            }
        }

        public List<HealthRenewalRegister> GetHealthRenewalListByFilter(filterRenewalDataModel filterRenewalDataModel)
        {
            DataSet ds = new DataSet();
            List<HealthRenewalRegister> lstHealthrenewal = new List<HealthRenewalRegister>();
            try
            {
                var firstDayOfMonth = new DateTime(filterRenewalDataModel.month.Year, filterRenewalDataModel.month.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

                string accCodes = "";
                if (filterRenewalDataModel.AccountCodes != null)
                {
                    for (int i = 0; i < filterRenewalDataModel.AccountCodes.Count(); i++)
                    {
                        accCodes += "'" + filterRenewalDataModel.AccountCodes[i] + "',";
                    }
                    accCodes = accCodes.Substring(0, accCodes.Count() - 1);
                }

                string responsibleParties = "";
                if (filterRenewalDataModel.ResponsibleParties != null)
                {
                    for (int i = 0; i < filterRenewalDataModel.ResponsibleParties.Count(); i++)
                    {
                        responsibleParties += "'" + filterRenewalDataModel.ResponsibleParties[i] + "',";
                    }
                    responsibleParties = responsibleParties.Substring(0, responsibleParties.Count() - 1);
                }

                string query = "";
                query = "SELECT * FROM HealthRenewalRegister WHERE 1=1";
                if (filterRenewalDataModel.month != null && filterRenewalDataModel.month != null)
                {
                    query += " AND Convert(date, ExpiryDate) >= '" + Convert.ToDateTime(firstDayOfMonth).Date + "' AND Convert(date, ExpiryDate) <='" + Convert.ToDateTime(lastDayOfMonth).Date + "'";
                }
                if (accCodes != null && accCodes != String.Empty)
                {
                    query += " AND AccountCode IN (" + accCodes + ")";
                }
                if (responsibleParties != null && responsibleParties != String.Empty)
                {
                    query += " AND ResponsibleParty IN (" + responsibleParties + ")";
                }
                if (filterRenewalDataModel.approvesendsms != null)
                {
                    if (filterRenewalDataModel.approvesendsms == "Pending")
                    {
                        query += " AND (ApproveSendSMS = '" + filterRenewalDataModel.approvesendsms + "' OR ApproveSendSMS is null)";
                    }

                    else
                    {
                        query += " AND ApproveSendSMS = '" + filterRenewalDataModel.approvesendsms + "'";
                    }
                }
                if (filterRenewalDataModel.personincharge == "true")
                {
                    query += " AND PersonInCharge is not null";
                }
                else if (filterRenewalDataModel.personincharge == "false")
                {
                    query += " AND PersonInCharge is null";
                }
                if (filterRenewalDataModel.RemarkId == true)
                {
                    query += " AND RemarkId is not null";
                }
                else if (filterRenewalDataModel.RemarkId == false)
                {
                    query += " AND (RemarkId is null OR RemarkId = '')";
                }

                if (filterRenewalDataModel.approvesendsms == "No_SMS")
                {
                    query += "  AND SMSFlag='0' and CRMRenewalCreateFlag ='0' order by ExpiryDate desc";
                }
                else if (filterRenewalDataModel.approvesendsms == "Pending" || filterRenewalDataModel.approvesendsms == "Send_SMS")
                {
                    query += "  AND SMSFlag='0' order by ExpiryDate desc";
                }
                else
                {
                    query += " AND SMSFlag ='0' and CRMRenewalCreateFlag ='0' and (ApproveSendSMS != 'Send_SMS' OR ApproveSendSMS is null) order by ExpiryDate desc";
                }


                CheckConnectionState();
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = connection;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds);
                    }
                }

                lstHealthrenewal = ConvertDataTable<HealthRenewalRegister>(ds.Tables[0]);
                return lstHealthrenewal;
            }
            catch(Exception ex)
            {
                return lstHealthrenewal;
            }
            finally
            {
                connection.Close();
            }
        }

        public List<ParenewalRegister> GetPARenewalListByFilter(filterRenewalDataModel filterRenewalDataModel)
        {
            DataSet ds = new DataSet();
            List<ParenewalRegister> lstPArenewal = new List<ParenewalRegister>();
            try
            {
                var firstDayOfMonth = new DateTime(filterRenewalDataModel.month.Year, filterRenewalDataModel.month.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

                string accCodes = "";
                if (filterRenewalDataModel.AccountCodes != null)
                {
                    for (int i = 0; i < filterRenewalDataModel.AccountCodes.Count(); i++)
                    {
                        accCodes += "'" + filterRenewalDataModel.AccountCodes[i] + "',";
                    }
                    accCodes = accCodes.Substring(0, accCodes.Count() - 1);
                }

                string responsibleParties = "";
                if (filterRenewalDataModel.ResponsibleParties != null)
                {
                    for (int i = 0; i < filterRenewalDataModel.ResponsibleParties.Count(); i++)
                    {
                        responsibleParties += "'" + filterRenewalDataModel.ResponsibleParties[i] + "',";
                    }
                    responsibleParties = responsibleParties.Substring(0, responsibleParties.Count() - 1);
                }

                string query = "";
                query = "SELECT * FROM PARenewalRegister WHERE 1=1";
                if (filterRenewalDataModel.month != null && filterRenewalDataModel.month != null)
                {
                    query += " AND Convert(date, ExpiryDate) >= '" + Convert.ToDateTime(firstDayOfMonth).Date + "' AND Convert(date, ExpiryDate) <='" + Convert.ToDateTime(lastDayOfMonth).Date + "'";
                }
                if (accCodes != null && accCodes != String.Empty)
                {
                    query += " AND AccountCode IN (" + accCodes + ")";
                }
                if (responsibleParties != null && responsibleParties != String.Empty)
                {
                    query += " AND ResponsibleParty IN (" + responsibleParties + ")";
                }
                if (filterRenewalDataModel.approvesendsms != null)
                {
                    if (filterRenewalDataModel.approvesendsms == "Pending")
                    {
                        query += " AND (ApproveSendSMS = '" + filterRenewalDataModel.approvesendsms + "' OR ApproveSendSMS is null)";
                    }

                    else
                    {
                        query += " AND ApproveSendSMS = '" + filterRenewalDataModel.approvesendsms + "'";
                    }
                }
                if (filterRenewalDataModel.personincharge == "true")
                {
                    query += " AND PersonInCharge is not null";
                }
                else if (filterRenewalDataModel.personincharge == "false")
                {
                    query += " AND PersonInCharge is null";
                }
                if (filterRenewalDataModel.RemarkId == true)
                {
                    query += " AND RemarkId is not null";
                }
                else if (filterRenewalDataModel.RemarkId == false)
                {
                    query += " AND (RemarkId is null OR RemarkId = '')";
                }

                if (filterRenewalDataModel.approvesendsms == "No_SMS")
                {
                    query += "  AND SMSFlag='0' and CRMRenewalCreateFlag ='0' order by ExpiryDate desc";
                }
                else if (filterRenewalDataModel.approvesendsms == "Pending" || filterRenewalDataModel.approvesendsms == "Send_SMS")
                {
                    query += "  AND SMSFlag='0' order by ExpiryDate desc";
                }
                else
                {
                    query += " AND SMSFlag ='0' and CRMRenewalCreateFlag ='0' and (ApproveSendSMS != 'Send_SMS' OR ApproveSendSMS is null) order by ExpiryDate desc";
                }


                CheckConnectionState();
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = connection;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds);
                    }
                }

                lstPArenewal = ConvertDataTable<ParenewalRegister>(ds.Tables[0]);
                return lstPArenewal;
            }
            catch (Exception ex)
            {
                return lstPArenewal;
            }
            finally
            {
                connection.Close();
            }
        }

        public List<EngineeringRenewalRegister> GetENGGRenewalListByFilter(filterRenewalDataModel filterRenewalDataModel)
        {
            DataSet ds = new DataSet();
            List<EngineeringRenewalRegister> lstENGGrenewal = new List<EngineeringRenewalRegister>();
            try
            {
                var firstDayOfMonth = new DateTime(filterRenewalDataModel.month.Year, filterRenewalDataModel.month.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

                string accCodes = "";
                if (filterRenewalDataModel.AccountCodes != null)
                {
                    for (int i = 0; i < filterRenewalDataModel.AccountCodes.Count(); i++)
                    {
                        accCodes += "'" + filterRenewalDataModel.AccountCodes[i] + "',";
                    }
                    accCodes = accCodes.Substring(0, accCodes.Count() - 1);
                }

                string responsibleParties = "";
                if (filterRenewalDataModel.ResponsibleParties != null)
                {
                    for (int i = 0; i < filterRenewalDataModel.ResponsibleParties.Count(); i++)
                    {
                        responsibleParties += "'" + filterRenewalDataModel.ResponsibleParties[i] + "',";
                    }
                    responsibleParties = responsibleParties.Substring(0, responsibleParties.Count() - 1);
                }

                string query = "";
                query = "SELECT * FROM EngineeringRenewalRegister WHERE 1=1";
                if (filterRenewalDataModel.month != null && filterRenewalDataModel.month != null)
                {
                    query += " AND Convert(date, ExpiryDate) >= '" + Convert.ToDateTime(firstDayOfMonth).Date + "' AND Convert(date, ExpiryDate) <='" + Convert.ToDateTime(lastDayOfMonth).Date + "'";
                }
                if (accCodes != null && accCodes != String.Empty)
                {
                    query += " AND AccountCode IN (" + accCodes + ")";
                }
                if (responsibleParties != null && responsibleParties != String.Empty)
                {
                    query += " AND ResponsibleParty IN (" + responsibleParties + ")";
                }
                if (filterRenewalDataModel.approvesendsms != null)
                {
                    if (filterRenewalDataModel.approvesendsms == "Pending")
                    {
                        query += " AND (ApproveSendSMS = '" + filterRenewalDataModel.approvesendsms + "' OR ApproveSendSMS is null)";
                    }

                    else
                    {
                        query += " AND ApproveSendSMS = '" + filterRenewalDataModel.approvesendsms + "'";
                    }
                }
                if (filterRenewalDataModel.personincharge == "true")
                {
                    query += " AND PersonInCharge is not null";
                }
                else if (filterRenewalDataModel.personincharge == "false")
                {
                    query += " AND PersonInCharge is null";
                }
                if (filterRenewalDataModel.RemarkId == true)
                {
                    query += " AND RemarkId is not null";
                }
                else if (filterRenewalDataModel.RemarkId == false)
                {
                    query += " AND (RemarkId is null OR RemarkId = '')";
                }

                if (filterRenewalDataModel.approvesendsms == "No_SMS")
                {
                    query += "  AND SMSFlag='0' and CRMRenewalCreateFlag ='0' order by ExpiryDate desc";
                }
                else if (filterRenewalDataModel.approvesendsms == "Pending" || filterRenewalDataModel.approvesendsms == "Send_SMS")
                {
                    query += "  AND SMSFlag='0' order by ExpiryDate desc";
                }
                else
                {
                    query += " AND SMSFlag ='0' and CRMRenewalCreateFlag ='0' and (ApproveSendSMS != 'Send_SMS' OR ApproveSendSMS is null) order by ExpiryDate desc";
                }


                CheckConnectionState();
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = connection;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds);
                    }
                }

                lstENGGrenewal = ConvertDataTable<EngineeringRenewalRegister>(ds.Tables[0]);
                return lstENGGrenewal;
            }
            catch (Exception ex)
            {
                return lstENGGrenewal;
            }
            finally
            {
                connection.Close();
            }
        }

        public List<PropertyRenewalRegister> GetPropertyRenewalListByFilter(filterRenewalDataModel filterRenewalDataModel)
        {
            DataSet ds = new DataSet();
            List<PropertyRenewalRegister> lstPropertyrenewal = new List<PropertyRenewalRegister>();
            try
            {
                var firstDayOfMonth = new DateTime(filterRenewalDataModel.month.Year, filterRenewalDataModel.month.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

                string accCodes = "";
                if (filterRenewalDataModel.AccountCodes != null)
                {
                    for (int i = 0; i < filterRenewalDataModel.AccountCodes.Count(); i++)
                    {
                        accCodes += "'" + filterRenewalDataModel.AccountCodes[i] + "',";
                    }
                    accCodes = accCodes.Substring(0, accCodes.Count() - 1);
                }

                string responsibleParties = "";
                if (filterRenewalDataModel.ResponsibleParties != null)
                {
                    for (int i = 0; i < filterRenewalDataModel.ResponsibleParties.Count(); i++)
                    {
                        responsibleParties += "'" + filterRenewalDataModel.ResponsibleParties[i] + "',";
                    }
                    responsibleParties = responsibleParties.Substring(0, responsibleParties.Count() - 1);
                }

                string query = "";
                query = "SELECT * FROM PropertyRenewalRegister WHERE 1=1";
                if (filterRenewalDataModel.month != null && filterRenewalDataModel.month != null)
                {
                    query += " AND Convert(date, ExpiryDate) >= '" + Convert.ToDateTime(firstDayOfMonth).Date + "' AND Convert(date, ExpiryDate) <='" + Convert.ToDateTime(lastDayOfMonth).Date + "'";
                }
                if (accCodes != null && accCodes != String.Empty)
                {
                    query += " AND AccountCode IN (" + accCodes + ")";
                }
                if (responsibleParties != null && responsibleParties != String.Empty)
                {
                    query += " AND ResponsibleParty IN (" + responsibleParties + ")";
                }
                if (filterRenewalDataModel.approvesendsms != null)
                {
                    if (filterRenewalDataModel.approvesendsms == "Pending")
                    {
                        query += " AND (ApproveSendSMS = '" + filterRenewalDataModel.approvesendsms + "' OR ApproveSendSMS is null)";
                    }

                    else
                    {
                        query += " AND ApproveSendSMS = '" + filterRenewalDataModel.approvesendsms + "'";
                    }
                }
                if (filterRenewalDataModel.personincharge == "true")
                {
                    query += " AND PersonInCharge is not null";
                }
                else if (filterRenewalDataModel.personincharge == "false")
                {
                    query += " AND PersonInCharge is null";
                }
                if (filterRenewalDataModel.RemarkId == true)
                {
                    query += " AND RemarkId is not null";
                }
                else if (filterRenewalDataModel.RemarkId == false)
                {
                    query += " AND (RemarkId is null OR RemarkId = '')";
                }

                if (filterRenewalDataModel.approvesendsms == "No_SMS")
                {
                    query += "  AND SMSFlag='0' and CRMRenewalCreateFlag ='0' order by ExpiryDate desc";
                }
                else if (filterRenewalDataModel.approvesendsms == "Pending" || filterRenewalDataModel.approvesendsms == "Send_SMS")
                {
                    query += "  AND SMSFlag='0' order by ExpiryDate desc";
                }
                else
                {
                    query += " AND SMSFlag ='0' and CRMRenewalCreateFlag ='0' and (ApproveSendSMS != 'Send_SMS' OR ApproveSendSMS is null) order by ExpiryDate desc";
                }


                CheckConnectionState();
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = connection;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds);
                    }
                }

                lstPropertyrenewal = ConvertDataTable<PropertyRenewalRegister>(ds.Tables[0]);
                return lstPropertyrenewal;
            }
            catch (Exception ex)
            {
                return lstPropertyrenewal;
            }
            finally
            {
                connection.Close();
            }
        }

        public List<MotorRenewalRegister> GetMotorRenewalRegisterReport(filterRenewalDataModel filterRenewalDataModel)
        {
            DataSet ds = new DataSet();
            List<MotorRenewalRegister> lstMotorrenewal = new List<MotorRenewalRegister>();
            try
            {
                var firstDayOfMonth = new DateTime(filterRenewalDataModel.month.Year, filterRenewalDataModel.month.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                string accCodes = "";
                if (filterRenewalDataModel.AccountCodes != null)
                {
                    for (int i = 0; i < filterRenewalDataModel.AccountCodes.Count(); i++)
                    {
                        accCodes += "'" + filterRenewalDataModel.AccountCodes[i] + "',";
                    }
                    accCodes = accCodes.Substring(0, accCodes.Count() - 1);
                }

                string query = "";
                query = "SELECT * FROM MotorRenewalRegister WHERE 1=1";
                if (filterRenewalDataModel.month != null && filterRenewalDataModel.month != null)
                {
                    query += " AND Convert(date, ExpiryDate) >= '" + Convert.ToDateTime(firstDayOfMonth).Date + "' AND Convert(date, ExpiryDate) <='" + Convert.ToDateTime(lastDayOfMonth).Date + "'";
                }
                if (accCodes != null && accCodes != String.Empty)
                {
                    query += " AND AccountCode IN (" + accCodes + ")";
                }

                query += " order by ExpiryDate desc";


                CheckConnectionState();
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = connection;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds);
                    }
                }

                lstMotorrenewal = ConvertDataTable<MotorRenewalRegister>(ds.Tables[0]);
                return lstMotorrenewal;
            }
            catch (Exception ex)
            {
                return lstMotorrenewal;
            }
            finally
            {
                connection.Close();
            }
        }

        public List<FireRenewalRegister> GetFireRenewalRegisterDataReport(filterRenewalDataModel filterRenewalDataModel)
        {
            DataSet ds = new DataSet();
            List<FireRenewalRegister> lstMotorrenewal = new List<FireRenewalRegister>();
            try
            {
                var firstDayOfMonth = new DateTime(filterRenewalDataModel.month.Year, filterRenewalDataModel.month.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

                string accCodes = "";
                if (filterRenewalDataModel.AccountCodes != null)
                {
                    for (int i = 0; i < filterRenewalDataModel.AccountCodes.Count(); i++)
                    {
                        accCodes += "'" + filterRenewalDataModel.AccountCodes[i] + "',";
                    }
                    accCodes = accCodes.Substring(0, accCodes.Count() - 1);
                }

                string query = "";
                query = "SELECT * FROM FireRenewalRegister WHERE 1=1";
                if (filterRenewalDataModel.month != null && filterRenewalDataModel.month != null)
                {
                    query += " AND Convert(date, ExpiryDate) >= '" + Convert.ToDateTime(firstDayOfMonth).Date + "' AND Convert(date, ExpiryDate) <='" + Convert.ToDateTime(lastDayOfMonth).Date + "'";
                }
                if (accCodes != null && accCodes != String.Empty)
                {
                    query += " AND AccountCode IN (" + accCodes + ")";
                } 

                query += " order by ExpiryDate desc";

                CheckConnectionState();
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = connection;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds);
                    }
                }

                lstMotorrenewal = ConvertDataTable<FireRenewalRegister>(ds.Tables[0]);
                return lstMotorrenewal;
            }
            catch (Exception ex)
            {
                return lstMotorrenewal;
            }
            finally
            {
                connection.Close();
            }
        }

        public List<HealthRenewalRegister> GetHealthRenewalRegisterDataReport(filterRenewalDataModel filterRenewalDataModel)
        {
            DataSet ds = new DataSet();
            List<HealthRenewalRegister> lstHealthrenewal = new List<HealthRenewalRegister>();
            try
            {
                var firstDayOfMonth = new DateTime(filterRenewalDataModel.month.Year, filterRenewalDataModel.month.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

                string accCodes = "";
                if (filterRenewalDataModel.AccountCodes != null)
                {
                    for (int i = 0; i < filterRenewalDataModel.AccountCodes.Count(); i++)
                    {
                        accCodes += "'" + filterRenewalDataModel.AccountCodes[i] + "',";
                    }
                    accCodes = accCodes.Substring(0, accCodes.Count() - 1);
                }

                string query = "";
                query = "SELECT * FROM HealthRenewalRegister WHERE 1=1";
                if (filterRenewalDataModel.month != null && filterRenewalDataModel.month != null)
                {
                    query += " AND Convert(date, ExpiryDate) >= '" + Convert.ToDateTime(firstDayOfMonth).Date + "' AND Convert(date, ExpiryDate) <='" + Convert.ToDateTime(lastDayOfMonth).Date + "'";
                }
                if (accCodes != null && accCodes != String.Empty)
                {
                    query += " AND AccountCode IN (" + accCodes + ")";
                }

                query += " order by ExpiryDate desc";

                CheckConnectionState();
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = connection;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds);
                    }
                }

                lstHealthrenewal = ConvertDataTable<HealthRenewalRegister>(ds.Tables[0]);
                return lstHealthrenewal;
            }
            catch (Exception ex)
            {
                return lstHealthrenewal;
            }
            finally
            {
                connection.Close();
            }
        }

        public List<ParenewalRegister> GetPARenewalRegisterDataReport(filterRenewalDataModel filterRenewalDataModel)
        {
            DataSet ds = new DataSet();
            List<ParenewalRegister> lstPArenewal = new List<ParenewalRegister>();
            try
            {
                var firstDayOfMonth = new DateTime(filterRenewalDataModel.month.Year, filterRenewalDataModel.month.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

                string accCodes = "";
                if (filterRenewalDataModel.AccountCodes != null)
                {
                    for (int i = 0; i < filterRenewalDataModel.AccountCodes.Count(); i++)
                    {
                        accCodes += "'" + filterRenewalDataModel.AccountCodes[i] + "',";
                    }
                    accCodes = accCodes.Substring(0, accCodes.Count() - 1);
                }

                string query = "";
                query = "SELECT * FROM PARenewalRegister WHERE 1=1";
                if (filterRenewalDataModel.month != null && filterRenewalDataModel.month != null)
                {
                    query += " AND Convert(date, ExpiryDate) >= '" + Convert.ToDateTime(firstDayOfMonth).Date + "' AND Convert(date, ExpiryDate) <='" + Convert.ToDateTime(lastDayOfMonth).Date + "'";
                }
                if (accCodes != null && accCodes != String.Empty)
                {
                    query += " AND AccountCode IN (" + accCodes + ")";
                }

                query += " order by ExpiryDate desc";

                CheckConnectionState();
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = connection;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds);
                    }
                }

                lstPArenewal = ConvertDataTable<ParenewalRegister>(ds.Tables[0]);
                return lstPArenewal;
            }
            catch (Exception ex)
            {
                return lstPArenewal;
            }
            finally
            {
                connection.Close();
            }
        }

        public List<EngineeringRenewalRegister> GetENGGRenewalRegisterDataReport(filterRenewalDataModel filterRenewalDataModel)
        {
            DataSet ds = new DataSet();
            List<EngineeringRenewalRegister> lstENGGrenewal = new List<EngineeringRenewalRegister>();
            try
            {
                var firstDayOfMonth = new DateTime(filterRenewalDataModel.month.Year, filterRenewalDataModel.month.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

                string accCodes = "";
                if (filterRenewalDataModel.AccountCodes != null)
                {
                    for (int i = 0; i < filterRenewalDataModel.AccountCodes.Count(); i++)
                    {
                        accCodes += "'" + filterRenewalDataModel.AccountCodes[i] + "',";
                    }
                    accCodes = accCodes.Substring(0, accCodes.Count() - 1);
                }

                string query = "";
                query = "SELECT * FROM EngineeringRenewalRegister WHERE 1=1";
                if (filterRenewalDataModel.month != null && filterRenewalDataModel.month != null)
                {
                    query += " AND Convert(date, ExpiryDate) >= '" + Convert.ToDateTime(firstDayOfMonth).Date + "' AND Convert(date, ExpiryDate) <='" + Convert.ToDateTime(lastDayOfMonth).Date + "'";
                }
                if (accCodes != null && accCodes != String.Empty)
                {
                    query += " AND AccountCode IN (" + accCodes + ")";
                }

                query += " order by ExpiryDate desc";

                CheckConnectionState();
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = connection;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds);
                    }
                }

                lstENGGrenewal = ConvertDataTable<EngineeringRenewalRegister>(ds.Tables[0]);
                return lstENGGrenewal;
            }
            catch (Exception ex)
            {
                return lstENGGrenewal;
            }
            finally
            {
                connection.Close();
            }
        }

        public List<PropertyRenewalRegister> GetPropertyRenewalRegisterDataReport(filterRenewalDataModel filterRenewalDataModel)
        {
            DataSet ds = new DataSet();
            List<PropertyRenewalRegister> lstPropertyrenewal = new List<PropertyRenewalRegister>();
            try
            {
                var firstDayOfMonth = new DateTime(filterRenewalDataModel.month.Year, filterRenewalDataModel.month.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

                string accCodes = "";
                if (filterRenewalDataModel.AccountCodes != null)
                {
                    for (int i = 0; i < filterRenewalDataModel.AccountCodes.Count(); i++)
                    {
                        accCodes += "'" + filterRenewalDataModel.AccountCodes[i] + "',";
                    }
                    accCodes = accCodes.Substring(0, accCodes.Count() - 1);
                }

                string query = "";
                query = "SELECT * FROM PropertyRenewalRegister WHERE 1=1";
                if (filterRenewalDataModel.month != null && filterRenewalDataModel.month != null)
                {
                    query += " AND Convert(date, ExpiryDate) >= '" + Convert.ToDateTime(firstDayOfMonth).Date + "' AND Convert(date, ExpiryDate) <='" + Convert.ToDateTime(lastDayOfMonth).Date + "'";
                }
                if (accCodes != null && accCodes != String.Empty)
                {
                    query += " AND AccountCode IN (" + accCodes + ")";
                }

                query += " order by ExpiryDate desc";

                CheckConnectionState();
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = connection;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds);
                    }
                }

                lstPropertyrenewal = ConvertDataTable<PropertyRenewalRegister>(ds.Tables[0]);
                return lstPropertyrenewal;
            }
            catch (Exception ex)
            {
                return lstPropertyrenewal;
            }
            finally
            {
                connection.Close();
            }
        }

        #region BulkSms
        public List<SentSmsVM> GetSentBulkSmsReport(SentSmsVM model)
        {
            DataSet ds = new DataSet();
            List<SentSmsVM> lstBulkSentSms = new List<SentSmsVM>();
            try
            {
                string query = "";
                query = "Select DISTINCT SentSMS.*, Category.Description as Category from SentSMS" + 
                        " Join Category ON Category.Id = SentSMS.CategoryId";
                if(model.fromDate != null && model.toDate != null)
                {
                    query += " AND Convert(date,SentDate) >= '" + model.fromDate + "' AND Convert(date,SentDate)<='" + model.toDate + "'";
                }
                if(model.StaffId != Guid.Empty)
                {
                    query += " AND SysId = '" + model.StaffId + "'";
                }
                if (model.CategoryId == Guid.Parse("019A983A-4323-4B12-9023-109CFD47C0E8"))
                {
                    query += " AND SMSFlag = '1'";
                }
                if (model.CategoryId == Guid.Parse("73C0DC6C-1249-4A8D-8D43-DA525A11327C"))
                {
                    query += " AND ActivitiesFlag = '1'";
                }
                query += " AND CategoryId = '" + model.CategoryId + "' ORDER BY SentDate DESC";

                CheckConnectionState();
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = connection;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds);
                    }
                }

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    SentSmsVM sentSmsVM = new SentSmsVM();
                    sentSmsVM.Id = Guid.Parse(dr["Id"].ToString());
                    sentSmsVM.PhoneNumber = Convert.ToString(dr["PhoneNumber"]);
                    sentSmsVM.UserName = Convert.ToString(dr["UserName"]);
                    sentSmsVM.VehicleNo = Convert.ToString(dr["VehicleNo"]);
                    sentSmsVM.PolicyNo = Convert.ToString(dr["PolicyNo"]);
                    sentSmsVM.Message = Convert.ToString(dr["Message"]);
                    sentSmsVM.SentDate = Convert.ToDateTime(dr["SentDate"]);
                    sentSmsVM.Smscount = Convert.ToString(dr["SMSCount"]);
                    sentSmsVM.Category = Convert.ToString(dr["Category"]);
                    lstBulkSentSms.Add(sentSmsVM);
                }
                return lstBulkSentSms;
            }
            catch (Exception ex)
            {
                return lstBulkSentSms;
            }
            finally
            {
                connection.Close();
            }
        }

        public List<SentSmsVM> GetRenewalDeclineFailSmsReport(SentSmsVM model)
        {
            DataSet ds = new DataSet();
            List<SentSmsVM> lstBulkSentSms = new List<SentSmsVM>();
            try
            {
                string query = "";
                query = "Select * from SentSMS WHERE" ;
                if (model.fromDate != null && model.toDate != null)
                {
                    query += " Convert(date,SentDate) >= '" + model.fromDate + "' AND Convert(date,SentDate)<='" + model.toDate + "'";
                }
                if (model.StaffId != Guid.Empty)
                {
                    query += " AND SysId = '" + model.StaffId + "'";
                }
                query += " AND CategoryId = '73C0DC6C-1249-4A8D-8D43-DA525A11327C' AND ActivitiesFlag = '0' ORDER BY SentDate DESC";

                CheckConnectionState();
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = connection;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds);
                    }
                }

                lstBulkSentSms = ConvertDataTable<SentSmsVM>(ds.Tables[0]);
                return lstBulkSentSms;
            }
            catch (Exception ex)
            {
                return lstBulkSentSms;
            }
            finally
            {
                connection.Close();
            }
        }
        #endregion

        #region PICCheckList
        /*public List<MotorRenewalRegister> GetMotorRenewalPICCheckListByFilter(filterRenewalDataModel filterRenewalDataModel)
        {
            DataSet ds = new DataSet();
            List<MotorRenewalRegister> lstMotorrenewal = new List<MotorRenewalRegister>();
            try
            {
                var firstDayOfMonth = new DateTime(filterRenewalDataModel.month.Year, filterRenewalDataModel.month.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                string accCodes = "";
                if (filterRenewalDataModel.AccountCodes != null)
                {
                    for (int i = 0; i < filterRenewalDataModel.AccountCodes.Count(); i++)
                    {
                        accCodes += "'" + filterRenewalDataModel.AccountCodes[i] + "',";
                    }
                    accCodes = accCodes.Substring(0, accCodes.Count() - 1);
                }

                string query = "";
                query = "SELECT * FROM MotorRenewalRegister WHERE 1=1";
                if (filterRenewalDataModel.month != null && filterRenewalDataModel.month != null)
                {
                    query += " AND Convert(date, ExpiryDate) >= '" + Convert.ToDateTime(firstDayOfMonth).Date + "' AND Convert(date, ExpiryDate) <='" + Convert.ToDateTime(lastDayOfMonth).Date + "'";
                }
                if (accCodes != null && accCodes != String.Empty)
                {
                    query += " AND AccountCode IN (" + accCodes + ")";
                }
                //if (filterRenewalDataModel.approvesendsms != null)
                //{
                //    if (filterRenewalDataModel.approvesendsms == "Pending")
                //    {
                //        query += " AND (ApproveSendSMS = '" + filterRenewalDataModel.approvesendsms + "' OR ApproveSendSMS is null OR ApproveSendSMS = ' ')";
                //    }

                //    else
                //    {
                //        query += " AND ApproveSendSMS = '" + filterRenewalDataModel.approvesendsms + "'";
                //    }
                //}

                query += " AND SMSFlag ='0' and CRMRenewalCreateFlag ='0' and PersonInCharge is null";

                CheckConnectionState();
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = connection;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds);
                    }
                }

                lstMotorrenewal = ConvertDataTable<MotorRenewalRegister>(ds.Tables[0]);
                return lstMotorrenewal;
            }
            catch (Exception ex)
            {
                return lstMotorrenewal;
            }
            finally
            {
                connection.Close();
            }
        }*/

        /*
        public List<FireRenewalRegister> GetFireRenewalPICCheckListByFilter(filterRenewalDataModel filterRenewalDataModel)
        {
            DataSet ds = new DataSet();
            List<FireRenewalRegister> lstFirerenewal = new List<FireRenewalRegister>();
            try
            {
                var firstDayOfMonth = new DateTime(filterRenewalDataModel.month.Year, filterRenewalDataModel.month.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

                string accCodes = "";
                if (filterRenewalDataModel.AccountCodes != null)
                {
                    for (int i = 0; i < filterRenewalDataModel.AccountCodes.Count(); i++)
                    {
                        accCodes += "'" + filterRenewalDataModel.AccountCodes[i] + "',";
                    }
                    accCodes = accCodes.Substring(0, accCodes.Count() - 1);
                }

                string query = "";
                query = "SELECT * FROM FireRenewalRegister WHERE 1=1";
                if (filterRenewalDataModel.month != null && filterRenewalDataModel.month != null)
                {
                    query += " AND Convert(date, ExpiryDate) >= '" + Convert.ToDateTime(firstDayOfMonth).Date + "' AND Convert(date, ExpiryDate) <='" + Convert.ToDateTime(lastDayOfMonth).Date + "'";
                }
                if (accCodes != null && accCodes != String.Empty)
                {
                    query += " AND AccountCode IN (" + accCodes + ")";
                }
                //if (filterRenewalDataModel.approvesendsms != null)
                //{
                //    if (filterRenewalDataModel.approvesendsms == "Pending")
                //    {
                //        query += " AND (ApproveSendSMS = '" + filterRenewalDataModel.approvesendsms + "' OR ApproveSendSMS is null)";
                //    }

                //    else
                //    {
                //        query += " AND ApproveSendSMS = '" + filterRenewalDataModel.approvesendsms + "'";
                //    }
                //}

                query += " AND SMSFlag ='0' and CRMRenewalCreateFlag ='0' and PersonInCharge is null";

                CheckConnectionState();
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = connection;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds);
                    }
                }

                lstFirerenewal = ConvertDataTable<FireRenewalRegister>(ds.Tables[0]);
                return lstFirerenewal;
            }
            catch (Exception ex)
            {
                return lstFirerenewal;
            }
            finally
            {
                connection.Close();
            }
        }*/

        #endregion

        public List<SMSRenewalViewModel> GetSMSRenewalDataById(Guid id)
        {
            DataSet ds = new DataSet();
            List<SMSRenewalViewModel> sMSRenewalViewModels = new List<SMSRenewalViewModel>();
            try
            {
                string query = "";
                query = "SELECT * FROM VW_SMSRenewalRegister WHERE 1=1 AND Id = '" + id + "'";
 
                CheckConnectionState();
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = connection;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds);
                    }
                }

                sMSRenewalViewModels = ConvertDataTable<SMSRenewalViewModel>(ds.Tables[0]);
                return sMSRenewalViewModels;
            }
            catch (Exception ex)
            {
                return sMSRenewalViewModels;
            }
            finally
            {
                connection.Close();
            }
        }

        public List<SMSRenewalViewModel> GetSMSRenewalListByFilter(filterRenewalSMSDataModel filterRenewalDataModel)
        {
            DataSet ds = new DataSet();
            List<SMSRenewalViewModel> sMSRenewalViewModels = new List<SMSRenewalViewModel>();
            try
            {
                var firstDayOfMonth = new DateTime(filterRenewalDataModel.month.Year, filterRenewalDataModel.month.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

                string query = "";
                query = "SELECT * FROM VW_SMSRenewalRegister WHERE 1=1";
                if (filterRenewalDataModel.month != null && filterRenewalDataModel.month != null)
                {
                    query += " AND Convert(date, ExpiryDate) >= '" + Convert.ToDateTime(firstDayOfMonth).Date + "' AND Convert(date, ExpiryDate) <='" + Convert.ToDateTime(lastDayOfMonth).Date + "'";
                }
                if (filterRenewalDataModel.classname != null && filterRenewalDataModel.classname != string.Empty)
                {
                    query += " AND ClassCode='" + filterRenewalDataModel.classname + "'";
                }
                query += " AND ApproveSendSMS = 'Send_SMS' and PhoneNumber is not null and InvoiceNo is not null and SMSFlag='0'";
                CheckConnectionState();
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = connection;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds);
                    }
                }

                sMSRenewalViewModels = ConvertDataTable<SMSRenewalViewModel>(ds.Tables[0]);
                return sMSRenewalViewModels;
            }
            catch (Exception ex)
            {
                return sMSRenewalViewModels;
            }
            finally
            {
                connection.Close();
            }
        }
        
        public List<SMSRenewalViewModel> GetRenewalFailCaseByFilter(filterRenewalFailCase filterRenewalDataModel)
        {
            DataSet ds = new DataSet();
            List<SMSRenewalViewModel> sMSRenewalViewModels = new List<SMSRenewalViewModel>();
            try
            {
             
                string query = "";
                query = "SELECT * FROM VW_SMSRenewalRegister WHERE 1=1";

                if (filterRenewalDataModel.filterFromDate != null && filterRenewalDataModel.filterToDate != null)
                {
                    query += " AND Convert(date, CRMRenewalCreatedDate) >= '" + filterRenewalDataModel.filterFromDate + "' AND Convert(date, CRMRenewalCreatedDate) <='" + filterRenewalDataModel.filterToDate + "'";
                }
                if (filterRenewalDataModel.classname != null && filterRenewalDataModel.classname != string.Empty)
                {
                    query += " and classcode='" + filterRenewalDataModel.classname + "'";
                }

                query += " AND ApproveSendSMS = 'Send_SMS' and PhoneNumber is not null and InvoiceNo is not null and CRMRenewalCreateFlag='0'";

                CheckConnectionState();
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = connection;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds);
                    }
                }

                sMSRenewalViewModels = ConvertDataTable<SMSRenewalViewModel>(ds.Tables[0]);
                return sMSRenewalViewModels;
            }
            catch (Exception ex)
            {
                return sMSRenewalViewModels;
            }
            finally
            {
                connection.Close();
            }
        }

        #region NoSMS Case
        public List<SMSRenewalViewModel> GetRenewalNoSMSCase()
        {
            DataSet ds = new DataSet();
            List<SMSRenewalViewModel> sMSRenewalViewModels = new List<SMSRenewalViewModel>();
            try
            {
                string query = "";
                query = "SELECT * FROM VW_SMSRenewalRegister WHERE 1=1";

                query += " AND ApproveSendSMS = 'No_SMS' and InvoiceNo is not null and CRMRenewalCreateFlag='0'";

                CheckConnectionState();
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = connection;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds);
                    }
                }

                sMSRenewalViewModels = ConvertDataTable<SMSRenewalViewModel>(ds.Tables[0]);
                return sMSRenewalViewModels;
            }
            catch (Exception ex)
            {
                return sMSRenewalViewModels;
            }
            finally
            {
                connection.Close();
            }
        }

        public List<SMSRenewalViewModel> GetRenewalNoSMSCaseByFilter(filterRenewalNoSMSCase filRenewalNoSMSDataModel)
        {
            DataSet ds = new DataSet();
            List<SMSRenewalViewModel> sMSRenewalViewModels = new List<SMSRenewalViewModel>();
            try
            {
                string query = "";
                query = "SELECT * FROM VW_SMSRenewalRegister WHERE 1=1";

                if (filRenewalNoSMSDataModel.filterFromDate != null && filRenewalNoSMSDataModel.filterToDate != null)
                {
                    query += " AND Convert(date, ApproveSendSMSDate) >= '" + Convert.ToDateTime(filRenewalNoSMSDataModel.filterFromDate).Date + "' AND Convert(date, ApproveSendSMSDate) <='" + Convert.ToDateTime(filRenewalNoSMSDataModel.filterToDate).Date + "'";
                }
                if (filRenewalNoSMSDataModel.classname != null && filRenewalNoSMSDataModel.classname != string.Empty)
                {
                    query += " and classcode='" + filRenewalNoSMSDataModel.classname + "'";
                }

                query += " AND ApproveSendSMS = 'No_SMS' and InvoiceNo is not null and CRMRenewalCreateFlag='0'";
                CheckConnectionState();
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = connection;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds);
                    }
                }

                sMSRenewalViewModels = ConvertDataTable<SMSRenewalViewModel>(ds.Tables[0]);
                return sMSRenewalViewModels;
            }
            catch(Exception ex)
            {
                return sMSRenewalViewModels;
            }
            finally
            {
                connection.Close();
            }
        }

        #endregion

        public List<SummaryRenewalRegister> GetSummaryRenewalRegister()
        {
            DataSet ds = new DataSet();
            List<SummaryRenewalRegister> sMSRenewalViewModels = new List<SummaryRenewalRegister>();
            try
            {
                string query = "";
                query = "SELECT Year,DateName( month , DateAdd( month , Month , -1 )) AS Month,ProductClass,sum(totalcount) totalcount FROM " +
                        "(SELECT Class AS ProductClass,"+
                        "  MONTH(ExpiryDate) Month,"+
                        "  YEAR(ExpiryDate) Year,"+
                        "  COUNT(Id) totalcount"+
                        " FROM "+
                            " VW_SMSRenewalRegister" +
                        " GROUP BY " +
                           " ExpiryDate, Class) tbl "+
                           "group by Month, ProductClass, Year";
                
                CheckConnectionState();
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = connection;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds);
                    }
                }

                sMSRenewalViewModels = ConvertDataTable<SummaryRenewalRegister>(ds.Tables[0]);
                return sMSRenewalViewModels;
            }
            catch (Exception ex)
            {
                return sMSRenewalViewModels;
            }
            finally
            {
                connection.Close();
            }
        }

        private static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }

        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();
            try
            {
                foreach (DataColumn column in dr.Table.Columns)
                {
                    foreach (PropertyInfo pro in temp.GetProperties())
                    {
                        if (pro.Name.ToLower() == column.ColumnName.ToLower())
                          pro.SetValue(obj, (dr[column.ColumnName] == DBNull.Value) ? null : dr[column.ColumnName], null);
                        else
                            continue;
                    }
                }
                return obj;
            }
            catch(Exception ex)
            {
                return obj;
            }
          
        }
    }
}
