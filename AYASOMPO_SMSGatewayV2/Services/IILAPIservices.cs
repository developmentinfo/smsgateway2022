﻿using AYASOMPO_SMSGatewayV2.Models;
using Microsoft.AspNetCore.Authentication.Twitter;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using static AYASOMPO_SMSGatewayV2.Models.DefaultPolicyAnswerResponse;
using static System.Net.WebRequestMethods;

namespace AYASOMPO_SMSGatewayV2.Services
{
    public class IILAPIservices
    {
        private HttpClient client;
        public IConfiguration Configuration { get; set; }
        MainDBContext _context = new MainDBContext();
        List<RuleResultViewModel> lstRuleResultViewModels = new List<RuleResultViewModel>();
        public string baseUrl { get; set; }
        private string apiKey = "";
        private string billingMerchantCode = "";
        private string AuthAPIAddress = "";
        private string username = "";
        private string password = "";
        private string paymentUrl = "";
        private string externalAPIURL = "";
        private string underwrittingAPIURL = "";
        public IILAPIservices()
        {
            var builder = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json");
            Configuration = builder.Build();

            baseUrl = Configuration["IILAPI:CustomerAPI"].ToString();
            //apiKey = Configuration["PaymentAPIKey:apiKey"].ToString();
            //billingMerchantCode = Configuration["PaymentAPIKey:billingMerchantCode"].ToString();
            AuthAPIAddress = Configuration["AuthAPI:IPAddress"].ToString();
            username = Configuration["AuthAPI:username"].ToString();
            password = Configuration["AuthAPI:password"].ToString();
            externalAPIURL = Configuration["ExternalAPI:IPAddress"].ToString();
            underwrittingAPIURL = Configuration["ExternalAPI:UnderWritingAPI"].ToString();
            client = new HttpClient();
        }

        public DefaultPolicyAnswerResponse GetDefaultPolicyAnswerObj(DefaultPolicyAnswerRequest request)
        {
            DefaultPolicyAnswerResponse defaultPolicyAnswerResponse = new DefaultPolicyAnswerResponse();
            try
            {
                if (request != null)
                {
                  
                    var stringPayload = JsonConvert.SerializeObject(request);
                    var jsonData = CallIILAPIAsync(stringPayload, "/v1/agent-service/underwriting/commonPolicyParameters/defaultPolicyAnswerObj");
                    if (jsonData.Result.ToString() != "-1")
                    {
                        defaultPolicyAnswerResponse = JsonConvert.DeserializeObject<DefaultPolicyAnswerResponse>(jsonData.Result.ToString());
                    }
                }

                return defaultPolicyAnswerResponse;
            }
            catch (Exception ex)
            {
                return defaultPolicyAnswerResponse;
            }
        }

        public async Task<List<RuleResultViewModel>> CreateProposalRequestAsync(DefaultPolicyAnswerResponse defaultPolicyAnswerResponse, FireRuleProposalViewModel ruleProposal)
        {
            try
            {
                if (defaultPolicyAnswerResponse != null)
                {
                    dynamic mainbody = new JObject();

                    mainbody.Add("commitFlag", "N");
                    mainbody.Add("examineState", "N");
                    mainbody.Add("paymentType", "cash");
                    mainbody.Add("backDate", 0.0);
                    mainbody.Add("userCode", ruleProposal.AccountCode);
                    mainbody.Add("userName", ruleProposal.AccountUserCode);
                    mainbody.Add("policyType", "P");

                    #region policyAnswerListDto
                    //// policyAnswerListDto
                    dynamic policyAnswerListDto = new JObject();
                    policyAnswerListDto.Add("createdBy", ruleProposal.AccountUserCode);
                    policyAnswerListDto.Add("createdDate", TimeZoneInfo.ConvertTimeToUtc(DateTime.Now));
                    policyAnswerListDto.Add("modifiedBy", ruleProposal.AccountUserCode);
                    policyAnswerListDto.Add("modifiedDate", TimeZoneInfo.ConvertTimeToUtc(DateTime.Now));
                    policyAnswerListDto.Add("customerCode", ruleProposal.CusCode);
                    policyAnswerListDto.Add("classCode", ruleProposal.Class);
                    policyAnswerListDto.Add("transitionDate", TimeZoneInfo.ConvertTimeToUtc(DateTime.Now));
                    policyAnswerListDto.Add("type", "P");
                    policyAnswerListDto.Add("className", "FIRE INSURANCE");

                    policyAnswerListDto.Add("productCode", ruleProposal.ProductCode);
                    policyAnswerListDto.Add("productName", "FIRE INSURANCE");

                    policyAnswerListDto.Add("customerName", ruleProposal.CusName);

                    #endregion

                    #region Customer
                    // Customer 
                    dynamic customer = new JObject();
                    customer.Add("customerCode", ruleProposal.CusCode);
                    customer.Add("customerName", ruleProposal.CusName);
                    policyAnswerListDto["customer"] = customer;
                    #endregion

                    #region agent
                    //agent
                    dynamic agent = new JObject();
                    agent.Add("accountHandlerCode", ruleProposal.AccountHandlerCode);
                    agent.Add("intermediaryCode", ruleProposal.AgentCode);
                    agent.Add("customerName", ruleProposal.CusName);
                    agent.Add("intermediary", ruleProposal.AgentName);
                    policyAnswerListDto["agent"] = agent;
                    #endregion

                    #region intermediaryType
                    //intermediaryType
                    dynamic intermediaryType = new JObject();
                    intermediaryType.Add("intermediaryCode", ruleProposal.IntermediaryCode);
                    intermediaryType.Add("description", ruleProposal.IntermediaryName);
                    policyAnswerListDto["intermediaryType"] = intermediaryType;

                    #endregion

                    #region policyAnswerDtos
                    policyAnswerListDto.policyAnswerDtos = new JArray() as dynamic;

                    var defaultAnsjsonResult = await getDefaultAnsAsync(ruleProposal.CusCode, ruleProposal.ProductCode, "P");
                    JObject o = JObject.Parse(defaultAnsjsonResult);

                    if (defaultPolicyAnswerResponse.policyAnswerDtos.Count() >= 0)
                    {
                        dynamic policyAnswer = new JObject();
                        foreach (var panswer in defaultPolicyAnswerResponse.policyAnswerDtos)
                        {
                            #region checking defaultAnswer and Lov Value 
                            string defaultAns = null;
                            LovValueViewModel lovValueViewModel = new LovValueViewModel();
                            if (panswer.qid != null)
                            {
                                JToken token = o[panswer.qid];
                                if (token != null)
                                {
                                    if (o[panswer.qid].Any(x => x.Type != JTokenType.Null) != false)
                                    {
                                        IList<string> allDrives = o[panswer.qid].Select(t => (string)t).ToList();
                                        defaultAns = allDrives[0];
                                    }
                                }
                                else
                                {
                                    if (panswer.fieldCode == "PL001")
                                    {
                                        defaultAns = Convert.ToDateTime(ruleProposal.PolicyStartDate).ToString("yyyy-MM-ddT0:00:00.000Z");
                                    }
                                    if (panswer.fieldCode == "PL002")
                                    {
                                        defaultAns = TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(ruleProposal.PolicyStartDate)).ToString("yyyy-MM-ddT0:00:00.000Z");
                                    }
                                    if (panswer.fieldCode == "PL009")
                                    {
                                        defaultAns = ruleProposal.ReferenceNo;
                                       
                                    }
                                    //if (autoUnderwriteSetting.IsPaymentDirect == false)
                                    //{
                                    //    if (panswer.fieldCode == "PL010")
                                    //    {
                                    //        defaultAns = "02";
                                    //    }
                                    //}
                                    //else
                                    //{
                                    //    if (panswer.fieldCode == "PL010")
                                    //    {
                                    //        defaultAns = "27";
                                    //    }
                                    //}
                                    if (panswer.fieldCode == "PL010")
                                    {
                                        defaultAns = "27";
                                    }

                                }
                            }

                            if (panswer.lovReq == "Y")
                            {
                                //if (panswer.ans == null)
                                //{
                                //    panswer.ans = defaultAns;
                                //}
                                panswer.ans = defaultAns;

                                lovValueViewModel = await GetLovValueAsync(panswer.qid, panswer.ans);
                                if (lovValueViewModel != null)
                                {
                                    defaultAns = lovValueViewModel.ans;
                                }

                            }

                            #endregion

                            policyAnswer = new JObject();

                            policyAnswer.Add(panswer.qid, defaultAns);
                            policyAnswer.Add("defaultPercentage", null);
                            policyAnswer.Add("defaultRate", null);
                            policyAnswer.Add("defaultRateLable", null);
                            policyAnswer.Add("defaultRateValue", null);

                            policyAnswer.Add("fieldMandatory", null);
                            policyAnswer.Add("maxPercentage", null);
                            policyAnswer.Add("maxRate", null);
                            policyAnswer.Add("minPercentage", null);
                            policyAnswer.Add("minRate", null);

                            policyAnswer.Add("perilRateDto", null);
                            policyAnswer.Add("rateType", null);
                            policyAnswer.Add("rateValue", null);
                            policyAnswer.Add("toggleType", null);
                            policyAnswer.Add("question", panswer.question);

                            policyAnswer.Add("page", panswer.page);
                            policyAnswer.Add("ans", defaultAns);
                            policyAnswer.Add("fieldCode", panswer.fieldCode);
                            policyAnswer.Add("limit", panswer.limit);
                            policyAnswer.Add("lovReq", panswer.lovReq);

                            policyAnswer.Add("ansId", lovValueViewModel.ansId);
                            policyAnswer.Add("displayOrder", panswer.displayOrder);
                            policyAnswer.Add("newRiskFlag", panswer.newRiskFlag);
                            policyAnswer.Add("plcSeqNo", panswer.plcSeqNo);
                            policyAnswer.Add("locCode", panswer.locCode);

                            policyAnswer.Add("prsSeqNo", panswer.prsSeqNo);
                            policyAnswer.Add("riskRSeq", panswer.riskRSeq);
                            policyAnswer.Add("newLocFlag", panswer.newLocFlag);
                            policyAnswer.Add("newPelFlag", panswer.newPelFlag);
                            policyAnswer.Add("riskSeq", panswer.riskSeq);

                            policyAnswer.Add("locSeq", panswer.locSeq);
                            policyAnswer.Add("dataType", panswer.dataType);
                            policyAnswer.Add("ponDefault", panswer.ponDefault);
                            policyAnswer.Add("qid", panswer.qid);

                            policyAnswerListDto.policyAnswerDtos.Add(policyAnswer);
                            
                        }
                    }
                    policyAnswerListDto["policyAnswerDtos"] = policyAnswerListDto.policyAnswerDtos;
                    #endregion

                    #region riskDetailDtos
                    // riskDetailDtos
                  
                    if (defaultPolicyAnswerResponse.riskDetailDtos.Count() > 0)
                    {
                        policyAnswerListDto.riskDetailDtos = new JArray() as dynamic;
                        foreach (var firerisk in ruleProposal.fireRiskdetails)
                        {
                            foreach (var riskDel in defaultPolicyAnswerResponse.riskDetailDtos)
                            {
                                dynamic riskDetail = new JObject();                               
                                riskDetail.locationDetails = new JArray() as dynamic;
                                riskDetail.riskDetails = new JArray() as dynamic;
                                riskDetail.coverDetails = new JArray() as dynamic;
                                riskDetail.inventory = new JArray() as dynamic;

                                if (riskDel.locationDetails.Count() > 0)
                                {
                                    #region Location Detail
                                    dynamic locDetails = new JObject();
                                    foreach (var loc in riskDel.locationDetails)
                                    {
                                        #region checking defaultAnswer and Lov Value 
                                        string defaultAns = null;
                                        LovValueViewModel lovValueViewModel = new LovValueViewModel();
                                        if (loc.qid != null)
                                        {
                                            if (loc.fieldCode == "PL001")
                                            {
                                                defaultAns = Convert.ToDateTime(ruleProposal.PolicyStartDate).ToString("yyyy-MM-dd");
                                            }
                                            if (loc.fieldCode == "PL002")
                                            {
                                                defaultAns = Convert.ToDateTime(ruleProposal.PolicyStartDate).ToString("yyyy-MM-dd");
                                            }
                                            if (loc.fieldCode == "PL009")
                                            {
                                                defaultAns = ruleProposal.ReferenceNo;
                                            }
                                            if (loc.fieldCode == "LOC01")
                                            {
                                                defaultAns = firerisk.BuildingNo;
                                            }
                                            //if (loc.fieldCode == "LOC02")
                                            //{
                                            //    defaultAns = firerisk.BuildingNo;
                                            //}
                                            if (loc.fieldCode == "LOC03")
                                            {
                                                defaultAns = firerisk.Street;
                                            }
                                            if (loc.fieldCode == "LOC04")
                                            {
                                                defaultAns = firerisk.WardCode;
                                            }
                                        }

                                        if (loc.lovReq == "Y")
                                        {
                                            //if (loc.ans == null)
                                            //{
                                            //    loc.ans = defaultAns;
                                            //}

                                            loc.ans = defaultAns;

                                            lovValueViewModel = await GetLovValueAsync(loc.qid, loc.ans);
                                            if (lovValueViewModel != null)
                                            {
                                                defaultAns = lovValueViewModel.ans;
                                            }
                                        }

                                        #endregion

                                        locDetails = new JObject();
                                        locDetails.Add(loc.qid, defaultAns);
                                        locDetails.Add("defaultPercentage", null);
                                        locDetails.Add("defaultRate", null);
                                        locDetails.Add("defaultRateLable", null);
                                        locDetails.Add("defaultRateValue", null);

                                        locDetails.Add("fieldMandatory", null);
                                        locDetails.Add("maxPercentage", null);
                                        locDetails.Add("maxRate", null);
                                        locDetails.Add("minPercentage", null);
                                        locDetails.Add("minRate", null);

                                        locDetails.Add("perilRateDto", null);
                                        locDetails.Add("rateType", null);
                                        locDetails.Add("rateValue", null);
                                        locDetails.Add("toggleType", null);
                                        locDetails.Add("question", loc.question);

                                        locDetails.Add("page", loc.page);
                                        locDetails.Add("ans", defaultAns);
                                        locDetails.Add("fieldCode", loc.fieldCode);
                                        locDetails.Add("limit", loc.limit);
                                        locDetails.Add("lovReq", loc.lovReq);

                                        locDetails.Add("ansId", lovValueViewModel.ansId);
                                        locDetails.Add("displayOrder", loc.displayOrder);
                                        locDetails.Add("newRiskFlag", loc.newRiskFlag);
                                        locDetails.Add("plcSeqNo", loc.plcSeqNo);
                                        locDetails.Add("locCode", loc.locCode);

                                        locDetails.Add("prsSeqNo", loc.prsSeqNo);
                                        locDetails.Add("riskRSeq", loc.riskRSeq);
                                        locDetails.Add("newLocFlag", loc.newLocFlag);
                                        locDetails.Add("newPelFlag", loc.newPelFlag);
                                        locDetails.Add("riskSeq", loc.riskSeq);

                                        locDetails.Add("locSeq", loc.locSeq);
                                        locDetails.Add("dataType", loc.dataType);
                                        locDetails.Add("ponDefault", loc.ponDefault);
                                        locDetails.Add("qid", loc.qid);

                                        riskDetail.locationDetails.Add(locDetails);
                                        AddQuestionIdAndAns(loc.qid, loc.question, defaultAns, firerisk.RiskName);

                                    }
                                    #endregion

                                    #region riskDetails
                                    dynamic riskDetails = new JObject();
                                    foreach (var riskdetail in riskDel.riskDetails)
                                    {
                                        #region checking defaultAnswer and Lov Value 
                                        string defaultAns = null;
                                        LovValueViewModel lovValueViewModel = new LovValueViewModel();
                                        if (riskdetail.qid != null)
                                        {
                                            JToken token = o[riskdetail.qid];
                                            if (token != null)
                                            {
                                                if (o[riskdetail.qid].Any(x => x.Type != JTokenType.Null) != false)
                                                {
                                                    IList<string> allDrives = o[riskdetail.qid].Select(t => (string)t).ToList();
                                                    defaultAns = allDrives[0];
                                                }
                                            }
                                            else
                                            {
                                             
                                                // Roof
                                                if (riskdetail.fieldCode == "RI01") // Roof
                                                {
                                                    defaultAns = firerisk.constructiondetail.Roof;                                                   
                                                }
                                                if (riskdetail.fieldCode == "RI04") // Occupancy
                                                {
                                                    defaultAns = firerisk.OccupancyCode;
                                                }
                                                if (riskdetail.fieldCode == "RI05") // Construction Class
                                                {
                                                    defaultAns = firerisk.constructiondetail.ConstructionClass;
                                                } 
                                                if(riskdetail.fieldCode == "RI02") // Columns
                                                {
                                                    defaultAns = firerisk.constructiondetail.Columns;
                                                }
                                                if(riskdetail.fieldCode == "RI09") // Floor
                                                {
                                                    defaultAns = firerisk.constructiondetail.Floor;
                                                }
                                                if (riskdetail.fieldCode == "RI03") // Number of Storey
                                                {
                                                    defaultAns = firerisk.constructiondetail.NumberOfStorey.ToString();
                                                }
                                                if (riskdetail.fieldCode == "RSK01") // Risk Name
                                                {
                                                    defaultAns = firerisk.RiskName;
                                                }
                                                if (riskdetail.fieldCode == "RSK02") // Risk Sumed insured
                                                {
                                                    defaultAns = firerisk.RiskSumInsured.ToString();
                                                }
                                                if (riskdetail.fieldCode == "RI08") // Walls
                                                {
                                                    defaultAns = firerisk.constructiondetail.Walls;
                                                }
                                                //if (riskdetail.fieldCode == "RI14") // Related Party Name
                                                //{
                                                //    defaultAns = "N";
                                                //}
                                                if (riskdetail.fieldCode == "RI13") // Related Party Transaction
                                                {
                                                    defaultAns = "N";
                                                }
                                                if (riskdetail.fieldCode == "RI20") // NUMBER OF ABC TYPE EXTINGUISHER
                                                {
                                                    defaultAns = firerisk.feainformation.NumberofAbctype.ToString();
                                                }
                                                if(riskdetail.fieldCode == "RI19") // GOOGLE MAP/ GEO COORDINATE
                                                {
                                                    defaultAns = firerisk.firesecurity.Geocoordinate.ToString();
                                                }
                                                if (riskdetail.fieldCode == "RI21") // NUMBER OF CO2 TYPE EXTINGUISHER
                                                {
                                                    defaultAns = firerisk.feainformation.NumberOfCo2type.ToString();
                                                }
                                                if(riskdetail.fieldCode == "RI22") // NUMBER OF OTHER EXTINGUISHER
                                                {
                                                    defaultAns = firerisk.feainformation.NumberOfOtherExtinguisher.ToString();
                                                }
                                                if (riskdetail.fieldCode == "RI23") // WEIGHT OF FIRE EXTINGUISHER
                                                {
                                                    defaultAns = firerisk.feainformation.WeightOfFireExtinguisher.ToString();
                                                }
                                                if (riskdetail.fieldCode == "RI24") // REQUIRED NUMBER OF EXTINGUISHERS
                                                {
                                                    defaultAns = firerisk.feainformation.RequiredNumberOfFireExtinguisher.ToString();
                                                }
                                                if (riskdetail.fieldCode == "RI28") // REQUIRED NUMBER OF EXTINGUISHERS
                                                {
                                                    defaultAns = firerisk.firewatertank.TypeofFireTank.ToString();
                                                }
                                                if (riskdetail.fieldCode == "RI49") // INVOICE VALIDITY PERIOD
                                                {
                                                    defaultAns = "100";
                                                }
                                                if (riskdetail.fieldCode == "RI34") // NUMBER OF CCTV CAMERA
                                                {
                                                    defaultAns = firerisk.firesecurity.NumberofCctvcamera.ToString();
                                                }                                             
                                                if (riskdetail.fieldCode == "RI35") // CCTV BACKDATED
                                                {
                                                    defaultAns = firerisk.firesecurity.Cctvbackdated.ToString();
                                                }
                                                if (riskdetail.fieldCode == "RI36") // ANY LOSS WITHIN PAST 5 YEARS
                                                {
                                                    defaultAns = firerisk.firesecurity.Losswithin5Years;
                                                }
                                                if (riskdetail.fieldCode == "RI31") // NUMBER OF LABOR
                                                {
                                                    defaultAns = firerisk.firesecurity.NumberofLabour;
                                                }
                                                if (riskdetail.fieldCode == "RI27") // NUMBER OF FIRE TANK
                                                {
                                                    defaultAns = firerisk.firewatertank.NumberofFireTank;
                                                }
                                                if (riskdetail.fieldCode == "RI25") // NUMBER OF HOSE REEL
                                                {
                                                    defaultAns = firerisk.feainformation.NumberOfHoseReel;
                                                }
                                                if (riskdetail.fieldCode == "RI39") // PAYMENT TYPE
                                                {
                                                    defaultAns = "F";
                                                }
                                                if (riskdetail.fieldCode == "RI26") // NUMBER OF HYDRAN
                                                {
                                                    defaultAns = firerisk.feainformation.NumberOfHydrant;
                                                }
                                                if (riskdetail.fieldCode == "RI29") // CAPACITY OF FIRE TANK
                                                {
                                                    defaultAns = firerisk.firewatertank.CapacityofFireTank;
                                                }
                                                if (riskdetail.fieldCode == "RI30") // NUMBER OF SECURITY GUARDS
                                                {
                                                    defaultAns = firerisk.firesecurity.NumberofSecurityGuards;
                                                }
                                                if (riskdetail.fieldCode == "RI32") // SECURITY GUARDS DUTY SHIFT
                                                {
                                                    defaultAns = firerisk.firesecurity.SecurityGuardsDutyShift;
                                                }
                                                if (riskdetail.fieldCode == "RI33") // SECURITY GUARDS DUTY HOUR PER DAY
                                                {
                                                    defaultAns = firerisk.firesecurity.SecurityGuardsDutyPerHour;
                                                }
                                                if (riskdetail.fieldCode == "RI52") // UNDERINSURANCE
                                                {
                                                    defaultAns = "N";
                                                }
                                                if (riskdetail.fieldCode == "RI52") // UNDERINSURANCE
                                                {
                                                    defaultAns = "N";
                                                }

                                            }
                                        }

                                        if (riskdetail.lovReq == "Y")
                                        {
                                            //if (riskdetail.ans == null)
                                            //{
                                            //    riskdetail.ans = defaultAns;
                                            //}
                                            riskdetail.ans = defaultAns;

                                            lovValueViewModel = await GetLovValueAsync(riskdetail.qid, riskdetail.ans);
                                            if (lovValueViewModel != null)
                                            {
                                                defaultAns = lovValueViewModel.ans;
                                            }
                                        }

                                        #endregion
                                       
                                        riskDetails = new JObject();
                                        riskDetails.Add(riskdetail.qid, defaultAns);
                                        riskDetails.Add("defaultPercentage", null);
                                        riskDetails.Add("defaultRate", null);
                                        riskDetails.Add("defaultRateLable", null);
                                        riskDetails.Add("defaultRateValue", null);

                                        riskDetails.Add("fieldMandatory", null);
                                        riskDetails.Add("maxPercentage", null);
                                        riskDetails.Add("maxRate", null);
                                        riskDetails.Add("minPercentage", null);
                                        riskDetails.Add("minRate", null);

                                        riskDetails.Add("perilRateDto", null);
                                        riskDetails.Add("rateType", null);
                                        riskDetails.Add("rateValue", null);
                                        riskDetails.Add("toggleType", null);
                                        riskDetails.Add("question", riskdetail.question);

                                        riskDetails.Add("page", riskdetail.page);
                                        riskDetails.Add("ans", defaultAns);
                                        riskDetails.Add("fieldCode", riskdetail.fieldCode);
                                        riskDetails.Add("limit", riskdetail.limit);
                                        riskDetails.Add("lovReq", riskdetail.lovReq);

                                        riskDetails.Add("ansId", lovValueViewModel.ansId);
                                        riskDetails.Add("displayOrder", riskdetail.displayOrder);
                                        riskDetails.Add("newRiskFlag", riskdetail.newRiskFlag);
                                        riskDetails.Add("plcSeqNo", riskdetail.plcSeqNo);
                                        riskDetails.Add("locCode", riskdetail.locCode);

                                        riskDetails.Add("prsSeqNo", riskdetail.prsSeqNo);
                                        riskDetails.Add("riskRSeq", riskdetail.riskRSeq);
                                        riskDetails.Add("newLocFlag", riskdetail.newLocFlag);
                                        riskDetails.Add("newPelFlag", riskdetail.newPelFlag);
                                        riskDetails.Add("riskSeq", riskdetail.riskSeq);

                                        riskDetails.Add("locSeq", riskdetail.locSeq);
                                        riskDetails.Add("dataType", riskdetail.dataType);
                                        riskDetails.Add("ponDefault", riskdetail.ponDefault);
                                        riskDetails.Add("qid", riskdetail.qid);

                                        riskDetail.riskDetails.Add(riskDetails);

                                        AddQuestionIdAndAns(riskdetail.qid, riskdetail.question, defaultAns, firerisk.RiskName);

                                    }
                                    #endregion

                                    #region coverDetails
                                    dynamic coverDetails = new JObject();
                                    foreach (var cover in riskDel.coverDetails)
                                    {
                                        #region checking defaultAnswer and Lov Value 
                                        string defaultAns = null;
                                        LovValueViewModel lovValueViewModel = new LovValueViewModel();
                                        if (cover.qid != null)
                                        {
                                            JToken token = o[cover.qid];
                                            if (token != null)
                                            {
                                                if (o[cover.qid].Any(x => x.Type != JTokenType.Null) != false)
                                                {
                                                    IList<string> allDrives = o[cover.qid].Select(t => (string)t).ToList();
                                                    defaultAns = allDrives[0];
                                                }
                                            }
                                            else
                                            {
                                                if(cover.fieldCode == "RP01") // RIOT STRIKE AND MALICIOUS DAMAGE
                                                {
                                                    if(firerisk.firecoverage.RiotStrikeAndMaliciousDamage == true)
                                                    {
                                                        defaultAns = "Y";
                                                    }
                                                    else
                                                    {
                                                        defaultAns = "N";
                                                    }
                                                }

                                                if (cover.fieldCode == "RP02") // AIRCRAFT DAMAGE
                                                {
                                                    if (firerisk.firecoverage.AircraftDamage == true)
                                                    {
                                                        defaultAns = "Y";
                                                    }
                                                    else
                                                    {
                                                        defaultAns = "N";
                                                    }
                                                }

                                                if (cover.fieldCode == "RP03") // IMPACT DAMAGE
                                                {
                                                    if (firerisk.firecoverage.ImpactDamage == true)
                                                    {
                                                        defaultAns = "Y";
                                                    }
                                                    else
                                                    {
                                                        defaultAns = "N";
                                                    }
                                                }

                                                if (cover.fieldCode == "RP04") // SUBSIDENCE & LANDSLIDE
                                                {
                                                    if (firerisk.firecoverage.SubsidenceAndLandslide == true)
                                                    {
                                                        defaultAns = "Y";
                                                    }
                                                    else
                                                    {
                                                        defaultAns = "N";
                                                    }
                                                }


                                                if (cover.fieldCode == "RP05") // EARTH-QUAKE FIRE, FIRE AND SHOCK DMG CAUSED BY EQ
                                                {
                                                    if (firerisk.firecoverage.EarthQuakeFireFireAndShockDamage == true)
                                                    {
                                                        defaultAns = "Y";
                                                    }
                                                    else
                                                    {
                                                        defaultAns = "N";
                                                    }
                                                }
                                                if (cover.fieldCode == "RP06") // EXPLOSION
                                                {
                                                    if (firerisk.firecoverage.Explosion == true)
                                                    {
                                                        defaultAns = "Y";
                                                    }
                                                    else
                                                    {
                                                        defaultAns = "N";
                                                    }
                                                }

                                                if (cover.fieldCode == "RP07") // SPONTANEOUS COMBUSTION
                                                {
                                                    if (firerisk.firecoverage.SpontaneousCombustion == true)
                                                    {
                                                        defaultAns = "Y";
                                                    }
                                                    else
                                                    {
                                                        defaultAns = "N";
                                                    }
                                                }

                                                if (cover.fieldCode == "RP08") // STORM, TYPHOON, HURRICANE, TEMPEST
                                                {
                                                    if (firerisk.firecoverage.StormTyphoonHurricaneTempest == true)
                                                    {
                                                        defaultAns = "Y";
                                                    }
                                                    else
                                                    {
                                                        defaultAns = "N";
                                                    }
                                                }

                                                if (cover.fieldCode == "RP09") // BURGLARY
                                                {
                                                    if (firerisk.firecoverage.Burglary == true)
                                                    {
                                                        defaultAns = "Y";
                                                    }
                                                    else
                                                    {
                                                        defaultAns = "N";
                                                    }
                                                }

                                                if (cover.fieldCode == "RP10") // WAR Risk
                                                {
                                                    if (firerisk.firecoverage.WarRisk == true)
                                                    {
                                                        defaultAns = "Y";
                                                    }
                                                    else
                                                    {
                                                        defaultAns = "N";
                                                    }
                                                }

                                                if (cover.fieldCode == "RP11") // FLOOD AND INUNDATION
                                                {
                                                    if (firerisk.firecoverage.FloodAndInundation == true)
                                                    {
                                                        defaultAns = "Y";
                                                    }
                                                    else
                                                    {
                                                        defaultAns = "N";
                                                    }
                                                }

                                            }
                                        }

                                        if (cover.lovReq == "Y")
                                        {
                                            //if (cover.ans == null)
                                            //{
                                            //    cover.ans = defaultAns;
                                            //}
                                            cover.ans = defaultAns;

                                            lovValueViewModel = await GetLovValueAsync(cover.qid, cover.ans);
                                            if (lovValueViewModel != null)
                                            {
                                                defaultAns = lovValueViewModel.ans;
                                            }
                                        }

                                        #endregion

                                        coverDetails = new JObject();
                                        coverDetails.Add(cover.qid, defaultAns);
                                        coverDetails.Add("defaultPercentage", null);
                                        coverDetails.Add("defaultRate", null);
                                        coverDetails.Add("defaultRateLable", null);
                                        coverDetails.Add("defaultRateValue", null);

                                        coverDetails.Add("fieldMandatory", null);
                                        coverDetails.Add("maxPercentage", null);
                                        coverDetails.Add("maxRate", null);
                                        coverDetails.Add("minPercentage", null);
                                        coverDetails.Add("minRate", null);

                                        coverDetails.Add("perilRateDto", null);
                                        coverDetails.Add("rateType", null);
                                        coverDetails.Add("rateValue", null);
                                        coverDetails.Add("toggleType", null);
                                        coverDetails.Add("question", cover.question);

                                        coverDetails.Add("page", cover.page);
                                        coverDetails.Add("ans", defaultAns);
                                        coverDetails.Add("fieldCode", cover.fieldCode);
                                        coverDetails.Add("limit", cover.limit);
                                        coverDetails.Add("lovReq", cover.lovReq);

                                        coverDetails.Add("ansId", lovValueViewModel.ansId);
                                        coverDetails.Add("displayOrder", cover.displayOrder);
                                        coverDetails.Add("newRiskFlag", cover.newRiskFlag);
                                        coverDetails.Add("plcSeqNo", cover.plcSeqNo);
                                        coverDetails.Add("locCode", cover.locCode);

                                        coverDetails.Add("prsSeqNo", cover.prsSeqNo);
                                        coverDetails.Add("riskRSeq", cover.riskRSeq);
                                        coverDetails.Add("newLocFlag", cover.newLocFlag);
                                        coverDetails.Add("newPelFlag", cover.newPelFlag);
                                        coverDetails.Add("riskSeq", cover.riskSeq);

                                        coverDetails.Add("locSeq", cover.locSeq);
                                        coverDetails.Add("dataType", cover.dataType);
                                        coverDetails.Add("ponDefault", cover.ponDefault);
                                        coverDetails.Add("qid", cover.qid);

                                        riskDetail.coverDetails.Add(coverDetails);
                                        AddQuestionIdAndAns(cover.qid, cover.question, defaultAns, firerisk.RiskName);
                                    }
                                    #endregion

                                    #region Inventory
                                    
                                    foreach (var item in firerisk.fireinventory)
                                    {
                                        dynamic inventory = new JObject();
                                        inventory.inventoryDetails = new JArray() as dynamic;

                                        inventory.Add("referenceNo", null);
                                        inventory.Add("declaredAmount", item.DeclaredAmount);
                                        inventory.Add("itemDescription", null);
                                        inventory.Add("riskSequenceNo", null);
                                        inventory.Add("newInventoryStatus", null);
                                        inventory.Add("policyInventory", null);

                                        #region codeDescriptionDto
                                        dynamic codeDescriptionDto = new JObject();
                                        codeDescriptionDto.Add("code", item.ItemCode);
                                        codeDescriptionDto.Add("description", item.ItemDescription);
                                        inventory["codeDescriptionDto"] = codeDescriptionDto;
                                        #endregion

                                        #region Inventory Detail
                                        dynamic inventoryDel = new JObject();

                                        inventoryDel.Add("inventoryDetSeq",null);
                                        inventoryDel.Add("referenceNo1", null);
                                        inventoryDel.Add("referenceNo2", null);
                                        inventoryDel.Add("referenceNo3", null);
                                        inventoryDel.Add("date", null);

                                        inventoryDel.Add("date2", null);
                                        inventoryDel.Add("value", null);
                                        inventoryDel.Add("value2", null);
                                        inventoryDel.Add("referenceNo4", null);
                                        inventoryDel.Add("referenceNo5", null);

                                        inventoryDel.Add("referenceNo6", null);
                                        inventoryDel.Add("inventoryDetDescription", null);
                                        inventoryDel.Add("invenDetflag", null);
                                        inventoryDel.Add("policyInventoryDet", null);

                                        inventory.inventoryDetails.Add(inventoryDel);


                                        #endregion

                                       
                                        riskDetail.inventory.Add(inventory);

                                    }
                                    #endregion
                                }

                                policyAnswerListDto.riskDetailDtos.Add(riskDetail);

                                policyAnswerListDto["riskDetailDtos"] = policyAnswerListDto.riskDetailDtos;
                            }
                           
                        }
                       
                    }
                    
                    #endregion 

                    mainbody["policyAnswerListDto"] = policyAnswerListDto;

                    var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(mainbody);

                    //string transferRefNo = tripInfo.Id.ToString();
                    //string BillRefNo = tripInfo.ReferenceNo.ToString();
                    //var jsonData = CallIILAPIAsync(jsonString, "/v1/agent-service/policy/savePolicyProposal", transferRefNo, BillRefNo, true);
                    //if (jsonData.Result.ToString() != "-1")
                    //{
                       
                    //}

                }
                return lstRuleResultViewModels;
            }
            catch (Exception ex)
            {
                return lstRuleResultViewModels;
            }
        }

        private List<RuleResultViewModel> AddQuestionIdAndAns(string qid, string questionname, string questionvalue, string riskName)
        {
            try
            {
                RuleResultViewModel ruleResultViewModel = new RuleResultViewModel();
                ruleResultViewModel.questionId = qid;
                ruleResultViewModel.questionname = questionname;
                ruleResultViewModel.questionvalue = questionvalue;
                ruleResultViewModel.riskName = riskName;
                lstRuleResultViewModels.Add(ruleResultViewModel);
                return lstRuleResultViewModels;
            }
            catch (Exception ex)
            {
                return lstRuleResultViewModels;
            }
        }

        private async Task<string> CallIILAPIAsync(string stringPayload, string url)
        {
            string jsonData = "-1";
            try
            {
                
                var httpRequestContent = new StringContent(stringPayload, Encoding.UTF8, "application/json");
                
                var message = await client.PostAsync(baseUrl + url, httpRequestContent);
                string response = await message.Content.ReadAsStringAsync();

                if (!message.IsSuccessStatusCode)
                {
                    return jsonData;
                }
                else
                {
                    jsonData = await message.Content.ReadAsStringAsync();
                    return jsonData;
                }
            }
            catch (Exception ex)
            {
                return jsonData;
            }
        }

        public async Task<CustomerInfoViewModel> GetCustomerInfoAsync(string cusCode)
        {
            CustomerInfoViewModel customerInfoViewModel = new CustomerInfoViewModel();
            try
            {
                HttpClient client = new HttpClient();

                string access_token = await GetTokenByUserInfo(client);
                if (access_token != null || access_token != "")
                {
                    //Call post api with access token
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + access_token);

                    var stringPayload = new
                    {
                        customerCode = cusCode                       
                    };

                    var json = JsonConvert.SerializeObject(stringPayload);

                    var httpRequestContent = new StringContent(json, Encoding.UTF8, "application/json");

                    var message = await client.PostAsync(externalAPIURL + "/customer/GetCustomer", httpRequestContent);
                    if(!message.IsSuccessStatusCode)
                    {
                        return customerInfoViewModel = null;
                    }
                    else
                    {
                        string jsonData = await message.Content.ReadAsStringAsync();
                        customerInfoViewModel = JsonConvert.DeserializeObject<CustomerInfoViewModel>(jsonData);
                        if(customerInfoViewModel != null)
                        {
                            return customerInfoViewModel;
                        }
                    }
                }

                return customerInfoViewModel;
            }
            catch(Exception ex)
            {
                return customerInfoViewModel;
            }
        }

        public async Task<LocationDetials> GetCustomerAddressDetailAsync(string wardCode)
        {
            LocationDetials locationDetials = new LocationDetials();
            try
            {
                HttpClient client = new HttpClient();

                string access_token = await GetTokenByUserInfo(client);
                if (access_token != null || access_token != "")
                {
                    //Call post api with access token
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + access_token);

                    var message = await client.GetAsync(underwrittingAPIURL + "/api/UnderWritting/GetLocationDetailByWardCode/"+ wardCode);
                    if (!message.IsSuccessStatusCode)
                    {
                        return locationDetials = null;
                    }
                    else
                    {
                        string jsonData = await message.Content.ReadAsStringAsync();
                        locationDetials = JsonConvert.DeserializeObject<LocationDetials>(jsonData);
                        if (locationDetials != null)
                        {
                            return locationDetials;
                        }
                    }
                }

                return locationDetials;
            }
            catch (Exception ex)
            {
                return locationDetials;
            }
        }

        public async Task<string> GetTokenByUserInfo(HttpClient client)
        {
            var json = JsonConvert.SerializeObject(new { username = username, password = password });
            var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");

            var message = await client.PostAsync(AuthAPIAddress, content);
            if (message.StatusCode == System.Net.HttpStatusCode.InternalServerError)
            {
                return null;
            }
            else if (message.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                return null;
            }
            else if (message.StatusCode == System.Net.HttpStatusCode.MethodNotAllowed)
            {
                return null;
            }
            else
            {
                message.EnsureSuccessStatusCode();

                if (message.IsSuccessStatusCode)
                {
                    TokenResponseViewModel tokenResponseViewModel = new TokenResponseViewModel();
                    string jsonData = await message.Content.ReadAsStringAsync();
                    tokenResponseViewModel = JsonConvert.DeserializeObject<TokenResponseViewModel>(jsonData);

                    if (tokenResponseViewModel.access_token != null)
                    {
                        return tokenResponseViewModel.access_token;
                    }
                }
            }
            return null;
        }

        public async Task<string> getDefaultAnsAsync(string customerCode, string productCode, string type)
        {
            var jsonData = "";
            try
            {
                GetDefaultAnsRequest request = new GetDefaultAnsRequest();
                request.customerCode = customerCode;
                request.productCode = productCode;
                request.type = type;

                var stringPayload = JsonConvert.SerializeObject(request);
                var httpRequestContent = new StringContent(stringPayload, Encoding.UTF8, "application/json");
                var message = await client.PostAsync(baseUrl + "/v1/agent-service/policy/getDefaultAns", httpRequestContent);

                if (!message.IsSuccessStatusCode)
                {
                    return jsonData;
                }
                else
                {
                    var json = await message.Content.ReadAsStringAsync();
                    jsonData = json;
                }
                return jsonData;
            }
            catch (Exception ex)
            {
                return jsonData;
            }
        }

        public async Task<LovValueViewModel> GetLovValueAsync(string qid, string ans)
        {
            string jsonData = "-1";
            LovValueViewModel lovValueViewModel = new LovValueViewModel();
            try
            {
                //qid = "0HEAD2200002903";
                List<List<string>> lstArray = new List<List<string>>();
                var message = await client.GetAsync(baseUrl + "/v1/agent-service/underwriting/commonPolicyParameters/classes/getLov/" + qid);
                if (!message.IsSuccessStatusCode)
                {
                    return lovValueViewModel;
                }
                else
                {
                    jsonData = await message.Content.ReadAsStringAsync();
                    lstArray = JsonConvert.DeserializeObject<List<List<string>>>(jsonData);
                    if (ans != null)
                    {
                        lovValueViewModel.ans = lstArray.Where(l => l[0] == ans).Select(l => l[1]).FirstOrDefault();
                        lovValueViewModel.ansId = lstArray.Where(l => l[0] == ans).Select(l => l[0]).FirstOrDefault();
                    }
                    else
                    {
                        lovValueViewModel.ans = lstArray.Where(l => l[0] == ans).Select(l => l[1]).FirstOrDefault();
                        lovValueViewModel.ansId = lstArray.Where(l => l[0] == ans).Select(l => l[0]).FirstOrDefault();
                    }

                    return lovValueViewModel;
                }
            }
            catch (Exception ex)
            {
                return lovValueViewModel;
            }
        }

    }
}
