﻿using AYASOMPO_SMSGatewayV2.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Services
{
    public class MiddlewareAPIService
    {
        private HttpClient client;
        public IConfiguration Configuration { get; set; }
        public string baseUrl { get; set; }
        
        public MiddlewareAPIService()
        {
            var builder = new ConfigurationBuilder()
          .SetBasePath(Directory.GetCurrentDirectory())
          .AddJsonFile("appsettings.json");
            Configuration = builder.Build();

            baseUrl = Configuration["CoreMiddleware:CoreMiddleIPAddress"].ToString();
            client = new HttpClient();
        }

        public async Task<List<MasterDataViewModel>> GetBuildingOccupationData()
        {
            List<MasterDataViewModel> lstMasterDataViewModels = new List<MasterDataViewModel>();
            try
            {
                var responseTask = await client.GetAsync(baseUrl + "/MasterData/GetBuildingOccupationData");
                if (!responseTask.IsSuccessStatusCode)
                {
                    return lstMasterDataViewModels;
                }
                else
                {
                    string response = await responseTask.Content.ReadAsStringAsync();
                    lstMasterDataViewModels = JsonConvert.DeserializeObject<List<MasterDataViewModel>>(response);
                }
                return lstMasterDataViewModels;
            }
            catch (Exception ex)
            {
                return lstMasterDataViewModels;
            }
        }

        public async Task<List<MasterDataViewModel>> GetFloorData()
        {
            List<MasterDataViewModel> lstMasterDataViewModels = new List<MasterDataViewModel>();
            try
            {
                var responseTask = await client.GetAsync(baseUrl + "/MasterData/GetFloorData");
                if (!responseTask.IsSuccessStatusCode)
                {
                    return lstMasterDataViewModels;
                }
                else
                {
                    string response = await responseTask.Content.ReadAsStringAsync();
                    lstMasterDataViewModels = JsonConvert.DeserializeObject<List<MasterDataViewModel>>(response);
                }
                return lstMasterDataViewModels;
            }
            catch (Exception ex)
            {
                return lstMasterDataViewModels;
            }
        }

        public async Task<List<MasterDataViewModel>> GetBuildingClassData()
        {
            List<MasterDataViewModel> lstMasterDataViewModels = new List<MasterDataViewModel>();
            try
            {
                var responseTask = await client.GetAsync(baseUrl + "/MasterData/GetBuildingClassData");
                if (!responseTask.IsSuccessStatusCode)
                {
                    return lstMasterDataViewModels;
                }
                else
                {
                    string response = await responseTask.Content.ReadAsStringAsync();
                    lstMasterDataViewModels = JsonConvert.DeserializeObject<List<MasterDataViewModel>>(response);
                }
                return lstMasterDataViewModels;
            }
            catch (Exception ex)
            {
                return lstMasterDataViewModels;
            }
        }

        public async Task<List<MasterDataViewModel>> GetRoofData()
        {
            List<MasterDataViewModel> lstMasterDataViewModels = new List<MasterDataViewModel>();
            try
            {
                var responseTask = await client.GetAsync(baseUrl + "/MasterData/GetRoofData");
                if (!responseTask.IsSuccessStatusCode)
                {
                    return lstMasterDataViewModels;
                }
                else
                {
                    string response = await responseTask.Content.ReadAsStringAsync();
                    lstMasterDataViewModels = JsonConvert.DeserializeObject<List<MasterDataViewModel>>(response);
                }
                return lstMasterDataViewModels;
            }
            catch (Exception ex)
            {
                return lstMasterDataViewModels;
            }
        }

        public async Task<List<MasterDataViewModel>> GetWallData()
        {
            List<MasterDataViewModel> lstMasterDataViewModels = new List<MasterDataViewModel>();
            try
            {
                var responseTask = await client.GetAsync(baseUrl + "/MasterData/GetWallData");
                if (!responseTask.IsSuccessStatusCode)
                {
                    return lstMasterDataViewModels;
                }
                else
                {
                    string response = await responseTask.Content.ReadAsStringAsync();
                    lstMasterDataViewModels = JsonConvert.DeserializeObject<List<MasterDataViewModel>>(response);
                }
                return lstMasterDataViewModels;
            }
            catch (Exception ex)
            {
                return lstMasterDataViewModels;
            }
        }

        public async Task<List<MasterDataViewModel>> GetColumnData()
        {
            List<MasterDataViewModel> lstMasterDataViewModels = new List<MasterDataViewModel>();
            try
            {
                var responseTask = await client.GetAsync(baseUrl + "/MasterData/GetColumnData");
                if (!responseTask.IsSuccessStatusCode)
                {
                    return lstMasterDataViewModels;
                }
                else
                {
                    string response = await responseTask.Content.ReadAsStringAsync();
                    lstMasterDataViewModels = JsonConvert.DeserializeObject<List<MasterDataViewModel>>(response);
                }
                return lstMasterDataViewModels;
            }
            catch (Exception ex)
            {
                return lstMasterDataViewModels;
            }
        }

        public async Task<List<MasterDataViewModel>> GetABCTypeData()
        {
            List<MasterDataViewModel> lstMasterDataViewModels = new List<MasterDataViewModel>();
            try
            {
                var responseTask = await client.GetAsync(baseUrl + "/MasterData/GetABCTypeData");
                if (!responseTask.IsSuccessStatusCode)
                {
                    return lstMasterDataViewModels;
                }
                else
                {
                    string response = await responseTask.Content.ReadAsStringAsync();
                    lstMasterDataViewModels = JsonConvert.DeserializeObject<List<MasterDataViewModel>>(response);
                }
                return lstMasterDataViewModels;
            }
            catch (Exception ex)
            {
                return lstMasterDataViewModels;
            }
        }

        public async Task<List<MasterDataViewModel>> GetCO2TypeData()
        {
            List<MasterDataViewModel> lstMasterDataViewModels = new List<MasterDataViewModel>();
            try
            {
                var responseTask = await client.GetAsync(baseUrl + "/MasterData/GetCO2TypeData");
                if (!responseTask.IsSuccessStatusCode)
                {
                    return lstMasterDataViewModels;
                }
                else
                {
                    string response = await responseTask.Content.ReadAsStringAsync();
                    lstMasterDataViewModels = JsonConvert.DeserializeObject<List<MasterDataViewModel>>(response);
                }
                return lstMasterDataViewModels;
            }
            catch (Exception ex)
            {
                return lstMasterDataViewModels;
            }
        }

        public async Task<List<MasterDataViewModel>> GetExtinguisherData()
        {
            List<MasterDataViewModel> lstMasterDataViewModels = new List<MasterDataViewModel>();
            try
            {
                var responseTask = await client.GetAsync(baseUrl + "/MasterData/GetExtinguisherData");
                if (!responseTask.IsSuccessStatusCode)
                {
                    return lstMasterDataViewModels;
                }
                else
                {
                    string response = await responseTask.Content.ReadAsStringAsync();
                    lstMasterDataViewModels = JsonConvert.DeserializeObject<List<MasterDataViewModel>>(response);
                }
                return lstMasterDataViewModels;
            }
            catch (Exception ex)
            {
                return lstMasterDataViewModels;
            }
        }

        public async Task<List<MasterDataViewModel>> GetWeightOfFireData()
        {
            List<MasterDataViewModel> lstMasterDataViewModels = new List<MasterDataViewModel>();
            try
            {
                var responseTask = await client.GetAsync(baseUrl + "/MasterData/GetWeightOfFireData");
                if (!responseTask.IsSuccessStatusCode)
                {
                    return lstMasterDataViewModels;
                }
                else
                {
                    string response = await responseTask.Content.ReadAsStringAsync();
                    lstMasterDataViewModels = JsonConvert.DeserializeObject<List<MasterDataViewModel>>(response);
                }
                return lstMasterDataViewModels;
            }
            catch (Exception ex)
            {
                return lstMasterDataViewModels;
            }
        }

        public async Task<List<MasterDataViewModel>> GetRequiredNumberOfExtinguisherData()
        {
            List<MasterDataViewModel> lstMasterDataViewModels = new List<MasterDataViewModel>();
            try
            {
                var responseTask = await client.GetAsync(baseUrl + "/MasterData/GetRequiredNumberOfExtinguisherData");
                if (!responseTask.IsSuccessStatusCode)
                {
                    return lstMasterDataViewModels;
                }
                else
                {
                    string response = await responseTask.Content.ReadAsStringAsync();
                    lstMasterDataViewModels = JsonConvert.DeserializeObject<List<MasterDataViewModel>>(response);
                }
                return lstMasterDataViewModels;
            }
            catch (Exception ex)
            {
                return lstMasterDataViewModels;
            }
        }

        public async Task<List<MasterDataViewModel>> GetTypeFireTankData()
        {
            List<MasterDataViewModel> lstMasterDataViewModels = new List<MasterDataViewModel>();
            try
            {
                var responseTask = await client.GetAsync(baseUrl + "/MasterData/GetTypeFireTankData");
                if (!responseTask.IsSuccessStatusCode)
                {
                    return lstMasterDataViewModels;
                }
                else
                {
                    string response = await responseTask.Content.ReadAsStringAsync();
                    lstMasterDataViewModels = JsonConvert.DeserializeObject<List<MasterDataViewModel>>(response);
                }
                return lstMasterDataViewModels;
            }
            catch (Exception ex)
            {
                return lstMasterDataViewModels;
            }
        }

        public async Task<List<MasterDataViewModel>> GetHydrantData()
        {
            List<MasterDataViewModel> lstMasterDataViewModels = new List<MasterDataViewModel>();
            try
            {
                var responseTask = await client.GetAsync(baseUrl + "/MasterData/GetHydrantData");
                if (!responseTask.IsSuccessStatusCode)
                {
                    return lstMasterDataViewModels;
                }
                else
                {
                    string response = await responseTask.Content.ReadAsStringAsync();
                    lstMasterDataViewModels = JsonConvert.DeserializeObject<List<MasterDataViewModel>>(response);
                }
                return lstMasterDataViewModels;
            }
            catch (Exception ex)
            {
                return lstMasterDataViewModels;
            }
        }

        public async Task<List<MasterDataViewModel>> GetHoseReelData()
        {
            List<MasterDataViewModel> lstMasterDataViewModels = new List<MasterDataViewModel>();
            try
            {
                var responseTask = await client.GetAsync(baseUrl + "/MasterData/GetHoseReelData");
                if (!responseTask.IsSuccessStatusCode)
                {
                    return lstMasterDataViewModels;
                }
                else
                {
                    string response = await responseTask.Content.ReadAsStringAsync();
                    lstMasterDataViewModels = JsonConvert.DeserializeObject<List<MasterDataViewModel>>(response);
                }
                return lstMasterDataViewModels;
            }
            catch (Exception ex)
            {
                return lstMasterDataViewModels;
            }
        }

        public async Task<List<MasterDataViewModel>> GetNumberFireTankData()
        {
            List<MasterDataViewModel> lstMasterDataViewModels = new List<MasterDataViewModel>();
            try
            {
                var responseTask = await client.GetAsync(baseUrl + "/MasterData/GetNumberFireTankData");
                if (!responseTask.IsSuccessStatusCode)
                {
                    return lstMasterDataViewModels;
                }
                else
                {
                    string response = await responseTask.Content.ReadAsStringAsync();
                    lstMasterDataViewModels = JsonConvert.DeserializeObject<List<MasterDataViewModel>>(response);
                }
                return lstMasterDataViewModels;
            }
            catch (Exception ex)
            {
                return lstMasterDataViewModels;
            }
        }

        public async Task<List<MasterDataViewModel>> GetSecurityGuardsData()
        {
            List<MasterDataViewModel> lstMasterDataViewModels = new List<MasterDataViewModel>();
            try
            {
                var responseTask = await client.GetAsync(baseUrl + "/MasterData/GetSecurityGuardsData");
                if (!responseTask.IsSuccessStatusCode)
                {
                    return lstMasterDataViewModels;
                }
                else
                {
                    string response = await responseTask.Content.ReadAsStringAsync();
                    lstMasterDataViewModels = JsonConvert.DeserializeObject<List<MasterDataViewModel>>(response);
                }
                return lstMasterDataViewModels;
            }
            catch (Exception ex)
            {
                return lstMasterDataViewModels;
            }
        }

        public async Task<List<MasterDataViewModel>> GetSecurityGuardsDutyShiftData()
        {
            List<MasterDataViewModel> lstMasterDataViewModels = new List<MasterDataViewModel>();
            try
            {
                var responseTask = await client.GetAsync(baseUrl + "/MasterData/GetSecurityGuardsDutyShiftData");
                if (!responseTask.IsSuccessStatusCode)
                {
                    return lstMasterDataViewModels;
                }
                else
                {
                    string response = await responseTask.Content.ReadAsStringAsync();
                    lstMasterDataViewModels = JsonConvert.DeserializeObject<List<MasterDataViewModel>>(response);
                }
                return lstMasterDataViewModels;
            }
            catch (Exception ex)
            {
                return lstMasterDataViewModels;
            }
        }

        public async Task<List<MasterDataViewModel>> GetSecurityGuardsDutyHourData()
        {
            List<MasterDataViewModel> lstMasterDataViewModels = new List<MasterDataViewModel>();
            try
            {
                var responseTask = await client.GetAsync(baseUrl + "/MasterData/GetSecurityGuardsDutyHourData");
                if (!responseTask.IsSuccessStatusCode)
                {
                    return lstMasterDataViewModels;
                }
                else
                {
                    string response = await responseTask.Content.ReadAsStringAsync();
                    lstMasterDataViewModels = JsonConvert.DeserializeObject<List<MasterDataViewModel>>(response);
                }
                return lstMasterDataViewModels;
            }
            catch (Exception ex)
            {
                return lstMasterDataViewModels;
            }
        }

        public async Task<List<MasterDataViewModel>> GetCCTVCameraData()
        {
            List<MasterDataViewModel> lstMasterDataViewModels = new List<MasterDataViewModel>();
            try
            {
                var responseTask = await client.GetAsync(baseUrl + "/MasterData/GetCCTVCameraData");
                if (!responseTask.IsSuccessStatusCode)
                {
                    return lstMasterDataViewModels;
                }
                else
                {
                    string response = await responseTask.Content.ReadAsStringAsync();
                    lstMasterDataViewModels = JsonConvert.DeserializeObject<List<MasterDataViewModel>>(response);
                }
                return lstMasterDataViewModels;
            }
            catch (Exception ex)
            {
                return lstMasterDataViewModels;
            }
        }

        public async Task<WardCodeLocation> GetLocationByWardCodeAsync(string wardcode)
        {
            WardCodeLocation wardCodeLocation = new WardCodeLocation();
            try
            {
                var responseTask = client.GetAsync(baseUrl + "/Customer/GetLocationDetailByWardCode/" + wardcode);
                var message = responseTask.Result;
                if (message.IsSuccessStatusCode)
                {
                    string jsonData = await message.Content.ReadAsStringAsync();
                    wardCodeLocation = JsonConvert.DeserializeObject<WardCodeLocation>(jsonData);
                    return wardCodeLocation;
                }
                else
                {
                    return wardCodeLocation;
                }
            }
            catch (Exception ex)
            {
                return wardCodeLocation;
            }
        }

        public async Task<List<ProductClassViewModel>> GetProductClassData(string classcode)
        {
            List<ProductClassViewModel> lstMasterDataViewModels = new List<ProductClassViewModel>();
            try
            {
                var responseTask = await client.GetAsync(baseUrl + "/MasterData/GetProductByClassCode?classcode="+ classcode);
                if (!responseTask.IsSuccessStatusCode)
                {
                    return lstMasterDataViewModels;
                }
                else
                {
                    string response = await responseTask.Content.ReadAsStringAsync();
                    lstMasterDataViewModels = JsonConvert.DeserializeObject<List<ProductClassViewModel>>(response);
                }
                return lstMasterDataViewModels;
            }
            catch (Exception ex)
            {
                return lstMasterDataViewModels;
            }
        }

        public async Task<List<InventoryItemByPCodeViewModel>> GetInventoryData(string productcode)
        {
            List<InventoryItemByPCodeViewModel> lstMasterDataViewModels = new List<InventoryItemByPCodeViewModel>();
            try
            {
                var responseTask = await client.GetAsync(baseUrl + "/MasterData/GetInventoryByProductCode?productcode=" + productcode);
                if (!responseTask.IsSuccessStatusCode)
                {
                    return lstMasterDataViewModels;
                }
                else
                {
                    string response = await responseTask.Content.ReadAsStringAsync();
                    lstMasterDataViewModels = JsonConvert.DeserializeObject<List<InventoryItemByPCodeViewModel>>(response);
                }
                return lstMasterDataViewModels;
            }
            catch (Exception ex)
            {
                return lstMasterDataViewModels;
            }
        }

        public async Task<List<AgentCodeMaster>> GetAgentCode()
        {
            List<AgentCodeMaster> lstMasterDataViewModels = new List<AgentCodeMaster>();
            try
            {
                var responseTask = await client.GetAsync(baseUrl + "/MasterData/GetAllAgentCode");
                if (!responseTask.IsSuccessStatusCode)
                {
                    return lstMasterDataViewModels;
                }
                else
                {
                    string response = await responseTask.Content.ReadAsStringAsync();
                    lstMasterDataViewModels = JsonConvert.DeserializeObject<List<AgentCodeMaster>>(response);
                }
                return lstMasterDataViewModels;
            }
            catch (Exception ex)
            {
                return lstMasterDataViewModels;
            }
        }

        public async Task<List<AccountCodeMaster>> GetAccountCode()
        {
            List<AccountCodeMaster> lstMasterDataViewModels = new List<AccountCodeMaster>();
            try
            {
                var responseTask = await client.GetAsync(baseUrl + "/MasterData/GetAllAccountCode");
                if (!responseTask.IsSuccessStatusCode)
                {
                    return lstMasterDataViewModels;
                }
                else
                {
                    string response = await responseTask.Content.ReadAsStringAsync();
                    lstMasterDataViewModels = JsonConvert.DeserializeObject<List<AccountCodeMaster>>(response);
                }
                return lstMasterDataViewModels;
            }
            catch (Exception ex)
            {
                return lstMasterDataViewModels;
            }
        }

        public async Task<List<IntemediaryViewModel>> GetIntemediary()
        {
            List<IntemediaryViewModel> lstMasterDataViewModels = new List<IntemediaryViewModel>();
            try
            {
                var responseTask = await client.GetAsync(baseUrl + "/MasterData/GetAllIntermediaryCode");
                if (!responseTask.IsSuccessStatusCode)
                {
                    return lstMasterDataViewModels;
                }
                else
                {
                    string response = await responseTask.Content.ReadAsStringAsync();
                    lstMasterDataViewModels = JsonConvert.DeserializeObject<List<IntemediaryViewModel>>(response);
                }
                return lstMasterDataViewModels;
            }
            catch (Exception ex)
            {
                return lstMasterDataViewModels;
            }
        }

    }
}
