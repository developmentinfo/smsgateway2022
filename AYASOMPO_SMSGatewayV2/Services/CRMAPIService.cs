﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace AYASOMPO_SMSGatewayV2.Services
{
    public class CRMAPIService
    {
        public IConfiguration Configuration { get; set; }
        //private static HttpClient client;
        public string ClientId { get; set; }
        public string Secret { get; set; }
        public string TenantId { get; set; }
        public string ResourceUri { get; set; }
        public string ServiceRoot { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public CRMAPIService()
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();

            ClientId = Configuration["CRMAPI:Client-ID"].ToString();
            Secret = Configuration["CRMAPI:Client-Secret"].ToString();
            TenantId = Configuration["CRMAPI:Tenant-ID"].ToString();
            ResourceUri = Configuration["CRMAPI:Resource-URL"].ToString();
            ServiceRoot = Configuration["CRMAPI:Service-Root"].ToString();
        }


        public String CRMGetCustomerByCustomerCode(HttpMethod httpMethod, object bodydata, string url)
        {
            string flag = null;
            var cRMAPIService = new CRMAPIService();
            var response = SendMessageAsync(cRMAPIService, httpMethod,
               cRMAPIService.ServiceRoot + url).Result;

            if (response.IsSuccessStatusCode)
            {
                JObject body = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                Console.WriteLine(body.ToString());
                return body.ToString(Newtonsoft.Json.Formatting.Indented);
            }
            else
            {
                Console.WriteLine("The request failed with a status of '{0}'",
                       response.ReasonPhrase);
                return flag;
            }
        }

        public string CreateActivities(HttpMethod httpMethod, object requestbody, string url)
        {
            string returnbody = "1";
            var cRMAPIService = new CRMAPIService();
            var response = SendMessageAsync(cRMAPIService, httpMethod,
               cRMAPIService.ServiceRoot + url, requestbody.ToString()).Result;

            if (response.IsSuccessStatusCode)
            {
                return returnbody;
            }
            else
            {
                JObject body = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                returnbody = body.ToString(Newtonsoft.Json.Formatting.Indented);
                return returnbody;
            }
        }

        public string CreateRenewal(HttpMethod httpMethod, object requestbody, string url)
        {
            string returnbody = "1";
            var cRMAPIService = new CRMAPIService();
            var response = SendMessageAsync(cRMAPIService, httpMethod,
               cRMAPIService.ServiceRoot + url, requestbody.ToString()).Result;

            if (response.IsSuccessStatusCode)
            {
                return returnbody;
            }
            else
            {
                JObject body = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                returnbody = body.ToString(Newtonsoft.Json.Formatting.Indented);
                return returnbody;
            }
        }

        public string CreateRenewalInvoiceActivities(HttpMethod httpMethod, object requestbody, string url)
        {
            string returnbody = "1";
            var cRMAPIService = new CRMAPIService();
            var response = SendMessageAsync(cRMAPIService, httpMethod,
               cRMAPIService.ServiceRoot + url, requestbody.ToString()).Result;

            if (response.IsSuccessStatusCode)
            {
                return returnbody;
            }
            else
            {
                JObject body = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                returnbody = body.ToString(Newtonsoft.Json.Formatting.Indented);
                return returnbody;
            }
        }

        public String CRMGetRenewalInvoiceByTempDN(HttpMethod httpMethod, object bodydata, string url)
        {
            string flag = "-1";
            var cRMAPIService = new CRMAPIService();
            var response = SendMessageAsync(cRMAPIService, httpMethod,
               cRMAPIService.ServiceRoot + url).Result;

            if (response.IsSuccessStatusCode)
            {
                //JObject body = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                JObject body = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                Console.WriteLine(body.ToString());
                return body.ToString(Newtonsoft.Json.Formatting.Indented);
            }
            else
            {
                Console.WriteLine("The request failed with a status of '{0}'",
                       response.ReasonPhrase);
                return flag;
            }
        }

        public static async Task<HttpResponseMessage> SendMessageAsync(CRMAPIService webConfig,
             HttpMethod httpMethod, string messageUri, string body = null)
        {
            // Get the access token that is required for authentication.
            var accessToken = await GetAccessToken(webConfig);

            // Create an HTTP message with the required WebAPI headers populated.
            var client = new HttpClient();
            var message = new HttpRequestMessage(httpMethod, messageUri);

            message.Headers.Add("OData-MaxVersion", "4.0");
            message.Headers.Add("OData-Version", "4.0");
            message.Headers.Add("Prefer", "odata.include-annotations=*");
            message.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            // Add any body content specified in the passed parameter.   
            if (body != null)
                message.Content = new StringContent(body, UnicodeEncoding.UTF8, "application/json");

            // Send the message to the WebAPI. 
            return await client.SendAsync(message);
        }

        public static async Task<string> GetAccessToken(CRMAPIService webConfig)
        {
            var credentials = new ClientCredential(webConfig.ClientId, webConfig.Secret);
            var authContext = new AuthenticationContext(
                "https://login.microsoftonline.com/" + webConfig.TenantId);
            var result = await authContext.AcquireTokenAsync(webConfig.ResourceUri, credentials);

            return result.AccessToken;
        }


    }
}
