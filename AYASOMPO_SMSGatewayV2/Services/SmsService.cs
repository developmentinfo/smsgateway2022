﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Xml;

namespace AYASOMPO_SMSGatewayV2.Services
{
    public class SmsService
    {
        public void InvokeService(string PhoneNo, string Message)
        {
            //Calling CreateSOAPWebRequest method  
            HttpWebRequest request = CreateSOAPWebRequest();
            //<SourceIP>172.20.188.202</SourceIP>
            //<SourceIP>172.20.122.201</SourceIP>
            XmlDocument SOAPReqBody = new XmlDocument();
            //SOAP Body Request  
            SOAPReqBody.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>  
            <soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:sms=""http://smsgw.ws.smsgw.isc.com/"">  
            <soapenv:Header/>
            <soapenv:Body>  
                <sms:SendSmsRequest> 
                  <SourceIP>172.20.122.201</SourceIP>
                  <SourceChannel>AYASOMPO</SourceChannel>
                  <SourceApplication>AYASOMPO</SourceApplication>
                    <SourceAddress>AYASOMPO</SourceAddress>
                    <SequenceNo>1</SequenceNo>         
                   <Address>" + PhoneNo + @"</Address>  
                  <Message>" + Message + @"</Message>
                 </sms:SendSmsRequest>  
              </soapenv:Body>  
            </soapenv:Envelope>");

            //< SourceIP > 172.20.122.201 </ SourceIP >

            using (Stream stream = request.GetRequestStream())
            {
                SOAPReqBody.Save(stream);
            }
            //Geting response from request  
            using (WebResponse Serviceres = request.GetResponse())
            {
                using (StreamReader rd = new StreamReader(Serviceres.GetResponseStream()))
                {
                    //reading stream  
                    var ServiceResult = rd.ReadToEnd();
                }
            }
        }

        public bool InvokeServiceForSendSmsBulkRequest(string PhoneNo, string Message)
        {
            bool flag = false;
            try
            {
                //Calling CreateSOAPWebRequest method  
                HttpWebRequest request = CreateSOAPWebRequest();
                XmlDocument SOAPReqBody = new XmlDocument();
                string s1, s2 = null, s3 = null, s4;
                XmlDocument xmlDoc = new XmlDocument();

                s1 = @"<?xml version=""1.0"" encoding=""utf-8""?> <soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:sms=""http://smsgw.ws.smsgw.isc.com/"">  
                     <soapenv:Header/>
                     <soapenv:Body>  
                         <sms:SendSmsBulkRequest> 
                           <SourceIP>172.20.122.201</SourceIP>
                           <SourceChannel>AYASOMPO</SourceChannel>
                           <SourceApplication>AYASOMPO</SourceApplication>
                             <SourceAddress>AYASOMPO</SourceAddress>
                             <SequenceNo>1</SequenceNo> 
                            <Message> " + Message + @" </Message>
                            <Address>" + PhoneNo + @"</Address>  
                            </sms:SendSmsBulkRequest>
                      </soapenv:Body>  
                    </soapenv:Envelope>";

                string finalXML = s1;
                SOAPReqBody.LoadXml(finalXML);
                using (Stream stream = request.GetRequestStream())
                {
                    SOAPReqBody.Save(stream);
                }
                //Geting response from request  
                using (WebResponse Serviceres = request.GetResponse())
                {
                    using (StreamReader rd = new StreamReader(Serviceres.GetResponseStream()))
                    {
                        //reading stream  
                        var ServiceResult = rd.ReadToEnd();
                    }
                }

                return flag = true;
            }
            catch (Exception ex)
            {
                return flag = false;
            }

        }

        public HttpWebRequest CreateSOAPWebRequest()
        {
            //Making Web Request

            //UAT
            HttpWebRequest Req = (HttpWebRequest)WebRequest.Create(@"http://172.20.176.76/smsgw-ws/services/SmsGwService");

            //LIVE
            //HttpWebRequest Req = (HttpWebRequest)WebRequest.Create(@"http://172.20.172.82/smsgw-ws/services/SmsGwService");

            //SOAPAction  
            //Req.Headers.Add(@"SOAPAction:http://tempuri.org/Addition");
            //Content_type  
            Req.ContentType = "text/xml;charset=\"utf-8\"";
            Req.Accept = "text/xml";
            //HTTP method  
            Req.Method = "POST";
            //return HttpWebRequest  
            return Req;
        }
    }
}
